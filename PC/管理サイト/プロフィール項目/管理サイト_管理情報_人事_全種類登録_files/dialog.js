/**
 *	dialog.js
 */
(function(jQuery, w, d){
	_Plugin.Modal = {
	        Wapper : null,
	        Overlay: null
	};

	/* -------------------------------------------------
	 * ModalDialog Class
	 *
	 *      type: 0 ... Cancel Button only
	 *              butonLabel: cancelBtnLabel
	 *
	 *      type: 1 ... Cancel & Blue-decide Buttons
	 *              butonLabel: cancelBtnLabel ( Cancel ),
	 *                          decideBtnLabel ( DecideBtn )
	 *
	 *      type: 2 ... Cancel & Red-decide Buttons
	 *              butonLabel: cancelBtnLabel ( Cancel ),
	 *                          decideBtnLabel ( DecideBtn )
	 *
	 * ------------------------------------------------- */
	_Plugin.Modal.Dialog = Backbone.View.extend({
		el: '.dialog',

		option: {
			type: 1,
			className     : 'dialog',
			activeClass   : 'active',
			title         : '確認',
			content       : '',
			cancelBtnLabel: 'キャンセル',
			decideBtnLabel: '決定',
			speed         : 300,
			bodyClass     : 'modalOpened',
			modalWapperClass: 'jsModal-Dialog'
		},

		templates: '<div class="dlgHeader">' +
				'<div class="heading"><h2></h2></div>' +
				'<span class="close"><a href="#"><i class="icon-xmark"></i></a></span>' +
			'</div>' +
			'<div class="dlgContainer"></div>' +
			'<div class="dlgFooter"></div>',
		buttons: {
			0: _.template('<div class="boxButtonArea ht right">' +
					'<div><a class="btnM btnBasic js-cancelBtn" href="/"><%= cancelBtnLabel %></a></div>' +
				'</div>'),
			1: _.template('<div class="boxButtonArea both">' +
					'<div><a class="btnM btnBasic js-cancelBtn" href="/"><%= cancelBtnLabel %></a></div>' +
					'<div><a class="btnM js-decideBtn" href="/"><%= decideBtnLabel %></a></div>' +
				'</div>'),
			2: _.template('<div class="boxButtonArea both">' +
					'<div><a class="btnM btnBasic js-cancelBtn" href="/"><%= cancelBtnLabel %></a></div>' +
					'<div><a class="btnM btnNotice js-decideBtn" href="/"><%= decideBtnLabel %></a></div>' +
				'</div>')
		},

		events: {
			'click .js-decideBtn': '_decide',
			'click .js-cancelBtn': '_cancel',
			'click .close': 'hide'
		},

		active: false,
		target : null,
		callback: null,
		cancelCallback: null,

		hideOnDecide: true, // 「決定（続行）」時にダイアログを消してしまうかどうか（デフォルト：隠す）

		/* -----------------------------------------------
		 *	initialize
		 * -----------------------------------------------*/
		initialize: function(arg) {
			var self = this;
			if(arg && arg.option && typeof(arg.option) === 'object') {
				var o = self.option = jQuery.extend({}, self.option, arg.option);
			}

	                self._setWapper();

	                if(!self.el || !arg.el) {
	                        self.render();
	                }

	                return this;
		},

	        _setWapper: function() {
	                if( !_Plugin.Modal.Dialog.Wapper ) {
	                        _Plugin.Modal.Dialog._setWapper();
	                }
	                this.wapper = _Plugin.Modal.Dialog.Wapper;
	                this.overlay = _Plugin.Modal.Dialog.Overlay;
	        },

		render: function() {
			var self = this, o = self.option;
			self.setElement( jQuery('<div class="' + o.className + '"></div>') );

			self.el.innerHTML = self.templates;
			self.el.style.display = 'none';

			self.setButtonType(o.type); // ボタンの表示タイプをセット
			self.setTitle(o.title);     // タイトルをセット
			if(o.content) {             // 内容をセット 
				self.setContent(o.content);
			}
			self.wapper.find('.modalDlgWrapper').append(self.$el);
		},

		/* -----------------------------------------------
		 *	Public Method
		 * -----------------------------------------------*/
		show: function(targetObj, callback, cancelCallback) {
			var self = this, o = self.option;
			self.target = targetObj? targetObj : self;

			// 実行ボタン(decide)用のcallback
			if(typeof(callback) === 'function') {
				self.callback = callback;
			}

			// キャンセルボタン用のcallback
			if(typeof(cancelCallback) === 'function') {
				self.cancelCallback = cancelCallback;
			}

	                self.overlay.on('click', {self: self}, self.closeEventListener);
	                self.wapper.addClass(o.modalWapperClass).show();
	                App.Utility.$b.addClass(o.bodyClass);
			self.$el.fadeIn(o.speed, function(evt) {
				self.active = true;
			});
			return false;
		},

		hide: function() {
			var self = this, o = self.option;
			self.overlay.off('click', self.closeEventListener);

			self.target = null;
			self.callback = null;


			self.$el.fadeOut(o.speed * 0.8, function(evt) {
	                        App.Utility.$b.removeClass(o.bodyClass);
				self.active = false;
				self.wapper.hide().removeClass(o.modalWapperClass);
			});

			return false;
		},

		// すぐさまダイアログを隠す
		immediatelyHide: function() {
			var self = this, o = self.option;
			self.overlay.off('click', self.closeEventListener);
			self.target = null;
			self.callback = null;
			self.cancelCallback = null;

			self.$el.hide();
			App.Utility.$b.removeClass(o.bodyClass);
			self.active = false;

			return false;
		},

		setContent: function(content) {

			// html要素だったらappend
			if(typeof(content) === 'object') {
				this.$el.find('.dlgContainer').append(content);
			}
			// 文字列だったらinnerHTML
			else {
				this.$el.find('.dlgContainer')[0].innerHTML = content;
			}
		},

		setTitle: function(title) {
			this.$el.find('.heading h2')[0].innerHTML = title;
		},

		setButtonType: function(type) {
			var buttonArea = this.buttons[type]({
				cancelBtnLabel: this.option.cancelBtnLabel,
				decideBtnLabel: this.option.decideBtnLabel
			});

			this.$el.find('.dlgFooter')[0].innerHTML = buttonArea;
		},

		_showActiveDialogCount: function() {
			var self = this, dialogs, isActiveCount = 0;

			dialogs = jQuery('.'+self.option.className);

			dialogs.each(function(num, element) {
				// dialog が表示されている場合、カウント
				if(element.style.display !== 'none') {
					isActiveCount++;
				}
			});

			return isActiveCount;
		},

		/* -----------------------------------------------
		 *	Private Method
		 * -----------------------------------------------*/
		_decide: function(evt) {
			var self = this;

			if(self.callback && typeof(self.callback) === 'function') {
				self.callback.call(self.target);
			}

			// 実行ボタン押下時にダイアログを消す設定なら消す
			if(self.hideOnDecide) {
				self.cancelCallback = null;
				self.hide();
			}

			return false;
		},

		_cancel: function() {
			var self = this, o = self.option;

			// キャンセルボタン用のcallbackがある場合
			if(self.cancelCallback && typeof(self.cancelCallback) === 'function') {
				self.cancelCallback.call(self.target);
			}
			// キャンセルボタン用のcallbackがない場合
			else {
				self.hide();
			}

			return false;
		},


		/* -----------------------------------------------
		 *	EventListener
		 * -----------------------------------------------*/
		closeEventListener: function(evt) {
			var self = evt.data.self;
			self.hide();
		}
	}, {
	        Wapper: null,
	        Overlay: null,
	        _setWapper: function() {
	                var bkClass = 'js-blackBack',
	                        $c = jQuery('#modalDlgContainer');

	                if(!$c.length) {
	                        $c = jQuery('<div id="modalDlgContainer" class="modalDlgState"></div>');
	                        $c[0].style.display = 'none';
	                        $c[0].innerHTML = '<div class="' + bkClass + '"></div>' + '<div class="modalDlgWrapper"></div>';
	                        this.Overlay = $c.find('.' + bkClass);

	                        $c.appendTo('body');
	                }
	                this.Wapper = $c;
	        }
	});

	_Plugin.ModalDialog = function(options) {
	        return new _Plugin.Modal.Dialog(options);
	};
})(jQuery, window, document);
