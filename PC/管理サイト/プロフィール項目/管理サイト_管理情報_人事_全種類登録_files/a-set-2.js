/**
 *	A-Set-2.js 内定者向け基本情報項目 作成 or 編集
 */
!function(jQuery, w) {
	var deleteDialog;

	// プレビューエリア
	var PreviewArea = function(elm, options) {
		this.init(elm, options);
	};
	jQuery.extend(PreviewArea.prototype, {
		options: {
			preview          : '.inner',
			content          : '.content',
			contentWidth     : 740,
			speed            : 0,
			activeId         : 0,
			activeClass      : 'active',
			inputTypeArea    : '.js-optionInputTypeArea',
			choicesArea      : '.js-optionChoicesArea',
			choiceWapper     : '.contentWapper',
			choiceSection    : '.choiceInputSection',
			choicePrevArea   : '.choicesContent',
			choicePrevSection: '.choiceView',
			addChoiceBtn     : '.js-addChoiceBtn',
			globalOptionAddressArea   : '.js-optionGlobalOptionAddressArea',
			globalOptionTelephoneArea : '.js-optionGlobalOptionTelephoneArea'
		},

		btnProgress: false,

		activeId: null,
		$preview: null,
		$type   : null,
		$choices: null,
		contens : null,
		$globalOptionAddress  : null,
		$globalOptionTelephone: null,

		collection: null,

		init: function(elm, options) {
			var self = this,
					$t = self.$t = (elm instanceof jQuery === true)? elm : jQuery(elm),
					o = self.options = jQuery.extend(self.options, options);

			self.btnProgress = true;

			self.activeId      = o.activeId;
			self.$preview      = $t.find(o.preview);
			self.$type         = $t.find(o.inputTypeArea);
			self.$choices      = $t.find(o.choicesArea);
			self.$globalOptionAddress   = $t.find(o.globalOptionAddressArea);
			self.$globalOptionTelephone = $t.find(o.globalOptionTelephoneArea);

			self._fieldTypeInit()._choicesInit();

			self.contens = [];

			self.$preview.find(o.content).each(function(i, elm) {
				var $c = jQuery(elm);
				if($c.attr('id')){
					var id = +$c.attr('id').split('js-previewContent-')[1]; //数値に変換
					if(id === self.activeId || (self.activeId === 6 && id === 3)) {
						$c.addClass(o.activeClass);
					}
					self.contens[id] = $c;
					if(id === 3) {
						self.contens[6] = $c; //簡易アンケートはcheckboxと同じプレビュー
					}
				}
			});

			if(self.activeId === 0) {
				self.$type.show();
			} else if( self.activeId === 2 || self.activeId === 3 || self.activeId === 6 ) {
				self.$choices.show();
			} else if( self.activeId === 5) {
				self.$globalOptionAddress.show();
				
			} else if( self.activeId === 7) {
				self.$globalOptionTelephone.show();
				
			}

			self.btnProgress = false;
		},

		// setter
		_setActiveId: function(id) {
			this.activeId = id - 0;
		},

		// setting
		_fieldTypeInit: function() {
			var $a = this.$type,
					v = $a.find('input:checked').val();
			if(!v) {
				$a.find('input:eq(0)').attr('checked', true);
			}
			return this;
		},

		// setting Choices
		_choicesInit: function() {
			var self = this,
					o = self.options,
					c = self.collection = new App.Admin.A_Set_2.choices(),
					$addBtn = self.$choices.find(o.addChoiceBtn),
					$wapper = self.$choices.find(o.choiceWapper),
					prevAreas = [];

			// create Model, Views
			$wapper.find(o.choiceSection).each(function(i, elm) {
				var m, v;
				m = self._createChoiceModel();
				v = new App.Admin.A_Set_2.choiceInput({
					model: m,
					el: elm
				});

				// add EventListener
				self._addChoiceEventListener(v);
			});

			// create Preview View
			self.$preview.find(o.choicePrevArea).each(function(i, e) {
				var type = i,
						$pA = prevAreas[i] = jQuery(e),
						modelNum = c.length,
						count;

				$pA.find(o.choicePrevSection).each(function(j, elm) {
					var m, v;
					m = c.models[j];
					v = new App.Admin.A_Set_2.choiceView({
						model: m,
						el: elm,
						option: {
							inputType: type
						}
					});
					count = j;
				});
				count += 1;
				if( modelNum > count ) {
					// console.log('how mwny create View? ', modelNum - count);
					for(var j=count; j<modelNum; j+=1) {
						var m = c.models[j];
						var v = self._createChoicePreviewView(m, type);
						$pA.append(v.$el);
					}
				}
			});

			//初期入力の反映
			c.checkInputVal();

			// add Choice
			$addBtn.on('click', function(evt) {
				if(self.btnProgress === false) {
					self.btnProgress = true;

					// create model
					var m = self._createChoiceModel();

					// Input Field View
					var inputView = new App.Admin.A_Set_2.choiceInput({
						model: m
					});
					self._addChoiceEventListener(inputView);

					// Preview Area's View
					for(var i=0, l=prevAreas.length; i<l; i+=1) {
						var preView = self._createChoicePreviewView(m, i);
						preView.$el.hide().appendTo(prevAreas[i]).slideDown(200);
					}

					inputView.$el.css({
						position: 'relative',
						left: '100%',
						opacity: 0
					}).appendTo($wapper).animate({
						left: 0,
						opacity: 1
					}, {
						duration: 200,
						complete: function(e) {
							inputView.focus().$el.removeAttr('style');
						}
					});

					self.btnProgress = false;
				}
				return false;
			});

			return this;
		},

		// create Choice Model
		_createChoiceModel: function() {
			var model = new App.Admin.A_Set_2.choiceModel({
				index: this.collection.length
			});
			this.collection.add(model);
			return model;
		},
		// create Choice Preview View
		_createChoicePreviewView: function(model, type) {
			return new App.Admin.A_Set_2.choiceView({
				model: model,
				option: {
					inputType: type
				}
			});
		},

		// add Choice EventListener
		_addChoiceEventListener: function(view) {
			view.listenTo(view, 'delete', this._showDeleteDialog);
		},

		// Callback
		_showDeleteDialog: function(arg) {
			var choice = this, $mode = jQuery('#mode');

			// 作成時はサクっと削除
			if($mode.val() === 'create') {
				choice._dispose();
			}
			// 編集時は確認ダイアログ表示
			else {
				deleteDialog.show(choice, choice._dispose);
			}
		},

		// change Preview Area
		chaneField: function(activeId) {
			var self = this,
					o = self.options,
					oldId = self.activeId,
					$old,
					$next,
					pos = o.contentWidth;


			if(oldId === activeId) {
					return false;
			} else if(activeId < oldId) {
				pos *= -1;
			}

			$old  = self.contens[oldId];
			$next = self.contens[activeId];

			$next.css({
				position: 'absolute',
				display : 'block',
				left    : pos,
				top     : 0
			});

			// hide options
			if(oldId === 0) {
				self.$type.hide();
			} else if( (oldId === 2 && activeId !== 3 && activeId !== 6) || (oldId === 3 && activeId !== 2 && activeId !== 6) || (oldId === 6 && activeId !== 2 && activeId !== 3) ) {
				self.$choices.hide();
			} else if(oldId === 5) {
				self.$globalOptionAddress.hide();
			} else if(oldId === 7) {
				self.$globalOptionTelephone.hide();
			}

			self.$preview.css('height', $next.height() ).animate({
				left: -pos
			}, o.speed, function(e) {
				$old.removeClass(o.activeClass);
				$next.addClass(o.activeClass).removeAttr('style');
				self.$preview.removeAttr('style');

				self._setActiveId( activeId );

				if( activeId === 0 ) {
					self.$type.slideDown(100);
				} else if( (activeId === 2 && oldId !== 3 && oldId !== 6) || (activeId === 3 && oldId !== 2 && oldId !== 6) || (activeId === 6 && oldId !== 2 && oldId !== 3) ) {
					self.$choices.slideDown(100);
				} else if(activeId === 5) {
					self.$globalOptionAddress.slideDown(100);
				} else if(activeId === 7) {
					self.$globalOptionTelephone.slideDown(100);
				}

			});
		}
	});


	// 項目名, 必須設定
	function setPreviewLabel($t, $a) {
		var v, $i, $requiredArea, $label, labelNode;

		// Field label
		$i = $t.find('.js-fieldLabelArea .inputField');
		$label = $a.find('.js-fieldLabel');
		labelNode = $label.find('span');
		$i.on('change', function(e) {
			var v = e.target.value;
			labelNode.text(v? v : '項目名');
		}).trigger('change');

		// Field Discription
		$i = $t.find('.js-fieldNoteArea .inputField');
		note = $a.find('.js-fieldNote');
		$i.on('change', function(e) {
			var v = e.target.value.trim().replace(/\r?\n/g, "<br />");
			note.text(v? v : '説明文');
		}).trigger('change');

		// check Required
		$requiredArea = $t.find('.js-isRequiredArea');
		$i = $requiredArea.find('input');

		$i.on('change', function(e) {
			var v = e.target.value - 0,
					checked = e.target.checked;
			// on/off しか無いのでこのやり方で。
			if(checked === true && v > 0) {
				$label.addClass('requiredMode');
			} else {
				$label.removeClass('requiredMode');
			}
		});
		// 初期化
		v = $requiredArea.find('input:checked').val() - 0;
		if(v > 0) {
			$label.addClass('requiredMode');
		}
	}

	//住所の国名、電話番号の国番号を表示・非表示
	function setGlobalOption($previewArea){
		//国名
		$previewArea.find('input[name="global_option_address"]:radio').on('change', function(e){
			var c = e.target.checked;
			var v = +e.target.value; //数値に変換
			var $countryName = $('#js-countryName');
			if(c && v){
				$countryName.show();
			}else if(c && !v){
				$countryName.hide();
			}
		}).trigger('change');

		//国番号
		$previewArea.find('input[name="global_option_telephone"]:radio').on('change', function(e){
			var c = e.target.checked;
			var v = +e.target.value; //数値に変換
			var $countryCode = $('#js-countryCode');
			if(c && v){
				$countryCode.show();
			}else if(c && !v){
				$countryCode.hide();
			}
		}).trigger('change');
	}

	jQuery(function(){
		var $t = jQuery('#main'),
				$pA = $t.find('.previewArea');

		// set Modal Dialog
		deleteDialog = _Plugin.ModalDialog({
			option: {
				type: 2,
				title: '削除の確認',
				decideBtnLabel: '削除',
				content: '選択肢を削除しようとしています。既にこの選択肢を選択しているユーザーが存在する場合、ユーザーが選択したデータも削除されます。よろしいですか？'
			}
		});

		setPreviewLabel($t, $pA);

		setGlobalOption($pA);

		var $fieldTypeArea = $t.find('.js-fieldTypeArea'),
				prevSection,
				activeId = $fieldTypeArea.find('input:checked').val();

		if(!activeId) {
			activeId = $fieldTypeArea.find('input:eq(0)').attr('checked', true).val();
		}
		activeId -= 0;

		prevSection = new PreviewArea( $pA, {
			activeId: activeId
		});

		$fieldTypeArea.find('input').on('change', function(e) {
			var v = e.target.value - 0,
					checked = e.target.checked;
			prevSection.chaneField(v);
		});

	});
}(jQuery, window);
