/**
 *	A-Set-2.js 内定者向け基本情報項目 作成 or 編集
 *		選択解答 項目
 */
(function(jQuery, w){
	App.Admin.A_Set_2 = {};

	App.Admin.A_Set_2.choiceModel = Backbone.Model.extend({
		urlRoot: 'aip',
		inputView: null,
		defaults: {
			index: 0,
			value: ''
		}
	});

	App.Admin.A_Set_2.choices = Backbone.Collection.extend({
		model: App.Admin.A_Set_2.choiceModel,
		checkInputVal: function() {
			for(var i in this.models) {
				var m = this.models[i],
						v = m.inputView,
						val = '';
				if(v && (val = v.getValue()) != '') {
					m.set({'value': val});
				}
			}
		}
	});

	// Preview Area View
	App.Admin.A_Set_2.choiceView = Backbone.View.extend({
		model: App.Admin.A_Set_2.choiceModel,
		//el: '.choiceView',

		option: {
			el: '<p class="choiceView"></p>',
			inputType  : 0,
			label      : '.labelName',
			defaultText: '選択肢名を入力してください'
		},

		template: _.template('<label class="bullet"><input type="<%= type %>" disabled="disabled"><span class="labelName"><%= text %></span></label>'),

		/* -----------------------------------------------
		 *	initialize
		 * -----------------------------------------------*/
		initialize: function(arg) {
			var self = this, o = self.option;
			if(arg && arg.option && typeof(arg.option) === 'object') {
				o = self.option = jQuery.extend({}, self.option, arg.option);
			}

			// 0...radio, 1...checkbox
			self.inputType = o.inputType? 'checkbox':'radio';

			if(!self.$el.context) {
				self._render();
			}

			self._setting();

			// add EventListener
			self.listenTo(self.model, 'change:value', self._changeValue)
			.listenTo(self.model, 'destroy', self._remove);

		},

		_render: function() {
			var self = this,
					o = self.option,
					$el = jQuery(o.el).append( self.template({
						type: self.inputType,
						text: o.defaultText
					}) );

			self.setElement($el);
			return $el;
		},

		_setting: function() {
			var self = this, o = self.option;
			self.$label = self.$el.find(o.label);
			return this;
		},

		// EventListener
		_dispose: function() {

		},

		// callBack
		_changeValue: function(model) {
			var v = model.get('value');
			if(!v) {
				v = this.option.defaultText;
			}
			// $.text(val) ... "<"や">"などはエスケープされてHTMLエンティティとして追加される。
			this.$label.text(v);
			return this;
		},

		_remove: function(model) {
			var self = this;
			self.$el.slideUp(300, function(evt) {
				self.remove();
			});
		}
	});

	// Input Area View
	App.Admin.A_Set_2.choiceInput = App.Admin.A_Set_2.choiceView.extend({
		//el: '.choiceInputSection',

		option: {
			el: '<p class="content choiceInputSection"></p>',
			field: 'input.choiceLabelInput',
			deleteBtn: '.js-deleteBtn'
		},

		events: {
			//'click'
		},

		template: _.template('<span class="addItem">\
			<input type="text" class="choiceLabelInput" name="profile_choices[]" size="25">\
			<a class="uiAnchor js-deleteBtn" href=""><i class="icon-xmark"></i></a>\
		</span>'),

		_render: function() {
			var self = this,
					o = self.option,
					id = self.model.get('index'),
					$el = jQuery(o.el).append( self.template({id: id}) );

			self.setElement($el);
			return $el;
		},

		_setting: function() {
			var self = this,
					o = self.option,
					$el = self.$el,
					$i = self.$field = $el.find(o.field),
					$b = $el.find(o.deleteBtn);

			$i.on('change', function(evt) {
				self.model.set({
					'value': evt.target.value
				});
			});

			$b.on('click', function(evt) {
				self.trigger('delete');
				return false;
			});

			self.model.inputView = self;
			return this;
		},

		// EventListener
		_dispose: function() {
			var self = this;
			self.model.destroy({
				success: function(model) {
				},
				error: function(model) {
					// API が無いから 取り敢えず error でキャッチ ... errorでもCollectionからmodelは削除されてる
					return false;
				}
			});
		},

		// callBack
		_changeValue: function(model) {
			return this;
		},

		// Public Event
		focus: function() {
			this.$field.focus();
			return this;
		},

		getValue: function() {
			return this.$field.val();
		}
	});



})(jQuery, window);
