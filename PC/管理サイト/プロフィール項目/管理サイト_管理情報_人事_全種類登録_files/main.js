(function() {
  var App, MessageList_View, add_initialize, add_route, fuel_fetch_token, fuel_set_token, getEmbeddedJSON, log, nl2br, parseJSON, sanitize_nl2br, __,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  App = {
    config: {},
    functions: {},
    classes: {},
    helpers: {},
    elements: {},
    partials: {},
    pages: {},
    instances: {},
    routes: [],
    initializes: [],
    current_user: {},
    $window: null,
    $document: null,
    $body: null
  };

  _.mixin(_.string.exports());

  App.base_url = location.pathname.replace(/(^\/[^\/]+\/[^\/]+)\/.*/, '$1');

  App.is_legacy_browser = !$.support.opacity;

  App.can_use_file_api = window.File != null;

  App.$window = $(window);

  App.$document = $(document);

  App.$html = $('html');

  App.$body = $('body');

  App.event = _.clone(Backbone.Events);

  App.$body.on('click', (function(_this) {
    return function(event) {
      return App.event.trigger('body_clicked', event);
    };
  })(this));

  App.$document.on('click', (function(_this) {
    return function(event) {
      return App.event.trigger('document_clicked', event);
    };
  })(this));

  App.event.global_balloon_mediator = _.extend({
    $active: null,
    init: function() {
      App.event.global_balloon_mediator.on('register', (function(_this) {
        return function(el) {
          _this.trigger('unregister');
          if (!el.show) {
            el = $(el);
          }
          return _this.$active = el.show();
        };
      })(this));
      return App.event.global_balloon_mediator.on('unregister', (function(_this) {
        return function() {
          if (_this.$active) {
            _this.$active.hide();
          }
          return _this.$active = null;
        };
      })(this));
    },
    register_document_clicked: function() {
      return App.event.on('document_clicked', (function(_this) {
        return function(ev) {
          var $clicked, cls, id, matched, _$active;
          $clicked = $(ev.target);
          _$active = _this.$active;
          if (!_$active) {
            return;
          }
          if (_$active.attr('id')) {
            id = _$active.attr('id');
            if ($clicked.attr('id') === id) {
              return;
            }
            matched = false;
            _.each($clicked.parents(), function(el) {
              if ($(el).attr('id') === id) {
                return matched = true;
              }
            });
            if (matched === false) {
              return _this.trigger('unregister');
            }
          } else if (_$active.attr('class')) {
            cls = _$active.attr('class');
            if (!$clicked.hasClass(cls) && !$clicked.parents().hasClass(cls)) {
              return _this.trigger('unregister');
            }
          } else {

          }
        };
      })(this));
    }
  }, Backbone.Events);

  App.config = {
    debug_mode: true,
    lang: {
      path: '/api/i18n.json'
    },
    security: {
      csrf_token_key: 'hZVLiFAGt6YSJEzt'
    }
  };

  App.functions.align_height = (function(_this) {
    return function($items, align_per_count) {
      var items, max_height, target_items, _results;
      items = $items.toArray();
      _results = [];
      while (items.length > 0) {
        target_items = [];
        max_height = 0;
        _(items.splice(0, align_per_count)).each(function(item) {
          var $target_item;
          $target_item = $(item);
          target_items.push($target_item);
          return max_height = Math.max(max_height, $target_item.height());
        });
        _results.push(_(target_items).each(function($item) {
          return $item.height(max_height);
        }));
      }
      return _results;
    };
  })(this);

  App.functions.disable_form_multiple_submits = (function(_this) {
    return function($form) {
      if (!($form instanceof jQuery)) {
        $form = $($form);
      }
      return $form.one('submit', function() {
        $form.on('submit', function() {
          return false;
        });
        return true;
      });
    };
  })(this);

  App.functions.disable_form_multiple_clicks = (function(_this) {
    return function($link) {
      if (!($link instanceof jQuery)) {
        $link = $($link);
      }
      $link.unbind();
      return $link.one('click', function() {
        $link.on('click', function() {
          return false;
        });
        return true;
      });
    };
  })(this);

  App.functions.fuel_fetch_token = function() {
    var c_end, c_name, c_start;
    if (document.cookie.length > 0) {
      c_name = App.config.security.csrf_token_key;
      c_start = document.cookie.indexOf(c_name + '=');
      if (c_start !== -1) {
        c_start = c_start + c_name.length + 1;
        c_end = document.cookie.indexOf(';', c_start);
        if (c_end === -1) {
          c_end = document.cookie.length;
        }
        return unescape(document.cookie.substring(c_start, c_end));
      }
    }
    return '';
  };

  fuel_fetch_token = App.functions.fuel_fetch_token;

  App.functions.fuel_set_token = function(form) {
    var $csrf, c_name, element, has_hidden, value, _i, _len, _ref;
    value = fuel_fetch_token();
    c_name = App.config.security.csrf_token_key;
    if (form instanceof jQuery) {
      $csrf = form.find("[name=\"" + c_name + "\"]");
      if ($csrf.length <= 0) {
        $csrf = $('<input type="hidden" name="' + App.config.security.csrf_token_key + '">');
        form.append($csrf);
      }
      return $csrf.val(value);
    } else if ((form != null ? form.elements : void 0) != null) {
      if (value !== '') {
        has_hidden = false;
        _ref = form.elements;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          element = _ref[_i];
          if (element.name === c_name) {
            element.value = value;
            has_hidden = true;
            break;
          }
        }
        if (!has_hidden) {
          return $(form).append("<input type=\"hidden\" name=\"" + c_name + "\" value=\"" + value + "\"");
        }
      }
    }
  };

  fuel_set_token = App.functions.fuel_set_token;

  App.functions.add_initialize = function(init) {
    if (typeof init === 'function') {
      return App.initializes.push(init);
    }
  };

  add_initialize = App.functions.add_initialize;

  App.functions.log = function() {
    var argument, _i, _len;
    if (!App.config.debug_mode) {
      return false;
    }
    if ((typeof console !== "undefined" && console !== null ? console.log : void 0) != null) {
      if (App.is_legacy_browser) {
        for (_i = 0, _len = arguments.length; _i < _len; _i++) {
          argument = arguments[_i];
          console.log(argument);
        }
      } else {
        console.log.apply(console, arguments);
      }
    } else {
      alert(arguments);
    }
    return true;
  };

  log = App.functions.log;

  App.functions.sanitize_nl2br = function(str) {
    str = _.template("<%- v %>", {
      v: str + ''
    });
    return App.functions.nl2br(str);
  };

  App.functions.nl2br = function(str) {
    var break_tag;
    break_tag = '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, ['$1', '$2'].join(break_tag));
  };

  sanitize_nl2br = App.functions.sanitize_nl2br;

  nl2br = App.functions.nl2br;

  App.functions.parseJSON = function(json_string, default_value) {
    var error, object, _ref;
    if (default_value == null) {
      default_value = null;
    }
    object = default_value;
    try {
      if (((_ref = Backbone.$) != null ? _ref.parseJSON : void 0) != null) {
        object = Backbone.$.parseJSON(json_string);
      } else if ((typeof JSON !== "undefined" && JSON !== null ? JSON.parse : void 0) != null) {
        object = JSON.parse(json_string);
      }
    } catch (_error) {
      error = _error;
    }
    return object;
  };

  parseJSON = App.functions.parseJSON;

  App.functions.getEmbeddedJSON = function(jquery_object_or_selector_string, defaults) {
    var $target, json;
    if (defaults == null) {
      defaults = null;
    }
    if (jquery_object_or_selector_string instanceof jQuery) {
      $target = jquery_object_or_selector_string;
    } else if (_.isString(jquery_object_or_selector_string)) {
      $target = $(jquery_object_or_selector_string);
    }
    if (($target == null) || $target.length === 0) {
      return defaults;
    }
    json = null;
    if (App.is_legacy_browser) {
      json = $target.html();
    } else {
      json = $target.text();
    }
    return parseJSON(json, defaults);
  };

  getEmbeddedJSON = App.functions.getEmbeddedJSON;

  App.functions.add_route = function(path, func) {
    return App.routes.push({
      path: path,
      cb: func
    });
  };

  add_route = App.functions.add_route;

  App.classes.sns = (function() {
    function sns() {}

    sns._init = function(cb) {
      App.sns = getEmbeddedJSON($('#js-sns-information-json'), {
        name: null,
        is_display_user_birthday: null
      });
      return cb();
    };

    sns.get_name = function() {
      return App.sns.name;
    };

    return sns;

  })();

  add_initialize(App.classes.sns._init);

  App.classes.Lang = (function() {
    function Lang() {}

    Lang.failed_value = '';

    Lang.language_data = {};

    Lang.translations = {};

    Lang.language = 'ja';

    Lang.init = function(cb) {
      if ((window.AIRY_LANG != null) && _.isObject(window.AIRY_LANG)) {
        App.classes.Lang.language_data = window.AIRY_LANG;
      }
      App.classes.Lang.set_language(App.classes.language);
      return cb();
    };

    Lang.load = function(group) {
      if (App.classes.Lang.translations[group] != null) {
        return App.classes.Lang.translations[group];
      } else {
        return false;
      }
    };

    Lang.set_language = function(language) {
      if (App.classes.Lang.language_data[language] != null) {
        App.classes.Lang.translations = App.classes.Lang.language_data[language];
        return true;
      } else {
        App.classes.Lang.translations = {};
        return false;
      }
    };

    Lang.replace_params = function(value, params) {
      var key, param, regexp;
      if (params == null) {
        params = {};
      }
      for (key in params) {
        param = params[key];
        if (key.substr(0, 1) !== ':') {
          key = ':' + key;
        }
        regexp = new RegExp(key, 'g');
        value = value.replace(regexp, param);
      }
      return App.classes.Lang.clear_params(value);
    };

    Lang.clear_params = function(value) {
      return value.replace(/\:[a-zA-Z0-9]+/g, '');
    };

    Lang.get = function(line, params, default_value) {
      var group, key, keys, translations, value, _i, _len;
      if (params == null) {
        params = {};
      }
      if (default_value == null) {
        default_value = null;
      }
      keys = line.split('.');
      if (keys.length <= 0) {
        return App.classes.Lang.failed_value;
      }
      group = keys.shift();
      translations = App.classes.Lang.load(group);
      if (!translations) {
        return App.classes.Lang.failed_value;
      }
      value = App.classes.Lang.failed_value;
      for (_i = 0, _len = keys.length; _i < _len; _i++) {
        key = keys[_i];
        if (translations[key] != null) {
          translations = value = translations[key];
        } else {
          value = false;
          break;
        }
      }
      if (!value) {
        if ((default_value != null) && default_value !== null) {
          return App.classes.Lang.replace_params(default_value, params);
        } else {
          return App.classes.Lang.failed_value;
        }
      } else {
        return App.classes.Lang.replace_params(value, params);
      }
    };

    return Lang;

  })();

  add_initialize(App.classes.Lang.init);

  __ = App.classes.Lang.get;

  App.classes.current_user = (function() {
    function current_user() {}

    current_user._init = function(cb) {
      App.current_user = getEmbeddedJSON($('#js-user-information-json'), {
        id: null,
        name: null,
        icon: null,
        type_name: null,
        role_name: null,
        lang: 'ja'
      });
      App.classes.Lang.set_language(App.current_user.lang);
      return cb();
    };

    current_user.get_id = function() {
      return App.current_user.id;
    };

    current_user.get_name = function() {
      return App.current_user.name;
    };

    current_user.get_icon = function() {
      return App.current_user.icon;
    };

    current_user.get_lang = function() {
      return App.current_user.lang;
    };

    current_user.is_role_common = function() {
      return App.current_user.role_name === 'common';
    };

    current_user.is_role_limit = function() {
      return App.current_user.role_name === 'limit';
    };

    current_user.is_role_work = function() {
      return App.current_user.role_name === 'work';
    };

    current_user.is_role_manager = function() {
      return App.current_user.role_name === 'manager';
    };

    current_user.is_role_transparent = function() {
      return App.current_user.role_name === 'transparent';
    };

    current_user.is_role_gx = function() {
      return App.current_user.role_name === 'gx';
    };

    current_user.get_data = function() {
      return App.current_user;
    };

    return current_user;

  })();

  add_initialize(App.classes.current_user._init);

  App.classes.num = (function() {
    function num() {}

    num.quantity = function(number) {
      var byte, gb, kb, mb, unit;
      kb = 1024;
      mb = kb * kb;
      gb = mb * kb;
      byte = 1;
      unit = 'B';
      if (number >= kb && number < mb) {
        byte = kb;
        unit = 'KB';
      } else if (number >= mb && number < gb) {
        byte = mb;
        unit = 'MB';
      } else if (number >= gb) {
        byte = gb;
        unit = 'GB';
      }
      return "" + ((Math.ceil(number / byte * 100) / 100).toString()) + " " + unit;
    };

    return num;

  })();

  App.classes.scroll = (function(_super) {
    __extends(scroll, _super);

    function scroll() {
      this.check_scroll_bar = __bind(this.check_scroll_bar, this);
      this.check_scroll = __bind(this.check_scroll, this);
      this.disable = __bind(this.disable, this);
      this.enable = __bind(this.enable, this);
      return scroll.__super__.constructor.apply(this, arguments);
    }

    scroll.prototype.enable = function() {
      return App.$window.on('scroll', this.check_scroll);
    };

    scroll.prototype.disable = function() {
      return App.$window.off('scroll', this.check_scroll);
    };

    scroll.prototype.check_scroll = function() {
      var window_scroll_top;
      window_scroll_top = App.$window.scrollTop();
      if (window_scroll_top === 0) {
        return this.trigger('top');
      } else if (window_scroll_top >= (App.$document.height() - App.$window.height() - 20)) {
        return this.trigger('bottom');
      }
    };

    scroll.prototype.check_scroll_bar = function() {
      if (App.$window.height() >= App.$document.height()) {
        return this.trigger('bottom');
      } else {
        return false;
      }
    };

    return scroll;

  })(Backbone.View);

  App.classes.file = (function() {
    file.prototype.accept_file_types = {
      images: {
        png: 'image/png',
        jpeg: 'image/jpeg',
        gif: 'image/gif'
      },
      files: {
        txt: 'text/plain',
        doc: 'application/msword',
        docx: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        xls: ['application/excel', 'application/x-excel', 'application/x-msexcel', 'application/msexcel', 'application/vnd.ms-excel'],
        xlsx: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        ppt: ['application/mspowerpoint', 'application/ppt', 'application/vnd.ms-powerpoint', 'application/powerpoint'],
        pptx: 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        pdf: ['application/pdf', 'application/x-pdf'],
        zip: ['application/x-compress', 'application/x-lha-compressed', 'application/x-zip-compressed', 'application/zip', 'application/x-lha', 'application/x-lk-rlestream', 'multipart/x-zip'],
        lzh: ['application/lha', 'application/x-compress', 'application/x-lha', 'application/x-lha-compressed', 'application/x-lk-rlestream', 'application/x-unknown-content-type-LhasaArchive', 'application/x-zaurus-zac', 'application/x-zip-compressed', 'application/zip', 'binary/lzh', 'application/x-lzh-compressed'],
        mov: 'video/quicktime',
        flv: ['video/x-flv', 'video/mp4', 'video/x-m4v', 'audio/mp4a-latm', 'video/3gpp', 'video/quicktime', 'audio/mp4'],
        avi: ['video/avi', 'video/msvideo', 'video/x-msvideo'],
        wmv: ['audio/x-ms-wmv', 'video/x-ms-wmv'],
        mp4: ['video/mp4', 'audio/mp4', 'application/mp4'],
        mpeg: ['video/mpeg', 'video/mpg'],
        bmp: 'image/bmp',
        tiff: 'image/tiff'
      }
    };

    file.prototype.extension_thumbnail_path = (function() {
      var thumbnail_paths;
      thumbnail_paths = {};
      _(['accdb', 'avi', 'bmp', 'css', 'txt', 'doc', 'docx', 'eml', 'eps', 'file', 'fla', 'gif', 'html', 'ind', 'ini', 'jpeg', 'jpg', 'jsf', 'midi', 'mov', 'mp3', 'mpeg', 'mpg', 'pdf', 'png', 'ppt', 'pptx', 'proj', 'psd', 'pst', 'pub', 'rar', 'readme', 'settings', 'text', 'tiff', 'tif', 'url', 'vsd', 'wav', 'wma', 'wmv', 'xls', 'xlsx', 'zip', 'lzh', 'mp4', 'flv']).each(function(extension) {
        return thumbnail_paths[extension] = "/assets/img/extensions/" + extension + ".png";
      });
      return thumbnail_paths;
    })();

    file.prototype.max_upload_file_size = 1024 * 1024 * 10;

    file.prototype.accept_images = true;

    file.prototype.accept_files = true;

    function file(accept_images, accept_files) {
      if (accept_images == null) {
        accept_images = true;
      }
      if (accept_files == null) {
        accept_files = true;
      }
      this.show_file_type_error = __bind(this.show_file_type_error, this);
      this.get_extension_thumbnail_path = __bind(this.get_extension_thumbnail_path, this);
      this.get_accept_file_types = __bind(this.get_accept_file_types, this);
      this.find_recursive = __bind(this.find_recursive, this);
      this.get_file_type = __bind(this.get_file_type, this);
      this.is_accept_file_size = __bind(this.is_accept_file_size, this);
      this.is_file_type_file = __bind(this.is_file_type_file, this);
      this.is_file_type_image = __bind(this.is_file_type_image, this);
      this.is_accept_file_type = __bind(this.is_accept_file_type, this);
      this.accept_images = !!accept_images;
      this.accept_files = !!accept_files;
    }

    file.prototype.is_accept_file_type = function(file_type) {
      file_type = file_type.toLowerCase();
      return (this.accept_images && this.is_file_type_image(file_type)) || (this.accept_files && this.is_file_type_file(file_type));
    };

    file.prototype.is_file_type_image = function(file_type) {
      file_type = file_type.toLowerCase();
      file_type = file_type.replace(/^jpg$/, 'jpeg');
      file_type = file_type.replace(/^tif$/, 'tiff');
      return this.find_recursive(this.accept_file_types.images, file_type) !== null;
    };

    file.prototype.is_file_type_file = function(file_type) {
      file_type = file_type.toLowerCase();
      return this.find_recursive(this.accept_file_types.files, file_type) !== null;
    };

    file.prototype.is_accept_file_size = function(file_size) {
      return +file_size <= this.max_upload_file_size;
    };

    file.prototype.get_file_type = function(file) {
      if ((file.type != null) && file.type.length > 0) {
        return file.type;
      }
      if (file.name.match(/\.doc$/i) !== null) {
        return 'application/msword';
      }
      if (file.name.match(/\.docx$/i) !== null) {
        return 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
      }
      if (file.name.match(/\.zip$/i) !== null) {
        return 'application/zip';
      }
      if (file.name.match(/\.lzh$/i) !== null) {
        return 'application/lha';
      }
      if (file.name.match(/\.flv$/i) !== null) {
        return 'video/x-flv';
      }
      return '';
    };

    file.prototype.find_recursive = function(object, item) {
      var key, value;
      for (key in object) {
        value = object[key];
        if (_.isArray(value)) {
          if (_.indexOf(value, item) >= 0) {
            return key;
          }
        } else {
          if (item === value) {
            return key;
          }
        }
      }
      return null;
    };

    file.prototype.get_accept_file_types = function(to_upper_case) {
      var accept_file_types;
      if (to_upper_case == null) {
        to_upper_case = true;
      }
      accept_file_types = [];
      if (this.accept_images) {
        accept_file_types = _.union(accept_file_types, _.keys(this.accept_file_types.images));
      }
      if (this.accept_files) {
        accept_file_types = _.union(accept_file_types, _.keys(this.accept_file_types.files));
      }
      return accept_file_types.map((function(_this) {
        return function(extension) {
          if (to_upper_case) {
            extension = extension.toUpperCase();
          }
          return extension;
        };
      })(this));
    };

    file.prototype.get_extension_thumbnail_path = function(file_name) {
      var extension, match;
      match = file_name.match(/\.([^.]+)$/);
      if ((match != null ? match[1] : void 0) != null) {
        extension = match[1].toLowerCase();
        if (this.extension_thumbnail_path[extension] != null) {
          return this.extension_thumbnail_path[extension];
        }
      }
      return false;
    };

    file.prototype.show_file_type_error = function() {
      if (this.modal == null) {
        this.modal = new App.elements.modal();
        this.modal.set_title('有効なファイルではありません');
        this.modal.set_body("<p>\n許可された有効なファイル（" + (this.get_accept_file_types().join(',')) + "）では無いか、ファイルが破損しているか、\n10MBを超えるファイルが指定されている可能性があります。\n</p>");
        this.modal.set_footer_cancel_only('閉じる');
      }
      return this.modal.show();
    };

    return file;

  })();

  App.functions.submit_once_form = (function(_this) {
    return function($form) {
      if (!($form instanceof jQuery)) {
        $form = $($form);
      }
      if ($form.length < 1 || $form[0].nodeName.toLowerCase() !== 'form') {
        return false;
      }
      return $form.each(function(index, form) {
        var $target_form;
        $target_form = $(form);
        return $target_form.one('submit', function() {
          $target_form.on('submit', function() {
            return false;
          });
          return true;
        });
      });
    };
  })(this);

  Backbone.Collection.prototype._toJSON = Backbone.Collection.prototype.toJSON;

  Backbone.Collection.prototype.toJSON = function(options) {
    var token;
    token = {};
    token[App.config.security.csrf_token_key] = fuel_fetch_token();
    this.map(function(model, index) {
      return token[index] = model.toJSON(options);
    });
    return token;
  };

  Backbone.Model.prototype._destroy = Backbone.Model.prototype.destroy;

  Backbone.Model.prototype.destroy = function(options) {
    var err;
    if (!(options != null)) {
      options = {};
    }
    if (!(options.data != null)) {
      options.data = {};
    }
    if (typeof options.data === 'string') {
      try {
        options.data = JSON.parse(options.data);
      } catch (_error) {
        err = _error;
        options.data = {};
      }
    }
    options.data[App.config.security.csrf_token_key] = fuel_fetch_token();
    options.data = JSON.stringify(options.data);
    return Backbone.Model.prototype._destroy.apply(this, [options]);
  };

  Backbone.Model.prototype._toJSON = Backbone.Model.prototype.toJSON;

  Backbone.Model.prototype.toJSON = function() {
    var token_obj;
    token_obj = {};
    token_obj[App.config.security.csrf_token_key] = fuel_fetch_token();
    return _.extend(Backbone.Model.prototype._toJSON.apply(this, arguments), token_obj);
  };

  Backbone._sync = Backbone.sync;

  Backbone.sync = (function(_this) {
    return function(method, model, options) {
      var ex_options;
      ex_options = _(options).clone();
      ex_options.error = function(xhr) {
        var content_type, error, response, _ref;
        if ((xhr != null ? xhr.getResponseHeader : void 0) != null) {
          content_type = xhr.getResponseHeader('content-type');
          if (content_type && content_type.match(/^(application\/json|text\/javascript)/)) {
            response = null;
            try {
              if (xhr.response != null) {
                response = xhr.response;
              } else if (((_ref = Backbone.$) != null ? _ref.parseJSON : void 0) != null) {
                response = Backbone.$.parseJSON(xhr.responseText);
              } else if ((typeof JSON !== "undefined" && JSON !== null ? JSON.parse : void 0) != null) {
                response = JSON.parse(xhr.responseText);
              }
            } catch (_error) {
              error = _error;
            }
            xhr.response = response;
          }
        }
        if ((options != null ? options.error : void 0) != null) {
          return options.error(xhr, ex_options);
        }
      };
      return Backbone._sync(method, model, ex_options);
    };
  })(this);

  App.classes.time = (function() {
    function time() {}

    time.now = function(format) {
      if (format == null) {
        format = 'Y-m-d H:i:s';
      }
      return App.classes.time.format(new Date(), format);
    };

    time.format = function(date, format) {
      var full_day, full_hour, full_minute, full_month, full_second, full_year, key, replace_texts, result, value;
      if (format == null) {
        format = 'Y-m-d H:i:s';
      }
      full_year = date.getFullYear().toString();
      full_month = (date.getMonth() + 1).toString();
      if (full_month.length < 2) {
        full_month = '0' + full_month;
      }
      full_day = date.getDate().toString();
      if (full_day.length < 2) {
        full_day = '0' + full_day;
      }
      full_hour = date.getHours().toString();
      if (full_hour.length < 2) {
        full_hour = '0' + full_hour;
      }
      full_minute = date.getMinutes().toString();
      if (full_minute.length < 2) {
        full_minute = '0' + full_minute;
      }
      full_second = date.getSeconds().toString();
      if (full_second.length < 2) {
        full_second = '0' + full_second;
      }
      replace_texts = {
        'Y': full_year,
        'm': full_month,
        'd': full_day,
        'H': full_hour,
        'i': full_minute,
        's': full_second
      };
      result = format;
      for (key in replace_texts) {
        value = replace_texts[key];
        result = result.replace(new RegExp(key, 'g'), value);
      }
      return result;
    };

    return time;

  })();

  App.classes.Validation = (function() {
    function Validation() {
      this._validation_array_length_between = __bind(this._validation_array_length_between, this);
      this._validation_array_exact_length = __bind(this._validation_array_exact_length, this);
      this._validation_array_max_length = __bind(this._validation_array_max_length, this);
      this._validation_array_min_length = __bind(this._validation_array_min_length, this);
      this._validation_valid_array = __bind(this._validation_valid_array, this);
      this._validation_valid_single_line = __bind(this._validation_valid_single_line, this);
      this._validation_valid_zip_code = __bind(this._validation_valid_zip_code, this);
      this._validation_compare_date = __bind(this._validation_compare_date, this);
      this._validation_valid_date = __bind(this._validation_valid_date, this);
      this._validation_valid_string = __bind(this._validation_valid_string, this);
      this._validation_required_with = __bind(this._validation_required_with, this);
      this._validation_numeric_between = __bind(this._validation_numeric_between, this);
      this._validation_numeric_max = __bind(this._validation_numeric_max, this);
      this._validation_numeric_min = __bind(this._validation_numeric_min, this);
      this._validation_valid_ip = __bind(this._validation_valid_ip, this);
      this._validation_valid_url = __bind(this._validation_valid_url, this);
      this._validation_valid_emails = __bind(this._validation_valid_emails, this);
      this._validation_valid_email = __bind(this._validation_valid_email, this);
      this._validation_exact_length = __bind(this._validation_exact_length, this);
      this._validation_max_length = __bind(this._validation_max_length, this);
      this._validation_min_length = __bind(this._validation_min_length, this);
      this._validation_match_field = __bind(this._validation_match_field, this);
      this._validation_match_pattern = __bind(this._validation_match_pattern, this);
      this._validation_match_value = __bind(this._validation_match_value, this);
      this._validation_required = __bind(this._validation_required, this);
      this._trim = __bind(this._trim, this);
      this._empty = __bind(this._empty, this);
      this._run_rule = __bind(this._run_rule, this);
      this.find_rule = __bind(this.find_rule, this);
      this.run = __bind(this.run, this);
      this.add_field = __bind(this.add_field, this);
    }

    Validation.validate_method_prefix = '_validation_';

    Validation.forge = function() {
      return new App.classes.Validation();
    };

    Validation.prototype.inputs = {};

    Validation.prototype.fields = {};

    Validation.prototype.validated = {};

    Validation.prototype.errors = {};

    Validation.prototype.add_field = function(name, label, rules) {
      var method, params, rule, rule_objects, _i, _len;
      rule_objects = [];
      if (typeof rules === 'object') {
        for (method in rules) {
          rule = rules[method];
          if (!(rule instanceof Array)) {
            rule = [rule];
          }
          rule_objects.push({
            method: method,
            label: label,
            params: rule
          });
        }
      } else if (typeof rules === 'string') {
        if (typeof rules === 'string') {
          rules = rules.split('|');
        }
        for (_i = 0, _len = rules.length; _i < _len; _i++) {
          rule = rules[_i];
          params = rule.match(/(.+)\[(.*)\]/);
          if (params !== null && params.length >= 3) {
            rule_objects.push({
              method: params[1],
              label: label,
              params: params[2].split(',')
            });
          } else {
            rule_objects.push({
              method: rule,
              label: label,
              params: []
            });
          }
        }
      }
      this.fields[name] = rule_objects;
      return this;
    };

    Validation.prototype.run = function(input) {
      var error, errors, field, result, rule, rules, validated_value, _i, _len, _ref, _ref1;
      if (!(input instanceof Object)) {
        return false;
      }
      this.inputs = input;
      this.validated = {};
      this.errors = {};
      _ref = this.fields;
      for (field in _ref) {
        rules = _ref[field];
        for (_i = 0, _len = rules.length; _i < _len; _i++) {
          rule = rules[_i];
          try {
            if (rule.method === 'required' || !this._empty(input[field])) {
              validated_value = this._run_rule(rule.method, input[field], rule.params);
              this.validated[field] = validated_value;
            }
          } catch (_error) {
            errors = _error;
            if (!(this.errors[field] != null)) {
              this.errors[field] = {};
            }
            this.errors[field][errors.rule] = {
              label: rule.label,
              value: errors.value,
              params: errors.params
            };
          }
        }
      }
      result = true;
      _ref1 = this.errors;
      for (field in _ref1) {
        error = _ref1[field];
        result = false;
        break;
      }
      return result;
    };

    Validation.prototype.find_rule = function(rule) {
      var method_name;
      method_name = App.classes.Validation.validate_method_prefix + rule;
      if (!(this[method_name] != null) || typeof this[method_name] !== 'function') {
        return false;
      } else {
        return this[method_name];
      }
    };

    Validation.prototype._run_rule = function(rule, value, params) {
      var method, result_or_validated_value;
      method = this.find_rule(rule);
      if (!method) {
        throw {
          rule: rule,
          value: value,
          params: params
        };
      }
      result_or_validated_value = method(value, params);
      if (result_or_validated_value === false) {
        throw {
          rule: rule,
          value: value,
          params: params
        };
      } else if (result_or_validated_value !== true) {
        value = result_or_validated_value;
      }
      return value;
    };

    Validation.prototype._empty = function(value) {
      return (value == null) || value === false || value === '' || ((value instanceof Array) && value.length === 0);
    };

    Validation.prototype._trim = function(text) {
      if (!_.isString(value)) {
        return '';
      }
      return value.trim();
    };

    Validation.prototype._validation_required = function(value) {
      return !this._empty(value);
    };

    Validation.prototype._validation_match_value = function(value, params) {
      var c, compare, key, _i, _len;
      compare = params[0];
      if (this._empty(value) || value === compare) {
        return true;
      }
      if (compare instanceof Array) {
        for (_i = 0, _len = compare.length; _i < _len; _i++) {
          c = compare[_i];
          if (value === c) {
            return true;
          }
        }
      } else if (compare instanceof Object) {
        for (key in compare) {
          c = compare[key];
          if (value === c) {
            return true;
          }
        }
      }
      return false;
    };

    Validation.prototype._validation_match_pattern = function(value, params) {
      var pattern;
      if (this._empty(value)) {
        return true;
      }
      pattern = params[0];
      if (typeof pattern === 'string') {
        pattern = new RegExp(pattern);
        return value.match(pattern) !== null;
      } else if (pattern instanceof RegExp) {
        return value.match(pattern) !== null;
      }
      return false;
    };

    Validation.prototype._validation_match_field = function(value, params) {
      var field_name, _ref;
      field_name = params[0];
      if (!(((_ref = this.inputs) != null ? _ref[field_name] : void 0) != null)) {
        return false;
      }
      return value === this.inputs[field_name];
    };

    Validation.prototype._validation_min_length = function(value, params) {
      value = this._trim(value);
      if (value.length >= parseInt(params[0], 10)) {
        return false;
      } else {
        return value;
      }
    };

    Validation.prototype._validation_max_length = function(value, params) {
      value = this._trim(value);
      if (value.length <= parseInt(params[0], 10)) {
        return false;
      } else {
        return value;
      }
    };

    Validation.prototype._validation_exact_length = function(value, params) {
      value = this._trim(value);
      if (value.length === parseInt(params[0], 10)) {
        return false;
      } else {
        return value;
      }
    };

    Validation.prototype._validation_valid_email = function(value) {
      if (this._empty(value)) {
        return true;
      }
      return value.match(/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/i) !== null;
    };

    Validation.prototype._validation_valid_emails = function(value, params) {
      var email, emails, separator, _i, _len;
      separator = ',';
      if (params.length > 0) {
        separator = params[0];
      }
      if (this._empty(value)) {
        return true;
      }
      emails = value.split(separator);
      for (_i = 0, _len = emails.length; _i < _len; _i++) {
        email = emails[_i];
        if (!this._validation_valid_email(email)) {
          return false;
        }
      }
      return true;
    };

    Validation.prototype._validation_valid_url = function(value) {
      if (this._empty(value)) {
        return true;
      }
      return value.match(/^(ht|f)tp(s?)\:\/\/([\w]+:\w+@)?([a-zA-Z]{1}([\w\-]+\.)+([\w]{2,5}))(:[\d]{1,5})?((\/?\w+\/?)+|\/?)(\w+)?(\.[\w]{3,4})?((\?\w+=[\.\+\w\-\*\:]+)?(&\w+=\S+)*){1,2}?(\#\S+)?$/) !== null;
    };

    Validation.prototype._validation_valid_ip = function(value) {
      var ip_and_range, range, segment, segment_num, segments, _i, _len;
      if (this._empty(value)) {
        return true;
      }
      if (value.match(/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}(\/\d{1,2})?$/) === null) {
        return false;
      }
      ip_and_range = value.split('/');
      if (ip_and_range.length < 1 || ip_and_range.length > 2) {
        return false;
      }
      segments = ip_and_range[0].split('.');
      if (segments.length !== 4) {
        return false;
      }
      for (_i = 0, _len = segments.length; _i < _len; _i++) {
        segment = segments[_i];
        segment_num = parseInt(segment, 10);
        if (isNaN(segment_num) || segment_num < 0 || segment_num > 255) {
          return false;
        }
        if (segment.length >= 2 && parseInt(segment[0], 10) === 0) {
          return false;
        }
      }
      if (ip_and_range[1] != null) {
        range = parseInt(ip_and_range[1], 10);
        if (range < 1 || range > 31) {
          return false;
        }
      }
      return true;
    };

    Validation.prototype._validation_numeric_min = function(value, params) {
      if (this._empty(value)) {
        return true;
      }
      return parseFloat(value) >= parseFloat(params[0]);
    };

    Validation.prototype._validation_numeric_max = function(value, params) {
      if (this._empty(value)) {
        return true;
      }
      return parseFloat(value) <= parseFloat(params[0]);
    };

    Validation.prototype._validation_numeric_between = function(value, params) {
      if (this._empty(value)) {
        return true;
      }
      value = parseFloat(value);
      return value >= parseFloat(params[0]) && value <= parseFloat(params[1]);
    };

    Validation.prototype._validation_required_with = function(value, params) {
      var field_name, _ref;
      field_name = params[0];
      return !(this._empty(value) && !(!(((_ref = this.inputs) != null ? _ref[field_name] : void 0) != null) || this._empty(inputs[field_name])));
    };

    Validation.prototype._validation_valid_string = function(value, params) {
      var flags, is_all, pattern, regexp;
      if (this._empty(value)) {
        return true;
      }
      is_all = false;
      flags = ['alpha'];
      if ((params != null) && (params instanceof Array) && params.length > 0) {
        if (params.length > 1) {
          flags = params;
        } else {
          switch (params[0]) {
            case 'alpha':
              flags = ['alpha'];
              break;
            case 'alpha_numeric':
              flags = ['alpha', 'numeric'];
              break;
            case 'url_safe':
              flags = ['alpha', 'numeric', 'dashes'];
              break;
            case 'integer':
            case 'numeric':
              flags = ['numeric'];
              break;
            case 'float':
              flags = ['numeric', 'dots'];
              break;
            case 'quotes':
              flags = ['singlequotes', 'doublequotes'];
              break;
            case 'all':
              is_all = true;
              flags = ['alpha', 'numeric', 'spaces', 'newlines', 'tabs', 'punctuation', 'singlequotes', 'doublequotes', 'dashes'];
              break;
            default:
              return false;
          }
        }
      } else {
        return false;
      }
      pattern = '';
      if (is_all || (_.indexOf(flags, 'uppercase') < 0 && _.indexOf(flags, 'alpha') > 0)) {
        pattern += 'a-z';
      }
      if (is_all || (_.indexOf(flags, 'lowercase') < 0 && _.indexOf(flags, 'alpha') > 0)) {
        pattern += 'A-Z';
      }
      if (is_all || _.indexOf(flags, 'numeric') > 0) {
        pattern += '0-9';
      }
      if (is_all || _.indexOf(flags, 'spaces') > 0) {
        pattern += ' ';
      }
      if (is_all || _.indexOf(flags, 'newlines') > 0) {
        pattern += "\\n";
      }
      if (is_all || _.indexOf(flags, 'tabs') > 0) {
        pattern += "\\t";
      }
      if (is_all || _.indexOf(flags, 'dots') > 0) {
        pattern += '\\.';
      }
      if (is_all || _.indexOf(flags, 'commas') > 0) {
        pattern += ',';
      }
      if (is_all || _.indexOf(flags, 'punctuation') > 0) {
        pattern += "\\.,\\!\\?:;\\&";
      }
      if (is_all || _.indexOf(flags, 'dashes') > 0) {
        pattern += '_\\-';
      }
      if (is_all || _.indexOf(flags, 'singlequotes') > 0) {
        pattern += '\'';
      }
      if (is_all || _.indexOf(flags, 'doublequotes') > 0) {
        pattern += '\\"';
      }
      if (this._empty(pattern)) {
        pattern = '^(.*)$';
      } else {
        pattern = '^([' + pattern + '])+$';
      }
      regexp = new RegExp(pattern);
      return value.match(regexp) !== null;
    };

    Validation.prototype._validation_valid_date = function(value) {
      var date;
      if (this._empty(value)) {
        return true;
      }
      date = new Date(value);
      return (date instanceof Date) && !isNaN(date.getTime());
    };

    Validation.prototype._validation_compare_date = function(value, params) {
      var future_date, old_date, old_date_field_name, _ref;
      if (this._empty(value)) {
        return true;
      }
      old_date_field_name = params[0];
      if (!(((_ref = this.inputs) != null ? _ref[old_date_field_name] : void 0) != null)) {
        return false;
      }
      old_date = this.inputs[old_date_field_name];
      if (this._empty(old_date)) {
        return true;
      }
      future_date = new Date(value);
      old_date = new Date(old_date);
      if (!this._validation_valid_date(future_date) || !this._validation_valid_date(old_date)) {
        return true;
      }
      return future_date.getTime() >= old_date.getTime();
    };

    Validation.prototype._validation_valid_zip_code = function(value) {
      if (this._empty(value)) {
        return true;
      }
      return value.match(/[0-9]{3}\-?[0-9]{4}/) !== null;
    };

    Validation.prototype._validation_valid_single_line = function(value) {
      if (this._empty(value)) {
        return true;
      }
      return value.match(/\r\n|\n\r|\r|\n/g) === null;
    };

    Validation.prototype._validation_valid_array = function(value) {
      return value instanceof Array;
    };

    Validation.prototype._validation_array_min_length = function(value, min_length) {
      min_length = parseInt(min_length, 10);
      return this._validation_valid_array(value) && !isNaN(min_length) && value.length >= min_length;
    };

    Validation.prototype._validation_array_max_length = function(value, max_length) {
      max_length = parseInt(max_length, 10);
      return this._validation_valid_array(value) && !isNaN(max_length) && value.length <= max_length;
    };

    Validation.prototype._validation_array_exact_length = function(value, exact_length) {
      exact_length = parseInt(exact_length, 10);
      return this._validation_valid_array(value) && !isNaN(exact_length) && value.length === exact_length;
    };

    Validation.prototype._validation_array_length_between = function(value, min_length, max_length) {
      return this._validation_array_min_length(value, min_length) && this._validation_array_max_length(value, max_length);
    };

    return Validation;

  })();

  App.elements.Balloon = (function(_super) {
    __extends(Balloon, _super);

    function Balloon() {
      return Balloon.__super__.constructor.apply(this, arguments);
    }

    Balloon.prototype.el = 'body';

    Balloon.prototype.$el = $('body');

    Balloon.prototype.template = '<div class="js-elements-balloon"><div class="content"></div></div>';

    Balloon.prototype._type = null;

    Balloon.prototype._options = null;

    Balloon.prototype.$balloon = null;

    Balloon.prototype.DEFAULTS = {
      placement: 'left',
      container: false
    };

    Balloon.prototype._arrowSize = {
      top: 10,
      left: 10
    };

    Balloon.prototype.initialize = function(options) {
      var k, _i, _len, _ref;
      this._type = options != null ? options.type : void 0;
      if ((options != null ? options.el : void 0) != null) {
        this.setElement(options.el);
      }
      if ((options != null ? options.template : void 0) != null) {
        this.template = options.template;
      }
      _ref = ['type', 'el', 'template'];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        k = _ref[_i];
        delete options[k];
      }
      this._options = this._getOptions(options.option);
      this.listenTo(App.event, 'body_clicked', this.hide);
      return this;
    };

    Balloon.prototype.setContainer = function(container) {
      return this._options.container = $(container);
    };

    Balloon.prototype.show = function() {
      var $balloon, actualHeight, actualWidth, calculatedOffset, left, placement, pos, top;
      $balloon = this.balloon();
      this._setContent();
      placement = this._options.placement + 'Tale';
      $balloon.detach().css({
        'top': 0,
        'left': 0,
        'display': 'block',
        'z-index': 100
      }).addClass("bln" + this._type.charAt(0).toUpperCase() + this._type.slice(1)).addClass(placement);
      if (this._options.container) {
        $balloon.appendTo(this._options.container);
      } else {
        $balloon.insertAfter(this.el);
      }
      pos = this._getPosition();
      actualWidth = $balloon[0].offsetWidth;
      actualHeight = $balloon[0].offsetHeight;
      calculatedOffset = this._getCalculatedOffset(this._options.placement, pos, actualWidth, actualHeight);
      top = pos.top + this.$el.height() + this._arrowSize.top;
      left = pos.left;
      if (this._options.placement === 'right') {
        left = calculatedOffset.left - $balloon.width() + this._arrowSize.left;
      }
      return $balloon.offset({
        top: top,
        left: left
      });
    };

    Balloon.prototype.hide = function() {
      return $(this.balloon()).hide();
    };

    Balloon.prototype.balloon = function() {
      return this.$balloon = this.$balloon || $(_.template(this.template)());
    };

    Balloon.prototype._getDefaults = function() {
      return this.DEFAULTS;
    };

    Balloon.prototype._getOptions = function(options) {
      options = $.extend({}, this._getDefaults(), options);
      return options;
    };

    Balloon.prototype._setContent = function() {
      var $balloon, content;
      $balloon = this.balloon();
      content = this._getContent();
      return $balloon.find('.content').html(content);
    };

    Balloon.prototype._getPosition = function() {
      var clientRect, el;
      el = this.el;
      clientRect = null;
      if (typeof el.getBoundingClientRect === 'function') {
        clientRect = el.getBoundingClientRect();
      } else {
        clientRect = {
          width: el.offsetWidth,
          height: el.offsetHeight
        };
      }
      return $.extend({}, clientRect, this.$el.offset());
    };

    Balloon.prototype._getCalculatedOffset = function(placement, pos, actualWidth, actualHeight) {
      if (placement === 'left') {
        return {
          top: pos.top + pos.height / 2 - actualHeight / 2,
          left: pos.left - actualWidth
        };
      } else {
        return {
          top: pos.top + pos.height / 2 - actualHeight / 2,
          left: pos.left + pos.width
        };
      }
    };

    return Balloon;

  })(Backbone.View);

  App.elements.MemberBalloon = (function(_super) {
    __extends(MemberBalloon, _super);

    MemberBalloon.prototype._data = [];

    MemberBalloon.prototype._has_more = false;

    function MemberBalloon(options) {
      this.initialize({
        type: 'member',
        option: options
      });
    }

    MemberBalloon.prototype.resetData = function() {
      return this._data = [];
    };

    MemberBalloon.prototype.addData = function(model) {
      return this._data.push(model);
    };

    MemberBalloon.prototype.hasMore = function() {
      return this._has_more = true;
    };

    MemberBalloon.prototype.noMore = function() {
      return this._has_more = false;
    };

    MemberBalloon.prototype._getContent = function() {
      return _.template('<ul>\n<% _.each(users, function(user) { %>\n  <li>\n    <a href="<%- user.get(\'permalink\') %>">\n      <span class="profile"><img class="thmb" src="<%- user.get(\'icon_url\') %>" alt="<%- user.get(\'full_name\') %>"></span>\n      <span class="text"><em><%- user.get(\'full_name\') %></em></span>\n    </a>\n  </li>\n<% }); %>\n</ul>\n<% if (has_more) { %>\n<div class="readMoreLoader">\n  <a href="#" class="js-member-balloon-read-more"><i class="icon-plus"></i>もっと見る</a>\n  <span class="js-loader" style="display: none;">\n  <span class="bkcurtain"></span>\n    <img src="/assets/img/loader/21-1.gif">\n  </span>\n</div>\n<% } %>', {
        base_url: App.base_url,
        users: this._data,
        has_more: this._has_more
      });
    };

    return MemberBalloon;

  })(App.elements.Balloon);

  App.elements.loader = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.elements.loader.templates.loader = {
    loader: _.template('<span class="bkcurtain"></span>\n<img src="/assets/img/loader/21-1.gif">'),
    black_back: _.template('<div class="js-blackBack"></div>')
  };

  App.elements.loader.views.loader = (function(_super) {
    __extends(loader, _super);

    function loader() {
      this.set_size = __bind(this.set_size, this);
      this.set_height = __bind(this.set_height, this);
      this.set_width = __bind(this.set_width, this);
      this.fade_out = __bind(this.fade_out, this);
      this.fade_in = __bind(this.fade_in, this);
      this.hide = __bind(this.hide, this);
      this.show = __bind(this.show, this);
      this.set_css = __bind(this.set_css, this);
      this.set_img_center = __bind(this.set_img_center, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return loader.__super__.constructor.apply(this, arguments);
    }

    loader.prototype.el = '<span class="js-loader"></span>';

    loader.prototype.use_black_back = null;

    loader.prototype.$img = null;

    loader.prototype.$black_back = null;

    loader.prototype.initialize = function(options) {
      if ((options != null ? options.use_black_back : void 0) != null) {
        this.use_black_back = !!options.use_black_back;
      }
      return this.render();
    };

    loader.prototype.render = function() {
      this.$el.html(App.elements.loader.templates.loader.loader());
      this.$img = this.$el.find('img');
      if (this.use_black_back) {
        this.$black_back = $(App.elements.loader.templates.loader.black_back());
        this.$el.prepend(this.$black_back);
        this.$el.css({
          position: 'absolute'
        });
        this.$black_back.css({
          position: 'absolute'
        });
        this.$img.css({
          position: 'absolute'
        });
      }
      this.$el.hide();
      return this.$el;
    };

    loader.prototype.set_img_center = function(css) {
      if (!this.use_black_back) {
        return this;
      }
      this.$img.css({
        top: "" + (Math.floor((this.$el.height() / 2) - 16)) + "px",
        left: "" + (Math.floor((this.$el.width() / 2) - 16)) + "px"
      });
      return this;
    };

    loader.prototype.set_css = function(css) {
      this.$el.css(css);
      return this;
    };

    loader.prototype.show = function() {
      this.$el.show();
      return this;
    };

    loader.prototype.hide = function() {
      this.$el.hide();
      return this;
    };

    loader.prototype.fade_in = function(speed) {
      if (speed == null) {
        speed = 300;
      }
      return this.$el.fadeIn(speed);
    };

    loader.prototype.fade_out = function(speed) {
      if (speed == null) {
        speed = 300;
      }
      return this.$el.fadeOut(speed);
    };

    loader.prototype.set_width = function(width) {
      this.$el.width(width);
      return this;
    };

    loader.prototype.set_height = function(height) {
      this.$el.height(height);
      return this;
    };

    loader.prototype.set_size = function(width, height) {
      this.set_width(width);
      this.set_height(height);
      return this;
    };

    return loader;

  })(Backbone.View);

  App.elements.message_banner = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.elements.message_banner.templates.banner = _.template('<p>\n  <span class="icon"><i class="icon-exclamation"></i></span>\n  <span class="txt">\n    <span class="val"><%= message %></span>\n  </span>\n  <% if(has_close){ %>\n    <span class="close">\n      <a class="uiAnchor js-message-banner-close-button" href="#">\n        <i class="icon-xmark"></i>\n      </a>\n    </span>\n  <% } %>\n</p>');

  App.elements.message_banner.views.message_banner = (function(_super) {
    __extends(message_banner, _super);

    function message_banner() {
      this.close = __bind(this.close, this);
      this.disable_close = __bind(this.disable_close, this);
      this.enable_close = __bind(this.enable_close, this);
      this.change_to_done = __bind(this.change_to_done, this);
      this.change_to_notice = __bind(this.change_to_notice, this);
      this.change_to_error = __bind(this.change_to_error, this);
      this.change_to_none = __bind(this.change_to_none, this);
      this.set_has_close = __bind(this.set_has_close, this);
      this.set_class_name = __bind(this.set_class_name, this);
      this.set_message = __bind(this.set_message, this);
      this.change_class = __bind(this.change_class, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      this.events = __bind(this.events, this);
      return message_banner.__super__.constructor.apply(this, arguments);
    }

    message_banner.prototype.el = '<div class="msgBanner"></div>';

    message_banner.prototype.events = function() {
      return {
        'click .js-message-banner-close-button': 'close'
      };
    };

    message_banner.prototype.has_close = false;

    message_banner.prototype.message = '';

    message_banner.prototype.class_name = 'error';

    message_banner.prototype.class_names = ['error', 'notice', 'done', 'none'];

    message_banner.prototype.initialize = function(options) {
      if (options != null) {
        if (options.message != null) {
          this.set_message(options.message);
        }
        if (options.class_name != null) {
          this.set_class_name(options.class_name);
        }
        if (options.has_close != null) {
          this.set_has_close(options.has_close);
        }
      }
      return this.render();
    };

    message_banner.prototype.render = function() {
      this.$el.html(App.elements.message_banner.templates.banner({
        message: this.message,
        has_close: this.has_close
      }));
      return this.change_class(this.class_name);
    };

    message_banner.prototype.change_class = function(class_name) {
      var _i, _len, _ref;
      if (class_name !== 'none') {
        this.$el.addClass(class_name);
      }
      _ref = this.class_names;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        class_name = _ref[_i];
        if (this.class_name !== class_name) {
          this.$el.removeClass(class_name);
        }
      }
      return this;
    };

    message_banner.prototype.set_message = function(message) {
      this.message = message;
      return this;
    };

    message_banner.prototype.set_class_name = function(class_name) {
      if (_.indexOf(this.class_names, class_name) >= 0) {
        this.class_name = class_name;
      }
      return this;
    };

    message_banner.prototype.set_has_close = function(has_close) {
      return this.has_close = !!has_close;
    };

    message_banner.prototype.change_to_none = function() {
      this.set_class_name('none');
      this.change_class();
      return this;
    };

    message_banner.prototype.change_to_error = function() {
      this.set_class_name('error');
      this.change_class();
      return this;
    };

    message_banner.prototype.change_to_notice = function() {
      this.set_class_name('notice');
      this.change_class();
      return this;
    };

    message_banner.prototype.change_to_done = function() {
      this.set_class_name('done');
      this.change_class();
      return this;
    };

    message_banner.prototype.enable_close = function() {
      this.set_has_close(true);
      this.render();
      return this;
    };

    message_banner.prototype.disable_close = function() {
      this.set_has_close(false);
      this.render();
      return this;
    };

    message_banner.prototype.close = function() {
      this.remove();
      return false;
    };

    return message_banner;

  })(Backbone.View);

  App.elements.file_input = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.elements.file_input.templates.thumbnail = {
    "delete": _.template('<a class="delete uiAnchor js-file-input-container-thumbnail-delete-button" href="#">\n  <i class="icon-xmark"></i>\n</a>'),
    dummy: _.template('<img class="thumb" src="<%= dummy_path %>" alt="">\n<input name="is_delete_image" class="js-is-delete-image" value="1" type="hidden" />'),
    thumbnail: _.template('<img class="thumb js-thumb" src="<%= src %>" alt="<%= name %>">')
  };

  App.elements.file_input.views.file_input = (function(_super) {
    __extends(file_input, _super);

    function file_input() {
      this.set_loader_size = __bind(this.set_loader_size, this);
      this.hide_loader = __bind(this.hide_loader, this);
      this.show_loader = __bind(this.show_loader, this);
      this.is_file_type_file = __bind(this.is_file_type_file, this);
      this.is_file_type_image = __bind(this.is_file_type_image, this);
      this.is_accept_file_type = __bind(this.is_accept_file_type, this);
      this.clear_file_select = __bind(this.clear_file_select, this);
      this.resize_thumbnail = __bind(this.resize_thumbnail, this);
      this.remove_current_thumbnail = __bind(this.remove_current_thumbnail, this);
      this.hide_delete_button = __bind(this.hide_delete_button, this);
      this.show_delete_button = __bind(this.show_delete_button, this);
      this.render_thumbnail = __bind(this.render_thumbnail, this);
      this.reset_thumbnail_to_dummy = __bind(this.reset_thumbnail_to_dummy, this);
      this.get_extension_thumbnail_path = __bind(this.get_extension_thumbnail_path, this);
      this.render_extension_thumbnail_by_name = __bind(this.render_extension_thumbnail_by_name, this);
      this.render_extension_thumbnail = __bind(this.render_extension_thumbnail, this);
      this.on_load_file = __bind(this.on_load_file, this);
      this.load_thumbnail = __bind(this.load_thumbnail, this);
      this.get_accept_file_types = __bind(this.get_accept_file_types, this);
      this.show_file_type_error = __bind(this.show_file_type_error, this);
      this.file_changed = __bind(this.file_changed, this);
      this.set_input = __bind(this.set_input, this);
      this.initialize = __bind(this.initialize, this);
      return file_input.__super__.constructor.apply(this, arguments);
    }

    file_input.prototype.el = null;

    file_input.prototype.events = {
      'change .js-file-input-container-input-area-input': 'file_changed',
      'click .js-file-input-container-thumbnail-delete-button': 'clear_file_select'
    };

    file_input.prototype.$thumbnail_area = null;

    file_input.prototype.$input_area = null;

    file_input.prototype.$input = null;

    file_input.prototype.$input_clone = null;

    file_input.prototype.$current_thumbnail_img = null;

    file_input.prototype.$delete = null;

    file_input.prototype.default_thumbnail_img_src = null;

    file_input.prototype.loader = null;

    file_input.prototype.enabled_file_api = true;

    file_input.prototype.accept_images = true;

    file_input.prototype.accept_files = true;

    file_input.prototype.dummy_path = '/assets/img/theme/common/community_x140.png';

    file_input.prototype.width = 140;

    file_input.prototype.height = 140;

    file_input.prototype.file = null;

    file_input.prototype.initialize = function(options) {
      var thumbnail_height, thumbnail_width;
      if ((options != null ? options.accept_images : void 0) != null) {
        this.accept_images = !!options.accept_images;
      }
      if ((options != null ? options.accept_files : void 0) != null) {
        this.accept_files = !!options.accept_files;
      }
      if ((options != null ? options.dummy_path : void 0) != null) {
        this.dummy_path = options.dummy_path;
      }
      this.file = new App.classes.file(this.accept_images, this.accept_files);
      if (((options != null ? options.width : void 0) != null) && !isNaN(thumbnail_width = parseInt(options.width, 10))) {
        this.width = thumbnail_width;
      }
      if (((options != null ? options.height : void 0) != null) && !isNaN(thumbnail_height = parseInt(options.height, 10))) {
        this.height = thumbnail_height;
      }
      this.$thumbnail_area = this.$el.find('.js-file-input-container-thumbnail-area');
      this.$input_area = this.$el.find('.js-file-input-container-input-area');
      this.$input = this.$input_area.find('.js-file-input-container-input-area-input');
      this.$input_clone = this.$input.clone();
      this.loader = new App.elements.loader.views.loader();
      this.loader.$el.css('top', 'inherit');
      this.$thumbnail_area.append(this.loader.hide().$el);
      this.$current_thumbnail_img = this.$thumbnail_area.find('img.thumb');
      this.default_thumbnail_img_src = this.$current_thumbnail_img.attr('src');
      this.$delete = $(App.elements.file_input.templates.thumbnail["delete"]()).hide();
      this.$thumbnail_area.append(this.$delete);
      this.enabled_file_api = window.File && window.FileReader && window.FileList && window.Blob && !App.current_user.is_android;
      if (this.$current_thumbnail_img.length > 0) {
        return this.show_delete_button();
      } else {
        return this.reset_thumbnail_to_dummy();
      }
    };

    file_input.prototype.set_input = function($input) {
      return this.$input = $input;
    };

    file_input.prototype.file_changed = function(event) {
      var file, file_type, has_accept_file, _i, _len, _ref, _ref1;
      if (!this.enabled_file_api || !((event != null ? (_ref = event.target) != null ? _ref.files : void 0 : void 0) != null)) {
        this.reset_thumbnail_to_dummy();
        this.$el.find('.js-is-delete-image').val('');
        return false;
      }
      has_accept_file = false;
      _ref1 = event.target.files;
      for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
        file = _ref1[_i];
        file_type = this.file.get_file_type(file);
        if (!this.is_accept_file_type(file_type)) {
          this.show_file_type_error();
          this.clear_file_select();
          continue;
        }
        if (this.is_file_type_image(file_type)) {
          this.load_thumbnail(file);
        } else {
          this.render_extension_thumbnail(file);
        }
        has_accept_file = true;
        break;
      }
      if (!has_accept_file) {
        this.reset_thumbnail_to_dummy();
      }
      return false;
    };

    file_input.prototype.show_file_type_error = function() {
      return this.file.show_file_type_error();
    };

    file_input.prototype.get_accept_file_types = function() {
      return this.file.get_accept_file_types();
    };

    file_input.prototype.load_thumbnail = function(file) {
      var reader;
      this.show_loader();
      reader = new window.FileReader();
      reader.onload = this.on_load_file;
      return reader.readAsDataURL(file);
    };

    file_input.prototype.on_load_file = function(event) {
      var _ref;
      if ((event != null ? (_ref = event.target) != null ? _ref.result : void 0 : void 0) != null) {
        return this.render_thumbnail(event.target.result);
      }
    };

    file_input.prototype.render_extension_thumbnail = function(file, show_loader, show_delete) {
      if (show_loader == null) {
        show_loader = true;
      }
      if (show_delete == null) {
        show_delete = true;
      }
      return this.render_extension_thumbnail_by_name(file.name, show_loader, show_delete);
    };

    file_input.prototype.render_extension_thumbnail_by_name = function(file_name, show_loader, show_delete) {
      var extension_thumbnail_path;
      if (show_loader == null) {
        show_loader = true;
      }
      if (show_delete == null) {
        show_delete = true;
      }
      extension_thumbnail_path = this.get_extension_thumbnail_path(file_name);
      if (extension_thumbnail_path) {
        return this.render_thumbnail(extension_thumbnail_path, file_name, show_loader, show_delete);
      } else {
        return this.reset_thumbnail_to_dummy();
      }
    };

    file_input.prototype.get_extension_thumbnail_path = function(file_name) {
      return this.file.get_extension_thumbnail_path(file_name);
    };

    file_input.prototype.reset_thumbnail_to_dummy = function() {
      this.render_thumbnail(null, null, false, false);
      return this;
    };

    file_input.prototype.render_thumbnail = function(src, name, show_loader, show_delete) {
      var $current_thumbnail_img;
      if (show_loader == null) {
        show_loader = true;
      }
      if (show_delete == null) {
        show_delete = true;
      }
      if (!src) {
        $current_thumbnail_img = $(App.elements.file_input.templates.thumbnail.dummy({
          dummy_path: this.dummy_path
        }));
      } else {
        if (show_loader) {
          this.show_loader();
        }
        if (!(name != null)) {
          name = '';
        }
        $current_thumbnail_img = $(App.elements.file_input.templates.thumbnail.thumbnail({
          src: src,
          name: name
        }));
        $current_thumbnail_img.on('error', (function(_this) {
          return function() {
            _this.show_file_type_error();
            _this.clear_file_select();
            if (show_loader) {
              _this.hide_loader();
            }
            return _this.trigger('load_thumbnail_failed');
          };
        })(this));
      }
      $current_thumbnail_img.on('load', (function(_this) {
        return function() {
          _this.resize_thumbnail();
          if (show_loader) {
            return _this.hide_loader();
          }
        };
      })(this));
      this.$thumbnail_area.append($current_thumbnail_img);
      this.remove_current_thumbnail();
      this.$current_thumbnail_img = $current_thumbnail_img;
      if (show_delete) {
        return this.show_delete_button();
      } else {
        return this.hide_delete_button();
      }
    };

    file_input.prototype.show_delete_button = function() {
      return this.$delete.show().appendTo(this.$thumbnail_area);
    };

    file_input.prototype.hide_delete_button = function() {
      return this.$delete.hide();
    };

    file_input.prototype.remove_current_thumbnail = function() {
      if (this.$current_thumbnail_img != null) {
        this.$current_thumbnail_img.remove();
        this.$current_thumbnail_img = null;
      }
      return this;
    };

    file_input.prototype.resize_thumbnail = function() {
      var left, new_height, new_width, thumbnail_height, thumbnail_width, top;
      thumbnail_width = this.$current_thumbnail_img.width();
      thumbnail_height = this.$current_thumbnail_img.height();
      if (thumbnail_width < thumbnail_height) {
        new_width = this.width;
        new_height = Math.round(this.width * thumbnail_height / thumbnail_width);
        top = -Math.floor((new_height - this.height) / 2);
        left = 0;
      } else {
        new_height = this.height;
        new_width = Math.round(this.height * thumbnail_width / thumbnail_height);
        top = 0;
        left = -Math.floor((new_width - this.width) / 2);
      }
      return this.$current_thumbnail_img.css({
        top: "" + top + "px",
        left: "" + left + "px",
        width: "" + new_width + "px",
        height: "" + new_height + "px"
      });
    };

    file_input.prototype.clear_file_select = function() {
      var input_file_path;
      this.$input.val('');
      input_file_path = this.$input.val();
      if ((input_file_path != null) && (input_file_path + "").length > 0) {
        this.$input.replaceWith(this.$input = this.$input_clone.clone());
      }
      this.$thumbnail_area.find('input[type="hidden"]').remove();
      this.reset_thumbnail_to_dummy();
      return false;
    };

    file_input.prototype.is_accept_file_type = function(file_type) {
      return this.file.is_accept_file_type(file_type);
    };

    file_input.prototype.is_file_type_image = function(file_type) {
      return this.file.is_file_type_image(file_type);
    };

    file_input.prototype.is_file_type_file = function(file_type) {
      return this.file.is_file_type_file(file_type);
    };

    file_input.prototype.show_loader = function() {
      this.loader.show();
      return this;
    };

    file_input.prototype.hide_loader = function() {
      this.loader.fade_out();
      return this;
    };

    file_input.prototype.set_loader_size = function(width, height) {
      this.loader.set_size(width, height);
      return this;
    };

    return file_input;

  })(Backbone.View);

  App.elements.setting_balloon = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.elements.setting_balloon.templates.balloon = _.template('<div class="setting js-setting-balloon">\n  <a class="uiAnchor js-setting-balloon-anchor" href="#"><i class="icon-setting"></i></a>\n  <div class="blnOption js-setting-balloon-options" style="display: none;">\n    <ul>\n      <% $.each(links, function(index, link){ %>\n      <li>\n        <a href="<%= link.path %>"<% if(\'target\' in link){ %> target="<%= link.target %>"<% } %>\n          <% if(\'event_name\' in link){ %> data-event-name="<%= link.event_name %>"<% } %>\n          class="js-setting-ballon-links-link">\n          <%= link.label %>\n        </a>\n      </li>\n      <% }); %>\n    </ul>\n  </div>\n</div>');

  App.elements.setting_balloon.views.setting_balloon = (function(_super) {
    __extends(setting_balloon, _super);

    function setting_balloon() {
      this.hide = __bind(this.hide, this);
      this.show = __bind(this.show, this);
      this.link_clicked = __bind(this.link_clicked, this);
      this.toggle = __bind(this.toggle, this);
      this.check_hide_on_other_setting_balloon_opened = __bind(this.check_hide_on_other_setting_balloon_opened, this);
      this.check_hide_on_document_clicked = __bind(this.check_hide_on_document_clicked, this);
      this.initialize = __bind(this.initialize, this);
      return setting_balloon.__super__.constructor.apply(this, arguments);
    }

    setting_balloon.prototype.el = null;

    setting_balloon.prototype.events = {
      'click .js-setting-balloon-anchor': 'toggle',
      'click .js-setting-ballon-links-link': 'link_clicked'
    };

    setting_balloon.prototype.$options = null;

    setting_balloon.prototype.initialize = function(options) {
      if (!this.$el.hasClass('js-setting-balloon') && ((options != null ? options.links : void 0) != null)) {
        this.el = App.elements.setting_balloon.templates.balloon({
          links: options.links
        });
        this.$el = $(this.el);
      }
      this.$options = this.$el.find('.js-setting-balloon-options');
      App.event.on('document_clicked', this.check_hide_on_document_clicked);
      return App.event.on('setting_balloon_opened', this.check_hide_on_other_setting_balloon_opened);
    };

    setting_balloon.prototype.check_hide_on_document_clicked = function(event) {
      if (this.$options.find(event.target).length === 0) {
        return this.hide();
      }
    };

    setting_balloon.prototype.check_hide_on_other_setting_balloon_opened = function(cid) {
      if (this.cid !== cid) {
        return this.hide();
      }
    };

    setting_balloon.prototype.toggle = function() {
      if (this.$options.is(':visible')) {
        this.hide();
      } else {
        this.show();
      }
      return false;
    };

    setting_balloon.prototype.link_clicked = function(event) {
      var $link, event_name;
      $link = $(event.target);
      event_name = $link.attr('data-event-name');
      if (event_name) {
        this.trigger(event_name);
        return false;
      }
      return true;
    };

    setting_balloon.prototype.show = function() {
      this.$options.show();
      App.event.trigger('setting_balloon_opened', this.cid);
      return this;
    };

    setting_balloon.prototype.hide = function() {
      this.$options.hide();
      return this;
    };

    return setting_balloon;

  })(Backbone.View);

  add_route('.*', function() {
    return $('.js-setting-balloon').each((function(_this) {
      return function(index, element) {
        return new App.elements.setting_balloon.views.setting_balloon({
          el: element
        });
      };
    })(this));
  });

  App.elements.schedule_balloon = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.elements.schedule_balloon.views.schedule_balloon = (function(_super) {
    __extends(schedule_balloon, _super);

    function schedule_balloon() {
      this._getCalculatedOffset = __bind(this._getCalculatedOffset, this);
      this._getPosition = __bind(this._getPosition, this);
      this.hide = __bind(this.hide, this);
      this.show = __bind(this.show, this);
      this.initialize = __bind(this.initialize, this);
      return schedule_balloon.__super__.constructor.apply(this, arguments);
    }

    schedule_balloon.prototype.initialize = function(options) {};

    schedule_balloon.prototype._arrowSize = {
      top: 30,
      left: 45
    };

    schedule_balloon.prototype.show = function() {
      var calculatedOffset, pos, position;
      pos = this._getPosition();
      position = this.$el.hasClass('leftTale') ? 'left' : 'right';
      calculatedOffset = this._getCalculatedOffset(position, pos, this.el.offsetWidth, this.el.offsetHeight);
      this.$el.css('top', this._arrowSize.top);
      if (position === 'right') {
        this.$el.css('left', calculatedOffset.left - this.$el.width() + this._arrowSize.left);
      }
      return this.$el.show();
    };

    schedule_balloon.prototype.hide = function() {
      return this.$el.hide();
    };

    schedule_balloon.prototype._getPosition = function() {
      var clientRect, el;
      el = this.el;
      clientRect = null;
      if (typeof el.getBoundingClientRect === 'function') {
        clientRect = el.getBoundingClientRect();
      } else {
        clientRect = {
          width: el.offsetWidth,
          height: el.offsetHeight
        };
      }
      return $.extend({}, clientRect, this.$el.offset());
    };

    schedule_balloon.prototype._getCalculatedOffset = function(placement, pos, actualWidth, actualHeight) {
      if (placement === 'left') {
        return {
          top: pos.top + pos.height / 2 - actualHeight / 2,
          left: pos.left - actualWidth
        };
      } else {
        return {
          top: pos.top + pos.height / 2 - actualHeight / 2,
          left: pos.left + pos.width
        };
      }
    };

    return schedule_balloon;

  })(Backbone.View);

  App.elements.show_more = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.elements.show_more.templates.button = _.template('<a href="#" class="js-show-more-button"><i class="icon-plus"></i><%= label %></a>');

  App.elements.show_more.views.show_more = (function(_super) {
    __extends(show_more, _super);

    function show_more() {
      this.fade_out = __bind(this.fade_out, this);
      this.fade_in = __bind(this.fade_in, this);
      this.hide = __bind(this.hide, this);
      this.show = __bind(this.show, this);
      this.hide_loader = __bind(this.hide_loader, this);
      this.show_loader = __bind(this.show_loader, this);
      this.clicked = __bind(this.clicked, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return show_more.__super__.constructor.apply(this, arguments);
    }

    show_more.prototype.el = '<div class="readMoreLoader"></div>';

    show_more.prototype.events = {
      'click .js-show-more-button': 'clicked'
    };

    show_more.prototype.label = 'もっと読む';

    show_more.prototype.loader = null;

    show_more.prototype.initialize = function(options) {
      if ((options != null ? options.label : void 0) != null) {
        this.label = options.label;
      }
      this.loader = new App.elements.loader.views.loader();
      return this.render();
    };

    show_more.prototype.render = function() {
      this.$el.html(App.elements.show_more.templates.button({
        label: this.label
      }));
      this.$el.append(this.loader.$el);
      return this.$el;
    };

    show_more.prototype.clicked = function() {
      this.trigger('clicked');
      return false;
    };

    show_more.prototype.show_loader = function() {
      return this.loader.show();
    };

    show_more.prototype.hide_loader = function() {
      return this.loader.hide();
    };

    show_more.prototype.show = function() {
      this.$el.show();
      return this;
    };

    show_more.prototype.hide = function() {
      this.$el.hide();
      return this;
    };

    show_more.prototype.fade_in = function(speed) {
      if (speed == null) {
        speed = 300;
      }
      return this.$el.fadeIn(speed);
    };

    show_more.prototype.fade_out = function(speed) {
      if (speed == null) {
        speed = 300;
      }
      return this.$el.fadeOut(speed);
    };

    return show_more;

  })(Backbone.View);

  App.elements.show_more.views.content_body_show_more = (function(_super) {
    __extends(content_body_show_more, _super);

    function content_body_show_more() {
      this.show_more = __bind(this.show_more, this);
      this.initialize = __bind(this.initialize, this);
      return content_body_show_more.__super__.constructor.apply(this, arguments);
    }

    content_body_show_more.prototype.el = null;

    content_body_show_more.prototype.events = {
      'click': 'show_more'
    };

    content_body_show_more.prototype.initialize = function() {
      return this.$parent_span = this.$el.parent();
    };

    content_body_show_more.prototype.show_more = function() {
      var is_url2link, original_string, url2link_class;
      if (this.$parent_span.hasClass('js-show-more-span')) {
        original_string = this.$el.data('original-string');
        is_url2link = this.$el.data('is-url2link');
        if (is_url2link) {
          url2link_class = new App.elements.url2link.views.url2link();
          this.$parent_span.html(url2link_class.done_url2link(original_string));
          url2link_class.renew_observe_click_url2link();
        } else {
          this.$parent_span.html(original_string);
        }
        return false;
      } else {
        return false;
      }
    };

    return content_body_show_more;

  })(Backbone.View);

  App.elements.show_more.views.content_body_show_more_modal = (function(_super) {
    __extends(content_body_show_more_modal, _super);

    function content_body_show_more_modal() {
      this.show_modal = __bind(this.show_modal, this);
      return content_body_show_more_modal.__super__.constructor.apply(this, arguments);
    }

    content_body_show_more_modal.prototype.el = null;

    content_body_show_more_modal.prototype.modal = null;

    content_body_show_more_modal.prototype.events = {
      'click': 'show_modal'
    };

    content_body_show_more_modal.prototype.show_modal = function() {
      var is_url2link, original_string, url2link_class;
      original_string = this.$el.data('original-string');
      is_url2link = this.$el.data('is-url2link');
      if (is_url2link) {
        url2link_class = new App.elements.url2link.views.url2link();
        original_string = url2link_class.done_url2link(original_string);
      }
      if (!this.modal) {
        this.modal = new App.elements.modal();
        this.modal.set_title(this.$el.data('string-title'));
        this.modal.set_body(original_string);
        this.modal.set_footer_cancel_only('閉じる');
        this.modal.set_height_low_class();
      }
      this.modal.show();
      if (is_url2link) {
        return url2link_class.renew_observe_click_url2link();
      }
    };

    return content_body_show_more_modal;

  })(Backbone.View);

  App.elements.datepicker = {
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.elements.datepicker.views.datepicker = (function(_super) {
    __extends(datepicker, _super);

    function datepicker() {
      this.initialize = __bind(this.initialize, this);
      return datepicker.__super__.constructor.apply(this, arguments);
    }

    datepicker.prototype.el = null;

    datepicker.options = {
      closeText: '閉じる',
      prevText: '<前',
      nextText: '次>',
      currentText: '今日',
      monthNames: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
      monthNamesShort: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
      dayNames: ['日曜日', '月曜日', '火曜日', '水曜日', '木曜日', '金曜日', '土曜日'],
      dayNamesShort: ['日', '月', '火', '水', '木', '金', '土'],
      dayNamesMin: ['日', '月', '火', '水', '木', '金', '土'],
      weekHeader: '週',
      dateFormat: 'yy/mm/dd',
      firstDay: 0,
      isRTL: false,
      showMonthAfterYear: true,
      yearSuffix: '年',
      minDate: new Date()
    };

    datepicker.prototype.initialize = function() {
      var option;
      option = App.elements.datepicker.views.datepicker.options;
      return _.each(this.$el, (function(_this) {
        return function(input) {
          var min_date;
          min_date = $(input).attr('data-min-date');
          if (min_date != null) {
            min_date = new Date(min_date);
            if (min_date) {
              option.minDate = min_date;
            }
          }
          return $(input).datepicker(option);
        };
      })(this));
    };

    return datepicker;

  })(Backbone.View);

  add_route('.*', function() {
    return new App.elements.datepicker.views.datepicker({
      el: $('[rel="datepicker"]')
    });
  });

  App.elements.tab = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.elements.tab.config.css_class_names = {
    active: 'active',
    active_container: 'active-content'
  };

  App.elements.tab.config.fade_speed = {
    "in": 300
  };

  App.elements.tab.config.fade_speed.out = App.elements.tab.config.fade_speed["in"] * 0.6;

  App.elements.tab.views.tab = (function(_super) {
    __extends(tab, _super);

    function tab() {
      this.trigger_shown = __bind(this.trigger_shown, this);
      this.trigger_hidden = __bind(this.trigger_hidden, this);
      this.show_container = __bind(this.show_container, this);
      this.tab_navigation_clicked = __bind(this.tab_navigation_clicked, this);
      this.initialize = __bind(this.initialize, this);
      return tab.__super__.constructor.apply(this, arguments);
    }

    tab.prototype.el = null;

    tab.prototype.events = {
      'click .js-tab-navigation li a': 'tab_navigation_clicked'
    };

    tab.prototype.containers = null;

    tab.prototype.current_shown_container_id = null;

    tab.prototype.is_progressing = false;

    tab.prototype.initialize = function() {
      var $containers;
      this.containers = {};
      $containers = this.$el.find('.js-tab-containers');
      return this.$el.find('.js-tab-navigation').find('li').each((function(_this) {
        return function(index, element) {
          var $container, $li, $link, container_id;
          $li = $(element);
          $link = $li.find('a');
          container_id = $link.attr('href');
          $container = $containers.find(container_id);
          container_id = container_id.replace(/^#/, '');
          if (!$container || $container.length <= 0) {
            return true;
          }
          _this.containers[container_id] = {
            $navigation: $li,
            $container: $container
          };
          if ($container.is(':visible')) {
            return _this.current_shown_container_id = container_id;
          }
        };
      })(this));
    };

    tab.prototype.tab_navigation_clicked = function(event) {
      var $link, action, link, target_container_id;
      $link = $(event.target);
      if (event.target.nodeName.toLowerCase() !== 'a') {
        $link = $link.parents('a');
      }
      target_container_id = $link.attr('href').replace(/^#/, '');
      if (App.current_user.is_smartphone === true) {
        if (location.href.match(/message/)) {
          action = target_container_id.replace(/js-input/, '').toLowerCase();
          if (action === 'text') {
            link = location.href.replace(/show/, ['message', 'new'].join('/'));
          } else if (action === 'file') {
            link = location.href.replace(/show/, ['message', 'file', 'new'].join('/'));
          }
          location.href = link;
        } else {
          action = target_container_id.replace(/post/, '');
          link = location.href.replace(/show/, ['feed', action].join('/'));
          location.href = link;
        }
      } else {
        this.show_container(target_container_id);
      }
      return false;
    };

    tab.prototype.show_container = function(target_container_id) {
      var current_container;
      if (this.containers[target_container_id] == null) {
        return false;
      }
      if (target_container_id === this.current_shown_container_id || this.is_progressing) {
        return true;
      }
      this.is_progressing = true;
      current_container = this.containers[this.current_shown_container_id];
      current_container.$navigation.removeClass(App.elements.tab.config.css_class_names.active);
      return current_container.$container.fadeOut(App.elements.tab.config.fade_speed.out, (function(_this) {
        return function() {
          var next_container;
          current_container.$container.removeClass(App.elements.tab.config.css_class_names.active_container);
          _this.trigger_hidden(_this.current_shown_container_id);
          next_container = _this.containers[target_container_id];
          next_container.$navigation.addClass(App.elements.tab.config.css_class_names.active);
          return next_container.$container.fadeIn(App.elements.tab.config.fade_speed["in"], function() {
            next_container.$container.addClass(App.elements.tab.config.css_class_names.active_container);
            _this.current_shown_container_id = target_container_id;
            _this.is_progressing = false;
            return _this.trigger_shown(_this.current_shown_container_id);
          });
        };
      })(this));
    };

    tab.prototype.trigger_hidden = function(container_id) {
      return this.trigger('hidden', container_id);
    };

    tab.prototype.trigger_shown = function(container_id) {
      return this.trigger('shown', container_id);
    };

    return tab;

  })(Backbone.View);

  add_route('.*', function() {
    return $('.js-tab').each((function(_this) {
      return function(index, element) {
        return new App.elements.tab.views.tab({
          el: element
        });
      };
    })(this));
  });

  App.elements.errors = {
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.elements.errors.views.errors = (function(_super) {
    __extends(errors, _super);

    function errors() {
      this.get_html = __bind(this.get_html, this);
      this.render = __bind(this.render, this);
      this.flatten = __bind(this.flatten, this);
      this.set_errors = __bind(this.set_errors, this);
      this.initialize = __bind(this.initialize, this);
      return errors.__super__.constructor.apply(this, arguments);
    }

    errors.prototype.el = '<em class="error"></em>';

    errors.prototype.template = _.template('：<span class="val"><%= errors %></span>');

    errors.prototype.errors = [];

    errors.prototype.initialize = function(options) {
      this.errors = [];
      if ((options != null ? options.errors : void 0) != null) {
        this.set_errors(options.errors);
      }
      return this.render();
    };

    errors.prototype.set_errors = function(errors) {
      errors = !(errors instanceof Object) ? [errors] : errors;
      this.errors = this.flatten(errors);
      return this;
    };

    errors.prototype.flatten = function(errors) {
      var error, flat, flatten, flatten_errors, index, _i, _len;
      flatten_errors = [];
      for (index in errors) {
        error = errors[index];
        if (typeof error === 'object') {
          flatten = this.flatten(error);
          for (_i = 0, _len = flatten.length; _i < _len; _i++) {
            flat = flatten[_i];
            flatten_errors.push(flat);
          }
        } else {
          flatten_errors.push(error);
        }
      }
      return flatten_errors;
    };

    errors.prototype.render = function() {
      this.$el.empty().html(this.template({
        errors: this.errors.join('：')
      }));
      return this.$el;
    };

    errors.prototype.get_html = function() {
      return this.$el.html();
    };

    return errors;

  })(Backbone.View);

  App.elements.file_form = (function(_super) {
    __extends(file_form, _super);

    function file_form() {
      this._parse_response_json = __bind(this._parse_response_json, this);
      this.upload = __bind(this.upload, this);
      this.initialize = __bind(this.initialize, this);
      return file_form.__super__.constructor.apply(this, arguments);
    }

    file_form.prototype.el = null;

    file_form.prototype.$input = null;

    file_form.prototype.url = null;

    file_form.prototype.use_iframe_on_upload = App.is_legacy_browser ? 1 : 0;

    file_form.prototype.templates = {
      progress_bar: _.template('<div class="progress progress-striped active">\n  <div class="bar" style="width: 0%;"></div>\n</div>')
    };

    file_form.prototype.$progress = null;

    file_form.prototype.$progress_bar = null;

    file_form.prototype.initialize = function(options) {
      var input;
      if ((options != null ? options.input : void 0) != null) {
        input = options.input;
      }
      if (input == null) {
        input = this.$el.find('input[type="file"]');
      }
      if (input instanceof jQuery) {
        this.$input = input;
      } else {
        this.$input = $(input);
      }
      if ((options != null ? options.url : void 0) != null) {
        this.url = options.url;
      }
      if (typeof this.url === 'function') {
        this.url = this.url();
      }
      return this.$el.fileupload({
        url: this.url,
        dataType: 'json',
        type: 'post',
        fileInput: null
      });
    };

    file_form.prototype.upload = function(data, done, fail, progress) {
      var key, params, value, _i, _len;
      if (data == null) {
        data = {};
      }
      params = [];
      if (data instanceof Array) {
        for (_i = 0, _len = data.length; _i < _len; _i++) {
          value = data[_i];
          if (((value != null ? value.name : void 0) != null) && (value.value != null) && value.name !== App.config.security.csrf_token_key) {
            params.push(value);
          }
        }
      } else {
        for (key in data) {
          value = data[key];
          if (key !== App.config.security.csrf_token_key) {
            if (typeof value !== 'function') {
              params.push({
                name: key,
                value: value
              });
            }
          }
        }
      }
      params.push({
        name: App.config.security.csrf_token_key,
        value: fuel_fetch_token()
      });
      params.push({
        name: 'response_with_html',
        value: App.can_use_file_api ? 0 : 1
      });
      this.$el.fileupload('option', 'formData', params);
      if ((done == null) && (data.done != null) && typeof data.done === 'function') {
        done = data.done;
      }
      if ((done == null) || typeof done !== 'function') {
        done = (function(_this) {
          return function(event, response) {};
        })(this);
      }
      this.$el.fileupload('option', 'done', (function(_this) {
        return function(event, response) {
          return done(event, response, _this._parse_response_json(response));
        };
      })(this));
      if ((fail == null) && (data.fail != null) && typeof data.fail === 'function') {
        fail = data.fail;
      }
      if ((fail == null) || typeof fail !== 'function') {
        fail = (function(_this) {
          return function(event, response) {};
        })(this);
      }
      this.$el.fileupload('option', 'fail', (function(_this) {
        return function(event, response) {
          return fail(event, _this._parse_response_json(response));
        };
      })(this));
      if (this.$input.val().length <= 0) {
        return this.$el.fileupload('add', {
          files: true
        });
      } else {
        return this.$el.fileupload('add', {
          fileInput: this.$input
        });
      }
    };

    file_form.prototype._parse_response_json = function(response) {
      var content_type, error, obj, _ref, _ref1;
      if (((_ref = response.jqXHR) != null ? _ref.getResponseHeader : void 0) != null) {
        content_type = response.jqXHR.getResponseHeader('content-type');
        if ((content_type != null) && content_type.match(/^(application\/json|text\/javascript)/)) {
          obj = null;
          try {
            if (response.jqXHR.response != null) {
              obj = response.jqXHR.response;
            } else if (((_ref1 = Backbone.$) != null ? _ref1.parseJSON : void 0) != null) {
              obj = Backbone.$.parseJSON(response.jqXHR.responseText);
            } else if ((typeof JSON !== "undefined" && JSON !== null ? JSON.parse : void 0) != null) {
              obj = JSON.parse(response.jqXHR.responseText);
            }
          } catch (_error) {
            error = _error;
          }
          response.jqXHR.response = obj;
        }
      }
      return response;
    };

    return file_form;

  })(Backbone.View);

  App.elements.form = {};

  add_route('.*', function() {
    return $('form').on('submit', (function(_this) {
      return function(event) {
        fuel_set_token(event.target);
        return true;
      };
    })(this));
  });

  App.elements.form.ajaxzip = {
    views: {}
  };

  App.elements.form.ajaxzip.views.ajaxzip = (function(_super) {
    __extends(ajaxzip, _super);

    function ajaxzip() {
      this.insert_address_from_zip = __bind(this.insert_address_from_zip, this);
      this.initialize = __bind(this.initialize, this);
      return ajaxzip.__super__.constructor.apply(this, arguments);
    }

    ajaxzip.prototype.el = 'body';

    ajaxzip.prototype.events = {
      'click .js-input-from-zip-button': 'insert_address_from_zip'
    };

    ajaxzip.prototype.initialize = function() {
      return AjaxZip2.JSONDATA = '/assets/js/ajaxzip2/data';
    };

    ajaxzip.prototype.insert_address_from_zip = function(event) {
      var profile_id;
      profile_id = event.target.id;
      AjaxZip2.zip2addr('profile[' + profile_id + '][postcode_zip]', 'profile[' + profile_id + '][state]', 'profile[' + profile_id + '][city_street]');
      return false;
    };

    return ajaxzip;

  })(Backbone.View);

  add_route('.*', function() {
    return new App.elements.form.ajaxzip.views.ajaxzip();
  });

  App.elements.form.datetime = {
    views: {}
  };

  App.elements.form.datetime.views.datetime = (function(_super) {
    __extends(datetime, _super);

    function datetime() {
      this.initialize = __bind(this.initialize, this);
      this.format_hour_minute = __bind(this.format_hour_minute, this);
      return datetime.__super__.constructor.apply(this, arguments);
    }

    datetime.prototype.el = 'body';

    datetime.prototype.format_hour_minute = function(hour, minute) {
      var hour_is_empty, minute_is_empty;
      if (hour.length === 0 || minute.length === 0) {
        return '';
      }
      hour = parseInt(hour, 10);
      hour_is_empty = isNaN(hour);
      minute = parseInt(minute, 10);
      minute_is_empty = isNaN(minute);
      if (!hour_is_empty && !minute_is_empty) {
        if (hour < 10) {
          hour = '0' + hour;
        }
        if (minute < 10) {
          minute = '0' + minute;
        }
        return "" + hour + ":" + minute;
      }
      return '';
    };

    datetime.prototype.initialize = function() {
      this.$el.find('span[rel="wrap-time"] select').on('change', (function(_this) {
        return function(event) {
          var $wrap_time, hour, minute, value;
          $wrap_time = $(event.target).parents('span[rel="wrap-time"]');
          hour = _($wrap_time.find('select[data-datetime-role="hour"]').val()).trim();
          minute = _($wrap_time.find('select[data-datetime-role="minute"]').val()).trim();
          if (hour.length <= 0) {
            value = '';
          } else {
            value = _this.format_hour_minute(hour, minute);
          }
          return $wrap_time.find('input[data-datetime-role="hidden"]').val(value);
        };
      })(this));
      return this.$el.find('span[rel="wrap-datetime"]').find('select, input').on('change', (function(_this) {
        return function(event) {
          var $wrap_time, date, hour_minute, value;
          $wrap_time = $(event.target).parents('span[rel="wrap-datetime"]');
          date = _($wrap_time.find('input[data-datetime-role="date"]').val()).trim();
          hour_minute = _this.format_hour_minute(_($wrap_time.find('select[data-datetime-role="hour"]').val()).trim(), _($wrap_time.find('select[data-datetime-role="minute"]').val()).trim());
          value = "" + date;
          if (value.length > 0) {
            value += ' ' + hour_minute;
          }
          return $wrap_time.find('input[data-datetime-role="hidden"]').val(value);
        };
      })(this));
    };

    return datetime;

  })(Backbone.View);

  add_route('.*', function() {
    return new App.elements.form.datetime.views.datetime();
  });

  App.elements.modal = (function(_super) {
    __extends(modal, _super);

    function modal() {
      this.is_active = __bind(this.is_active, this);
      this.modal_keydown_controlle = __bind(this.modal_keydown_controlle, this);
      this.unfreeze = __bind(this.unfreeze, this);
      this.freeze = __bind(this.freeze, this);
      this.hide_on_document_clicked = __bind(this.hide_on_document_clicked, this);
      this.prevent_multiple_submit = __bind(this.prevent_multiple_submit, this);
      this.set_height_high_class = __bind(this.set_height_high_class, this);
      this.set_height_low_class = __bind(this.set_height_low_class, this);
      this.clear_height_classes = __bind(this.clear_height_classes, this);
      this.hide_loader = __bind(this.hide_loader, this);
      this.show_loader = __bind(this.show_loader, this);
      this.get_body = __bind(this.get_body, this);
      this.get_header = __bind(this.get_header, this);
      this.hide = __bind(this.hide, this);
      this.show = __bind(this.show, this);
      this.set_footer = __bind(this.set_footer, this);
      this.set_footer_cancel_only_left = __bind(this.set_footer_cancel_only_left, this);
      this.set_footer_cancel_action = __bind(this.set_footer_cancel_action, this);
      this.set_footer_cancel_notice = __bind(this.set_footer_cancel_notice, this);
      this.set_footer_cancel_attention = __bind(this.set_footer_cancel_attention, this);
      this.set_footer_cancel_only = __bind(this.set_footer_cancel_only, this);
      this.set_footer_decide_only = __bind(this.set_footer_decide_only, this);
      this.set_footer_cancel_delete = __bind(this.set_footer_cancel_delete, this);
      this.set_footer_cancel_decide = __bind(this.set_footer_cancel_decide, this);
      this.set_decide_button_label = __bind(this.set_decide_button_label, this);
      this.set_cancel_button_label = __bind(this.set_cancel_button_label, this);
      this.set_body = __bind(this.set_body, this);
      this.set_title = __bind(this.set_title, this);
      this.add_header_heding_class = __bind(this.add_header_heding_class, this);
      this.add_container_class = __bind(this.add_container_class, this);
      this.decide = __bind(this.decide, this);
      this.cancel = __bind(this.cancel, this);
      this.cancel_button_clicked = __bind(this.cancel_button_clicked, this);
      this.disable_cancel_auto_hide = __bind(this.disable_cancel_auto_hide, this);
      this.enable_cancel_auto_hide = __bind(this.enable_cancel_auto_hide, this);
      this.enable_auto_set_top = __bind(this.enable_auto_set_top, this);
      this.disable_auto_set_top = __bind(this.disable_auto_set_top, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return modal.__super__.constructor.apply(this, arguments);
    }

    modal.body_class = 'modalOpened';

    modal.fade_in_speed = 300;

    modal.fade_out_speed = 300;

    modal.prototype.el = '<div class="dialog"></div>';

    modal.prototype.templates = null;

    modal.prototype.events = {
      'click .dlgHeader .close a': 'hide',
      'click .js-modal-cancel-button': 'cancel_button_clicked',
      'click .js-modal-decide-button': 'decide'
    };

    modal.prototype.$container = null;

    modal.prototype.$container_modal_wrapper = null;

    modal.prototype.$header = null;

    modal.prototype.$title = null;

    modal.prototype.$modal_body = null;

    modal.prototype.$footer = null;

    modal.prototype.$cancel_button = null;

    modal.prototype.$decide_button = null;

    modal.prototype.$close_button = null;

    modal.prototype.title = '';

    modal.prototype.body = '';

    modal.prototype.loader = null;

    modal.prototype.set_buttons = true;

    modal.prototype.cancel_button_label = '取り消す';

    modal.prototype.decide_button_label = '保存';

    modal.prototype.buttons_type = 'cancel_decide';

    modal.prototype.enable_auto_top = true;

    modal.prototype.is_enabled_cancel_button_auto_hide = true;

    modal.prototype.initialize = function(options) {
      var modal_class, _i, _len, _ref;
      this.templates = App.elements._modal_templates;
      if (options != null) {
        if (options.title != null) {
          this.title = options.title;
        }
        if (options.body != null) {
          this.body = options.body;
        }
        if (options.set_buttons != null) {
          this.set_buttons = !!options.set_buttons;
        }
        if (options.cancel_button_label != null) {
          this.cancel_button_label = options.cancel_button_label;
        }
        if (options.decide_button_label != null) {
          this.decide_button_label = options.decide_button_label;
        }
        if ((options.buttons_type != null) && (options.buttons_type === 'cancel_only' || options.buttons_type === 'decide_only' || options.buttons_type === 'cancel_decide' || options.buttons_type === 'cancel_delete')) {
          this.buttons_type = options.buttons_type;
        }
        if (options.modal_classes != null) {
          this.$el.removeClass('dialog');
          _ref = options.modal_classes;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            modal_class = _ref[_i];
            this.$el.addClass(modal_class);
          }
        }
      }
      this.$container = $(this.templates.container());
      this.$container_modal_wrapper = this.$container.find('.modalDlgWrapper');
      this.$container_modal_wrapper.append(this.$el);
      App.$body.append(this.$container);
      this.on({
        show: (function(_this) {
          return function() {
            App.$window.on('keydown', _this.modal_keydown_controlle);
            return App.$document.on('click', _this.hide_on_document_clicked);
          };
        })(this),
        hide: (function(_this) {
          return function() {
            App.$window.off('keydown', _this.modal_keydown_controlle);
            return App.$document.off('click', _this.hide_on_document_clicked);
          };
        })(this)
      });
      return this.render();
    };

    modal.prototype.render = function() {
      this.$el.html(this.templates.modal({
        title: this.title,
        body: this.body,
        set_buttons: this.set_buttons,
        footer: this.templates.buttons[this.buttons_type]({
          cancel_button_label: this.cancel_button_label,
          decide_button_label: this.decide_button_label
        })
      }));
      this.$header = this.$el.find('.dlgHeader');
      this.$title = this.$el.find('.dlgHeader .heading h2');
      this.$modal_body = this.$el.find('.dlgContainer');
      this.$footer = this.$el.find('.dlgFooter');
      this.$cancel_button = this.$el.find('.js-modal-cancel-button');
      this.$decide_button = this.$el.find('.js-modal-decide-button');
      this.$close_button = this.$el.find('.dlgHeader .close a');
      this.loader = new App.elements.loader.views.loader({
        use_black_back: true
      });
      this.$el.append(this.loader.$el);
      this.$el.hide();
      return this.$el;
    };

    modal.prototype.disable_auto_set_top = function() {
      this.enable_auto_top = false;
      return this;
    };

    modal.prototype.enable_auto_set_top = function() {
      return this.enable_auto_top = true;
    };

    modal.prototype.enable_cancel_auto_hide = function() {
      this.is_enabled_cancel_button_auto_hide = true;
      return this;
    };

    modal.prototype.disable_cancel_auto_hide = function() {
      this.is_enabled_cancel_button_auto_hide = false;
      return this;
    };

    modal.prototype.cancel_button_clicked = function() {
      if (this.is_enabled_cancel_button_auto_hide) {
        this.hide();
      } else {
        this.cancel();
      }
      return false;
    };

    modal.prototype.cancel = function() {
      this.trigger('cancel');
      return false;
    };

    modal.prototype.decide = function() {
      if (this.$decide_button.attr('disabled') === 'disabled') {
        return false;
      }
      this.trigger('decide');
      return false;
    };

    modal.prototype.add_container_class = function(classes) {
      if (_.isArray(classes)) {
        classes = classes.join(' ');
      }
      return this.$container.addClass(classes);
    };

    modal.prototype.add_header_heding_class = function(classes) {
      if (_.isArray(classes)) {
        classes = classes.join(' ');
      }
      return this.$header.find('.heading').addClass(classes);
    };

    modal.prototype.set_title = function(title, classes) {
      if (classes == null) {
        classes = null;
      }
      this.title = title;
      this.$title.text(this.title).html();
      if (classes != null) {
        if (_.isArray(classes)) {
          classes = classes.join(' ');
        }
        this.$title.removeAttr('class').attr('class', classes);
      }
      return this;
    };

    modal.prototype.set_body = function(body) {
      this.body = body;
      this.$modal_body.html(this.body);
      return this;
    };

    modal.prototype.set_cancel_button_label = function(label_text) {
      this.cancel_button_label = label_text;
      this.$cancel_button.html(label_text);
      return this;
    };

    modal.prototype.set_decide_button_label = function(label_text) {
      this.decide_button_label = label_text;
      this.$decide_button.html(label_text);
      return this;
    };

    modal.prototype.set_footer_cancel_decide = function(cancel_button_label, decide_button_label) {
      if (cancel_button_label == null) {
        cancel_button_label = null;
      }
      if (decide_button_label == null) {
        decide_button_label = null;
      }
      this.buttons_type = 'cancel_decide';
      if (cancel_button_label != null) {
        this.set_cancel_button_label(cancel_button_label);
      }
      if (decide_button_label != null) {
        this.set_decide_button_label(decide_button_label);
      }
      this.set_footer();
      return this;
    };

    modal.prototype.set_footer_cancel_delete = function(cancel_button_label, decide_button_label) {
      if (cancel_button_label == null) {
        cancel_button_label = null;
      }
      if (decide_button_label == null) {
        decide_button_label = null;
      }
      this.buttons_type = 'cancel_delete';
      if (cancel_button_label != null) {
        this.set_cancel_button_label(cancel_button_label);
      }
      if (decide_button_label != null) {
        this.set_decide_button_label(decide_button_label);
      }
      this.set_footer();
      return this;
    };

    modal.prototype.set_footer_decide_only = function(decide_button_label) {
      if (decide_button_label == null) {
        decide_button_label = null;
      }
      this.buttons_type = 'decide_only';
      if (decide_button_label != null) {
        this.set_decide_button_label(decide_button_label);
      }
      this.set_footer();
      return this;
    };

    modal.prototype.set_footer_cancel_only = function(cancel_button_label) {
      if (cancel_button_label == null) {
        cancel_button_label = null;
      }
      this.buttons_type = 'cancel_only';
      if (cancel_button_label != null) {
        this.set_cancel_button_label(cancel_button_label);
      }
      this.set_footer();
      return this;
    };

    modal.prototype.set_footer_cancel_attention = function(cancel_button_label, decide_button_label) {
      if (cancel_button_label == null) {
        cancel_button_label = null;
      }
      if (decide_button_label == null) {
        decide_button_label = null;
      }
      this.buttons_type = 'cancel_attention';
      if (cancel_button_label != null) {
        this.set_cancel_button_label(cancel_button_label);
      }
      if (decide_button_label != null) {
        this.set_decide_button_label(decide_button_label);
      }
      this.set_footer();
      return this;
    };

    modal.prototype.set_footer_cancel_notice = function(cancel_button_label, decide_button_label) {
      if (cancel_button_label == null) {
        cancel_button_label = null;
      }
      if (decide_button_label == null) {
        decide_button_label = null;
      }
      this.buttons_type = 'cancel_notice';
      if (cancel_button_label != null) {
        this.set_cancel_button_label(cancel_button_label);
      }
      if (decide_button_label != null) {
        this.set_decide_button_label(decide_button_label);
      }
      this.set_footer();
      return this;
    };

    modal.prototype.set_footer_cancel_action = function(cancel_button_label, decide_button_label) {
      if (cancel_button_label == null) {
        cancel_button_label = null;
      }
      if (decide_button_label == null) {
        decide_button_label = null;
      }
      this.buttons_type = 'cancel_action';
      if (cancel_button_label != null) {
        this.set_cancel_button_label(cancel_button_label);
      }
      if (decide_button_label != null) {
        this.set_decide_button_label(decide_button_label);
      }
      this.set_footer();
      return this;
    };

    modal.prototype.set_footer_cancel_only_left = function(cancel_button_label) {
      if (cancel_button_label == null) {
        cancel_button_label = null;
      }
      this.buttons_type = 'cancel_only_left';
      if (cancel_button_label != null) {
        this.set_cancel_button_label(cancel_button_label);
      }
      this.set_footer();
      return this;
    };

    modal.prototype.set_footer = function() {
      this.$footer.html(this.templates.buttons[this.buttons_type]({
        cancel_button_label: this.cancel_button_label,
        decide_button_label: this.decide_button_label
      }));
      return this;
    };

    modal.prototype.show = function() {
      var modal_height, top, window_height;
      this.trigger('show');
      App.$body.addClass(App.elements.modal.body_class);
      this.$container.show();
      if (!App.current_user.is_smartphone) {
        if (this.enable_auto_top) {
          this.$el.css('visibility', 'hidden').show();
          modal_height = this.$el.height();
          this.$el.css('visibility', '').hide();
          window_height = App.$window.height();
          top = (window_height - modal_height) / 2;
          if (top < 10) {
            top = 10;
          }
          this.$container_modal_wrapper.css('top', "" + ((top / window_height) * 100) + "%");
        }
      } else {
        this.$el.css('visibility', 'hidden').show();
        modal_height = this.$el.height();
        this.$el.css('visibility', '').hide();
        top = event.pageY - (modal_height / 2);
        if (top < 10) {
          top = 10;
        }
        this.$container_modal_wrapper.css('top', "" + top + "px");
        $('div.js-blackBack').css({
          'height': App.$body.height() + modal_height,
          'width': App.$document.width()
        });
      }
      this.$el.fadeIn(App.elements.modal.fade_in_speed, (function(_this) {
        return function() {
          return _this.trigger('shown');
        };
      })(this));
      return false;
    };

    modal.prototype.hide = function() {
      if (this.$cancel_button.attr('disabled') === 'disabled') {
        return false;
      }
      this.trigger('hide');
      App.$body.removeClass(App.elements.modal.body_class);
      this.$container.hide();
      this.$el.fadeOut(App.elements.modal.fade_out_speed, (function(_this) {
        return function() {
          return _this.trigger('hidden');
        };
      })(this));
      this.cancel();
      return false;
    };

    modal.prototype.get_header = function() {
      return this.$header;
    };

    modal.prototype.get_body = function() {
      return this.$modal_body;
    };

    modal.prototype.show_loader = function(disable_buttons) {
      var body_position, padding;
      if (disable_buttons == null) {
        disable_buttons = true;
      }
      padding = 20;
      body_position = this.$modal_body.position();
      this.loader.set_css({
        top: body_position.top + padding,
        left: body_position.left + padding,
        width: this.$modal_body.width(),
        height: this.$modal_body.height()
      }).set_img_center().fade_in();
      if (disable_buttons) {
        this.$decide_button.attr('disabled', 'disabled');
        this.$cancel_button.attr('disabled', 'disabled');
      }
      return this;
    };

    modal.prototype.hide_loader = function(enable_buttons) {
      if (enable_buttons == null) {
        enable_buttons = true;
      }
      this.loader.fade_out();
      if (enable_buttons) {
        this.$decide_button.removeAttr('disabled');
        return this.$cancel_button.removeAttr('disabled');
      }
    };

    modal.prototype.clear_height_classes = function() {
      this.$container.removeClass('fixedHeightHigh');
      this.$container.removeClass('fixedHeightLow');
      return this;
    };

    modal.prototype.set_height_low_class = function() {
      this.clear_height_classes();
      this.$container.addClass('fixedHeightLow');
      return this;
    };

    modal.prototype.set_height_high_class = function() {
      this.clear_height_classes();
      this.$container.addClass('fixedHeightHigh');
      return this;
    };

    modal.prototype.prevent_multiple_submit = function($form) {
      if (!($form instanceof jQuery)) {
        $form = $($form);
      }
      return $form.one('submit', (function(_this) {
        return function() {
          $form.on('submit', function() {
            return false;
          });
          return true;
        };
      })(this));
    };

    modal.prototype.hide_on_document_clicked = function(event) {
      if ($(event.target).is('div.js-blackBack')) {
        return this.hide();
      }
    };

    modal.prototype.freeze = function() {
      App.$document.off('click', this.hide_on_document_clicked);
      this.$el.off('click', '.dlgHeader .close a');
      this.$el.off('click', '.js-modal-cancel-button');
      return this.$el.off('click', '.js-modal-decide-button');
    };

    modal.prototype.unfreeze = function() {
      this.freeze();
      App.$document.on('click', this.hide_on_document_clicked);
      this.$el.on('click', '.dlgHeader .close a', this.hide);
      this.$el.on('click', '.js-modal-cancel-button', this.cancel_button_clicked);
      return this.$el.on('click', '.js-modal-decide-button', this.decide);
    };

    modal.prototype.modal_keydown_controlle = function(event) {
      if (this.is_active()) {
        if (event.keyCode === $.ui.keyCode.ESCAPE) {
          return this.hide();
        }
      }
    };

    modal.prototype.is_active = function() {
      if (this.$container.is(':visible')) {
        return true;
      } else {
        return false;
      }
    };

    return modal;

  })(Backbone.View);

  App.elements._modal_templates = {
    container: _.template('<div id="modalDlgContainer" class="modalDlgState jsModal-Dialog" style="display: none">\n  <div class="js-blackBack"></div>\n  <div class="modalDlgWrapper"></div>\n</div>'),
    modal: _.template('<div class="dlgHeader">\n  <div class="heading">\n    <h2><%= title %></h2>\n  </div>\n  <span class="close"><a href="#"><i class="icon-xmark"></i></a></span>\n</div>\n<div class="dlgContainer"><%= body %></div>\n<% if(set_buttons){ %>\n  <div class="dlgFooter"><%= footer %></div>\n<% } %>'),
    buttons: {
      decide_only: _.template('<div class="boxButtonArea ht right">\n  <div>\n    <a class="btnM js-modal-decide-button" href="#">\n      <%= decide_button_label %>\n    </a>\n  </div>\n</div>'),
      cancel_only: _.template('<div class="boxButtonArea ht right">\n  <div>\n    <a class="btnM btnBasic js-modal-cancel-button" href="#">\n      <%= cancel_button_label %>\n    </a>\n  </div>\n</div>'),
      cancel_decide: _.template('<div class="boxButtonArea both">\n  <div>\n    <a class="btnM btnBasic js-modal-cancel-button" href="#">\n      <%= cancel_button_label %>\n    </a>\n  </div>\n  <div>\n    <a class="btnM js-modal-decide-button" href="#">\n      <%= decide_button_label %>\n    </a>\n  </div>\n</div>'),
      cancel_delete: _.template('<div class="boxButtonArea both">\n  <div>\n    <a class="btnM btnBasic js-modal-cancel-button" href="#">\n      <%= cancel_button_label %>\n    </a>\n  </div>\n  <div>\n    <a class="btnM btnNotice js-modal-decide-button" href="#">\n      <%= decide_button_label %>\n    </a>\n  </div>\n</div>'),
      cancel_attention: _.template('<div class="boxButtonArea both">\n  <div>\n    <a class="btnM btnBasic js-modal-cancel-button" href="#">\n      <%= cancel_button_label %>\n    </a>\n  </div>\n  <div>\n    <a class="btnM btnAttention js-modal-decide-button" href="#">\n      <%= decide_button_label %>\n    </a>\n  </div>\n</div>'),
      cancel_only_left: _.template('<div class="boxButtonArea ht left">\n  <div>\n    <a class="btnM btnBasic js-modal-cancel-button" href="#">\n      <%= cancel_button_label %>\n    </a>\n  </div>\n</div>'),
      cancel_notice: _.template('<div class="boxButtonArea both">\n  <div>\n    <a class="btnM btnBasic js-modal-cancel-button" href="#">\n      <%= cancel_button_label %>\n    </a>\n  </div>\n  <div>\n    <a class="btnM btnNotice js-modal-decide-button" href="#">\n      <%= decide_button_label %>\n    </a>\n  </div>\n</div>'),
      cancel_action: _.template('<div class="boxButtonArea both">\n  <div>\n    <a class="btnM btnBasic js-modal-cancel-button" href="#">\n      <%= cancel_button_label %>\n    </a>\n  </div>\n  <div>\n    <a class="btnM btnAction js-modal-decide-button" href="#">\n      <%= decide_button_label %>\n    </a>\n  </div>\n</div>')
    }
  };

  App.elements._modal = (function(_super) {
    __extends(_modal, _super);

    function _modal() {
      this.hide = __bind(this.hide, this);
      this.show = __bind(this.show, this);
      this.set_save_button_text = __bind(this.set_save_button_text, this);
      this.set_cancel_button_text = __bind(this.set_cancel_button_text, this);
      this.set_body = __bind(this.set_body, this);
      this.set_title = __bind(this.set_title, this);
      this.submit = __bind(this.submit, this);
      this.canceled = __bind(this.canceled, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return _modal.__super__.constructor.apply(this, arguments);
    }

    _modal.prototype.el = '<div class="modal hide fade" role="dialog"></div>';

    _modal.prototype.events = {
      'click .js-modal-cancel-button': 'canceled',
      'click .js-modal-close-button': 'canceled',
      'click .js-modal-save-button': 'submit'
    };

    _modal.prototype.template = _.template('<div class="modal-header">\n  <button type="button" class="close js-modal-close-button" data-dismiss="modal">×</button>\n  <h3 class="js-modal-title"><%= title %></h3>\n</div>\n<div class="modal-body js-modal-body">\n  <%= body %>\n</div>\n<div class="modal-footer">\n  <button class="btn js-modal-cancel-button" data-dismiss="modal"><%= cancel_button_text %></button>\n  <button class="btn btn-primary js-modal-save-button"><%= save_button_text %></button>\n</div>');

    _modal.prototype.$title = null;

    _modal.prototype.$body = null;

    _modal.prototype.$cancel_button = null;

    _modal.prototype.$save_button = null;

    _modal.prototype.title = '';

    _modal.prototype.body = '';

    _modal.prototype.cancel_button_text = 'キャンセル';

    _modal.prototype.save_button_text = '保存';

    _modal.prototype.initialize = function(options) {
      if (options != null) {
        if (options.title != null) {
          this.title = options.title;
        }
        if (options.body != null) {
          this.body = options.body;
        }
        if (options.cancel_button_text != null) {
          this.cancel_button_text = options.cancel_button_text;
        }
        if (options.save_button_text != null) {
          this.save_button_text = options.save_button_text;
        }
      }
      return this.render();
    };

    _modal.prototype.render = function() {
      this.$el.html(this.template({
        title: this.title,
        body: this.body,
        cancel_button_text: this.cancel_button_text,
        save_button_text: this.save_button_text
      }));
      this.$title = this.$el.find('.js-modal-title');
      this.$body = this.$el.find('.js-modal-body');
      this.$cancel_button = this.$el.find('.js-modal-cancel-button');
      this.$save_button = this.$el.find('.js-modal-save-button');
      return this.$el;
    };

    _modal.prototype.canceled = function() {
      return this.trigger('canceled');
    };

    _modal.prototype.submit = function() {
      return this.trigger('submit');
    };

    _modal.prototype.set_title = function(title) {
      this.title = title;
      return this.$title.html(this.title);
    };

    _modal.prototype.set_body = function(body) {
      this.body = body;
      return this.$body.html(this.body);
    };

    _modal.prototype.set_cancel_button_text = function(text) {
      this.cancel_button_text = text;
      return this.$cancel_button.html(text);
    };

    _modal.prototype.set_save_button_text = function(text) {
      this.save_button_text = text;
      return this.$save_button.html(text);
    };

    _modal.prototype.show = function() {
      return this.$el.modal('show');
    };

    _modal.prototype.hide = function() {
      return this.$el.modal('hide');
    };

    return _modal;

  })(Backbone.View);

  App.elements.schedule_modal = {
    templates: {},
    events: {},
    views: {}
  };

  App.elements.schedule_modal.templates["delete"] = $.extend({}, App.elements._modal_templates, {
    body: _.template('<input type="hidden" name="schedule_id" value="<%- data.schedule_id %>">\n<input type="hidden" name="user_id" value="<%- data.user_id %>">\n\n<div class="msgBanner notice"><p><span class="icon"><i class="icon-exclamation"></i></span><span class="txt">削除を実行しようとしています。注意文をご確認ください。</span></p></div>\n<% if(data.type == 1) { %>\n  この長期不在を削除します。削除すると、タイトル、不在期間など、この長期不在に関連したすべての情報が削除されます。削除された情報は、元に戻すことができません。\n<% } else { %>\n  このスケジュールを削除します。削除すると、タイトル、期間など、このスケジュールに関連したすべての情報が削除されます。削除された情報は、元に戻すことができません。\n<% } %>\n<br /><br />\nよろしいですか？ ')
  });

  App.elements.schedule_modal.templates.site = $.extend({}, App.elements._modal_templates, {
    body: _.template('<input type="hidden" name="schedule_id" value="<%- data.schedule_id %>">\n<input type="hidden" name="type" value="<%- data.type %>">\n\n<div class="boxLabeled">\n  <p class="label" id=\'js-schedule-form-title-label\'>\n     <i class="required">*</i><label for="form_title">スケジュール名</label>\n  </p>\n  <p class="content">\n    <input type="text" name="title" placeholder="スケジュール名を入力してください" value="<%- data.title %>"  id="js-schedule-form-title">\n  </p>\n</div>\n<div class="boxLabeled">\n    <p class="label" id=\'js-schedule-form-date-label\'>\n      <i class="required">*</i>日時\n    </p>\n    <div class="content">\n      <input type="text" name="start_date" rel="datepicker" class="inputField startDate hasDatepicker" placeholder="年/月/日" size="6" value="<%- data.start_date %>" id="js-schedule-form-start-date" style="position: relative; z-index: 200;">\n      <span class="delimit">～</span>\n      <input type="text" name="end_date" rel="datepicker" class="inputField endDate hasDatepicker" placeholder="年/月/日" size="6" value="<%- data.end_date %>" id="js-schedule-form-end-date" style="position: relative; z-index: 200;">\n</div>\n<div class="boxLabeled">\n    <p class="label">備考</p>\n    <p class="content">\n      <textarea rows="4" class="js-input-field" name="content" id="js-schedule-form-content"><%- data.content %></textarea>\n    </p>\n</div>')
  });

  App.elements.schedule_modal.templates.user = $.extend({}, App.elements._modal_templates, {
    container: _.template('<div id="editModalDlgContainer" class="modalDlgState jsModal-Dialog" style="display: none; z-index:200;">\n  <div class="js-blackBack"></div>\n  <div class="modalDlgWrapper"></div>\n</div>'),
    body: _.template('<input type="hidden" name="schedule_id" value="<%- data.schedule_id %>">\n<input type="hidden" name="user_id" value="<%- data.user_id %>">\n<input type="hidden" name="type" value="<%- data.type %>">\n\n<div class="boxLabeled">\n  <p class="label"><i class="required">*</i>タイトル</p>\n  <div class="content">\n    <input type="text" name="title" placeholder="<%- data.str_placeholder %>" value="<%- data.title %>">\n  </div>\n</div>\n<div class="boxLabeled">\n  <p class="label"><i class="required">*</i>不在期間</p>\n  <div class="content">\n    <input type="text" rel="datepicker" class="inputField startDate hasDatepicker" name="start_date" placeholder="年月日" size="6" value="<%- data.start_date %>" style="position: relative; z-index: 200;"> - <input type="text" rel="datepicker" class="inputField endDate hasDatepicker" name="end_date" placeholder="年月日" size="6" value="<%- data.end_date %>" style="position: relative; z-index: 200;">\n   </div>\n</div>\n<div class="boxLabeled">\n    <p class="label">備考</p>\n    <div class="content">\n      <textarea name="content" rows="4"><%- data.content %></textarea>\n    </div>\n</div>')
  });

  App.elements.schedule_modal.views.base = (function(_super) {
    __extends(base, _super);

    function base() {
      this.get_by_name = __bind(this.get_by_name, this);
      this.show = __bind(this.show, this);
      this.set_datepicker_options = __bind(this.set_datepicker_options, this);
      this.initialize = __bind(this.initialize, this);
      return base.__super__.constructor.apply(this, arguments);
    }

    base.prototype.datepicker_options = App.elements.datepicker.views.datepicker.options;

    base.prototype.initialize = function(options) {
      var modal_class, _i, _len, _ref;
      if (options != null) {
        if (options.title != null) {
          this.title = options.title;
        }
        if (options.body != null) {
          this.body = options.body;
        }
        if (options.set_buttons != null) {
          this.set_buttons = !!options.set_buttons;
        }
        if (options.cancel_button_label != null) {
          this.cancel_button_label = options.cancel_button_label;
        }
        if (options.decide_button_label != null) {
          this.decide_button_label = options.decide_button_label;
        }
        if ((options.buttons_type != null) && (options.buttons_type === 'cancel_only' || options.buttons_type === 'cancel_decide' || options.buttons_type === 'cancel_delete')) {
          this.buttons_type = options.buttons_type;
        }
        if (options.modal_classes != null) {
          this.$el.removeClass('dialog');
          _ref = options.modal_classes;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            modal_class = _ref[_i];
            this.$el.addClass(modal_class);
          }
        }
      }
      this.$container = $(this.templates.container());
      this.$container_modal_wrapper = this.$container.find('.modalDlgWrapper');
      this.$container_modal_wrapper.append(this.$el);
      App.$body.append(this.$container);
      this.set_datepicker_options((options != null ? options.config : void 0) != null ? options != null ? options.config : void 0 : {});
      this.$container.find('.js-blackBack').click((function(_this) {
        return function(ev) {
          return _this.hide();
        };
      })(this));
      return this.render();
    };

    base.prototype.set_datepicker_options = function(options) {
      return _.each(options, (function(_this) {
        return function(val, key) {
          return _this.datepicker_options[key] = val;
        };
      })(this));
    };

    base.prototype.show = function(data) {
      if (data.user_id) {
        this.str_placeholder = '登録した長期不在は、本人と管理ユーザーのみ確認することができます。';
      } else {
        this.str_placeholder = '登録した長期不在は、あなたと管理ユーザーのみ確認することができます。';
      }
      this.set_body(this.templates.body({
        data: {
          schedule_id: (data != null ? data.schedule_id : void 0) != null ? data.schedule_id : null,
          user_id: (data != null ? data.user_id : void 0) != null ? data.user_id : null,
          title: (data != null ? data.title : void 0) != null ? data.title : null,
          type: (data != null ? data.type : void 0) != null ? data.type : null,
          start_date: (data != null ? data.start_date : void 0) != null ? data.start_date : null,
          end_date: (data != null ? data.end_date : void 0) != null ? data.end_date : null,
          content: (data != null ? data.content : void 0) != null ? data.content : null,
          str_placeholder: this.str_placeholder
        }
      }));
      this.$container.find('[rel="datepicker"]').removeClass('hasDatepicker').removeData('datepicker').unbind().datepicker(this.datepicker_options);
      return base.__super__.show.apply(this, arguments);
    };

    base.prototype.get_by_name = function(name) {
      var _ref;
      if ((_ref = this.$container.find("[name='" + name + "']")) != null ? _ref.val() : void 0) {
        return this.$container.find("[name='" + name + "']").val();
      } else {
        return '';
      }
    };

    return base;

  })(App.elements.modal);

  App.elements.schedule_modal.views["delete"] = (function(_super) {
    __extends(_delete, _super);

    function _delete() {
      this.initialize = __bind(this.initialize, this);
      return _delete.__super__.constructor.apply(this, arguments);
    }

    _delete.prototype.templates = App.elements.schedule_modal.templates["delete"];

    _delete.prototype.initialize = function(options) {
      options = $.extend({}, {
        title: '長期不在(削除)',
        body: '<div class="msgBanner notice"><p><span class="icon"><i class="icon-exclamation"></i></span><span class="txt">削除を実行しようとしています。注意文をご確認ください。</span></p></div>この長期不在を削除します。削除すると、タイトル、不在期間など、この長期不在に関連したすべての情報が削除されます。削除された情報は、元に戻すことができません。<br /><br />よろしいですか？',
        decide_button_label: '削除する(元に戻せません)',
        buttons_type: 'cancel_delete'
      }, options);
      return _delete.__super__.initialize.call(this, options);
    };

    return _delete;

  })(App.elements.schedule_modal.views.base);

  App.elements.schedule_modal.views.site = (function(_super) {
    __extends(site, _super);

    function site() {
      this.initialize = __bind(this.initialize, this);
      return site.__super__.constructor.apply(this, arguments);
    }

    site.prototype.templates = App.elements.schedule_modal.templates.site;

    site.prototype.initialize = function(options) {
      options = $.extend({}, {
        title: '公開or非公開スケジュールの登録',
        decide_button_label: '登録する',
        buttons_type: 'cancel_decide'
      }, options);
      return site.__super__.initialize.call(this, options);
    };

    return site;

  })(App.elements.schedule_modal.views.base);

  App.elements.schedule_modal.views.user = (function(_super) {
    __extends(user, _super);

    function user() {
      this.initialize = __bind(this.initialize, this);
      return user.__super__.constructor.apply(this, arguments);
    }

    user.prototype.templates = App.elements.schedule_modal.templates.user;

    user.prototype.initialize = function(options) {
      options = $.extend({}, {
        title: '長期不在の登録',
        decide_button_label: '登録する',
        cancel_button_label: '取り消す',
        buttons_type: 'cancel_decide'
      }, options);
      return user.__super__.initialize.call(this, options);
    };

    return user;

  })(App.elements.schedule_modal.views.base);

  App.elements.notification = {
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.elements.notification.models.notification = (function(_super) {
    __extends(notification, _super);

    function notification() {
      return notification.__super__.constructor.apply(this, arguments);
    }

    notification.prototype.urlRoot = App.base_url + '/api/notification/info';

    notification.prototype.defaults = {
      id: null,
      type: 0,
      user_name: '',
      message: '',
      link: '',
      is_read: false
    };

    return notification;

  })(Backbone.Model);

  App.elements.notification.collections.notifications = (function(_super) {
    __extends(notifications, _super);

    function notifications() {
      return notifications.__super__.constructor.apply(this, arguments);
    }

    notifications.prototype.url = App.base_url + '/api/notification/last';

    notifications.prototype.model = App.elements.notification.models.notification;

    return notifications;

  })(Backbone.Collection);

  App.elements.notification.views.notification = (function(_super) {
    __extends(notification, _super);

    function notification() {
      this.clicked = __bind(this.clicked, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return notification.__super__.constructor.apply(this, arguments);
    }

    notification.prototype.el = '<li></li>';

    notification.prototype.model = null;

    notification.prototype.events = {
      'click': 'clicked'
    };

    notification.prototype.templates = {
      link: _.template('<a href="<%= model.get(\'link\') %>">\n  <% if(!model.get(\'is_read\')){ %><span class="label label-important">未</span><% } %>\n  <%= model.get(\'message\') %>\n</a>')
    };

    notification.prototype.initialize = function() {
      return this.model.on('change', this.render);
    };

    notification.prototype.render = function() {
      this.$el.html(this.templates.link({
        model: this.model
      }));
      return this.$el;
    };

    notification.prototype.clicked = function() {
      this.model.save({
        is_read: true
      });
      return true;
    };

    return notification;

  })(Backbone.View);

  App.elements.notification.views.notifications = (function(_super) {
    __extends(notifications, _super);

    function notifications() {
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return notifications.__super__.constructor.apply(this, arguments);
    }

    notifications.prototype.el = '#notifications-container';

    notifications.prototype.collection = null;

    notifications.prototype.$label = null;

    notifications.prototype.$ul = null;

    notifications.prototype.$count = null;

    notifications.prototype.initialize = function() {
      this.$label = this.$el.find('.js-notifications-label');
      this.$ul = this.$el.find('.js-notifications-list');
      this.$count = this.$el.find('.js-notifications-count');
      this.collection = new App.elements.notification.collections.notifications();
      return this.collection.fetch({
        success: (function(_this) {
          return function() {
            return _this.render();
          };
        })(this)
      });
    };

    notifications.prototype.render = function() {
      var method, unread_count;
      this.$ul.empty();
      unread_count = 0;
      this.collection.forEach((function(_this) {
        return function(notification, index) {
          var notification_view;
          notification_view = new App.elements.notification.views.notification({
            model: notification
          });
          _this.$ul.append(notification_view.render());
          if (!notification.get('is_read')) {
            return unread_count++;
          }
        };
      })(this));
      method = unread_count > 0 ? 'addClass' : 'removeClass';
      this.$label[method]('label-important');
      return this.$count.text(unread_count);
    };

    return notifications;

  })(Backbone.View);

  App.elements.timeline = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.elements.timeline.templates.no_feed_message = _.template('<li class="js-timeline-no-feed-message">\n  <div class="boxBlankMsg">\n    <div class="msgBanner notice">\n      <p>\n        <span class="icon">\n          <i class="icon-exclamation"></i>\n        </span>\n        <span class="txt">\n          <span class="val">まだ1件も投稿がありません</span>\n        </span>\n      </p>\n    </div>\n  </div>\n</li>');

  App.elements.timeline.templates.profile = _.template('<div class="profile">\n  <% if ((data.user_data != null) && (data.user_data.id != null) && (data.user_data.icon != null) && (data.user_data.name != null)) { %>\n  <a href="<%= data.base_url %>/user/<%= data.user_data.id %>">\n    <img class="thumb" src="<%= data.user_data.icon_x50 %>" alt="<%- data.user_data.name %>">\n  </a>\n  <% //システムフィードではユーザのプロフィール写真はださない  %>\n  <% //TODO ここらへのif文が非常にカオスっているのでどうにかする。テンプレートもっとわけた方がよさそう  %>\n  <% }else if(data.model.get(\'user_id\') && data.model.get(\'type\') != 3){ %>\n  <a href="<%= data.base_url %>/user/<%= data.model.get(\'user_id\') %>">\n    <img class="thumb" src="<%= data.model.get(\'user_icon\') %>" alt="<%- data.model.get(\'user_name\') %>">\n  </a>\n  <% //誕生日のとき %>\n  <% }else if(data.model.get(\'feed_system_type_id\') == 1){ %>\n    <img class="thumb" src="/assets/img/theme/common/birthday_x60.png" alt="">\n  <% }else{ %>\n    <img class="thumb" src="/assets/img/theme/common/system_x60.png" alt="">\n  <% } %>\n</div>', void 0, {
    variable: 'data'
  });

  App.elements.timeline.templates.legacy_input = _.template('<li>\n  <div class="content">\n    <p class="content">\n      <span class="linkAdd">\n        <i class="icon-add"></i>\n        <a href="#" class="js-timeline-post-input-legacy-input-add-thumb-link">ファイルを追加する</a>\n      </span>\n    </p>\n  </div>\n</li>');

  App.elements.timeline.templates.input_image_thumbnail = {
    item: _.template('<a class="uiAnchor js-file-input-image-thumbnail-edit-caption-anchor" href="#">\n  <span class="js-thumbarea js-file-input-container-thumbnail-area"></span>\n  <i class="icon-setting"></i>\n</a>')
  };

  App.elements.timeline.templates.input_file_thumbnail = {
    item: _.template('<p class="figure">\n  <span class="js-thumbarea js-file-input-container-thumbnail-area"></span>\n</p>\n<div class="content">\n  <p class="name">\n    <%= name %>\n    <span class="size">(<%= size %>)</span>\n  </p>\n  <p class="desc">\n    <input type="text" name="caption" placeholder="概要を入力する" value="<%= caption %>">\n  </p>\n  <p class="delete">\n    <i class="icon-delete"></i>\n    <a class="deleteButton js-timeline-input-file-thumbnail-delete-button" href="#">ファイルを削除する</a>\n  </p>\n</div>')
  };

  App.elements.timeline.templates.input_legacy_thumbnail = {
    item: _.template('<div class="content">\n  <p class="name js-timeline-input-file-thumbnail-legacy-file-input-container">\n    <a href="#" class="js-timeline-input-file-thumbnail-delete-button">\n      <i class="icon-xmark"></i>\n    </a>\n  </p>\n  <p class="desc">\n    <input type="text" name="caption" placeholder="ファイルの概要文を入力してください" value="<%= caption %>">\n  </p>\n</div>'),
    posted: _.template('<div class="content">\n  <p class="name js-timeline-input-file-thumbnail-legacy-file-input-container">\n    <a href="<%= model.get(\'file\').get(\'path\') %>" target="_blank">\n      <%= model.get(\'file\').get(\'name\') %>\n    </a>\n  </p>\n  <p class="desc">\n    <input type="text" name="caption" placeholder="ファイルの概要文を入力してください" value="<%= caption %>">\n  </p>\n</div>')
  };

  App.elements.timeline.templates.edit_feed = {
    wysiwyg: _.template('<form class="js-timeline-feed-edit-wysiwyg-input-form">\n  <div class="editorWysiwyg">\n    <textarea></textarea>\n  </div>\n  <button class="post-button js-postBtn" type="submit">編集完了</button>\n</form>'),
    textarea: _.template('<form class="js-timeline-feed-edit-text-input-form">\n  <div class="editContent">\n    <textarea class="inputField" placefolder="写真のタイトル"></textarea>\n    <button class="post-button js-postBtn" type="submit">編集完了</button>\n  </div>\n</form>'),
    text: _.template('<form class="js-timeline-feed-edit-text-input-form">\n  <div class="editContent">\n    <input class="inputField" type="text" value="写真のタイトル">\n    <button class="post-button js-postBtn" type="submit">編集完了</button>\n  </div>\n</form>')
  };

  App.elements.timeline.templates.feed_modals = {
    delete_body: _.template('<div class="msgBanner notice"><p><span class="icon"><i class="icon-exclamation"></i></span><span class="txt">削除を実行しようとしています。注意文をご確認ください。</span></p></div>\nこの投稿を削除します。削除すると、この投稿へのコメント、スター、写真、ファイルが削除されます。削除された情報は元に戻すことができません。<br />\n<br />\nよろしいですか？'),
    delete_communication_body: _.template('<div class="msgBanner notice"><p><span class="icon"><i class="icon-exclamation"></i></span><span class="txt">削除を実行しようとしています。注意文をご確認ください。</span></p></div>\n自己紹介のための質問を削除します。削除すると、回答した内容や、この投稿へのコメント、スター、写真、ファイルが削除されます。削除された情報は元に戻すことができません。<br />\n<br />\nよろしいですか？')
  };

  App.elements.timeline.templates.feed_image_modals = {
    delete_body: _.template('<span class="warning">※コメントも削除されてしまいます。</span>\n<br>\nこの動作はもとに戻せませんが本当によろしいですか？')
  };

  App.elements.timeline.templates.feed_image = {
    title: _.template('<p class="title textContent"><%= data.title %></p>', void 0, {
      variable: 'data'
    }),
    item: _.template('<div class="boxPhotoItem styleBorder">\n  <div class="photoWrapper">\n    <p class="photo js-timeline-feed-images-feed-image-image-container">\n      <% if (data.is_smartphone) { %>\n        <img src="<%= data.model.get(\'file\').get(\'path\') %>/thumb/588/588"\n          alt="<%= _.escape(data.model.get(\'file\').get(\'caption\')) %>"\n          class="js-timeline-feed-images-feed-image-image"\n          >\n      <% } else { %>\n      <a class="photoLink js-timeline-feed-images-feed-image-link" href="#">\n        <img src="<%= data.model.get(\'file\').get(\'path\') %>/thumb/588/588"\n          alt="<%= _.escape(data.model.get(\'file\').get(\'caption\')) %>"\n          class="js-timeline-feed-images-feed-image-image"\n          >\n      </a>\n      <% } %>\n    </p>\n  </div>\n  <div class="footer">\n    <div class="star js-timeline-feed-images-feed-image-star-button-container"></div>\n    <div class="comment js-timeline-feed-images-feed-image-comment-button-container"></div>\n  </div>\n</div>', void 0, {
      variable: 'data'
    })
  };

  App.elements.timeline.templates.feed = {
    container: _.template('<div class="boxFeedArticle feedContent js-timeline-feed-main-container<% if( data.model.get(\'is_edited\') ){ %> edited<% } %>">\n  <%= this.profile(data) %>\n  <div class="content">\n    <% if(data.feed_header && typeof(data.feed_header) === \'function\'){ %>\n      <%= data.feed_header(data) %>\n    <% } %>\n    <div class="boxBalloon">\n      <div class="feedSection<% if(\'section_class_name\' in data && data.section_class_name){ %> <%= data.section_class_name %><% } %>">\n        <% if(data.show_summary == true){ %>\n          <% if(data.model.is_system_feed() && data.model.get(\'community\') == null && data.model.get(\'event\') == null ){ %>\n          <div class="summary">\n            <div class="figure">\n            <% if ((data.user_data != null) && (data.user_data.id != null) && (data.user_data.icon != null) && (data.user_data.name != null)) { %>\n            <a href="<%= data.base_url %>/user/<%= data.user_data.id %>">\n              <img class="thumb" src="<%= data.user_data.icon_x50 %>" alt="<%- data.user_data.name %>">\n            </a>\n            <% }else if(data.model.get(\'user_id\')){ %>\n              <img class="thumb" src="<%= data.model.get(\'user_icon\') %>" alt="<%- data.model.get(\'user_name\') %>">\n            <% } %>\n            </div>\n          <div class="content middle">\n          <% } %>\n          <%= this.header(data) %>\n          <% if(data.model.is_system_feed() && data.model.get(\'community\') == null && data.model.get(\'event\') == null ){ %>\n            </div>\n          </div>\n          <% } %>\n        <% } %>\n        <% if((\'body_template\' in data) && typeof(data.body_template) === \'function\'){ %>\n          <%= data.body_template(data) %>\n        <% }else{ %>\n          <%= this.body(data) %>\n        <% } %>\n      </div>\n      <%= this.link_more(data) %>\n      <%= this.footer(data) %>\n    </div>\n  </div>\n</div>', void 0, {
      variable: 'data'
    }),
    profile: App.elements.timeline.templates.profile,
    header: _.template('<p class="header">\n  <% //コミュニティ、イベントのデータがあり、退会のメッセージでない場合ヘッダーに表示  %>\n  <% if(data.show_related && data.model.get(\'feed_system_type_id\') != 4){ %>\n    <% if(data.model.get(\'area\') == 1 && data.model.get(\'community\') != null){ %>\n      <a href="<%= data.base_url %>/community/show/<%= data.model.get(\'community\').id %>">\n        <%= data.model.get(\'community\').name %>\n      </a>\n      <% //非公開状態のコミュニティには非公開マーク追加 %>\n      <% if(!data.model.get(\'community\').is_public){ %><i class="icon-keyLock"></i><% } %>\n    <% }else if(data.model.get(\'area\') == 2 && data.model.get(\'event\') != null){ %>\n      <a href="<%= data.base_url %>/event/show/<%= data.model.get(\'event\').id %>">\n        <%= data.model.get(\'event\').name %>\n      </a>\n      <% //非公開状態のイベントには非公開マーク追加 %>\n      <% if(!data.model.get(\'event\').is_public){ %><i class="icon-keyLock"></i><% } %>\n    <% } %>\n    <% //コミュニティ、イベントのフィードの場合でシステムフィードでない場合は＞＞を追加  %>\n    <% if(!data.model.is_system_feed() && (data.model.get(\'community\') || data.model.get(\'event\'))){ %>&gt;&gt;<% } %>\n  <% } %>\n  <% if(data.section_class_name == \'question\'){ %>\n      Q. <%= data.model.get(\'title\') %>\n  <% }else{ %>\n    <% if(data.model.get(\'feed_system_type_id\') == 1){ %>\n      <%= data.model.get(\'birthday\') %>は\n    <% } %>\n    <% //コミュニティ、イベントへのフィードで、システムフィードの場合は出さない %>\n    <% if((data.model.get(\'area\') != 1 && data.model.get(\'area\') != 2) || !data.model.is_system_feed()){ %>\n      <a href="<%= data.base_url %>/user/<%= data.model.get(\'user_id\') %>">\n        <%- data.model.get(\'user_name\') %>\n      </a>\n    <% } %>\n    <% if(data.model.get(\'title\')){ %>\n     さんが「<%= data.model.get(\'title\') %>」に回答しました。\n    <% }else if(data.model.get(\'feed_system_type_id\') == 0 && data.model.is_system_feed()){ %>\n     さんが<%= data.site_name %>に参加しました。\n    <% }else if(data.model.get(\'feed_system_type_id\') == 1){ %>\n     さんの誕生日です。\n    <% } %>\n  <% } %>\n</p>', void 0, {
      variable: 'data'
    }),
    body: _.template('<div class="postModule js-timeline-feed-body-container">\n  <div class="wysiwygContent js-timeline-feed-body-html-container">\n    <%= data.body_content %>\n  </div>\n</div>', void 0, {
      variable: 'data'
    }),
    link_more: _.template('<% _.each(data.linkMores, function(linkMore){ %>\n  <p class="linkMore"><a href="<%= linkMore.url %>"><%= linkMore.anchor %></a></p>\n<% }); %>', void 0, {
      variable: 'data'
    }),
    footer: _.template('<div class="boxFeedFooter">\n\n  <% // 退会コミュニティ・イベントの場合は非表示 %>\n  <% if(data.model.get(\'feed_system_type_id\') != 4 && data.model.get(\'feed_system_type_id\') != 6 ){ %>\n  <div class="action">\n    <div class="star js-timeline-feed-star-button-container"></div>\n    <div class="comment js-timeline-feed-comment-button-container"></div>\n  </div>\n  <% } %><% // 退会コミュニティ・イベントの場合は非表示 %>\n\n  <div class="data js-timeline-feed-footer-data-container">\n    <div class="timeStamp js-timeline-feed-footer-timestamp-container">\n      <%= this.footer_timestamp(data) %>\n    </div>\n  </div>\n</div>', void 0, {
      variable: 'data'
    }),
    footer_timestamp: _.template('<% // [PCサイトforスマホ]スマホからのアクセスだったら、リンク先URLにパラメータとして表示中ページのURLを追加。スマホ専用サイトが用意されたらifの方のコードを消して下さい。 %>\n<% if (data.is_smartphone) { %>\n<a class="uiAnchor " href="<%= data.base_url %>/feed/show/<%= data.model.get(\'id\') %>?home_url=<%= location.href.split(data.base_url+\'/\')[1] %>">\n<% } else { %>\n<a class="uiAnchor " href="<%= data.base_url %>/feed/show/<%= data.model.get(\'id\') %><% if(data.base_page){ %>#<%= data.base_page %><% } %>">\n<% } %>\n  <span class="txt"><%= data.model.get(\'created_at\') %></span> <i class="icon-edited" title="コメント編集済み"></i>\n</a>', void 0, {
      variable: 'data'
    })
  };

  App.elements.timeline.templates.star_balloon = {
    users: _.template('<div class="blnMember leftTale">\n  <div class="content">\n    <ul>\n      <% data.collection.each(function(star){ %>\n      <li>\n        <a href="<%= data.base_url %>/user/<%= star.get(\'user_id\') %>">\n          <span class="profile">\n            <img class="thumb" src="<%= star.get(\'user_icon\') %>" alt="<%= star.get(\'user_name\') %>">\n          </span>\n          <span class="text"><em><%= star.get(\'user_name\') %></em></span>\n        </a>\n      </li>\n      <% }) %>\n    </ul>\n  </div>\n</div>', void 0, {
      variable: 'data'
    })
  };

  App.elements.timeline.templates.star = _.template('<% if(can_star){ %><a class="uiAnchor js-timeline-feed-star-button" href="#" title="スターをつける"><% } %>\n<i class="icon-star starIcon"></i>\n<% if(can_star){ %></a><% } %>\n<% if(stars.length > 0){ %>\n  <a class="uiAnchor js-timeline-feed-star-stared-count-link" href="#">\n    <span class="txt"><%= stars.length %></span>\n  </a>\n<% } %>');

  App.elements.timeline.templates.comment_button = _.template('<% if(can_comment){ %><a class="uiAnchor" href="#" title="クリックでコメントを投稿"><% } %>\n<i class="icon-comment"></i><% if(comments.length > 0 ){ %><span class="txt"><%= comments.length %></span><% } %>\n<% if(can_comment){ %></a><% } %>');

  App.elements.timeline.templates.comment_modals = {
    delete_body: _.template('この動作はもとに戻せませんがよろしいですか？')
  };

  App.elements.timeline.templates.comment = {
    container: _.template('<div class="boxFeedComment">\n  <%= this.profile(data) %>\n  <div class="content">\n    <div class="boxBalloon">\n      <div class="feedSection">\n        <%= this.header(data) %>\n        <%= this.body(data) %>\n      </div>\n      <%= this.footer(data) %>\n    </div>\n  </div>\n</div>', void 0, {
      variable: 'data'
    }),
    profile: App.elements.timeline.templates.profile,
    header: _.template('<p class="header">\n  <a href="<%= data.base_url %>/user/<%= data.model.get(\'user_id\') %>">\n    <%- data.model.get(\'user_name\') %>\n  </a>\n</p>', void 0, {
      variable: 'data'
    }),
    body: _.template('<div class="postModule">\n  <div class="wysiwygContent js-timeline-feed-comment-body-container">\n    <%= data.model.get(\'body\') %>\n  </div>\n</div>', void 0, {
      variable: 'data'
    }),
    footer: _.template('<div class="boxFeedFooter">\n  <div class="action">\n    <div class="star js-timeline-feed-comment-star-button-container"></div>\n  </div>\n  <div class="data js-timeline-feed-comment-footer-data-container">\n    <div class="timeStamp">\n      <% // [PCサイトforスマホ]スマホからのアクセスだったら、リンク先URLにパラメータとして表示中ページのURLを追加。スマホ専用サイトが用意されたらifの方のコードを消して下さい。 %>\n      <% if (data.is_smartphone) { %>\n      <a class="uiAnchor" href="<%= data.base_url %>/feed/show/<%= data.model.get(\'feed_id\') %>?home_url=<%= location.href.split(data.base_url+\'/\')[1] %>">\n      <% } else { %>\n      <a class="uiAnchor" href="<%= data.base_url %>/feed/show/<%= data.model.get(\'feed_id\') %><% if(data.parent_feed_base_page){ %>#<%= data.parent_feed_base_page %><% } %>">\n      <% } %>\n        <span class="txt"><%= data.model.get(\'commented_at\') %></span> <i class="icon-edited" title="コメント編集済み"></i>\n      </a>\n    </div>\n  </div>\n</div>', void 0, {
      variable: 'data'
    })
  };

  App.elements.timeline.templates.comments = {
    list_container: _.template('<ul class="commentList"></ul>'),
    input: _.template('<div class="boxFeedComment post">\n  <% if (data.is_smartphone == false) { %>\n  <%= this.profile(data) %>\n  <div class="content">\n    <div class="boxBalloon">\n      <div class="postModule">\n        <div class="wysiwygContent js-timeline-feed-comments-dummy-comment-input-container" style="display: block;">\n          <input class="editorDefault js-timeline-feed-comments-dummy-comment-input" type="text" placeholder="コメントを入力する">\n        </div>\n        <div class="editorWysiwyg js-timeline-feed-comments-new-comment-textarea-container" style="display: none;">\n          <form>\n            <textarea class="js-timeline-feed-comments-new-comment-textarea"></textarea>\n            <button class="btn post-button js-timeline-feed-comments-post-button" type="button">投稿する</button>\n          </form>\n        </div>\n      </div>\n    </div>\n  </div>\n  <% } else { %>\n  <div class="content">\n    <div class="boxButtonArea ht right">\n      <p><a href="<%= [data.base_url, \'feed/show\', data.model.id].join("/") + \'?\' + data.home_url %>" class="btn btnM">コメントを入力する</a></p>\n    </div>\n  </div>\n  <% } %>\n</div>', void 0, {
      variable: 'data'
    }),
    profile: App.elements.timeline.templates.profile
  };

  App.elements.timeline.templates.show_all = _.template('<a class="uiAnchor js-timeline-feed-show-all-link" href="#">\n  <i class="icon-comment"></i><span class="txt">他<span class="more-comments-count"><%= count %></span>件のコメントを表示</span>\n</a>');

  App.elements.timeline.templates.feed_image_modal = {
    container: _.template('<div class="content">\n  <%= this.photo_container(data) %>\n  <%= this.feed_container(data) %>\n</div>', void 0, {
      variable: 'data'
    }),
    photo_container: _.template('<div class="boxPhotoItem">\n  <div class="photosWapper">\n    <p class="photo">\n      <img src="<%= data.model.get(\'file\').get(\'path\') %>/thumb/588/588" alt="">\n    </p>\n  </div>\n  <% if(data.has_multiple){ %>\n  <p class="prev"><a href="#"><i class="icon-arrowLeft"></i></a></p>\n  <p class="next"><a href="#"><i class="icon-arrowRight"></i></a></p>\n  <% } %>\n</div>', void 0, {
      variable: 'data'
    }),
    feed_container: _.template('<div class="boxPhotoFeed">\n  <div class="photoFeedContent">\n    <%= this.feed(data) %>\n    <%= this.comments(data) %>\n  </div>\n</div>', void 0, {
      variable: 'data'
    }),
    feed: _.template('<div class="boxFeedArticle feedContent js-timeline-image-feed-feed-image-modal-feed-article-content <% if( data.model.get(\'is_edited\') ){ %> edited<% } %>">\n  <%= this.profile(data) %>\n  <div class="content">\n    <div class="feedSection">\n      <p class="header">\n        <a href="<%= data.base_url %>/user/<%= data.model.get(\'user_id\') %>">\n          <%= _.escape(data.model.get(\'user_name\')) %>\n        </a>\n        <% if(data.show_related_icons){ %>\n          <% if(data.model.get(\'is_area_community\') && data.model.get(\'community_name\')){ %>\n            &gt;&gt;\n            <a href="<%= data.model.get(\'community_url\') %>">\n              <i title="<%= data.model.get(\'community_name\') %>" class="icon-community"></i>\n            </a>\n          <% } %>\n          <% if(data.model.get(\'is_area_event\') && data.model.get(\'event_name\')){ %>\n            &gt;&gt;\n            <a href="<%= data.model.get(\'event_url\') %>">\n              <i title="<%= data.model.get(\'event_name\') %>" class="icon-event"></i>\n            </a>\n          <% } %>\n        <% } %>\n      </p>\n    </div>\n    <div class="boxFeedFooter">\n      <div class="action">\n        <div class="star js-timeline-image-feed-feed-image-modal-feed-article-star"></div>\n        <div class="comment js-timeline-image-feed-feed-image-modal-feed-article-comment-button"></div>\n      </div>\n      <div class="data js-timeline-image-feed-feed-image-modal-feed-article-data-container">\n        <div class="timeStamp">\n          <span class="txt"><%= data.model.get(\'created_at\') %></span> <i class="icon-edited" title="コメント編集済み"></i>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>', void 0, {
      variable: 'data'
    }),
    comments: _.template('<div class="commentWrapper js-timeline-image-feed-feed-image-modal-comments-wrapper">\n  <div class="commentContainer js-timeline-image-feed-feed-image-modal-comments-container"></div>\n</div>', void 0, {
      variable: 'data'
    }),
    profile: App.elements.timeline.templates.profile
  };

  App.elements.timeline.templates.file_feed = {
    body: _.template('<div class="postModule js-timeline-feed-body-html-container">\n  <%= data.body_content %>\n</div>', void 0, {
      variable: 'data'
    }),
    title: _.template('<p class="title js-timeline-feed-file-feed-body-title">\n  <%= data.title %>\n</p>', void 0, {
      variable: 'data'
    }),
    item: _.template('<li class="listItem<% if(data.model.get(\'is_edited\', false)){ %> edited<% } %>">\n  <p class="figure">\n    <img class="thumb" src="<%= data.model.get(\'file_thumbnail_path\') %>" alt="<%= data.model.get(\'file\').get(\'name\') %>">\n  </p>\n  <div class="content">\n    <p class="name">\n      <a href="<%= data.model.get(\'file\').get(\'path\') %>" target="_blank">\n        <%= data.model.get(\'file\').get(\'name\') %>\n      </a>\n      <span class="size">\n        (<%= data.file_size %>)\n      </span>\n    </p>\n    <div class="desc">\n      <div class="feedFileText text">\n        <p class="textContent js-timeline-feed-file-item-caption-container">\n          <%= data.model.get(\'file\').get(\'caption\') %>\n        </p>\n      </div>\n      <div class="data">\n        <i class="icon-edited" title="コメント編集済み"></i>\n        <div class="setting"></div>\n      </div>\n    </div>\n  </div>\n</li>', void 0, {
      variable: 'data'
    }),
    caption_input: _.template('<div class="editContent">\n  <input class="inputField" type="text" value="<%= data.model.get(\'file\').get(\'caption\') %>">\n  <button class="post-button js-timeline-feed-file-item-caption-edit-button" type="button">編集完了</button>\n</div>', void 0, {
      variable: 'data'
    })
  };

  App.elements.timeline.templates.communication_question_feed = {
    body: _.template('<% _.each(data.body_content, function(answer){ %>\n <div class="boxVoteItem" data-questionnaire-item-id="1">\n  <div class="label">\n    <p class="bar">\n      <span class="text"><%= answer.choice %></span>\n      <span class="rate" style="width: <%= answer.bar_percent %>%;"></span>\n    </p>\n  </div>\n  <p class="voter">\n  <% _.each(answer.selected_users, function(user){ %>\n    <img class="thumb" src="<%= user.user_icon %>" alt="<%= user.last_name %> <%= user.first_name %>">\n  <% }); %>\n    <span class="omitWapper" style="position: relative;">\n     <% if(answer.other_count > 0){ %>\n     <span class="omitBox">+<%= answer.other_count %></span>\n     <% } %>\n    </span>\n  </p>\n </div>\n<% }); %>', void 0, {
      variable: 'data'
    }),
    feed_header: _.template('<div class="feedHeader">\n  <p class="heading">自己紹介のための質問に回答がありました。</p>\n</div>\n<hr>', void 0, {
      variable: 'data'
    })
  };

  App.elements.timeline.templates.system_feed = {
    container: _.template('<% //コミュニティのフィードの時  %>\n<% if(data.model.get(\'area\') == 1 && data.model.get(\'community\') != null){ %>\n<div class="summary">\n  <div class="figure">\n    <% if(data.model.get(\'community\').thumbnail_path){ %>\n      <img class="thumb" src="<%= data.model.get(\'community\').thumbnail_path %>" alt="">\n    <% }else{ %>\n      <span class="coverDef x140" data-firstletter="<%= data.model.get(\'community\').first_letter %>"></span>\n    <% } %>\n    <% if(data.model.get(\'community\').is_pickup){ %><span class="icon"><img src="/assets/img/theme/icon/badge_hot.png" alt=""></span><% } %>\n  </div>\n  <div class="content">\n   <% if(data.model.get(\'feed_system_type_id\') == 2){ %>\n     <a href="<%= data.base_url %>/user/<%= data.model.get(\'user_id\') %>"><%- data.model.get(\'user_name\') %></a> さんがコミュニティを登録しました。\n   <% //複数人の参加用のフィードの場合はDBから取りに行く %>\n   <% }else if(data.model.get(\'feed_system_type_id\') == 5){ %>\n      <%= this.body_content %>\n   <% //一人の参加用のフィードの場合 %>\n   <% }else{ %>\n     <a href="<%= data.base_url %>/user/<%= data.model.get(\'user_id\') %>"><%- data.model.get(\'user_name\') %></a> さんがコミュニティメンバーになりました。\n   <% } %>\n   </div>\n</div>\n<% //イベントのフィードの時  %>\n<% }else if(data.model.get(\'area\') == 2 && data.model.get(\'event\') != null){ %>\n<div class="summary">\n   <div class="figure">\n     <% if(data.model.get(\'event\').thumbnail_path){ %>\n       <img class="thumb" src="<%= data.model.get(\'event\').thumbnail_path %>" alt="">\n     <% }else{ %>\n       <span class="coverDef x140" data-firstletter="<%= data.model.get(\'event\').first_letter %>"></span>\n     <% } %>\n     <% if(data.model.get(\'event\').is_pickup){ %><span class="icon"><img src="/assets/img/theme/icon/badge_hot.png" alt=""></span><% } %>\n   </div>\n   <div class="content">\n   <% if(data.model.get(\'feed_system_type_id\') == 2){ %>\n     <a href="<%= data.base_url %>/user/<%= data.model.get(\'user_id\') %>"><%- data.model.get(\'user_name\') %></a> さんがイベントを登録しました。\n   <% //複数人の参加用のフィードの場合はDBから取りに行く %>\n   <% }else if(data.model.get(\'feed_system_type_id\') == 5){ %>\n      <%= this.body_content %>\n   <% //一人の参加用のフィードの場合 %>\n   <% }else{ %>\n     <a href="<%= data.base_url %>/user/<%= data.model.get(\'user_id\') %>"><%- data.model.get(\'user_name\') %></a> さんがイベントメンバーになりました。\n   <% } %>\n  </div>\n</div>\n<% } %>', void 0, {
      variable: 'data'
    }),
    remove_container: _.template('<div class="feedHeader">\n  <p class="heading">\n    <% //退会ユーザーが一人の場合 %>\n    <% if(data.model.get(\'feed_system_type_id\') == 4 ){ %>\n      <a href="<%= data.base_url %>/user/<%= data.model.get(\'user_id\') %>"><%- data.model.get(\'user_name\') %></a>\n      <% if(data.model.get(\'area\') == 1 && data.model.get(\'community\') != null){ %>\n        さんが<% if(data.show_related && data.model.get(\'community\').id){ %>  <a href="<%= data.base_url %>/community/show/<%= data.model.get(\'community\').id %>"><%= data.model.get(\'community\').name %></a><% } else { %>コミュニティ<% } %>を退会しました。\n      <% }else if(data.model.get(\'area\') == 2 && data.model.get(\'event\') != null){ %>\n        さんが<% if(data.show_related && data.model.get(\'event\').id){ %>  <a href="<%= data.base_url %>/event/show/<%= data.model.get(\'event\').id %>"><%= data.model.get(\'event\').name %></a><% } else { %>イベント<% } %>を退会しました。\n      <% } %>\n    <% //退会ユーザーが複数人の場合 %>\n    <% }else if(data.model.get(\'feed_system_type_id\') == 6){ %>\n      <%= this.body_content %>\n    <% } %>\n  </p>\n</div>\n<hr>', void 0, {
      variable: 'data'
    })
  };

  App.elements.timeline.templates.input_file = _.template('<input class="js-timeline-input-image-input-file js-file-input-container-input-area-input"\n  type="file" name="timeline-input-image-file">');

  App.elements.timeline.config = {
    api_base_url: "" + App.base_url + "/api/timeline",
    show_related: true,
    base_page: null
  };

  App.elements.timeline.config.file_size = {
    max_upload_file_size: 1024 * 1024 * 10
  };

  App.elements.timeline.config.file_types = {
    files: ['text/plain', 'application/pdf', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
    images: ['image/png', 'image/jpeg', 'image/gif']
  };

  App.elements.timeline.config.file_types.all = _.union(App.elements.timeline.config.file_types.files, App.elements.timeline.config.file_types.images);

  App.elements.timeline.config.balloon = {
    margin: 20,
    area_padding: 10,
    arrow_width: 8
  };

  App.elements.timeline.config.drop_area = {
    active_class_name: 'active'
  };

  App.elements.timeline.config.comments = {
    initial_count: 3
  };

  App.elements.timeline.config.linkMore = {
    edit_communication_url: "" + App.base_url + "/user/edit_communication/",
    edit_communication_anchor: '自分も回答する',
    edit_communication_anchor_for_my_profile: '回答する',
    show_others_answer_url: "" + App.base_url + "/communication/profile/",
    show_others_answer_anchor: 'すべての回答を見る',
    show_detail_url: "" + App.base_url + "/feed/show/",
    show_detail_anchor: '詳細を見る'
  };

  App.elements.timeline.models.feed_star = (function(_super) {
    __extends(feed_star, _super);

    function feed_star() {
      this.urlRoot = __bind(this.urlRoot, this);
      return feed_star.__super__.constructor.apply(this, arguments);
    }

    feed_star.prototype.urlRoot = function() {
      return "" + App.elements.timeline.config.api_base_url + "/feed_star";
    };

    feed_star.prototype.defaults = {
      id: null,
      user_id: null,
      user_name: '',
      user_icon: '',
      is_current_user: false
    };

    return feed_star;

  })(Backbone.Model);

  App.elements.timeline.models.feed_comment = (function(_super) {
    __extends(feed_comment, _super);

    function feed_comment() {
      this.initialize = __bind(this.initialize, this);
      this.parse = __bind(this.parse, this);
      this.urlRoot = __bind(this.urlRoot, this);
      return feed_comment.__super__.constructor.apply(this, arguments);
    }

    feed_comment.prototype.urlRoot = function() {
      return "" + App.elements.timeline.config.api_base_url + "/feed_comment";
    };

    feed_comment.prototype.defaults = {
      id: null,
      user_name: '',
      body: '',
      stars: [],
      commented_at: null,
      is_edited: false,
      can_edit: false,
      can_star: true,
      parent_feed_base_page: null
    };

    feed_comment.prototype.properties = null;

    feed_comment.prototype.parse = function(response) {
      if ((response.stars != null) && (this.get('stars') instanceof App.elements.timeline.collections.feed_stars)) {
        this.get('stars').set(response.stars, {
          add: true,
          remove: true,
          merge: true
        });
        response.stars = this.get('stars');
      } else {
        response.stars = new App.elements.timeline.collections.feed_stars(response.stars);
      }
      return response;
    };

    feed_comment.prototype.initialize = function() {
      return this.set('stars', new App.elements.timeline.collections.feed_stars(this.get('stars')));
    };

    return feed_comment;

  })(Backbone.Model);

  App.elements.timeline.models.file = (function(_super) {
    __extends(file, _super);

    function file() {
      return file.__super__.constructor.apply(this, arguments);
    }

    file.prototype.defaults = {
      id: null,
      caption: '',
      extension: '',
      name: '',
      path: '',
      size: 0,
      thumb_path: '',
      is_image: false,
      ratio_type: null,
      width: 0,
      height: 0
    };

    return file;

  })(Backbone.Model);

  App.elements.timeline.models.feed_image = (function(_super) {
    __extends(feed_image, _super);

    function feed_image() {
      this._init = __bind(this._init, this);
      this.initialize = __bind(this.initialize, this);
      this.parse = __bind(this.parse, this);
      this.urlRoot = __bind(this.urlRoot, this);
      return feed_image.__super__.constructor.apply(this, arguments);
    }

    feed_image.prototype.urlRoot = function() {
      return "" + App.elements.timeline.config.api_base_url + "/feed_image";
    };

    feed_image.prototype.defaults = {
      id: null,
      file: App.elements.timeline.models.file,
      stars: [],
      comments: [],
      created_at: null,
      user_id: null,
      user_icon: '',
      user_name: '',
      can_edit: false,
      can_delete: false,
      is_edited: false,
      can_comment: true,
      can_star: true,
      is_area_community: false,
      is_area_event: false,
      community_name: null,
      community_url: null,
      event_name: null,
      event_url: null
    };

    feed_image.prototype.properties = null;

    feed_image.prototype.parse = function(response, xhr) {
      return this._init(response);
    };

    feed_image.prototype.initialize = function() {
      this.properties = {
        file: App.elements.timeline.models.file,
        stars: App.elements.timeline.collections.feed_stars,
        comments: App.elements.timeline.collections.feed_comments
      };
      return this._init();
    };

    feed_image.prototype._init = function(response) {
      var key, obj, _ref;
      _ref = this.properties;
      for (key in _ref) {
        obj = _ref[key];
        if (response != null) {
          if (this.get(key) instanceof obj && this.get(key) instanceof Backbone.Collection) {
            this.get(key).set(response[key], {
              add: true,
              remove: true,
              merge: true
            });
            response[key] = this.get(key);
          } else {
            response[key] = new obj(response[key]);
          }
        } else {
          this.set(key, new obj(this.get(key)));
        }
      }
      if (response != null) {
        return response;
      }
    };

    return feed_image;

  })(Backbone.Model);

  App.elements.timeline.models.feed_file = (function(_super) {
    __extends(feed_file, _super);

    function feed_file() {
      this.parse = __bind(this.parse, this);
      this.initialize = __bind(this.initialize, this);
      this.urlRoot = __bind(this.urlRoot, this);
      return feed_file.__super__.constructor.apply(this, arguments);
    }

    feed_file.prototype.urlRoot = function() {
      return "" + App.elements.timeline.config.api_base_url + "/feed_file";
    };

    feed_file.prototype.defaults = {
      id: null,
      file: App.elements.timeline.models.file
    };

    feed_file.prototype.initialize = function() {
      return this.set('file', new App.elements.timeline.models.file(this.get('file')));
    };

    feed_file.prototype.parse = function(response, options) {
      var file;
      if (response.file != null) {
        if (this.get('file') instanceof App.elements.timeline.models.file) {
          file = this.get('file');
        } else {
          file = new App.elements.timeline.models.file();
        }
        response.file = file.set(response.file);
      }
      return response;
    };

    return feed_file;

  })(Backbone.Model);

  App.elements.timeline.models.feed = (function(_super) {
    __extends(feed, _super);

    function feed() {
      this.is_communication_feed = __bind(this.is_communication_feed, this);
      this.is_system_feed = __bind(this.is_system_feed, this);
      this.is_file_feed = __bind(this.is_file_feed, this);
      this.is_image_feed = __bind(this.is_image_feed, this);
      this.is_text_feed = __bind(this.is_text_feed, this);
      this.initialize = __bind(this.initialize, this);
      this.parse = __bind(this.parse, this);
      this.urlRoot = __bind(this.urlRoot, this);
      return feed.__super__.constructor.apply(this, arguments);
    }

    feed.prototype.urlRoot = function() {
      return "" + App.elements.timeline.config.api_base_url + "/feed";
    };

    feed.prototype.defaults = {
      id: null,
      type: 0,
      title: '',
      body: '',
      row_body: '',
      area: 0,
      community: null,
      event: null,
      profile_id: null,
      feed_system_type_id: null,
      images: [],
      files: [],
      stars: [],
      comments: [],
      user_name: '',
      user_icon: '',
      created_at: null,
      last_update: null,
      is_edited: false,
      can_edit: false,
      can_delete: false,
      is_public: false,
      is_starred: false,
      is_truncate_body: true,
      can_comment: true,
      can_star: true,
      show_edit_communication_link: true,
      birthday: ''
    };

    feed.prototype.parse = function(response, options) {
      response.images = new App.elements.timeline.collections.feed_images(_(response.images).map((function(_this) {
        return function(feed_image_data) {
          return _.extend(feed_image_data, {
            user_id: response.user_id != null ? response.user_id : null,
            user_name: response.user_name != null ? response.user_name : '',
            user_icon: response.user_icon != null ? response.user_icon : '',
            title: response.title != null ? response.title : '',
            profile_id: response.profile_id != null ? response.profile_id : '',
            feed_system_type_id: response.feed_system_type_id != null ? response.feed_system_type_id : ''
          });
        };
      })(this)));
      response.files = new App.elements.timeline.collections.feed_files(response.files);
      if ((response.stars != null) && (this.get('stars') instanceof App.elements.timeline.collections.feed_stars)) {
        this.get('stars').set(response.stars, {
          add: true,
          remove: true,
          merge: true
        });
        response.stars = this.get('stars');
      } else {
        response.stars = new App.elements.timeline.collections.feed_stars(response.stars);
      }
      response.is_starred = response.stars.find((function(_this) {
        return function(star) {
          return star.get('is_current_user');
        };
      })(this)) != null;
      response.comments = new App.elements.timeline.collections.feed_comments(response.comments);
      return response;
    };

    feed.prototype.initialize = function() {
      if (this.get('created_at') === null) {
        return this.set('created_at', App.classes.time.now());
      }
    };

    feed.prototype.is_text_feed = function() {
      return +this.get('type') === 0;
    };

    feed.prototype.is_image_feed = function() {
      return +this.get('type') === 1;
    };

    feed.prototype.is_file_feed = function() {
      return +this.get('type') === 2;
    };

    feed.prototype.is_system_feed = function() {
      return +this.get('type') === 3;
    };

    feed.prototype.is_communication_feed = function() {
      return +this.get('type') === 4;
    };

    return feed;

  })(Backbone.Model);

  App.elements.timeline.collections.feeds = (function(_super) {
    __extends(feeds, _super);

    function feeds() {
      this.url = __bind(this.url, this);
      return feeds.__super__.constructor.apply(this, arguments);
    }

    feeds.prototype.url = function() {
      return "" + App.elements.timeline.config.api_base_url + "/feeds";
    };

    feeds.prototype.model = App.elements.timeline.models.feed;

    feeds.prototype.parse = function(response) {
      if (!response) {
        return [];
      } else {
        return response;
      }
    };

    return feeds;

  })(Backbone.Collection);

  App.elements.timeline.collections.private_image_feeds = (function(_super) {
    __extends(private_image_feeds, _super);

    function private_image_feeds() {
      this.url = __bind(this.url, this);
      return private_image_feeds.__super__.constructor.apply(this, arguments);
    }

    private_image_feeds.prototype.url = function() {
      return "" + App.elements.timeline.config.api_base_url + "/private_feeds/image?v=" + ((new Date()).getTime());
    };

    return private_image_feeds;

  })(App.elements.timeline.collections.feeds);

  App.elements.timeline.collections.private_file_feeds = (function(_super) {
    __extends(private_file_feeds, _super);

    function private_file_feeds() {
      this.url = __bind(this.url, this);
      return private_file_feeds.__super__.constructor.apply(this, arguments);
    }

    private_file_feeds.prototype.url = function() {
      return "" + App.elements.timeline.config.api_base_url + "/private_feeds/file?v=" + ((new Date()).getTime());
    };

    return private_file_feeds;

  })(App.elements.timeline.collections.feeds);

  App.elements.timeline.collections.feed_images = (function(_super) {
    __extends(feed_images, _super);

    function feed_images() {
      return feed_images.__super__.constructor.apply(this, arguments);
    }

    feed_images.prototype.model = App.elements.timeline.models.feed_image;

    return feed_images;

  })(Backbone.Collection);

  App.elements.timeline.collections.feed_files = (function(_super) {
    __extends(feed_files, _super);

    function feed_files() {
      return feed_files.__super__.constructor.apply(this, arguments);
    }

    feed_files.prototype.model = App.elements.timeline.models.feed_file;

    return feed_files;

  })(Backbone.Collection);

  App.elements.timeline.collections.feed_stars = (function(_super) {
    __extends(feed_stars, _super);

    function feed_stars() {
      this.is_starred = __bind(this.is_starred, this);
      this.url = __bind(this.url, this);
      return feed_stars.__super__.constructor.apply(this, arguments);
    }

    feed_stars.prototype.url = function() {
      return "" + App.elements.timeline.config.api_base_url + "/feed_stars?v=" + ((new Date()).getTime());
    };

    feed_stars.prototype.model = App.elements.timeline.models.feed_star;

    feed_stars.prototype.is_starred = function() {
      return !!this.find((function(_this) {
        return function(star) {
          return star.get('is_current_user');
        };
      })(this));
    };

    return feed_stars;

  })(Backbone.Collection);

  App.elements.timeline.collections.feed_comments = (function(_super) {
    __extends(feed_comments, _super);

    function feed_comments() {
      return feed_comments.__super__.constructor.apply(this, arguments);
    }

    feed_comments.prototype.model = App.elements.timeline.models.feed_comment;

    return feed_comments;

  })(Backbone.Collection);

  App.elements.timeline.views.elements = {};

  App.elements.timeline.views.elements.input_image_ballon = (function(_super) {
    __extends(input_image_ballon, _super);

    function input_image_ballon() {
      this.remove_current_thumbnail = __bind(this.remove_current_thumbnail, this);
      this.set_position = __bind(this.set_position, this);
      this.set_caption = __bind(this.set_caption, this);
      this.hide = __bind(this.hide, this);
      this.show = __bind(this.show, this);
      this.close_button_clicked = __bind(this.close_button_clicked, this);
      this.check_close = __bind(this.check_close, this);
      this.initialize = __bind(this.initialize, this);
      return input_image_ballon.__super__.constructor.apply(this, arguments);
    }

    input_image_ballon.prototype.el = null;

    input_image_ballon.prototype.events = {
      'click .js-timeline-input-image-balloon-close-button': 'close_button_clicked',
      'click .js-timeline-input-image-balloon-delete-button': 'remove_current_thumbnail'
    };

    input_image_ballon.prototype.$document = null;

    input_image_ballon.prototype.$parent = null;

    input_image_ballon.prototype.$arrow = null;

    input_image_ballon.prototype.$caption = null;

    input_image_ballon.prototype.current_thumbnail = null;

    input_image_ballon.prototype.initialize = function() {
      this.$document = $(document);
      this.$parent = this.$el.parent();
      this.$arrow = this.$el.find('.js-timeline-input-image-balloon-arrow');
      return this.$caption = this.$el.find('.js-timeline-input-image-balloon-caption');
    };

    input_image_ballon.prototype.check_close = function(event) {
      if (this.$el.find(event.target).length > 0) {
        return true;
      }
      this.hide();
      return false;
    };

    input_image_ballon.prototype.close_button_clicked = function() {
      this.hide();
      return false;
    };

    input_image_ballon.prototype.show = function(thumbnail) {
      if (this.current_thumbnail && this.current_thumbnail !== thumbnail && this.$el.is(':visible')) {
        this.set_caption();
      }
      this.current_thumbnail = thumbnail;
      this.set_position();
      this.$caption.val(this.current_thumbnail.get_caption());
      this.$document.on('click', this.check_close);
      this.$el.show();
      return this;
    };

    input_image_ballon.prototype.hide = function() {
      this.set_caption();
      this.$document.off('click', this.check_close);
      this.$el.hide();
      return this;
    };

    input_image_ballon.prototype.set_caption = function() {
      if (this.current_thumbnail) {
        this.current_thumbnail.set_caption(this.$caption.val());
      }
      return this;
    };

    input_image_ballon.prototype.set_position = function() {
      var area_padding, arrow_left, half_balloon_width, half_thumbnail_width, left, max_position, min_position, thumbnail_height, thumbnail_position, thumbnail_width, top;
      thumbnail_position = this.current_thumbnail.get_position();
      thumbnail_width = this.current_thumbnail.get_width();
      thumbnail_height = this.current_thumbnail.get_height();
      half_thumbnail_width = thumbnail_width / 2;
      half_balloon_width = this.$el.width() / 2;
      top = thumbnail_position.top + thumbnail_height + App.elements.timeline.config.balloon.margin;
      left = Math.floor(thumbnail_position.left - half_balloon_width + half_thumbnail_width);
      area_padding = App.elements.timeline.config.balloon.area_padding;
      min_position = -area_padding;
      max_position = this.$parent.width() - this.$el.width() + area_padding;
      if (left < min_position) {
        left = min_position;
      } else if (left > max_position) {
        left = max_position;
      }
      arrow_left = half_thumbnail_width - (left - thumbnail_position.left) - App.elements.timeline.config.balloon.arrow_width;
      this.$el.css({
        top: "" + top + "px",
        left: "" + left + "px"
      });
      this.$arrow.css({
        left: "" + arrow_left + "px"
      });
      return this;
    };

    input_image_ballon.prototype.remove_current_thumbnail = function() {
      if (this.current_thumbnail != null) {
        this.current_thumbnail.remove();
        this.current_thumbnail = null;
        this.hide();
      }
      return false;
    };

    return input_image_ballon;

  })(Backbone.View);

  App.elements.timeline.views.elements.input_image_thumbnail = (function(_super) {
    __extends(input_image_thumbnail, _super);

    function input_image_thumbnail() {
      this.delete_self = __bind(this.delete_self, this);
      this.get_model = __bind(this.get_model, this);
      this.is_posted = __bind(this.is_posted, this);
      this.remove_input = __bind(this.remove_input, this);
      this.get_file = __bind(this.get_file, this);
      this.remove = __bind(this.remove, this);
      this.get_height = __bind(this.get_height, this);
      this.get_width = __bind(this.get_width, this);
      this.get_position = __bind(this.get_position, this);
      this.fire_clicked = __bind(this.fire_clicked, this);
      this.set_errors = __bind(this.set_errors, this);
      this.set_caption = __bind(this.set_caption, this);
      this.get_caption = __bind(this.get_caption, this);
      this.get_input = __bind(this.get_input, this);
      this.hide_loader = __bind(this.hide_loader, this);
      this.show_loader = __bind(this.show_loader, this);
      this.get_html = __bind(this.get_html, this);
      this.render_thumbnail = __bind(this.render_thumbnail, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return input_image_thumbnail.__super__.constructor.apply(this, arguments);
    }

    input_image_thumbnail.prototype.el = '<li class="js-file-input-container"></li>';

    input_image_thumbnail.prototype.events = {
      'click .js-file-input-image-thumbnail-edit-caption-anchor': 'fire_clicked',
      'click .js-timeline-input-file-thumbnail-delete-button': 'delete_self'
    };

    input_image_thumbnail.prototype.$input = null;

    input_image_thumbnail.prototype.width = 100;

    input_image_thumbnail.prototype.height = 100;

    input_image_thumbnail.prototype.file_input = null;

    input_image_thumbnail.prototype.file_input_dummy_path = null;

    input_image_thumbnail.prototype.file = null;

    input_image_thumbnail.prototype.caption = '';

    input_image_thumbnail.prototype.is_posted_thumbnail = false;

    input_image_thumbnail.prototype.model = null;

    input_image_thumbnail.prototype.initialize = function(options) {
      var _ref, _ref1, _ref2;
      if ((options != null ? (_ref = options.$input) != null ? (_ref1 = _ref[0]) != null ? (_ref2 = _ref1.files) != null ? _ref2[0] : void 0 : void 0 : void 0 : void 0) != null) {
        this.$input = options.$input;
        this.$input.hide();
        this.file = this.$input[0].files[0];
      } else if ((options != null ? options.file : void 0) != null) {
        this.file = options.file;
      } else if ((options != null ? options.$input : void 0) != null) {
        this.$input = options.$input;
      }
      this.render();
      if (this.file) {
        this.file_input = new App.elements.file_input.views.file_input({
          el: this.$el,
          width: this.width,
          height: this.height,
          dummy_path: this.file_input_dummy_path
        });
        this.file_input.on('load_thumbnail_failed', this.remove);
        if (this.$input != null) {
          this.file_input.set_input(this.$input);
        }
        this.render_thumbnail();
      }
      if (this.file_input) {
        return this.file_input.set_loader_size(this.width, this.height);
      }
    };

    input_image_thumbnail.prototype.render = function() {
      if (App.can_use_file_api) {
        this.$el.html(this.get_html());
        if (this.$input != null) {
          this.$el.append(this.$input);
        }
      } else {
        if (this.is_posted()) {
          this.$el.html(App.elements.timeline.templates.input_legacy_thumbnail.posted({
            model: this.model,
            caption: this.get_caption()
          }));
        } else {
          this.$el.html(App.elements.timeline.templates.input_legacy_thumbnail.item({
            caption: ''
          }));
        }
        if (this.$input != null) {
          this.$el.find('.js-timeline-input-file-thumbnail-legacy-file-input-container').prepend(this.$input);
        }
      }
      return this.$el;
    };

    input_image_thumbnail.prototype.render_thumbnail = function() {
      if (this.file_input) {
        return this.file_input.load_thumbnail(this.file);
      }
    };

    input_image_thumbnail.prototype.get_html = function() {
      return App.elements.timeline.templates.input_image_thumbnail.item();
    };

    input_image_thumbnail.prototype.show_loader = function() {
      if (this.file_input) {
        this.file_input.show_loader();
      }
      return this;
    };

    input_image_thumbnail.prototype.hide_loader = function() {
      if (this.file_input) {
        this.file_input.hide_loader();
      }
      return this;
    };

    input_image_thumbnail.prototype.get_input = function() {
      return this.$input;
    };

    input_image_thumbnail.prototype.get_caption = function() {
      if (App.can_use_file_api) {
        return this.caption;
      } else {
        return this.$el.find('input[name="caption"]').val();
      }
    };

    input_image_thumbnail.prototype.set_caption = function(caption) {
      this.caption = caption;
      return this;
    };

    input_image_thumbnail.prototype.set_errors = function(errors, feed_model_objects_key) {
      var message_banner, messages;
      messages = [];
      $.each(errors, (function(_this) {
        return function(index, error) {
          var err, key, _results;
          if (!(_.isObject(error) || _.isArray(error))) {
            error = [error];
          }
          _results = [];
          for (key in error) {
            err = error[key];
            _results.push(messages.push(err));
          }
          return _results;
        };
      })(this));
      message_banner = new App.elements.message_banner.views.message_banner({
        message: messages.join('<br>'),
        class_name: 'error',
        has_close: true
      });
      if (feed_model_objects_key === 'files') {
        this.$el.find('.content').append(message_banner.$el);
      } else {
        this.$el.append(message_banner.$el);
      }
      return this;
    };

    input_image_thumbnail.prototype.fire_clicked = function() {
      this.trigger('clicked', this);
      return false;
    };

    input_image_thumbnail.prototype.get_position = function() {
      return this.$el.position();
    };

    input_image_thumbnail.prototype.get_width = function() {
      return this.$el.width();
    };

    input_image_thumbnail.prototype.get_height = function() {
      return this.$el.height();
    };

    input_image_thumbnail.prototype.remove = function(destroy_model) {
      if (destroy_model == null) {
        destroy_model = true;
      }
      if (!!destroy_model && (this.model != null) && this.model) {
        this.model.destroy();
      }
      this.trigger('removed', this);
      return input_image_thumbnail.__super__.remove.apply(this, arguments);
    };

    input_image_thumbnail.prototype.get_file = function() {
      return this.file;
    };

    input_image_thumbnail.prototype.remove_input = function() {
      if (this.$input != null) {
        this.$input.remove;
      }
      return this;
    };

    input_image_thumbnail.prototype.is_posted = function() {
      return this.is_posted_thumbnail;
    };

    input_image_thumbnail.prototype.get_model = function() {
      return this.model;
    };

    input_image_thumbnail.prototype.delete_self = function() {
      this.remove();
      return false;
    };

    return input_image_thumbnail;

  })(Backbone.View);

  App.elements.timeline.views.elements.posted_input_image_thumbnail = (function(_super) {
    __extends(posted_input_image_thumbnail, _super);

    function posted_input_image_thumbnail() {
      this.render_thumbnail = __bind(this.render_thumbnail, this);
      this.initialize = __bind(this.initialize, this);
      return posted_input_image_thumbnail.__super__.constructor.apply(this, arguments);
    }

    posted_input_image_thumbnail.prototype.el = '<li></li>';

    posted_input_image_thumbnail.prototype.file_input = null;

    posted_input_image_thumbnail.prototype.is_posted_thumbnail = true;

    posted_input_image_thumbnail.prototype.initialize = function() {
      this.render();
      this.file_input = new App.elements.file_input.views.file_input({
        el: this.$el,
        width: this.width,
        height: this.height
      });
      this.file_input.set_loader_size(this.width, this.height);
      this.render_thumbnail();
      return this.set_caption(this.model.get('file').get('caption'));
    };

    posted_input_image_thumbnail.prototype.render_thumbnail = function() {
      return this.file_input.render_thumbnail(this.model.get('file').get('path'), this.model.get('file').get('name'), true, false);
    };

    return posted_input_image_thumbnail;

  })(App.elements.timeline.views.elements.input_image_thumbnail);

  App.elements.timeline.views.elements.input_file_thumbnail = (function(_super) {
    __extends(input_file_thumbnail, _super);

    function input_file_thumbnail() {
      this.set_caption = __bind(this.set_caption, this);
      this.get_caption = __bind(this.get_caption, this);
      this.render_thumbnail = __bind(this.render_thumbnail, this);
      this.get_html = __bind(this.get_html, this);
      this.render = __bind(this.render, this);
      return input_file_thumbnail.__super__.constructor.apply(this, arguments);
    }

    input_file_thumbnail.prototype.width = 60;

    input_file_thumbnail.prototype.height = 60;

    input_file_thumbnail.prototype.file_input_dummy_path = '/assets/img/extensions/file.png';

    input_file_thumbnail.prototype.$caption = null;

    input_file_thumbnail.prototype.render = function() {
      input_file_thumbnail.__super__.render.apply(this, arguments);
      return this.$caption = this.$el.find('input[name="caption"]');
    };

    input_file_thumbnail.prototype.get_html = function() {
      return App.elements.timeline.templates.input_file_thumbnail.item({
        name: this.file.name,
        size: App.classes.num.quantity(this.file.size),
        caption: ''
      });
    };

    input_file_thumbnail.prototype.render_thumbnail = function() {
      return this.file_input.render_extension_thumbnail(this.file, true, false);
    };

    input_file_thumbnail.prototype.get_caption = function() {
      var caption;
      caption = this.$caption != null ? this.$caption.val() : '';
      return caption;
    };

    input_file_thumbnail.prototype.set_caption = function(caption) {
      if (this.$caption != null) {
        this.$caption.val(caption);
      }
      return this;
    };

    return input_file_thumbnail;

  })(App.elements.timeline.views.elements.input_image_thumbnail);

  App.elements.timeline.views.elements.posted_input_file_thumbnail = (function(_super) {
    __extends(posted_input_file_thumbnail, _super);

    function posted_input_file_thumbnail() {
      this.render_thumbnail = __bind(this.render_thumbnail, this);
      this.get_html = __bind(this.get_html, this);
      this.initialize = __bind(this.initialize, this);
      return posted_input_file_thumbnail.__super__.constructor.apply(this, arguments);
    }

    posted_input_file_thumbnail.prototype.el = '<li></li>';

    posted_input_file_thumbnail.prototype.file_input = null;

    posted_input_file_thumbnail.prototype.is_posted_thumbnail = true;

    posted_input_file_thumbnail.prototype.initialize = function() {
      this.render();
      this.file_input = new App.elements.file_input.views.file_input({
        el: this.$el,
        width: this.width,
        height: this.height
      });
      this.file_input.set_loader_size(this.width, this.height);
      return this.render_thumbnail();
    };

    posted_input_file_thumbnail.prototype.get_html = function() {
      var file_model;
      file_model = this.model.get('file');
      return App.elements.timeline.templates.input_file_thumbnail.item({
        name: file_model.get('name'),
        size: App.classes.num.quantity(file_model.get('size')),
        caption: file_model.get('caption')
      });
    };

    posted_input_file_thumbnail.prototype.render_thumbnail = function() {
      return this.file_input.render_extension_thumbnail_by_name(this.model.get('file').get('name'), true, false);
    };

    return posted_input_file_thumbnail;

  })(App.elements.timeline.views.elements.input_file_thumbnail);

  App.elements.timeline.views.elements.show_all = (function(_super) {
    __extends(show_all, _super);

    function show_all() {
      this.link_clicked = __bind(this.link_clicked, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return show_all.__super__.constructor.apply(this, arguments);
    }

    show_all.prototype.el = '<p class="showAll"></p>';

    show_all.prototype.events = {
      'click .js-timeline-feed-show-all-link': 'link_clicked'
    };

    show_all.prototype.initialize = function() {
      this.collection.on('add', this.render);
      this.collection.on('remove', this.render);
      return this.render();
    };

    show_all.prototype.render = function(count) {
      if (count == null) {
        count = null;
      }
      count = this.collection.length - App.elements.timeline.config.comments.initial_count;
      if (count > 0) {
        this.$el.html(App.elements.timeline.templates.show_all({
          count: count
        }));
      }
      return this.$el;
    };

    show_all.prototype.link_clicked = function() {
      this.trigger('show_all');
      return false;
    };

    return show_all;

  })(Backbone.View);

  App.elements.timeline.views.timeline_input_text = (function(_super) {
    __extends(timeline_input_text, _super);

    function timeline_input_text() {
      this.set_error_messages_from_array = __bind(this.set_error_messages_from_array, this);
      this.clear_form_errors = __bind(this.clear_form_errors, this);
      this.set_form_errors = __bind(this.set_form_errors, this);
      this.check_editor_focus = __bind(this.check_editor_focus, this);
      this.show_editor = __bind(this.show_editor, this);
      this.add_feed = __bind(this.add_feed, this);
      this.initialize = __bind(this.initialize, this);
      return timeline_input_text.__super__.constructor.apply(this, arguments);
    }

    timeline_input_text.prototype.el = '#postTweet';

    timeline_input_text.prototype.collection = null;

    timeline_input_text.prototype.events = {
      'focus .js-timeline-input-text-default': 'show_editor',
      'submit .js-timeline-input-text-form': 'add_feed'
    };

    timeline_input_text.prototype.feed_type = 0;

    timeline_input_text.prototype.$default = null;

    timeline_input_text.prototype.$container = null;

    timeline_input_text.prototype.$input_text_button = null;

    timeline_input_text.prototype.$input_text_wysiwyg = null;

    timeline_input_text.prototype.$form = null;

    timeline_input_text.prototype.initialize = function() {
      this.$default = this.$el.find('.js-timeline-input-text-default');
      this.$container = this.$el.find('.js-timeline-input-text-container');
      this.$input_text_wysiwyg = this.$el.find('.js-timeline-input-text-wysiwyg');
      this.$ckeditor = CKEDITOR.instances[this.$input_text_wysiwyg.attr('name')];
      this.$form = this.$container.find('.js-timeline-input-text-form');
      App.functions.disable_form_multiple_clicks($('#js-timeline-input-text-button'));
      if ($(':focus').hasClass('js-timeline-input-text-default')) {
        return this.show_editor();
      }
    };

    timeline_input_text.prototype.add_feed = function(event) {
      var feed, post_html;
      this.clear_form_errors();
      post_html = this.$ckeditor.getData();
      if (_.trim(_.escape(post_html)).length <= 0) {
        this.set_form_errors('本文は入力必須です');
        App.functions.disable_form_multiple_clicks($('#js-timeline-input-text-button'));
        return false;
      }
      feed = new App.elements.timeline.models.feed({
        type: this.feed_type,
        body: post_html
      });
      feed.save({}, {
        success: (function(_this) {
          return function() {
            if (feed.changed.error === '1') {
              _this.set_error_messages_from_array(feed.changed.message.body);
              _this.is_post_processing = false;
              return false;
            }
            feed.set('prepend', true);
            feed.set({
              body: feed.get('escape_body'),
              row_body: feed.get('escape_row_body')
            }, {
              silent: true
            });
            _this.collection.push(feed);
            return _this.$form.find('iframe').contents().find('body').html('');
          };
        })(this),
        error: (function(_this) {
          return function(feed, jqXHR) {
            var _ref;
            if ((jqXHR != null ? (_ref = jqXHR.response) != null ? _ref.errors : void 0 : void 0) != null) {
              _this.set_error_messages_from_array(jqXHR.response.errors);
            }
            return _this.is_post_processing = false;
          };
        })(this),
        complete: (function(_this) {
          return function() {
            return App.functions.disable_form_multiple_clicks($('#js-timeline-input-text-button'));
          };
        })(this)
      });
      return false;
    };

    timeline_input_text.prototype.show_editor = function() {
      this.$form.find('iframe').contents().find('body').html('');
      this.$default.hide();
      this.$container.show();
      this.$default.removeAttr('readonly');
      this.$ckeditor.on('instanceReady', (function(_this) {
        return function() {
          return _this.$ckeditor.focus();
        };
      })(this));
      this.check_editor_focus();
      return false;
    };

    timeline_input_text.prototype.check_editor_focus = function() {
      this.$ckeditor.focus();
      if (!this.$ckeditor.focusManager.hasFocus) {
        return setTimeout((function(_this) {
          return function() {
            return _this.check_editor_focus();
          };
        })(this), 500);
      } else {
        return false;
      }
    };

    timeline_input_text.prototype.set_form_errors = function(error_message) {
      var banner;
      this.clear_form_errors();
      banner = new App.elements.message_banner.views.message_banner({
        message: error_message,
        class_name: 'error',
        has_close: true
      });
      this.$el.append(banner.$el);
      return this;
    };

    timeline_input_text.prototype.clear_form_errors = function() {
      this.$el.find('.error').remove();
      return this;
    };

    timeline_input_text.prototype.set_error_messages_from_array = function(ary_errors) {
      var err, error, error_message_from_response, k, key, _results;
      error_message_from_response = [];
      _results = [];
      for (key in ary_errors) {
        error = ary_errors[key];
        if (_.isObject(error)) {
          for (k in error) {
            err = error[k];
            error_message_from_response.push(err);
          }
        } else {
          error_message_from_response.push(error);
        }
        if (error_message_from_response.length === 0) {
          error_message_from_response.push('投稿に失敗しました');
        }
        _results.push(this.set_form_errors(error_message_from_response.join(',')));
      }
      return _results;
    };

    return timeline_input_text;

  })(Backbone.View);

  App.elements.timeline.views.timeline_input_image = (function(_super) {
    __extends(timeline_input_image, _super);

    function timeline_input_image() {
      this.check_thumbnails_count = __bind(this.check_thumbnails_count, this);
      this.is_upload_target_thumbnail = __bind(this.is_upload_target_thumbnail, this);
      this.get_current_thumbnails_count = __bind(this.get_current_thumbnails_count, this);
      this.file_drag_leave = __bind(this.file_drag_leave, this);
      this.file_drag_enter = __bind(this.file_drag_enter, this);
      this.file_drag_over = __bind(this.file_drag_over, this);
      this.file_drop = __bind(this.file_drop, this);
      this.inactivate_drop_area = __bind(this.inactivate_drop_area, this);
      this.activate_drop_area = __bind(this.activate_drop_area, this);
      this.show_balloon = __bind(this.show_balloon, this);
      this.add_new_input = __bind(this.add_new_input, this);
      this.thumbnail_removed = __bind(this.thumbnail_removed, this);
      this.add_new_thumbnail = __bind(this.add_new_thumbnail, this);
      this.add_posted_thumbnail = __bind(this.add_posted_thumbnail, this);
      this.file_to_thumbnail = __bind(this.file_to_thumbnail, this);
      this.current_input_to_thumbnail = __bind(this.current_input_to_thumbnail, this);
      this.show_check_files_error = __bind(this.show_check_files_error, this);
      this.check_files = __bind(this.check_files, this);
      this.register_image = __bind(this.register_image, this);
      this.add_button_clicked = __bind(this.add_button_clicked, this);
      this.edit_private_feed = __bind(this.edit_private_feed, this);
      this.upload_image = __bind(this.upload_image, this);
      this.reset_input = __bind(this.reset_input, this);
      this.clear_form_errors = __bind(this.clear_form_errors, this);
      this.set_form_errors = __bind(this.set_form_errors, this);
      this.set_error_messages_from_array = __bind(this.set_error_messages_from_array, this);
      this.save_feed = __bind(this.save_feed, this);
      this.delete_private_feeds = __bind(this.delete_private_feeds, this);
      this.initialize = __bind(this.initialize, this);
      this.upload_url = __bind(this.upload_url, this);
      return timeline_input_image.__super__.constructor.apply(this, arguments);
    }

    timeline_input_image.prototype.el = '#postPhoto';

    timeline_input_image.prototype.collection = null;

    timeline_input_image.prototype.upload_url = function() {
      return "" + App.elements.timeline.config.api_base_url + "/image";
    };

    timeline_input_image.prototype.events = {
      'submit .js-timeline-input-image-upload-form': 'save_feed',
      'click .js-timeline-input-image-add-button': 'add_button_clicked',
      'change .js-timeline-input-image-input-file': 'register_image'
    };

    timeline_input_image.prototype.feed_type = 1;

    timeline_input_image.prototype.feed_model_objects_key = 'images';

    timeline_input_image.prototype.use_iframe_on_upload = !App.can_use_file_api;

    timeline_input_image.prototype.max_upload_image_count = 20;

    timeline_input_image.prototype.current_feed = null;

    timeline_input_image.prototype.private_feed_collection = null;

    timeline_input_image.prototype.private_feed_collection_obj = App.elements.timeline.collections.private_image_feeds;

    timeline_input_image.prototype.thumbnail_view_class_name = 'input_image_thumbnail';

    timeline_input_image.prototype.thumbnail_view_posted_class_name = 'posted_input_image_thumbnail';

    timeline_input_image.prototype.thumbnails = null;

    timeline_input_image.prototype.$input_image_title = null;

    timeline_input_image.prototype.$input_image_upload_form = null;

    timeline_input_image.prototype.$input_file_container = null;

    timeline_input_image.prototype.$input_file = null;

    timeline_input_image.prototype.$thumb_area = null;

    timeline_input_image.prototype.$shade = null;

    timeline_input_image.prototype.$add_legacy_input = null;

    timeline_input_image.prototype.balloon = null;

    timeline_input_image.prototype.accept_images = true;

    timeline_input_image.prototype.accept_files = false;

    timeline_input_image.prototype.file = null;

    timeline_input_image.prototype.is_post_processing = false;

    timeline_input_image.prototype.error_modal = null;

    timeline_input_image.prototype.initialize = function() {
      var $balloon_container;
      this.thumbnails = [];
      this.file = new App.classes.file(this.accept_images, this.accept_files);
      this.$input_image_title = this.$el.find('.js-timeline-input-image-title');
      this.$input_image_upload_form = this.$el.find('.js-timeline-input-image-upload-form');
      this.$input_file_container = this.$el.find('.js-timeline-input-image-input-file-container');
      this.$input_file = this.$input_file_container.find('.js-timeline-input-image-input-file');
      this.$thumb_area = this.$el.find('.listContainer');
      this.$shade = this.$input_file_container.find('.js-timeline-input-image-input-file-drop-area-shade');
      $balloon_container = this.$el.find('.js-timeline-input-image-balloon-container');
      this.balloon = new App.elements.timeline.views.elements.input_image_ballon({
        el: $balloon_container
      });
      this.$input_image_upload_form.fileupload({
        url: this.upload_url(),
        dataType: 'json',
        type: 'post',
        autoUpload: !App.can_use_file_api,
        fileInput: null,
        paramName: 'timeline-input-image-file'
      });
      this.private_feed_collection = new this.private_feed_collection_obj();
      if (App.can_use_file_api) {
        this.$input_file.addClass('attach');
        this.$input_file_container.on('drop', this.file_drop).on('dragover', this.file_drag_over).on('dragenter', this.file_drag_enter);
        return this.$shade.on('dragleave', this.file_drag_leave);
      } else {
        this.$el.addClass('static');
        this.$thumb_area.removeClass('listContainer');
        this.$el.find('i').hide();
        this.$input_file_container.hide();
        this.$add_legacy_input = $(App.elements.timeline.templates.legacy_input());
        this.$add_legacy_input.find('.js-timeline-post-input-legacy-input-add-thumb-link').on('click', (function(_this) {
          return function() {
            if (!_this.check_thumbnails_count(1)) {
              return false;
            }
            _this.add_new_thumbnail(_this.thumbnail_view_class_name, {
              $input: $('<input type="file">')
            });
            return false;
          };
        })(this));
        this.$add_legacy_input.appendTo(this.$thumb_area);
        return this.$input_file.on('change', this.register_image);
      }
    };

    timeline_input_image.prototype.delete_private_feeds = function() {
      return this.private_feed_collection.fetch({
        success: (function(_this) {
          return function() {
            _this.private_feed_collection.each(function(private_feed_model) {
              return private_feed_model.destroy();
            });
            return _this.current_feed = null;
          };
        })(this)
      });
    };

    timeline_input_image.prototype.save_feed = function() {
      var feed, has_file_choiced;
      if (this.is_post_processing) {
        return false;
      }
      this.is_post_processing = true;
      this.clear_form_errors();
      has_file_choiced = this.get_current_thumbnails_count() > 0;
      if (!has_file_choiced && (this.current_feed != null)) {
        has_file_choiced = this.current_feed.get('images').length > 0 || this.current_feed.get('files').length > 0;
      }
      if (!has_file_choiced) {
        this.set_form_errors({
          error: {
            0: 'ファイルがひとつも選択されていません'
          }
        });
        this.is_post_processing = false;
        return false;
      }
      if (this.current_feed === null) {
        feed = new App.elements.timeline.models.feed();
      } else {
        feed = this.current_feed;
      }
      feed.save({
        type: this.feed_type,
        body: this.$input_image_title.val()
      }, {
        success: (function(_this) {
          return function(saved_feed) {
            if (saved_feed.changed.error === '1') {
              _this.set_error_messages_from_array(saved_feed.changed.message.body);
              _this.is_post_processing = false;
              _this.delete_private_feeds();
              return false;
            }
            _this.$input_image_upload_form.find('.alert').remove();
            _this.current_feed = feed;
            return _this.upload_image(saved_feed.get('id'), 0, function() {
              return feed.save({
                body: _this.$input_image_title.val(),
                complete: true
              }, {
                success: function() {
                  feed.set('prepend', true);
                  feed.set({
                    body: feed.get('escape_body'),
                    row_body: feed.get('escape_row_body')
                  }, {
                    silent: true
                  });
                  _this.collection.push(feed);
                  _this.reset_input();
                  return _this.current_feed = null;
                },
                error: function() {
                  _this.delete_private_feeds();
                  return _this.set_form_errors({
                    enable_feed: {
                      0: 'フィードの公開に失敗しました'
                    }
                  });
                },
                complete: function() {
                  return _this.is_post_processing = false;
                }
              });
            });
          };
        })(this),
        error: (function(_this) {
          return function(feed, xhr) {
            var _ref, _ref1;
            if ((xhr != null ? (_ref = xhr.response) != null ? (_ref1 = _ref.errors) != null ? _ref1.body : void 0 : void 0 : void 0) != null) {
              _this.set_error_messages_from_array(xhr.response.errors.body);
            }
            _this.is_post_processing = false;
            return _this.delete_private_feeds();
          };
        })(this)
      });
      return false;
    };

    timeline_input_image.prototype.set_error_messages_from_array = function(ary_errors) {
      var err, error, error_message_from_response, k, key;
      error_message_from_response = [];
      for (key in ary_errors) {
        error = ary_errors[key];
        if (_.isObject(error)) {
          for (k in error) {
            err = error[k];
            error_message_from_response.push(err);
          }
        } else {
          error_message_from_response.push(error);
        }
      }
      if (error_message_from_response.length === 0) {
        error_message_from_response.push('投稿に失敗しました');
      }
      return this.set_form_errors(error_message_from_response.join(','));
    };

    timeline_input_image.prototype.set_form_errors = function(errors, class_name) {
      var banner, error, error_message, index, message, rule;
      if (class_name == null) {
        class_name = 'error';
      }
      this.clear_form_errors();
      error_message = '';
      for (index in errors) {
        error = errors[index];
        for (rule in error) {
          message = error[rule];
          error_message += "" + message;
        }
      }
      banner = new App.elements.message_banner.views.message_banner({
        message: error_message,
        class_name: class_name,
        has_close: true
      });
      this.$input_image_upload_form.append(banner.$el);
      return this;
    };

    timeline_input_image.prototype.clear_form_errors = function() {
      this.$input_image_upload_form.find('.error').remove();
      return this;
    };

    timeline_input_image.prototype.reset_input = function() {
      var thumbnail, _i, _len, _ref;
      this.$input_image_title.val('');
      this.$input_image_upload_form.find('.js-timeline-input-image-container,.progress,.alert').remove();
      _ref = this.thumbnails;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        thumbnail = _ref[_i];
        if (thumbnail != null) {
          thumbnail.remove(false);
        }
      }
      this.thumbnails = [];
      if (!App.can_use_file_api) {
        return this.add_new_thumbnail(this.thumbnail_view_class_name, {
          $input: $('<input type="file">')
        }, 'prepend');
      }
    };

    timeline_input_image.prototype.upload_image = function(feed_id, current_index, cb) {
      var caption, file, model, thumbnail, _ref;
      if ((((_ref = this.thumbnails) != null ? _ref[current_index] : void 0) != null) && this.is_upload_target_thumbnail(this.thumbnails[current_index])) {
        thumbnail = this.thumbnails[current_index];
        caption = thumbnail.get_caption();
        if (thumbnail.is_posted()) {
          thumbnail.show_loader();
          model = thumbnail.get_model();
          model.set('caption', caption);
          model.save({}, {
            success: (function(_this) {
              return function() {
                thumbnail.hide_loader();
                return _this.upload_image(feed_id, ++current_index, cb);
              };
            })(this)
          });
          return;
        }
        if (!this.use_iframe_on_upload) {
          file = thumbnail.get_file();
        } else {
          file = thumbnail.get_input();
        }
        if (!file) {
          this.upload_image(feed_id, ++current_index, cb);
          return;
        }
        thumbnail.show_loader();
        this.$input_image_upload_form.fileupload('option', 'formData', [
          {
            name: App.config.security.csrf_token_key,
            value: fuel_fetch_token()
          }, {
            name: 'response_with_html',
            value: this.use_iframe_on_upload ? 1 : 0
          }, {
            name: 'feed_id',
            value: feed_id
          }, {
            name: 'caption',
            value: thumbnail.get_caption()
          }
        ]);
        this.$input_image_upload_form.fileupload('option', 'done', (function(_this) {
          return function(event, response) {
            var _ref1, _ref2;
            if (((response != null ? (_ref1 = response.result) != null ? _ref1.errors : void 0 : void 0) != null) || (((response != null ? (_ref2 = response.result) != null ? _ref2.result : void 0 : void 0) != null) && response.result.result === false)) {
              thumbnail.set_errors(response.result.errors);
              return _this.is_post_processing = false;
            } else {
              thumbnail.hide_loader();
              thumbnail.remove_input();
              return _this.upload_image(feed_id, ++current_index, cb);
            }
          };
        })(this));
        this.$input_image_upload_form.fileupload('option', 'fail', (function(_this) {
          return function(event, data) {
            var err, error, errors, field, rule, _ref1, _ref2, _ref3;
            errors = [];
            if ((data != null ? data.jqXHR : void 0) != null) {
              if ((data.jqXHR.status != null) && +data.jqXHR.status === 413) {
                errors.push('ファイル容量が大きすぎます。10MB以内のファイルしかアップロードできません');
              }
              if (((_ref1 = data.jqXHR.responseJSON) != null ? _ref1.errors : void 0) != null) {
                _ref2 = data.jqXHR.responseJSON.errors;
                for (field in _ref2) {
                  error = _ref2[field];
                  if (_.isObject(error)) {
                    for (rule in error) {
                      err = error[rule];
                      errors.push(err);
                    }
                  } else {
                    errors.push(error);
                  }
                }
              }
              if (((_ref3 = data.jqXHR.responseJSON) != null ? _ref3.message : void 0) != null) {
                errors.push(data.jqXHR.responseJSON.message);
              }
            }
            if (errors.length <= 0) {
              errors.push('ファイルのアップロードに失敗しました。ファイル容量が10MBを超えているか、許可されていない形式のファイルの可能性があります');
            }
            thumbnail.set_errors(errors, _this.feed_model_objects_key);
            thumbnail.hide_loader();
            _this.is_post_processing = false;
            return _this.delete_private_feeds();
          };
        })(this));
        if (!this.use_iframe_on_upload) {
          this.$input_image_upload_form.fileupload('send', {
            files: [file]
          });
        } else {
          this.$input_image_upload_form.fileupload('add', {
            fileInput: file
          });
        }
      } else {
        if (current_index < this.thumbnails.length) {
          this.upload_image(feed_id, ++current_index, cb);
        } else {
          if (typeof cb === 'function') {
            cb();
          }
        }
      }
      return false;
    };

    timeline_input_image.prototype.edit_private_feed = function(private_feed) {
      this.current_feed = private_feed;
      this.$input_image_upload_form.find('.js-timeline-input-image-container').remove();
      private_feed.get(this.feed_model_objects_key).each((function(_this) {
        return function(feed_image) {
          return _this.add_posted_thumbnail(feed_image);
        };
      })(this));
      this.$input_image_title.val(private_feed.get('body'));
      return false;
    };

    timeline_input_image.prototype.add_button_clicked = function() {
      this.$input_file.trigger('click');
      return false;
    };

    timeline_input_image.prototype.register_image = function(event) {
      var _ref;
      if (!this.check_thumbnails_count(1)) {
        return false;
      }
      if (!App.can_use_file_api) {
        this.current_input_to_thumbnail();
        return false;
      }
      if (((event != null ? (_ref = event.target) != null ? _ref.files : void 0 : void 0) == null) && event.target.files.length > 0) {
        return false;
      }
      if (this.check_files(event.target.files)) {
        this.current_input_to_thumbnail();
      } else {
        this.show_check_files_error();
      }
      return false;
    };

    timeline_input_image.prototype.check_files = function(files) {
      var file, file_type, _i, _len;
      for (_i = 0, _len = files.length; _i < _len; _i++) {
        file = files[_i];
        file_type = this.file.get_file_type(file);
        if (!(file_type.length > 0 && (file.size != null) && this.file.is_accept_file_type(file_type) && this.file.is_accept_file_size(file.size))) {
          return false;
        }
      }
      return true;
    };

    timeline_input_image.prototype.show_check_files_error = function() {
      return this.file.show_file_type_error();
    };

    timeline_input_image.prototype.current_input_to_thumbnail = function() {
      this.$input_file.off('change', this.register_image);
      this.add_new_thumbnail(this.thumbnail_view_class_name, {
        $input: this.$input_file
      });
      return this.add_new_input();
    };

    timeline_input_image.prototype.file_to_thumbnail = function(file) {
      return this.add_new_thumbnail(this.thumbnail_view_class_name, {
        file: file
      });
    };

    timeline_input_image.prototype.add_posted_thumbnail = function(feed_image) {
      return this.add_new_thumbnail(this.thumbnail_view_posted_class_name, {
        model: feed_image
      });
    };

    timeline_input_image.prototype.add_new_thumbnail = function(class_name, argument, method) {
      var thumbnail;
      if (method == null) {
        method = 'append';
      }
      thumbnail = new App.elements.timeline.views.elements[class_name](argument);
      thumbnail.on('clicked', this.show_balloon);
      thumbnail.on('removed', this.thumbnail_removed);
      this.thumbnails.push(thumbnail);
      this.$thumb_area[method](thumbnail.$el);
      if (this.$add_legacy_input != null) {
        this.$add_legacy_input.appendTo(this.$thumb_area);
      }
      return thumbnail;
    };

    timeline_input_image.prototype.thumbnail_removed = function(removed_thumbnail) {
      var index;
      index = _.indexOf(this.thumbnails, removed_thumbnail);
      delete this.thumbnails[index];
      return this;
    };

    timeline_input_image.prototype.add_new_input = function() {
      var $input;
      $input = $(App.elements.timeline.templates.input_file());
      if (App.can_use_file_api) {
        $input.addClass('attach');
      }
      this.$input_file_container.append($input);
      this.$input_file = $input;
      if (!App.can_use_file_api) {
        return this.$input_file.on('change', this.register_image);
      }
    };

    timeline_input_image.prototype.show_balloon = function(thumbnail) {
      return this.balloon.show(thumbnail);
    };

    timeline_input_image.prototype.activate_drop_area = function() {
      this.$input_file_container.addClass(App.elements.timeline.config.drop_area.active_class_name);
      return this;
    };

    timeline_input_image.prototype.inactivate_drop_area = function() {
      this.$input_file_container.removeClass(App.elements.timeline.config.drop_area.active_class_name);
      return this;
    };

    timeline_input_image.prototype.file_drop = function(event) {
      var file, files, _i, _len, _ref;
      if ((event != null ? (_ref = event.originalEvent.dataTransfer) != null ? _ref.files : void 0 : void 0) == null) {
        return false;
      }
      files = event.originalEvent.dataTransfer.files;
      this.inactivate_drop_area();
      if (((files != null ? files.length : void 0) == null) || files.length <= 0) {
        return false;
      }
      if (!this.check_thumbnails_count(files.length)) {
        return false;
      }
      if (this.check_files(files)) {
        for (_i = 0, _len = files.length; _i < _len; _i++) {
          file = files[_i];
          this.file_to_thumbnail(file);
        }
      } else {
        this.show_check_files_error();
      }
      return false;
    };

    timeline_input_image.prototype.file_drag_over = function(event) {
      event.preventDefault();
      event.originalEvent.dataTransfer.dropEffect = 'copy';
      return false;
    };

    timeline_input_image.prototype.file_drag_enter = function(event) {
      this.activate_drop_area();
      event.stopPropagation();
      event.preventDefault();
      return false;
    };

    timeline_input_image.prototype.file_drag_leave = function(event) {
      this.inactivate_drop_area();
      event.preventDefault();
      return false;
    };

    timeline_input_image.prototype.get_current_thumbnails_count = function() {
      var count, key, thumbnail, _ref;
      count = 0;
      _ref = this.thumbnails;
      for (key in _ref) {
        thumbnail = _ref[key];
        if (this.is_upload_target_thumbnail(thumbnail)) {
          count++;
        }
      }
      return count;
    };

    timeline_input_image.prototype.is_upload_target_thumbnail = function(thumbnail) {
      var $input_file, input_file_value;
      if (App.can_use_file_api) {
        return true;
      }
      $input_file = thumbnail.$el.find('input[type="file"]');
      if (!((($input_file != null ? $input_file.length : void 0) != null) && $input_file.length >= 1)) {
        return true;
      }
      input_file_value = $input_file.val();
      return input_file_value.length > 0;
    };

    timeline_input_image.prototype.check_thumbnails_count = function(additional_count) {
      if (this.get_current_thumbnails_count() + additional_count <= this.max_upload_image_count) {
        return true;
      }
      if (!this.error_modal) {
        this.error_modal = new App.elements.modal();
        this.error_modal.set_title('これ以上追加する事ができません');
        this.error_modal.set_body("<p>一度にアップロードできる数は " + this.max_upload_image_count + "個までです</p>");
        this.error_modal.set_footer_cancel_only('閉じる');
      }
      this.error_modal.show();
      return false;
    };

    return timeline_input_image;

  })(Backbone.View);

  App.elements.timeline.views.timeline_input_file = (function(_super) {
    __extends(timeline_input_file, _super);

    function timeline_input_file() {
      this.upload_url = __bind(this.upload_url, this);
      return timeline_input_file.__super__.constructor.apply(this, arguments);
    }

    timeline_input_file.prototype.el = '#postFile';

    timeline_input_file.prototype.collection = null;

    timeline_input_file.prototype.upload_url = function() {
      return "" + App.elements.timeline.config.api_base_url + "/file";
    };

    timeline_input_file.prototype.feed_type = 2;

    timeline_input_file.prototype.feed_model_objects_key = 'files';

    timeline_input_file.prototype.max_upload_image_count = 5;

    timeline_input_file.prototype.private_feed_collection_obj = App.elements.timeline.collections.private_file_feeds;

    timeline_input_file.prototype.thumbnail_view_class_name = 'input_file_thumbnail';

    timeline_input_file.prototype.thumbnail_view_posted_class_name = 'posted_input_file_thumbnail';

    timeline_input_file.prototype.accept_files = true;

    return timeline_input_file;

  })(App.elements.timeline.views.timeline_input_image);

  App.elements.timeline.views.feed_image = (function(_super) {
    __extends(feed_image, _super);

    function feed_image() {
      this.link_clicked = __bind(this.link_clicked, this);
      this.set_image_size = __bind(this.set_image_size, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      this.comment_url_base = __bind(this.comment_url_base, this);
      this.star_url_base = __bind(this.star_url_base, this);
      return feed_image.__super__.constructor.apply(this, arguments);
    }

    feed_image.prototype.el = '<li class="listItem"></li>';

    feed_image.prototype.model = null;

    feed_image.prototype.events = {
      'click .js-timeline-feed-images-feed-image-link': 'link_clicked'
    };

    feed_image.prototype.star_url_base = function() {
      return "" + App.elements.timeline.config.api_base_url + "/feed_image_star";
    };

    feed_image.prototype.comment_url_base = function() {
      return "" + App.elements.timeline.config.api_base_url + "/feed_image_comment";
    };

    feed_image.prototype.stars_view = null;

    feed_image.prototype.comments_button_view = null;

    feed_image.prototype.$img = null;

    feed_image.prototype.initialize = function() {
      return this.render();
    };

    feed_image.prototype.render = function() {
      this.$el.empty().html(App.elements.timeline.templates.feed_image.item({
        model: this.model,
        is_smartphone: App.current_user.is_smartphone
      }));
      this.$img = this.$el.find('.js-timeline-feed-images-feed-image-image');
      this.set_image_size();
      if (App.current_user.is_smartphone === false) {
        this.comments_button_view = new App.elements.timeline.views.comment_button({
          collection: this.model.get('comments'),
          can_comment: this.model.get('can_comment', true)
        });
        this.comments_button_view.on('clicked', this.link_clicked);
        this.$el.find('.js-timeline-feed-images-feed-image-comment-button-container').append(this.comments_button_view.$el);
        this.stars_view = new App.elements.timeline.views.star({
          model: this.model,
          url: "" + (this.star_url_base()) + "/" + (this.model.get('id'))
        });
        this.$el.find('.js-timeline-feed-images-feed-image-star-button-container').append(this.stars_view.$el);
      }
      return this.$el;
    };

    feed_image.prototype.set_image_size = function() {
      if (this.model.get('file').get('ratio_type') === 'horizontal') {
        return this.$img.removeClass('hLong').addClass('wLong');
      } else {
        return this.$img.removeClass('wLong').addClass('hLong');
      }
    };

    feed_image.prototype.link_clicked = function() {
      this.trigger('clicked', this.model);
      return false;
    };

    return feed_image;

  })(Backbone.View);

  App.elements.timeline.views.comment_button = (function(_super) {
    __extends(comment_button, _super);

    function comment_button() {
      this.link_clicked = __bind(this.link_clicked, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return comment_button.__super__.constructor.apply(this, arguments);
    }

    comment_button.prototype.el = '<span></span>';

    comment_button.prototype.events = {
      'click': 'link_clicked'
    };

    comment_button.prototype.collection = null;

    comment_button.prototype.can_comment = true;

    comment_button.prototype.initialize = function(options) {
      if ((options != null ? options.can_comment : void 0) != null) {
        this.can_comment = !!options.can_comment;
      }
      this.collection.on('add', this.render);
      this.collection.on('remove', this.render);
      return this.render();
    };

    comment_button.prototype.render = function() {
      this.$el.html(App.elements.timeline.templates.comment_button({
        comments: this.collection,
        can_comment: this.can_comment
      }));
      return this.$el;
    };

    comment_button.prototype.link_clicked = function() {
      this.trigger('clicked');
      return false;
    };

    return comment_button;

  })(Backbone.View);

  App.elements.timeline.views.star = (function(_super) {
    __extends(star, _super);

    function star() {
      this.hide_balloon = __bind(this.hide_balloon, this);
      this.toggle_balloon = __bind(this.toggle_balloon, this);
      this.inactivate = __bind(this.inactivate, this);
      this.animate = __bind(this.animate, this);
      this.activate = __bind(this.activate, this);
      this.toggle_star = __bind(this.toggle_star, this);
      this.change_active = __bind(this.change_active, this);
      this.is_active = __bind(this.is_active, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return star.__super__.constructor.apply(this, arguments);
    }

    star.prototype.el = '<span></span>';

    star.prototype.model = null;

    star.prototype.url = null;

    star.prototype.events = {
      'click a.js-timeline-feed-star-button': 'toggle_star',
      'click a.js-timeline-feed-star-stared-count-link': 'toggle_balloon'
    };

    star.prototype.$icon = null;

    star.prototype.in_prosessing = false;

    star.prototype.in_animation = false;

    star.prototype.$balloon = null;

    star.prototype.initialize = function(options) {
      this.url = options.url;
      this.render();
      this.listenTo(this.model.get('stars'), 'add', this.render);
      this.listenTo(this.model.get('stars'), 'remove', this.render);
      return App.$document.on('click', (function(_this) {
        return function(event) {
          if (_this.$balloon && _this.$balloon.find(event.target).length === 0) {
            _this.hide_balloon();
          }
          return true;
        };
      })(this));
    };

    star.prototype.render = function() {
      this.$el.html(App.elements.timeline.templates.star({
        stars: this.model.get('stars'),
        can_star: this.model.get('can_star', true)
      }));
      this.$icon = this.$el.find('.starIcon');
      this.change_active(false);
      return this.$el;
    };

    star.prototype.is_active = function() {
      return this.model.get('stars').is_starred();
    };

    star.prototype.change_active = function() {
      if (this.is_active()) {
        return this.activate(false);
      } else {
        return this.inactivate();
      }
    };

    star.prototype.toggle_star = function() {
      if (this.in_prosessing) {
        return false;
      }
      this.in_prosessing = true;
      if (!this.is_active()) {
        this.activate(!App.is_legacy_browser);
      } else {
        this.inactivate();
      }
      this.model.save({}, {
        url: typeof this.url === 'function' ? this.url() : this.url,
        error: (function(_this) {
          return function() {
            if (_this.model.get('stars').is_starred()) {
              _this.activate();
            } else {
              _this.inactivate();
            }
            return _this.model.set('can_star', false);
          };
        })(this),
        complete: (function(_this) {
          return function() {
            _this.render();
            return _this.in_prosessing = false;
          };
        })(this)
      });
      return false;
    };

    star.prototype.activate = function(enable_animation) {
      if (enable_animation == null) {
        enable_animation = false;
      }
      this.$el.addClass('active');
      if (enable_animation) {
        this.animate();
      }
      return this;
    };

    star.prototype.animate = function() {
      var $icon, position, width;
      if (this.in_animation) {
        return this;
      }
      this.in_animation = true;
      $icon = this.$icon.clone().insertAfter(this.$icon);
      this.$icon.css('visibility', 'hidden');
      position = this.$icon.position();
      width = $icon.width();
      $icon.css({
        position: 'absolute',
        top: "" + (position.top - 2) + "px",
        left: "" + (position.left - 2) + "px",
        'font-size': '16px',
        'line-height': 1
      }).animate({
        width: 0,
        top: "" + (position.top - 8) + "px",
        left: "" + (position.left - 3) + "px"
      }, {
        queue: true,
        duration: 100,
        easing: 'easeInOutCubic'
      }).animate({
        width: width,
        top: "" + (position.top - 2) + "px",
        left: "" + (position.left - 1) + "px"
      }, {
        queue: true,
        duration: 150,
        easing: 'easeInOutCubic',
        complete: (function(_this) {
          return function() {
            $icon.remove();
            _this.$icon.removeAttr('style');
            return _this.in_animation = false;
          };
        })(this)
      });
      return this;
    };

    star.prototype.inactivate = function() {
      this.$el.removeClass('active');
      return this;
    };

    star.prototype.toggle_balloon = function() {
      if (this.$balloon) {
        this.hide_balloon();
        return false;
      }
      this.$balloon = $(App.elements.timeline.templates.star_balloon.users({
        base_url: App.base_url,
        collection: this.model.get('stars')
      }));
      this.$el.after(this.$balloon);
      return false;
    };

    star.prototype.hide_balloon = function() {
      if (this.$balloon) {
        this.$balloon.remove();
        return this.$balloon = null;
      }
    };

    return star;

  })(Backbone.View);

  App.elements.timeline.views.comment = (function(_super) {
    __extends(comment, _super);

    function comment() {
      this.hide_balloon = __bind(this.hide_balloon, this);
      this.delete_comment = __bind(this.delete_comment, this);
      this.clear_form_errors = __bind(this.clear_form_errors, this);
      this.set_form_errors = __bind(this.set_form_errors, this);
      this.edit_comment = __bind(this.edit_comment, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      this.comment_url_base = __bind(this.comment_url_base, this);
      return comment.__super__.constructor.apply(this, arguments);
    }

    comment.prototype.el = '<li class="commentSection"></li>';

    comment.prototype.model = null;

    comment.prototype.parent_feed_base_page = null;

    comment.prototype.stars_view = null;

    comment.prototype.setting_balloon = null;

    comment.prototype.$body = null;

    comment.prototype.$input = null;

    comment.prototype.$wysiwyg = null;

    comment.prototype.comment_url_base = function() {
      return "" + App.elements.timeline.config.api_base_url + "/comment_star";
    };

    comment.prototype.in_editting = false;

    comment.prototype.in_deleting = false;

    comment.prototype.modal = null;

    comment.prototype.error_modal = null;

    comment.prototype.initialize = function(options) {
      if ((options != null ? options.parent_feed_base_page : void 0) != null) {
        this.parent_feed_base_page = options.parent_feed_base_page;
      }
      return this.render();
    };

    comment.prototype.render = function() {
      var links, url2link_class;
      url2link_class = new App.elements.url2link.views.url2link();
      this.model.set({
        body: url2link_class.done_url2link(this.model.get('body'))
      }, {
        silent: true
      });
      this.$el.empty().html(App.elements.timeline.templates.comment.container({
        base_url: App.base_url,
        model: this.model,
        parent_feed_base_page: this.parent_feed_base_page,
        is_smartphone: App.current_user.is_smartphone
      }));
      this.$body = this.$el.find('.js-timeline-feed-comment-body-container');
      this.stars_view = new App.elements.timeline.views.star({
        model: this.model,
        url: "" + (this.comment_url_base()) + "/" + (this.model.get('id'))
      });
      this.$el.find('.js-timeline-feed-comment-star-button-container').append(this.stars_view.render());
      links = [];
      if (this.model.get('can_edit')) {
        links.push({
          path: '#',
          label: '編集する',
          event_name: 'edit'
        });
      }
      if (this.model.get('can_delete')) {
        links.push({
          path: '#',
          label: '削除する',
          event_name: 'delete'
        });
      }
      if (links.length > 0) {
        this.setting_balloon = new App.elements.setting_balloon.views.setting_balloon({
          links: links
        });
        this.setting_balloon.on('edit', this.edit_comment);
        this.setting_balloon.on('delete', this.delete_comment);
        this.$el.find('.js-timeline-feed-comment-footer-data-container').append(this.setting_balloon.$el);
      }
      if (!!this.model.get('is_edited')) {
        this.$el.addClass('edited');
      } else {
        this.$el.removeClass('edited');
      }
      url2link_class.renew_observe_click_url2link();
      return this.$el;
    };

    comment.prototype.edit_comment = function() {
      var $textarea, home_url, param;
      this.hide_balloon();
      if (App.current_user.is_smartphone) {
        home_url = location.href.split(App.base_url + "/")[1];
        param = ["fd=" + this.model.get('feed_id'), "home_url=" + home_url].join("&");
        location.href = [App.base_url, "feed/show", this.model.get('feed_id')].join("/") + "?" + param;
      } else {
        if (this.in_editting) {
          return false;
        }
        this.in_editting = true;
        this.$edit_form = $(App.elements.timeline.templates.edit_feed.wysiwyg());
        $textarea = this.$edit_form.find('textarea');
        $textarea.html(_(this.model.get('body')).escape());
        this.$body.hide();
        this.$body.after(this.$edit_form);
        if (App.current_user.is_smartphone === false) {
          new App.elements.wysiwyg.views.wysiwyg_user({
            el: $textarea
          });
        }
        return this.$edit_form.on('submit', (function(_this) {
          return function() {
            var new_body;
            new_body = CKEDITOR.instances[$textarea.attr('name')].getData();
            if (_this.model.get('body') === new_body) {
              _this.$body.show();
              if (_this.$edit_form) {
                _this.$edit_form.remove();
              }
              _this.clear_form_errors();
              _this.in_editting = false;
              return false;
            }
            _this.model.save({
              body: new_body
            }, {
              success: function() {
                var url2link_class;
                url2link_class = new App.elements.url2link.views.url2link();
                _this.model.set({
                  body: url2link_class.done_url2link(_this.model.get('escape_body'))
                }, {
                  silent: true
                });
                _this.$body.html(_this.model.get('body')).show();
                _this.$edit_form.remove();
                if (!!_this.model.get('is_edited')) {
                  _this.$el.addClass('edited');
                }
                _this.clear_form_errors();
                return url2link_class.renew_observe_click_url2link();
              },
              error: function(model, jqXHR) {
                var err, error, k, key, messages, _ref, _ref1;
                messages = [];
                if ((jqXHR != null ? (_ref = jqXHR.response) != null ? _ref.errors : void 0 : void 0) != null) {
                  _ref1 = jqXHR.response.errors;
                  for (key in _ref1) {
                    error = _ref1[key];
                    if (_.isObject(error)) {
                      for (k in error) {
                        err = error[k];
                        messages.push(err);
                      }
                    } else {
                      messages.push(error);
                    }
                  }
                }
                if (messages.length === 0) {
                  messages.push('保存できませんでした。');
                }
                return _this.set_form_errors(messages.join(','));
              },
              complete: function() {
                return _this.in_editting = false;
              }
            });
            return false;
          };
        })(this));
      }
    };

    comment.prototype.set_form_errors = function(error_message) {
      var banner;
      this.clear_form_errors();
      banner = new App.elements.message_banner.views.message_banner({
        message: error_message,
        class_name: 'error',
        has_close: true
      });
      this.$edit_form.after(banner.$el);
      return this;
    };

    comment.prototype.clear_form_errors = function() {
      this.$el.find('.error').remove();
      return this;
    };

    comment.prototype.delete_comment = function() {
      if (!this.modal) {
        this.modal = new App.elements.modal();
        this.modal.add_container_class('jsModal-Dialog');
        this.modal.set_title('コメントの削除');
        this.modal.set_body(App.elements.timeline.templates.comment_modals.delete_body());
        this.modal.set_footer_cancel_delete(null, '削除');
        this.modal.on('cancel', this.hide_balloon);
        this.modal.on('decide', (function(_this) {
          return function() {
            if (_this.in_deleting) {
              return false;
            }
            _this.in_deleting = true;
            return _this.model.destroy({
              success: function() {
                _this.modal.hide();
                return _this.$el.slideUp(500, function() {
                  return _this.remove();
                });
              },
              error: function() {
                _this.modal.hide();
                if (!_this.error_modal) {
                  _this.error_modal = new App.elements.modal();
                  _this.error_modal.set_title('削除できませんでした');
                  _this.error_modal.set_body('<p>このコメントは既に削除されている可能性があります。</p>\n<p>画面をリロードしてください。</p>');
                  _this.error_modal.set_footer_cancel_only('閉じる');
                }
                return _this.error_modal.show();
              },
              complete: function() {
                return _this.in_deleting = false;
              }
            });
          };
        })(this));
      }
      this.modal.show();
      return false;
    };

    comment.prototype.hide_balloon = function() {
      this.setting_balloon.hide();
      return this;
    };

    return comment;

  })(Backbone.View);

  App.elements.timeline.views.comments = (function(_super) {
    __extends(comments, _super);

    function comments() {
      this.clear_form_errors = __bind(this.clear_form_errors, this);
      this.set_form_errors = __bind(this.set_form_errors, this);
      this.send_comment = __bind(this.send_comment, this);
      this.change_textarea_to_wysiwyg = __bind(this.change_textarea_to_wysiwyg, this);
      this.set_scroll_target = __bind(this.set_scroll_target, this);
      this.focus_wysiwyg = __bind(this.focus_wysiwyg, this);
      this.close_wysiwyg = __bind(this.close_wysiwyg, this);
      this.open_wysiwyg = __bind(this.open_wysiwyg, this);
      this.render_comment = __bind(this.render_comment, this);
      this.render_comments = __bind(this.render_comments, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return comments.__super__.constructor.apply(this, arguments);
    }

    comments.prototype.el = '<div class="commentContainer"></div>';

    comments.prototype.collection = null;

    comments.prototype.model = null;

    comments.prototype.url = null;

    comments.prototype.parent_feed_base_page = null;

    comments.prototype.$scroll_target = null;

    comments.prototype.$container = null;

    comments.prototype.$input = null;

    comments.prototype.$wysiwyg = null;

    comments.prototype.$dummy_input_container = null;

    comments.prototype.$wysiwyg_container = null;

    comments.prototype.comment_was_be_wysiwyg = false;

    comments.prototype.comment_button_view = null;

    comments.prototype.rendered_comment_views = {};

    comments.prototype.events = {
      'focus .js-timeline-feed-comments-dummy-comment-input': 'open_wysiwyg',
      'click .js-timeline-feed-comments-post-button': 'send_comment'
    };

    comments.prototype.initialize = function(options) {
      this.rendered_comment_views = {};
      this.url = options.url;
      this.$scroll_target = $('body');
      if ((options != null ? options.comment_button_view : void 0) != null) {
        this.comment_button_view = options.comment_button_view;
        this.comment_button_view.on('clicked', this.open_wysiwyg);
      }
      if ((options != null ? options.parent_feed_base_page : void 0) != null) {
        this.parent_feed_base_page = options.parent_feed_base_page;
      }
      return this.render();
    };

    comments.prototype.render = function() {
      this.$container = $(App.elements.timeline.templates.comments.list_container());
      this.$el.append(this.$container);
      if (this.model.get('can_comment', true)) {
        this.$input = $(App.elements.timeline.templates.comments.input({
          model: this.model,
          base_url: App.base_url,
          user_data: App.classes.current_user.get_data(),
          is_smartphone: App.current_user.is_smartphone,
          home_url: 'home_url=' + location.href.split(App.base_url + "/")[1]
        }));
        this.$el.append(this.$input);
        App.functions.disable_form_multiple_clicks(this.$input.find('.js-timeline-feed-comments-post-button'));
        this.$dummy_input_container = this.$input.find('.js-timeline-feed-comments-dummy-comment-input-container');
        this.$wysiwyg_container = this.$input.find('.js-timeline-feed-comments-new-comment-textarea-container');
        this.$wysiwyg = this.$wysiwyg_container.find('.js-timeline-feed-comments-new-comment-textarea');
      }
      return this.$el;
    };

    comments.prototype.render_comments = function(limit, sort_asc) {
      var method;
      if (limit == null) {
        limit = null;
      }
      if (sort_asc == null) {
        sort_asc = true;
      }
      if (limit === null) {
        return _(this.collection.sortBy((function(_this) {
          return function(comment) {
            return -comment.get('id');
          };
        })(this))).each((function(_this) {
          return function(comment) {
            if (_this.rendered_comment_views[comment.cid] == null) {
              return _this.render_comment(comment, 'prepend');
            }
          };
        })(this));
      } else {
        method = sort_asc ? 'first' : 'last';
        return _(_(this.collection.filter((function(_this) {
          return function(comment) {
            return _this.rendered_comment_views[comment.cid] == null;
          };
        })(this)))[method](+limit)).each((function(_this) {
          return function(comment) {
            return _this.render_comment(comment);
          };
        })(this));
      }
    };

    comments.prototype.render_comment = function(comment_model, method) {
      var comment_view;
      if (method == null) {
        method = 'append';
      }
      comment_view = new App.elements.timeline.views.comment({
        model: comment_model,
        parent_feed_base_page: this.parent_feed_base_page
      });
      this.rendered_comment_views[comment_model.cid] = comment_view;
      return this.$container[method](comment_view.$el);
    };

    comments.prototype.open_wysiwyg = function() {
      if (App.current_user.is_smartphone) {
        location.href = [App.base_url, "feed/show", this.model.get('id')].join("/");
      } else {
        if (!this.model.get('can_comment', true)) {
          return false;
        }
        this.$dummy_input_container.hide();
        this.change_textarea_to_wysiwyg();
        this.$wysiwyg_container.show();
        return this.focus_wysiwyg();
      }
    };

    comments.prototype.close_wysiwyg = function() {
      if (!this.model.get('can_comment', true)) {
        return false;
      }
      this.$wysiwyg_container.hide();
      return this.$dummy_input_container.show();
    };

    comments.prototype.focus_wysiwyg = function() {
      if (!this.model.get('can_comment', true)) {
        return false;
      }
      return this.$scroll_target.animate({
        scrollTop: this.$input.offset().top - 12
      }, 300, (function(_this) {
        return function() {
          return setTimeout((function() {
            return CKEDITOR.instances[_this.$wysiwyg.attr('name')].focus();
          }), 300);
        };
      })(this));
    };

    comments.prototype.set_scroll_target = function($scroll_target) {
      return this.$scroll_target = $scroll_target;
    };

    comments.prototype.change_textarea_to_wysiwyg = function() {
      if (!this.model.get('can_comment', true)) {
        return this;
      }
      if (this.comment_was_be_wysiwyg) {
        return this;
      }
      new App.elements.wysiwyg.views.wysiwyg_user({
        el: this.$wysiwyg
      });
      this.comment_was_be_wysiwyg = true;
      return this;
    };

    comments.prototype.send_comment = function() {
      var comment;
      if (!this.model.get('can_comment', true)) {
        return false;
      }
      comment = new App.elements.timeline.models.feed_comment();
      comment.save({
        body: CKEDITOR.instances[this.$wysiwyg.attr('name')].getData()
      }, {
        url: _.result(this, 'url'),
        success: (function(_this) {
          return function() {
            var url2link_class;
            url2link_class = new App.elements.url2link.views.url2link();
            comment.set({
              body: url2link_class.done_url2link(comment.get('escape_body'))
            }, {
              silent: true
            });
            _this.collection.add(comment);
            _this.render_comment(comment);
            _this.$wysiwyg_container.find('iframe').contents().find('body').html('');
            _this.close_wysiwyg();
            _this.clear_form_errors();
            if (_this.comment_button_view != null) {
              _this.comment_button_view.render();
            }
            return url2link_class.renew_observe_click_url2link();
          };
        })(this),
        error: (function(_this) {
          return function(model, jqXHR) {
            var err, error, k, key, messages, _ref, _ref1;
            messages = [];
            if ((jqXHR != null ? (_ref = jqXHR.response) != null ? _ref.errors : void 0 : void 0) != null) {
              _ref1 = jqXHR.response.errors;
              for (key in _ref1) {
                error = _ref1[key];
                if (_.isObject(error)) {
                  for (k in error) {
                    err = error[k];
                    messages.push(err);
                  }
                } else {
                  messages.push(error);
                }
              }
            }
            if (messages.length === 0) {
              messages.push('保存できませんでした。');
            }
            return _this.set_form_errors(messages.join(','));
          };
        })(this),
        complete: (function(_this) {
          return function() {
            return App.functions.disable_form_multiple_clicks($('.js-timeline-feed-comments-post-button'));
          };
        })(this)
      });
      return false;
    };

    comments.prototype.set_form_errors = function(error_message) {
      var banner;
      this.clear_form_errors();
      banner = new App.elements.message_banner.views.message_banner({
        message: error_message,
        class_name: 'error',
        has_close: true
      });
      this.$el.find('.boxFeedComment.post .postModule').after(banner.$el);
      return this;
    };

    comments.prototype.clear_form_errors = function() {
      this.$el.find('.error').remove();
      return this;
    };

    return comments;

  })(Backbone.View);

  App.elements.timeline.views.feed_images = (function(_super) {
    __extends(feed_images, _super);

    function feed_images() {
      this.hide_balloon = __bind(this.hide_balloon, this);
      this.delete_complete = __bind(this.delete_complete, this);
      this.delete_feed_image = __bind(this.delete_feed_image, this);
      this.clear_error_message_banners = __bind(this.clear_error_message_banners, this);
      this.edit_feed_image = __bind(this.edit_feed_image, this);
      this.load_more_comments = __bind(this.load_more_comments, this);
      this.get_model_from_collection = __bind(this.get_model_from_collection, this);
      this.show_next_modal = __bind(this.show_next_modal, this);
      this.show_previous_modal = __bind(this.show_previous_modal, this);
      this.open_modal = __bind(this.open_modal, this);
      this.set_class = __bind(this.set_class, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      this.comment_url_base = __bind(this.comment_url_base, this);
      this.star_url_base = __bind(this.star_url_base, this);
      return feed_images.__super__.constructor.apply(this, arguments);
    }

    feed_images.prototype.el = '<ul class="listContainer photoListContainer"></ul>';

    feed_images.prototype.collection = null;

    feed_images.prototype.show_all_view = null;

    feed_images.prototype.stars_view = null;

    feed_images.prototype.comments_view = null;

    feed_images.prototype.balloon = null;

    feed_images.prototype.error_message_banners = [];

    feed_images.prototype.modal = null;

    feed_images.prototype.current_modal_feed_image = null;

    feed_images.prototype.in_editting = false;

    feed_images.prototype.in_submitting = false;

    feed_images.prototype.show_related_icons = false;

    feed_images.prototype.star_url_base = function() {
      return "" + App.elements.timeline.config.api_base_url + "/feed_image_star";
    };

    feed_images.prototype.comment_url_base = function() {
      return "" + App.elements.timeline.config.api_base_url + "/feed_image_comment";
    };

    feed_images.prototype.initialize = function() {
      this.modal = new App.elements.modal({
        modal_classes: ['dialogPhoto', 'multiplePhotoModal'],
        set_buttons: false
      });
      this.modal.disable_auto_set_top();
      this.modal.add_container_class('jsModal-Photo');
      this.modal.add_header_heding_class('postModule');
      this.modal.set_title('', 'title textContent');
      this.modal.$el.on('click', '.prev a', this.show_previous_modal);
      this.modal.$el.on('click', '.next a', this.show_next_modal);
      return this.render();
    };

    feed_images.prototype.render = function() {
      this.$el.empty();
      this.set_class();
      this.collection.forEach((function(_this) {
        return function(feed_image) {
          var feed_image_view;
          feed_image_view = new App.elements.timeline.views.feed_image({
            model: feed_image
          });
          feed_image_view.on('clicked', _this.open_modal);
          return _this.$el.append(feed_image_view.$el);
        };
      })(this));
      return this.$el;
    };

    feed_images.prototype.set_class = function() {
      var add_class_name;
      _(['item1', 'item2', 'item3', 'item4']).each((function(_this) {
        return function(class_name) {
          return _this.$el.removeClass(class_name);
        };
      })(this));
      switch (this.collection.length) {
        case 0:
          add_class_name = '';
          break;
        case 1:
          add_class_name = 'item1';
          break;
        case 2:
          add_class_name = 'item2';
          break;
        case 3:
          add_class_name = 'item3';
          break;
        default:
          add_class_name = 'item4';
      }
      return this.$el.addClass(add_class_name);
    };

    feed_images.prototype.open_modal = function(feed_image) {
      var $comments_container, comments, links, url2link_class;
      this.modal.set_title(feed_image.get('file').get('caption'));
      this.modal.set_body(App.elements.timeline.templates.feed_image_modal.container({
        base_url: App.base_url,
        model: feed_image,
        has_multiple: this.collection.length > 1,
        show_related_icons: this.show_related_icons
      }));
      this.modal.$el.keypress((function(_this) {
        return function(event) {
          if (event.which && event.which === 13 || event.keyCode && event.keyCode === 13) {
            return false;
          }
        };
      })(this));
      links = [];
      if (feed_image.get('can_edit')) {
        links.push({
          path: '#',
          label: '編集する',
          event_name: 'edit'
        });
      }
      if (feed_image.get('can_delete')) {
        links.push({
          path: '#',
          label: '削除する',
          event_name: 'delete'
        });
      }
      if (links.length > 0) {
        this.balloon = new App.elements.setting_balloon.views.setting_balloon({
          links: links
        });
        this.balloon.on('edit', this.edit_feed_image);
        this.balloon.on('delete', this.delete_feed_image);
        this.modal.$el.find('.js-timeline-image-feed-feed-image-modal-feed-article-data-container').append(this.balloon.$el);
      }
      comments = feed_image.get('comments');
      if (comments.length - App.elements.timeline.config.comments.initial_count > 0) {
        if (this.show_all_view != null) {
          this.show_all_view.remove();
        }
        this.show_all_view = new App.elements.timeline.views.elements.show_all({
          collection: comments
        });
        this.show_all_view.on('show_all', this.load_more_comments);
        this.modal.$el.find('.js-timeline-image-feed-feed-image-modal-comments-wrapper').prepend(this.show_all_view.$el);
      }
      if (this.comment_button_view != null) {
        this.comment_button_view.remove();
      }
      this.comment_button_view = new App.elements.timeline.views.comment_button({
        collection: feed_image.get('comments'),
        can_comment: feed_image.get('can_comment', true)
      });
      this.modal.$el.find('.js-timeline-image-feed-feed-image-modal-feed-article-comment-button').append(this.comment_button_view.$el);
      if (this.stars_view != null) {
        this.stars_view.remove();
      }
      this.stars_view = new App.elements.timeline.views.star({
        model: feed_image,
        url: "" + (this.star_url_base()) + "/" + (feed_image.get('id'))
      });
      this.modal.$el.find('.js-timeline-image-feed-feed-image-modal-feed-article-star').append(this.stars_view.$el);
      if (this.comments_view != null) {
        this.comments_view.remove();
      }
      this.comments_view = new App.elements.timeline.views.comments({
        collection: feed_image.get('comments'),
        model: feed_image,
        url: "" + (this.comment_url_base()) + "/" + (feed_image.get('id')),
        comment_button_view: this.comment_button_view
      });
      $comments_container = this.modal.$el.find('.js-timeline-image-feed-feed-image-modal-comments-container');
      this.comments_view.set_scroll_target($comments_container);
      $comments_container.append(this.comments_view.$el);
      this.comments_view.render_comments(App.elements.timeline.config.comments.initial_count, false);
      this.modal.show();
      url2link_class = new App.elements.url2link.views.url2link();
      url2link_class.renew_observe_click_url2link(true);
      return this.current_modal_feed_image = feed_image;
    };

    feed_images.prototype.show_previous_modal = function() {
      this.open_modal(this.get_model_from_collection('previous'));
      return false;
    };

    feed_images.prototype.show_next_modal = function() {
      this.open_modal(this.get_model_from_collection('next'));
      return false;
    };

    feed_images.prototype.get_model_from_collection = function(method) {
      var index, model;
      if (method == null) {
        method = 'next';
      }
      index = this.collection.indexOf(this.current_modal_feed_image);
      if (method === 'next') {
        index++;
      } else {
        index--;
      }
      model = this.collection.at(index);
      if (!model) {
        if (method === 'next') {
          model = this.collection.at(0);
        } else {
          model = this.collection.at(this.collection.length - 1);
        }
      }
      return model;
    };

    feed_images.prototype.load_more_comments = function() {
      this.comments_view.render_comments();
      return this.show_all_view.$el.slideUp(300, (function(_this) {
        return function() {
          _this.show_all_view.remove();
          return delete _this.show_all_view;
        };
      })(this));
    };

    feed_images.prototype.edit_feed_image = function() {
      var $form, $h2, $input;
      if (this.in_editting) {
        return false;
      }
      this.in_editting = true;
      if (this.clear_error_message_banners != null) {
        this.clear_error_message_banners();
      }
      this.hide_balloon();
      $h2 = this.modal.$el.find('.heading h2.title');
      $form = $(App.elements.timeline.templates.edit_feed.text());
      $input = $form.find('input');
      $input.val($h2.text());
      $h2.after($form);
      $h2.hide();
      this.in_submitting = false;
      return $form.on('submit', (function(_this) {
        return function() {
          if (_this.in_submitting) {
            return false;
          }
          _this.in_submitting = true;
          if (_this.clear_error_message_banners != null) {
            _this.clear_error_message_banners();
          }
          $form.find('.js-timeline-feed-edit-text-input-form-error-message').remove();
          _this.current_modal_feed_image.save({
            caption: $input.val()
          }, {
            success: function() {
              $form.remove();
              $h2.html(_this.current_modal_feed_image.get('file').get('caption')).show();
              if (_this.current_modal_feed_image.get('is_edited')) {
                return _this.modal.$el.find('.js-timeline-image-feed-feed-image-modal-feed-article-content').addClass('edited');
              }
            },
            error: function(model, jqXHR) {
              var err, error, error_message_banner, k, key, messages, _ref, _ref1;
              messages = [];
              if ((jqXHR != null ? (_ref = jqXHR.response) != null ? _ref.errors : void 0 : void 0) != null) {
                _ref1 = jqXHR.response.errors;
                for (key in _ref1) {
                  error = _ref1[key];
                  if (_.isObject(error)) {
                    for (k in error) {
                      err = error[k];
                      messages.push(err);
                    }
                  } else {
                    messages.push(error);
                  }
                }
              }
              if (messages.length === 0) {
                messages.push('キャプションの更新に失敗しました。文字数がオーバーしているか、フィードが削除された可能性があります。');
              }
              error_message_banner = new App.elements.message_banner.views.message_banner({
                message: messages.join(','),
                class_name: 'error',
                has_close: false
              });
              _this.error_message_banners.push(error_message_banner);
              return $form.append(error_message_banner.$el);
            },
            complete: function() {
              _this.in_editting = false;
              return _this.in_submitting = false;
            }
          });
          return false;
        };
      })(this));
    };

    feed_images.prototype.clear_error_message_banners = function() {
      var banner, _i, _len, _ref;
      _ref = this.error_message_banners;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        banner = _ref[_i];
        banner.close();
      }
      return this.error_message_banners = [];
    };

    feed_images.prototype.delete_feed_image = function() {
      var modal;
      modal = new App.elements.modal();
      modal.add_container_class('jsModal-Dialog');
      modal.set_title('画像の削除');
      modal.set_body(App.elements.timeline.templates.feed_image_modals.delete_body());
      modal.set_footer_cancel_delete(null, '削除');
      modal.show();
      modal.on('cancel', this.hide_balloon);
      return modal.on('decide', (function(_this) {
        return function() {
          return _this.current_modal_feed_image.destroy({
            success: function() {
              modal.hide();
              _this.modal.hide();
              return _this.delete_complete();
            },
            error: function() {
              return log('err', arguments);
            }
          });
        };
      })(this));
    };

    feed_images.prototype.delete_complete = function() {
      if (this.collection.length > 0) {
        this.render();
      } else {
        this.trigger('image_empty');
      }
      return this.current_modal_feed_image = null;
    };

    feed_images.prototype.hide_balloon = function() {
      if (this.balloon) {
        return this.balloon.hide();
      }
    };

    return feed_images;

  })(Backbone.View);

  App.elements.timeline.views.feed_files = (function(_super) {
    __extends(feed_files, _super);

    function feed_files() {
      this.delete_feed_file = __bind(this.delete_feed_file, this);
      this.edit_feed_file = __bind(this.edit_feed_file, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return feed_files.__super__.constructor.apply(this, arguments);
    }

    feed_files.prototype.el = '<ul class="summary fileListContainer"></ul>';

    feed_files.prototype.collection = null;

    feed_files.prototype.modal = null;

    feed_files.forms = {};

    feed_files.prototype.initialize = function() {
      this.forms = {};
      return this.render();
    };

    feed_files.prototype.render = function() {
      this.$el.empty();
      this.collection.each((function(_this) {
        return function(feed_file) {
          var $item, links, setting_balloon;
          $item = $(App.elements.timeline.templates.file_feed.item({
            model: feed_file,
            file_size: App.classes.num.quantity(feed_file.get('file').get('size'))
          }));
          if (App.current_user.is_smartphone === false) {
            links = [];
            if (feed_file.get('can_edit')) {
              links.push({
                path: '#',
                label: '編集する',
                event_name: 'edit_feed_file'
              });
            }
            if (feed_file.get('can_delete')) {
              links.push({
                path: '#',
                label: '削除する',
                event_name: 'delete_feed_file'
              });
            }
            if (links.length > 0) {
              setting_balloon = new App.elements.setting_balloon.views.setting_balloon({
                links: links
              });
              setting_balloon.on('edit_feed_file', function() {
                return _this.edit_feed_file(feed_file, $item, setting_balloon);
              });
              setting_balloon.on('delete_feed_file', function() {
                return _this.delete_feed_file(feed_file, $item, setting_balloon);
              });
              $item.find('.setting').append(setting_balloon.$el);
            }
          }
          return _this.$el.append($item);
        };
      })(this));
      return this.$el;
    };

    feed_files.prototype.edit_feed_file = function(feed_file, $item, setting_balloon) {
      var $caption, $form, $input;
      setting_balloon.hide();
      if (this.forms[feed_file.cid] != null) {
        return false;
      }
      $form = $(App.elements.timeline.templates.file_feed.caption_input({
        model: feed_file
      }));
      this.forms[feed_file.cid] = $form;
      $input = $form.find('input');
      $caption = $item.find('.js-timeline-feed-file-item-caption-container');
      $form.find('.js-timeline-feed-file-item-caption-edit-button').on('click', (function(_this) {
        return function() {
          _this.$el.find('.error').remove();
          if (feed_file.get('file').get('caption') === $input.val()) {
            $caption.text(feed_file.get('caption')).show();
            $form.remove();
            _this.forms[feed_file.cid] = null;
            return false;
          }
          feed_file.save({
            caption: $input.val()
          }, {
            success: function() {
              $caption.text(feed_file.get('caption')).show();
              $form.remove();
              _this.forms[feed_file.cid] = null;
              if (feed_file.get('is_edited')) {
                return $item.addClass('edited');
              }
            },
            error: function(model, jqXHR) {
              var error, error_obj, errors, field, message_banner, rule, _ref, _ref1;
              errors = [];
              if ((jqXHR != null ? (_ref = jqXHR.response) != null ? _ref.errors : void 0 : void 0) != null) {
                _ref1 = jqXHR.response.errors;
                for (field in _ref1) {
                  error_obj = _ref1[field];
                  for (rule in error_obj) {
                    error = error_obj[rule];
                    errors.push(error);
                  }
                }
              }
              if (errors.length === 0) {
                errors.push('保存できませんでした。キャプションが長すぎる可能性があります。');
              }
              message_banner = new App.elements.message_banner.views.message_banner({
                message: errors.join(','),
                class_name: 'error',
                has_close: true
              });
              return $form.after(message_banner.$el);
            }
          });
          return false;
        };
      })(this));
      $caption.hide();
      $caption.after($form);
      return $input.focus();
    };

    feed_files.prototype.delete_feed_file = function(feed_file, $item, setting_balloon) {
      setting_balloon.hide();
      if (!this.modal) {
        this.modal = new App.elements.modal();
        this.modal.set_title('ファイルの削除');
        this.modal.set_body('この動作はもとに戻せませんがよろしいですか？');
        this.modal.set_footer_cancel_delete(null, '削除');
      }
      this.modal.on('decide', (function(_this) {
        return function() {
          return feed_file.destroy({
            success: function() {
              $item.fadeOut(300, function() {
                return $item.remove();
              });
              if (_this.collection.length <= 0) {
                return _this.trigger('file_empty');
              }
            },
            complete: function() {
              return _this.modal.hide();
            }
          });
        };
      })(this));
      this.modal.on('cancel', (function(_this) {
        return function() {
          return _this.modal.off('decide');
        };
      })(this));
      return this.modal.show();
    };

    return feed_files;

  })(Backbone.View);

  App.elements.timeline.views.timeline_feed = (function(_super) {
    __extends(timeline_feed, _super);

    function timeline_feed() {
      this.hide_balloon = __bind(this.hide_balloon, this);
      this.delete_feed_complete = __bind(this.delete_feed_complete, this);
      this.delete_feed = __bind(this.delete_feed, this);
      this.delete_feed_confirm = __bind(this.delete_feed_confirm, this);
      this.edit_complete = __bind(this.edit_complete, this);
      this.clear_form_errors = __bind(this.clear_form_errors, this);
      this.set_form_errors = __bind(this.set_form_errors, this);
      this.save_edit_feed = __bind(this.save_edit_feed, this);
      this.edit_via_textarea = __bind(this.edit_via_textarea, this);
      this.edit_via_wysiwyg = __bind(this.edit_via_wysiwyg, this);
      this.edit_feed = __bind(this.edit_feed, this);
      this.load_more_comments = __bind(this.load_more_comments, this);
      this.get_body_content = __bind(this.get_body_content, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      this.comment_url_base = __bind(this.comment_url_base, this);
      this.star_url_base = __bind(this.star_url_base, this);
      return timeline_feed.__super__.constructor.apply(this, arguments);
    }

    timeline_feed.prototype.el = '<li class="sectionFeed"></li>';

    timeline_feed.prototype.model = null;

    timeline_feed.prototype.stars_view = null;

    timeline_feed.prototype.comment_button_view = null;

    timeline_feed.prototype.comments_view = null;

    timeline_feed.prototype.images_view = null;

    timeline_feed.prototype.files_view = null;

    timeline_feed.prototype.show_all_view = null;

    timeline_feed.prototype.in_editting = false;

    timeline_feed.prototype.balloon = null;

    timeline_feed.prototype.delete_error_modal = null;

    timeline_feed.prototype.$el = null;

    timeline_feed.prototype.$body = null;

    timeline_feed.prototype.$timestamp = null;

    timeline_feed.prototype.$edit_form = null;

    timeline_feed.prototype.before_edit_body = null;

    timeline_feed.prototype.star_url_base = function() {
      return "" + App.elements.timeline.config.api_base_url + "/feed_star";
    };

    timeline_feed.prototype.comment_url_base = function() {
      return "" + App.elements.timeline.config.api_base_url + "/feed_comment";
    };

    timeline_feed.prototype.initialize = function(options) {
      this.show_communication_link_mores = options.show_communication_link_mores;
      this.show_detail_link = options.show_detail_link;
      return this.render();
    };

    timeline_feed.prototype.render = function() {
      var balloon_options, body_template, comments, feed_class, feed_header, linkMores, section_class_name, show_summary;
      feed_class = 'feedTweet';
      section_class_name = null;
      body_template = null;
      show_summary = true;
      linkMores = [];
      if (this.model.get('is_truncate_body') && this.show_detail_link) {
        if (App.current_user.is_smartphone) {
          linkMores.push({
            url: ("" + (App.elements.timeline.config.linkMore.show_detail_url + this.model.get('id'))) + '?home_url=' + location.href.split(App.base_url + '/')[1],
            anchor: "" + App.elements.timeline.config.linkMore.show_detail_anchor
          });
        } else {
          linkMores.push({
            url: "" + (App.elements.timeline.config.linkMore.show_detail_url + this.model.get('id')),
            anchor: "" + App.elements.timeline.config.linkMore.show_detail_anchor
          });
        }
      }
      if (this.model.is_image_feed()) {
        feed_class = 'feedPhoto';
        section_class_name = 'photo';
      } else if (this.model.is_file_feed()) {
        feed_class = 'feedFile';
        section_class_name = 'file';
        body_template = App.elements.timeline.templates.file_feed.body;
      } else if (this.model.is_system_feed()) {
        if (this.model.get('area') === 1 && this.model.get('community') !== null) {
          section_class_name = 'community';
        } else if (this.model.get('area') === 2 && this.model.get('event') !== null) {
          section_class_name = 'event';
        }
        switch (this.model.get('feed_system_type_id')) {
          case 2:
          case 3:
          case 5:
            body_template = App.elements.timeline.templates.system_feed.container;
            break;
          case 4:
          case 6:
            feed_header = function() {};
            body_template = App.elements.timeline.templates.system_feed.remove_container;
            break;
          case 7:
            show_summary = false;
        }
      } else if (this.model.is_communication_feed()) {
        feed_class = 'feedCommunication feedSystem';
        if (typeof this.model.get('body') === 'string') {
          section_class_name = 'communication';
          if (this.show_communication_link_mores) {
            linkMores.push({
              url: "" + (App.elements.timeline.config.linkMore.show_others_answer_url + this.model.get('profile_id')),
              anchor: "" + App.elements.timeline.config.linkMore.show_others_answer_anchor
            });
          }
        } else {
          section_class_name = 'question';
          body_template = App.elements.timeline.templates.communication_question_feed.body;
          feed_header = App.elements.timeline.templates.communication_question_feed.feed_header;
        }
        if (this.model.get('show_edit_communication_link')) {
          linkMores.push({
            url: "" + (App.elements.timeline.config.linkMore.edit_communication_url + this.model.get('profile_id')),
            anchor: "" + App.elements.timeline.config.linkMore.edit_communication_anchor
          });
        }
      }
      _(['feedTweet', 'feedPhoto', 'feedFile', 'feedCommunication', 'feedSystem']).each((function(_this) {
        return function(feed_class_name) {
          return _this.$el.removeClass(feed_class_name);
        };
      })(this));
      this.$el.addClass(feed_class);
      this.$el.html(App.elements.timeline.templates.feed.container({
        model: this.model,
        body_template: body_template,
        show_summary: show_summary,
        show_related: App.elements.timeline.config.show_related,
        base_url: App.base_url,
        section_class_name: section_class_name,
        linkMores: linkMores,
        body_content: this.get_body_content(),
        base_page: App.elements.timeline.config.base_page,
        feed_header: feed_header,
        site_name: App.sns.name,
        is_smartphone: App.current_user.is_smartphone
      }));
      balloon_options = [];
      if (this.model.get('can_edit', false)) {
        if (this.model.is_communication_feed()) {
          balloon_options.push({
            path: "" + (App.elements.timeline.config.linkMore.edit_communication_url + this.model.get('profile_id')),
            label: '編集する',
            event_name: 'edit'
          });
        } else {
          balloon_options.push({
            path: '#',
            label: '編集する',
            event_name: 'edit'
          });
        }
      }
      if (this.model.get('can_delete', false)) {
        balloon_options.push({
          path: '#',
          label: '削除する',
          event_name: 'delete'
        });
      }
      if (balloon_options.length > 0) {
        this.balloon = new App.elements.setting_balloon.views.setting_balloon({
          links: balloon_options
        });
        this.balloon.on('edit', this.edit_feed);
        this.balloon.on('delete', this.delete_feed_confirm);
        this.$el.find('.js-timeline-feed-footer-data-container').append(this.balloon.$el);
      }
      this.$body = this.$el.find('.js-timeline-feed-body-html-container');
      this.$timestamp = this.$el.find('.js-timeline-feed-footer-timestamp-container');
      if (this.model.is_image_feed()) {
        this.images_view = new App.elements.timeline.views.feed_images({
          collection: this.model.get('images')
        });
        this.images_view.once('image_empty', this.delete_feed_complete);
        this.$el.find('.js-timeline-feed-body-container').after(this.images_view.$el);
      }
      if (this.model.is_file_feed()) {
        this.files_view = new App.elements.timeline.views.feed_files({
          collection: this.model.get('files')
        });
        this.files_view.once('file_empty', this.delete_feed_complete);
        this.$body.after(this.files_view.$el);
      }
      this.stars_view = new App.elements.timeline.views.star({
        model: this.model,
        url: "" + (this.star_url_base()) + "/" + (this.model.get('id'))
      });
      this.$el.find('.js-timeline-feed-star-button-container').append(this.stars_view.$el);
      if (App.current_user.is_smartphone === false) {
        this.comment_button_view = new App.elements.timeline.views.comment_button({
          collection: this.model.get('comments'),
          can_comment: this.model.get('can_comment', true)
        });
        this.$el.find('.js-timeline-feed-comment-button-container').append(this.comment_button_view.$el);
      }
      comments = this.model.get('comments');
      if (comments.length - App.elements.timeline.config.comments.initial_count > 0) {
        this.show_all_view = new App.elements.timeline.views.elements.show_all({
          collection: comments
        });
        this.show_all_view.on('show_all', this.load_more_comments);
        this.$el.append(this.show_all_view.$el);
      }
      this.comments_view = new App.elements.timeline.views.comments({
        collection: this.model.get('comments'),
        model: this.model,
        url: "" + (this.comment_url_base()) + "/" + (this.model.get('id')),
        comment_button_view: this.comment_button_view,
        parent_feed_base_page: App.elements.timeline.config.base_page
      });
      this.$el.append(this.comments_view.$el);
      this.comments_view.render_comments(App.elements.timeline.config.comments.initial_count, false);
      return this.$el;
    };

    timeline_feed.prototype.get_body_content = function() {
      var body_content;
      if (!this.model.get('is_truncate_body')) {
        body_content = this.model.get('row_body');
      } else {
        body_content = this.model.get('body');
      }
      if (this.model.is_image_feed()) {
        body_content = App.elements.timeline.templates.feed_image.title({
          title: body_content
        });
      } else if (this.model.is_file_feed()) {
        body_content = App.elements.timeline.templates.file_feed.title({
          title: body_content
        });
      }
      return body_content;
    };

    timeline_feed.prototype.load_more_comments = function() {
      this.comments_view.render_comments();
      return this.show_all_view.$el.slideUp(300, (function(_this) {
        return function() {
          _this.show_all_view.remove();
          return delete _this.show_all_view;
        };
      })(this));
    };

    timeline_feed.prototype.edit_feed = function() {
      var home_url, param, type_text;
      this.balloon.hide();
      if (App.current_user.is_smartphone) {
        switch (+this.model.get('type')) {
          case 0:
            type_text = 'tweet';
            break;
          case 1:
            type_text = 'photo';
            break;
          case 2:
            type_text = 'file';
        }
        home_url = location.href.split(App.base_url + "/")[1];
        param = ["fd=" + this.model.get('id'), "type=" + type_text, "home_url=" + home_url].join("&");
        location.href = [App.base_url, "feed/show", this.model.get('id')].join("/") + "?" + param;
      } else {
        if (this.in_editting) {
          return false;
        }
        this.in_editting = true;
        switch (+this.model.get('type')) {
          case 0:
            return this.edit_via_wysiwyg(_(this.model.get('row_body')).escape());
          case 1:
          case 2:
            return this.edit_via_textarea();
          case 4:
            return location.href = App.elements.timeline.config.linkMore.edit_communication_url + this.model.get('profile_id');
        }
      }
    };

    timeline_feed.prototype.edit_via_wysiwyg = function(current_input_html) {
      var $textarea;
      if (current_input_html == null) {
        current_input_html = '';
      }
      this.$edit_form = $(App.elements.timeline.templates.edit_feed.wysiwyg());
      $textarea = this.$edit_form.find('textarea');
      $textarea.html(current_input_html);
      this.$body.hide();
      this.$body.after(this.$edit_form);
      new App.elements.wysiwyg.views.wysiwyg_user({
        el: $textarea
      });
      return this.$edit_form.on('submit', (function(_this) {
        return function() {
          _this.save_edit_feed(CKEDITOR.instances[$textarea.attr('name')].getData());
          return false;
        };
      })(this));
    };

    timeline_feed.prototype.edit_via_textarea = function() {
      var $input, $title;
      this.$edit_form = $(App.elements.timeline.templates.edit_feed.textarea());
      $input = this.$edit_form.find('textarea');
      $input.val(_.unescape(this.model.get('row_body')).replace(/&#039;/ig, "'").replace(/(?:<br\s?\/?>|<a[^>]+>|<\/a>)/ig, ''));
      if (this.model.is_image_feed()) {
        this.$body.hide();
        this.$body.after(this.$edit_form);
      } else if (this.model.is_file_feed()) {
        $title = this.$body.find('.js-timeline-feed-file-feed-body-title');
        $title.hide();
        $title.after(this.$edit_form);
      }
      $input.focus();
      this.model.set('complete', false);
      return this.$edit_form.on('submit', (function(_this) {
        return function() {
          _this.save_edit_feed($input.val());
          return false;
        };
      })(this));
    };

    timeline_feed.prototype.save_edit_feed = function(new_body) {
      this.clear_form_errors();
      if (this.get_body_content() === new_body) {
        this.$body.show();
        if (this.$edit_form) {
          this.$edit_form.remove();
        }
        this.in_editting = false;
        return false;
      }
      return this.model.save({
        body: new_body
      }, {
        success: (function(_this) {
          return function() {
            return _this.edit_complete();
          };
        })(this),
        error: (function(_this) {
          return function(model, jqXHR) {
            var err, error, k, key, messages, _ref, _ref1;
            messages = [];
            if ((jqXHR != null ? (_ref = jqXHR.response) != null ? _ref.errors : void 0 : void 0) != null) {
              _ref1 = jqXHR.response.errors;
              for (key in _ref1) {
                error = _ref1[key];
                if (_.isObject(error)) {
                  for (k in error) {
                    err = error[k];
                    messages.push(err);
                  }
                } else {
                  messages.push(error);
                }
              }
            }
            if (messages.length === 0) {
              messages.push('保存できませんでした。');
            }
            return _this.set_form_errors(messages.join(','));
          };
        })(this)
      });
    };

    timeline_feed.prototype.set_form_errors = function(error_message) {
      var banner;
      this.clear_form_errors();
      banner = new App.elements.message_banner.views.message_banner({
        message: error_message,
        class_name: 'error',
        has_close: true
      });
      this.$edit_form.parents('.postModule').after(banner.$el);
      return this;
    };

    timeline_feed.prototype.clear_form_errors = function() {
      this.$el.find('.error').remove();
      return this;
    };

    timeline_feed.prototype.edit_complete = function() {
      this.url2link_class = new App.elements.url2link.views.url2link();
      this.model.set({
        body: this.url2link_class.done_url2link(this.model.get('escape_body')),
        row_body: this.url2link_class.done_url2link(this.model.get('escape_row_body'))
      }, {
        silent: true
      });
      this.$body.html(this.get_body_content()).show();
      this.$timestamp.html(App.elements.timeline.templates.feed.footer_timestamp({
        model: this.model,
        base_url: App.base_url
      }));
      if (this.$edit_form) {
        this.$edit_form.remove();
      }
      this.in_editting = false;
      if (this.model.get('is_edited')) {
        this.$el.find('.js-timeline-feed-main-container').addClass('edited');
      }
      return this.url2link_class.renew_observe_click_url2link();
    };

    timeline_feed.prototype.delete_feed_confirm = function() {
      var modal, _body, _title;
      modal = new App.elements.modal();
      modal.add_container_class('jsModal-Dialog');
      if (+this.model.get('type') === 4) {
        _title = '自己紹介のための質問(削除)';
        _body = App.elements.timeline.templates.feed_modals.delete_communication_body();
      } else {
        _title = '投稿(削除)';
        _body = App.elements.timeline.templates.feed_modals.delete_body();
      }
      modal.set_title(_title);
      modal.set_body(_body);
      modal.set_footer_cancel_delete(null, '削除する(元に戻せません)');
      modal.show();
      modal.on('cancel', this.hide_balloon);
      return modal.on('decide', (function(_this) {
        return function() {
          return _this.delete_feed(modal);
        };
      })(this));
    };

    timeline_feed.prototype.delete_feed = function(modal) {
      if (modal == null) {
        modal = null;
      }
      return this.model.destroy({
        success: (function(_this) {
          return function() {
            return _this.delete_feed_complete(modal);
          };
        })(this),
        error: (function(_this) {
          return function() {
            var error_message;
            if (!_this.delete_error_modal) {
              _this.delete_error_modal = new App.elements.modal();
              _this.delete_error_modal.set_title('削除に失敗しました');
              error_message = new App.elements.message_banner.views.message_banner({
                message: 'フィードの削除に失敗しました。既に削除されている可能性があります'
              });
              error_message.change_to_error();
              _this.delete_error_modal.$modal_body.append(error_message.$el);
              _this.delete_error_modal.set_footer_cancel_only('閉じる');
            }
            return _this.delete_error_modal.show();
          };
        })(this)
      });
    };

    timeline_feed.prototype.delete_feed_complete = function(modal) {
      if (modal == null) {
        modal = null;
      }
      if (modal != null) {
        modal.hide();
      }
      return this.$el.slideUp(500, (function(_this) {
        return function() {
          return _this.remove();
        };
      })(this));
    };

    timeline_feed.prototype.hide_balloon = function() {
      if (this.balloon) {
        return this.balloon.hide();
      }
    };

    return timeline_feed;

  })(Backbone.View);

  App.elements.timeline.views.timeline = (function(_super) {
    __extends(timeline, _super);

    function timeline() {
      this.fetch_more_feeds = __bind(this.fetch_more_feeds, this);
      this.check_scroll = __bind(this.check_scroll, this);
      this.reset_feeds = __bind(this.reset_feeds, this);
      this.add_feed = __bind(this.add_feed, this);
      this.disable_scroll_check = __bind(this.disable_scroll_check, this);
      this.enable_scroll_check = __bind(this.enable_scroll_check, this);
      this.check_feeds_count = __bind(this.check_feeds_count, this);
      this.initialize = __bind(this.initialize, this);
      return timeline.__super__.constructor.apply(this, arguments);
    }

    timeline.prototype.el = '.js-timeline';

    timeline.prototype.collection = App.elements.timeline.collections.feeds;

    timeline.prototype.fetch_volume = 20;

    timeline.prototype.retry = 0;

    timeline.prototype.use_iframe_on_upload = App.is_legacy_browser;

    timeline.prototype.max_upload_image_count = 5;

    timeline.prototype.$inputs = null;

    timeline.prototype.$container = null;

    timeline.prototype.$no_feed_message = null;

    timeline.prototype.show_communication_link_mores = true;

    timeline.prototype.show_detail_link = true;

    timeline.prototype.initialize = function(options) {
      if ((options != null ? options.show_communication_link_mores : void 0) != null) {
        this.show_communication_link_mores = options.show_communication_link_mores;
      }
      if ((options != null ? options.show_detail_link : void 0) != null) {
        this.show_detail_link = options.show_detail_link;
      }
      this.$inputs = this.$el.find('#feedPostArea');
      this.$container = this.$el.find('.js-timeline-feed-container');
      this.collection = new this.collection();
      this.collection.on('reset', this.reset_feeds);
      this.collection.on('add', this.add_feed);
      this.collection.fetch({
        url: "" + (_.result(this.collection, 'url')) + "?v=" + ((new Date()).getTime()),
        complete: this.check_feeds_count
      });
      this.$window = $(window);
      this.$document = $(document);
      this.enable_scroll_check();
      if (this.$inputs.length > 0) {
        new App.elements.timeline.views.timeline_input_text({
          collection: this.collection
        });
        new App.elements.timeline.views.timeline_input_image({
          collection: this.collection
        });
        return new App.elements.timeline.views.timeline_input_file({
          collection: this.collection
        });
      }
    };

    timeline.prototype.check_feeds_count = function() {
      if (this.collection.length === 0) {
        this.$no_feed_message = $(App.elements.timeline.templates.no_feed_message());
        return this.$container.append(this.$no_feed_message);
      }
    };

    timeline.prototype.enable_scroll_check = function() {
      return this.$window.on('scroll', this.check_scroll);
    };

    timeline.prototype.disable_scroll_check = function() {
      return this.$window.off('scroll', this.check_scroll);
    };

    timeline.prototype.add_feed = function(feed) {
      var feed_view, method, url2link_class;
      if (parseInt(feed.get('feed_system_type_id'), 10) === 1 && App.sns.is_display_user_birthday === false) {
        return false;
      }
      if (feed.url != null) {
        delete feed.url;
      }
      if (this.$no_feed_message) {
        this.$no_feed_message.remove();
        this.$no_feed_message = null;
      }
      url2link_class = new App.elements.url2link.views.url2link();
      feed.set({
        body: url2link_class.done_url2link(feed.get('body')),
        row_body: url2link_class.done_url2link(feed.get('row_body'))
      }, {
        silent: true
      });
      feed_view = new App.elements.timeline.views.timeline_feed({
        model: feed,
        show_communication_link_mores: this.show_communication_link_mores,
        show_detail_link: this.show_detail_link
      });
      method = feed.get('prepend') ? 'prepend' : 'append';
      this.$container[method](feed_view.$el);
      return url2link_class.renew_observe_click_url2link();
    };

    timeline.prototype.reset_feeds = function(feeds) {
      return feeds.forEach((function(_this) {
        return function(feed) {
          return _this.add_feed(feed);
        };
      })(this));
    };

    timeline.prototype.check_scroll = function() {
      if (this.$window.scrollTop() >= (this.$document.height() - this.$window.height() - 20)) {
        return this.fetch_more_feeds();
      } else {
        if (this.$window.scrollTop() >= (this.$document.height() - window.innerHeight - 20)) {
          return this.fetch_more_feeds();
        }
      }
    };

    timeline.prototype.fetch_more_feeds = function() {
      var before_collection_length, url;
      if (this.collection.length > 0) {
        this.disable_scroll_check();
        if (typeof this.collection.url === 'function') {
          url = this.collection.url();
        } else {
          url = this.collection.url;
        }
        before_collection_length = this.collection.length;
        return this.collection.fetch({
          update: true,
          remove: false,
          add: true,
          url: "" + url + "/" + this.fetch_volume + "/" + this.collection.length + "?v=" + ((new Date()).getTime()),
          success: (function(_this) {
            return function(collection, response) {
              if (before_collection_length === _this.collection.length && _this.retry <= 3) {
                _this.fetch_volume += 10;
                _this.fetch_more_feeds();
                return _this.retry++;
              } else {
                if (_this.collection.length !== before_collection_length) {
                  _this.enable_scroll_check();
                }
                return _this.retry = 0;
              }
            };
          })(this)
        });
      }
    };

    return timeline;

  })(Backbone.View);

  App.elements.user_select = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.elements.user_select.templates.modal = {
    header: _.template('<div>\n  <select>\n    <option>ユーザーから選択</option>\n    <option>グループから選択</option>\n  </select>\n</div>'),
    body: _.template('<div class="textLead wysiwygContent">\n  <p>登録するユーザーを選択してください。</p>\n</div>\n<div class="boxUserAdd">\n  <div class="searchSection">\n    <div class="content">\n      <span class="icon"><i class="icon-search"></i></span>\n      <span class="input">\n        <input type="text" placeholder="名前で検索する" class="js-user-select-modal-filter-input">\n      </span>\n    </div>\n    <div class="mySelection">\n      <% if (use_mysearch) { %>\n      <div class="col">\n        <select class="js-user-select-modal-fileter-select">\n        </select>\n      </div>\n      <% } %>\n      <div class="col">\n        <a href="#" class="btnM js-user-select-modal-all-user-select">リスト内のユーザーを全員追加する</a>\n      </div>\n    </div>\n  </div>\n  <div class="suggestSection">\n    <div class="content">\n      <ul class="js-user-select-modal-users-list">\n      </ul>\n    </div>\n  </div>\n  <div class="selectedSection">\n    <div class="content js-user-select-modal-selected-users-list">\n    </div>\n  </div>\n  <% if(show_filter_checkbox){ %>\n  <p class="textOptionCheck">\n    <label>\n      <input type="checkbox" checked="checked" class="js-user-select-modal-filter-checkbox">\n      別の期限で対象になっているユーザーを除外する\n    </label>\n  </p>\n  <% } %>\n</div>'),
    user: _.template('<li>\n  <a href="#">\n    <span class="profile">\n      <img class="thumb" src="<%= model.get(\'icon_x24\') %>" alt="<%- model.get(\'name\') %>">\n    </span>\n    <span class="name"><%- model.get(\'name\') %></span>\n    <span class="post">（<%- model.get(\'type_show_name\') %>）</span>\n    <% if(!model.get(\'is_in_account_expiration\')){ %>\n      <span class="textAttention">(アカウント期限切れ)</span>\n    <% }else if(model.get(\'is_login_rejected\') && is_admin_controller){ %>\n      <span class="textAttention">(ログイン停止中)</span>\n    <% } %>\n  </a>\n</li>'),
    user_selected: _.template('<%- model.get(\'name\') %>\n<a href="#"><i class="icon-xmark"></i></a>')
  };

  App.elements.user_select.models.user = (function(_super) {
    __extends(user, _super);

    function user() {
      this.is_selected = __bind(this.is_selected, this);
      this.toggle_selected = __bind(this.toggle_selected, this);
      this.initialize = __bind(this.initialize, this);
      return user.__super__.constructor.apply(this, arguments);
    }

    user.prototype.defaults = {
      id: null,
      name: '',
      kana: '',
      icon: '',
      icon_x24: '',
      type: '',
      type_show_name: '',
      is_in_account_expiration: '',
      is_login_rejected: '',
      keyword: '',
      selected: false,
      visible: true
    };

    user.prototype.initialize = function() {
      return this.set('keyword', (this.get('name') + this.get('kana')).replace(/[\s　]+/g, ''));
    };

    user.prototype.toggle_selected = function() {
      return this.set('selected', !this.get('selected'));
    };

    user.prototype.is_selected = function() {
      return !!this.get('selected', false);
    };

    return user;

  })(Backbone.Model);

  App.elements.user_select.models.search_option = (function(_super) {
    __extends(search_option, _super);

    function search_option() {
      return search_option.__super__.constructor.apply(this, arguments);
    }

    search_option.prototype.defaults = {
      id: null,
      name: ''
    };

    return search_option;

  })(Backbone.Model);

  App.elements.user_select.collections.users = (function(_super) {
    __extends(users, _super);

    function users() {
      this.get_count = __bind(this.get_count, this);
      this.change_selected = __bind(this.change_selected, this);
      this.added_model = __bind(this.added_model, this);
      this.initialize = __bind(this.initialize, this);
      this.parse = __bind(this.parse, this);
      this.url = __bind(this.url, this);
      return users.__super__.constructor.apply(this, arguments);
    }

    users.prototype.url = function() {
      return App.base_url + '/api/user/users' + '?v=' + (new Date()).getTime();
    };

    users.prototype.model = App.elements.user_select.models.user;

    users.prototype.count = 0;

    users.prototype.exclude = [];

    users.prototype.parse = function(users_data) {
      var excluded_ary, user_data, _i, _len;
      excluded_ary = [];
      for (_i = 0, _len = users_data.length; _i < _len; _i++) {
        user_data = users_data[_i];
        if (((user_data != null ? user_data.id : void 0) != null) && _(this.exclude).indexOf(parseInt(user_data.id, 10)) < 0) {
          excluded_ary.push(user_data);
        }
      }
      return excluded_ary;
    };

    users.prototype.initialize = function(models, options) {
      if ((options != null ? options.exclude : void 0) != null) {
        this.exclude = _.map(options.exclude, (function(_this) {
          return function(id) {
            return parseInt(id, 10);
          };
        })(this));
      }
      return this.on('add', this.added_model);
    };

    users.prototype.added_model = function(user) {
      return user.on('change:selected', this.change_selected);
    };

    users.prototype.change_selected = function() {
      this.count = (this.filter((function(_this) {
        return function(user) {
          return user.get('selected');
        };
      })(this))).length;
      return this.trigger('change_selected_count');
    };

    users.prototype.get_count = function() {
      return this.count;
    };

    return users;

  })(Backbone.Collection);

  App.elements.user_select.collections.selected_users = (function(_super) {
    __extends(selected_users, _super);

    function selected_users() {
      return selected_users.__super__.constructor.apply(this, arguments);
    }

    selected_users.prototype.model = App.elements.user_select.models.user;

    return selected_users;

  })(Backbone.Collection);

  App.elements.user_select.collections.search_options = (function(_super) {
    __extends(search_options, _super);

    function search_options() {
      this.parse = __bind(this.parse, this);
      this.url = __bind(this.url, this);
      return search_options.__super__.constructor.apply(this, arguments);
    }

    search_options.prototype.url = function() {
      return App.base_url + '/api/user/select/search_options' + '?v=' + (new Date()).getTime();
    };

    search_options.prototype.model = App.elements.user_select.models.search_option;

    search_options.prototype.parse = function(response) {
      if (response.error) {
        return;
      }
      return response.data;
    };

    return search_options;

  })(Backbone.Collection);

  App.elements.user_select.views.modal_user_selected = (function(_super) {
    __extends(modal_user_selected, _super);

    function modal_user_selected() {
      this.change_selected = __bind(this.change_selected, this);
      this.unselect = __bind(this.unselect, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return modal_user_selected.__super__.constructor.apply(this, arguments);
    }

    modal_user_selected.prototype.el = '<span class="labelTagPositive"></span>';

    modal_user_selected.prototype.model = null;

    modal_user_selected.prototype.events = {
      'click': 'unselect'
    };

    modal_user_selected.prototype.initialize = function() {
      return this.model.on('change:selected', this.change_selected);
    };

    modal_user_selected.prototype.render = function() {
      this.$el.html(App.elements.user_select.templates.modal.user_selected({
        model: this.model
      }));
      return this.$el;
    };

    modal_user_selected.prototype.unselect = function() {
      this.model.set('selected', false);
      return false;
    };

    modal_user_selected.prototype.change_selected = function() {
      if (!this.model.get('selected')) {
        this.$el.next('.js-user-select-nbsp').remove();
        this.remove();
      }
      return false;
    };

    return modal_user_selected;

  })(Backbone.View);

  App.elements.user_select.views._modal_user = (function(_super) {
    __extends(_modal_user, _super);

    function _modal_user() {
      this.change_visible = __bind(this.change_visible, this);
      this.change_checked = __bind(this.change_checked, this);
      this.change_selected = __bind(this.change_selected, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return _modal_user.__super__.constructor.apply(this, arguments);
    }

    _modal_user.prototype.el = '<li></li>';

    _modal_user.prototype.model = null;

    _modal_user.prototype.events = {
      'change input[type="checkbox"]': 'change_checked'
    };

    _modal_user.prototype.template = _.template('<label class="checkbox">\n  <input type="checkbox" value="<%= model.get(\'id\') %>"<% if(model.get(\'selected\')){ %> checked="checked"<% } %>>\n  <img src="<%= model.get(\'icon\') %>"> <%= model.get(\'name\') %>\n</label>');

    _modal_user.prototype.initialize = function() {
      this.model.on('change:selected', this.change_selected);
      return this.model.on('change:visible', this.change_visible);
    };

    _modal_user.prototype.render = function() {
      this.$el.html(this.template({
        model: this.model
      }));
      return this.$el;
    };

    _modal_user.prototype.change_selected = function() {
      var $checkbox;
      $checkbox = this.$el.find('input[type="checkbox"]');
      if (this.model.get('selected')) {
        return $checkbox.attr('checked', 'checked');
      } else {
        return $checkbox.removeAttr('checked');
      }
    };

    _modal_user.prototype.change_checked = function() {
      return this.model.set('selected', this.$el.find('input[type="checkbox"]').is(':checked'));
    };

    _modal_user.prototype.change_visible = function() {
      if (this.model.get('visible')) {
        return this.$el.show();
      } else {
        return this.$el.hide();
      }
    };

    return _modal_user;

  })(Backbone.View);

  App.elements.user_select.views.modal_user = (function(_super) {
    __extends(modal_user, _super);

    function modal_user() {
      this.change_visible = __bind(this.change_visible, this);
      this.selected_changed = __bind(this.selected_changed, this);
      this.change_selected = __bind(this.change_selected, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return modal_user.__super__.constructor.apply(this, arguments);
    }

    modal_user.prototype.el = '<li></li>';

    modal_user.prototype.model = null;

    modal_user.prototype.events = {
      'click': 'change_selected'
    };

    modal_user.prototype.initialize = function() {
      this.model.on('change:selected', this.selected_changed);
      this.model.on('change:visible', this.change_visible);
      this.page_url_from_call = window.location.href;
      this.search_url = App.base_url + '/admin';
      if (this.page_url_from_call.indexOf(this.search_url) === -1) {
        return this.is_admin_controller = false;
      } else {
        return this.is_admin_controller = true;
      }
    };

    modal_user.prototype.render = function() {
      this.$el.html(App.elements.user_select.templates.modal.user({
        model: this.model,
        is_admin_controller: this.is_admin_controller
      }));
      if (this.model.get('selected', false) || !this.model.get('visible', true)) {
        this.$el.hide();
      } else {
        this.$el.show();
      }
      return this.$el;
    };

    modal_user.prototype.change_selected = function() {
      this.model.toggle_selected();
      return false;
    };

    modal_user.prototype.selected_changed = function() {
      var method;
      method = this.model.get('selected') ? 'hide' : 'show';
      return this.$el[method]();
    };

    modal_user.prototype.change_visible = function() {
      if (this.model.get('visible')) {
        return this.$el.show();
      } else {
        return this.$el.hide();
      }
    };

    return modal_user;

  })(Backbone.View);

  App.elements.user_select.views.selected_user = (function(_super) {
    __extends(selected_user, _super);

    function selected_user() {
      this.remove_self = __bind(this.remove_self, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return selected_user.__super__.constructor.apply(this, arguments);
    }

    selected_user.prototype.el = '<li></li>';

    selected_user.prototype.events = {
      'click': 'remove_self'
    };

    selected_user.prototype.name = 'users[]';

    selected_user.prototype.model = null;

    selected_user.prototype.template = _.template('<span class="label label-info">\n  <img src="<%= model.get(\'icon\') %>"> <%= model.get(\'name\') %>\n</span>\n<input type="hidden" name="<%= name %>" value="<%= model.get(\'id\') %>">');

    selected_user.prototype.initialize = function(options) {
      if ((options != null ? options.name : void 0) != null) {
        return this.name = options.name;
      }
    };

    selected_user.prototype.render = function() {
      this.$el.html(this.template({
        model: this.model,
        name: this.name
      }));
      return this.$el;
    };

    selected_user.prototype.remove_self = function() {
      this.model.clear({
        silent: true
      }).destroy();
      this.remove();
      return false;
    };

    return selected_user;

  })(Backbone.View);

  App.elements.user_select.views._user_select = (function(_super) {
    __extends(_user_select, _super);

    function _user_select() {
      this.change_selected_count = __bind(this.change_selected_count, this);
      this.filter_users = __bind(this.filter_users, this);
      this.fetch_collection = __bind(this.fetch_collection, this);
      this.fetch_selected_collection = __bind(this.fetch_selected_collection, this);
      this.render_selected_users = __bind(this.render_selected_users, this);
      this.submit = __bind(this.submit, this);
      this.get_selected_user_ids = __bind(this.get_selected_user_ids, this);
      this.render_users_on_modal = __bind(this.render_users_on_modal, this);
      this.btn_clicked = __bind(this.btn_clicked, this);
      this.set_selected_users = __bind(this.set_selected_users, this);
      this.initialize = __bind(this.initialize, this);
      return _user_select.__super__.constructor.apply(this, arguments);
    }

    _user_select.prototype.el = null;

    _user_select.prototype.events = {
      'click .js-user-select-launch-button': 'btn_clicked',
      'click .js-user-select-submit': 'submit',
      'keyup .js-user-select-filter-input': 'filter_users'
    };

    _user_select.prototype.name_attribute = 'users[]';

    _user_select.prototype._default_selected_user_ids = [];

    _user_select.prototype._exclude_user_ids = [];

    _user_select.prototype.is_fetched = false;

    _user_select.prototype.collection = null;

    _user_select.prototype.selected_collection = null;

    _user_select.prototype.modal_users = null;

    _user_select.prototype.$btn = null;

    _user_select.prototype.$container = null;

    _user_select.prototype.$modal = null;

    _user_select.prototype.$input = null;

    _user_select.prototype.$modal_selected = null;

    _user_select.prototype.$modal_users = null;

    _user_select.prototype.$submit = null;

    _user_select.prototype.$loading = null;

    _user_select.prototype.$count = null;

    _user_select.prototype.initialize = function() {
      this.modal_users = new Object();
      this._default_selected_user_ids = new Array();
      this._exclude_user_ids = new Array();
      this._exclude_user_ids = this.$el.attr('data-exclude').split(':');
      this.collection = new App.elements.user_select.collections.users(null, {
        exclude: this._exclude_user_ids
      });
      this.collection.on('change_selected_count', this.change_selected_count);
      this.fetch_collection();
      this.selected_collection = new App.elements.user_select.collections.selected_users();
      this.$btn = this.$el.find('.js-user-select-launch-button');
      this.$container = this.$el.find('.js-user-select-selected-container');
      this.$modal = this.$el.find('.js-user-select-modal');
      this.$input = this.$modal.find('.js-user-select-filter-input');
      this.$modal_selected = this.$modal.find('.js-user-select-modal-selected');
      this.$modal_users = this.$modal.find('.js-user-select-modal-users');
      this.$submit = this.$modal.find('.js-user-select-submit');
      this.$loading = this.$modal.find('.js-user-select-loading');
      this.$count = this.$modal.find('.js-user-select-modal-count');
      this.name_attribute = this.$el.attr('data-name');
      this._default_selected_user_ids = this.$el.attr('data-selected').split(':');
      if (!this.is_fetched) {
        return this.on('fetch_complete', this.set_selected_users);
      } else {
        return this.set_selected_users();
      }
    };

    _user_select.prototype.set_selected_users = function() {
      _(this._default_selected_user_ids).each((function(_this) {
        return function(user_id) {
          var user;
          user_id = parseInt(user_id, 10);
          if (isNaN(user_id)) {
            return true;
          }
          user = _this.collection.get(user_id);
          if (user == null) {
            return true;
          }
          return _this.selected_collection.push(user.clone());
        };
      })(this));
      delete this._default_selected_user_ids;
      return this.render_selected_users();
    };

    _user_select.prototype.btn_clicked = function() {
      if (!this.is_fetched) {
        this.on('fetch_complete', this.render_users_on_modal);
      } else {
        this.render_users_on_modal();
      }
      this.$modal.modal('show');
      return false;
    };

    _user_select.prototype.render_users_on_modal = function() {
      var selected_user_ids;
      selected_user_ids = this.get_selected_user_ids();
      this.collection.each((function(_this) {
        return function(user) {
          var modal_user, user_id;
          user_id = parseInt(user.get('id'), 10);
          user.set('selected', _.indexOf(selected_user_ids, user_id) >= 0);
          if (_this.modal_users[user_id] != null) {
            return true;
          }
          modal_user = new App.elements.user_select.views._modal_user({
            model: user
          });
          _this.modal_users[user_id] = modal_user;
          return _this.$modal_users.append(modal_user.render());
        };
      })(this));
      return this.$loading.hide();
    };

    _user_select.prototype.get_selected_user_ids = function() {
      var selected_user_ids;
      selected_user_ids = [];
      this.selected_collection.each((function(_this) {
        return function(selected_user) {
          return selected_user_ids.push(parseInt(selected_user.get('id'), 10));
        };
      })(this));
      return selected_user_ids;
    };

    _user_select.prototype.submit = function() {
      this.fetch_selected_collection();
      this.render_selected_users();
      this.$modal.modal('hide');
      return false;
    };

    _user_select.prototype.render_selected_users = function() {
      this.$container.empty();
      return this.selected_collection.each((function(_this) {
        return function(user) {
          var user_view;
          user_view = new App.elements.user_select.views.selected_user({
            model: user,
            name: _this.name_attribute
          });
          return _this.$container.append(user_view.render());
        };
      })(this));
    };

    _user_select.prototype.fetch_selected_collection = function() {
      delete this.selected_collection;
      this.selected_collection = new App.elements.user_select.collections.selected_users();
      this.collection.each((function(_this) {
        return function(user) {
          if (user.get('selected')) {
            return _this.selected_collection.push(user.clone());
          }
        };
      })(this));
      return true;
    };

    _user_select.prototype.fetch_collection = function() {
      return this.collection.fetch({
        update: true,
        remove: false,
        add: true,
        success: (function(_this) {
          return function() {
            _this.is_fetched = true;
            return _this.trigger('fetch_complete');
          };
        })(this)
      });
    };

    _user_select.prototype.filter_users = function() {
      var search_word_text, search_words;
      search_word_text = _(this.$input.val().replace(/[\s　]+/g, ' ')).trim();
      if (search_word_text.length === 0) {
        return this.collection.map((function(_this) {
          return function(user) {
            user.set('visible', true);
            return user;
          };
        })(this));
      } else {
        search_words = search_word_text.split(' ');
        return this.collection.each((function(_this) {
          return function(user) {
            var keyword, word, _i, _len;
            keyword = user.get('keyword');
            for (_i = 0, _len = search_words.length; _i < _len; _i++) {
              word = search_words[_i];
              if (keyword.indexOf(word) < 0) {
                user.set('visible', false);
                return true;
              }
            }
            return user.set('visible', true);
          };
        })(this));
      }
    };

    _user_select.prototype.change_selected_count = function() {
      return this.$count.text(this.collection.get_count());
    };

    return _user_select;

  })(Backbone.View);

  App.elements.user_select.views.user_select = (function(_super) {
    __extends(user_select, _super);

    function user_select() {
      this.add_all_users = __bind(this.add_all_users, this);
      this.is_filter_enabled = __bind(this.is_filter_enabled, this);
      this.filter_users = __bind(this.filter_users, this);
      this.fetch_collection = __bind(this.fetch_collection, this);
      this.fetch_selected_collection = __bind(this.fetch_selected_collection, this);
      this.render_selected_users = __bind(this.render_selected_users, this);
      this.submit = __bind(this.submit, this);
      this.is_unfiltered_user_id = __bind(this.is_unfiltered_user_id, this);
      this.is_filtered_user_id = __bind(this.is_filtered_user_id, this);
      this.get_unfiltered_user_ids = __bind(this.get_unfiltered_user_ids, this);
      this.get_filtered_user_ids = __bind(this.get_filtered_user_ids, this);
      this.get_selected_user_ids = __bind(this.get_selected_user_ids, this);
      this.change_modal_user_selected = __bind(this.change_modal_user_selected, this);
      this.render_mysearch_on_modal = __bind(this.render_mysearch_on_modal, this);
      this.render_users_on_modal = __bind(this.render_users_on_modal, this);
      this.launch = __bind(this.launch, this);
      this.set_selected_users = __bind(this.set_selected_users, this);
      this.build_modal = __bind(this.build_modal, this);
      this.initialize = __bind(this.initialize, this);
      return user_select.__super__.constructor.apply(this, arguments);
    }

    user_select.prototype.el = null;

    user_select.prototype.events = {
      'click .js-user-select-launch-button': 'launch'
    };

    user_select.prototype.name_attribute = 'users[]';

    user_select.prototype.default_selected_user_ids = [];

    user_select.prototype.exclude_user_ids = [];

    user_select.prototype.is_fetched = false;

    user_select.prototype.collection = null;

    user_select.prototype.selected_collection = null;

    user_select.prototype.mysearch_collection = null;

    user_select.prototype.modal_users = null;

    user_select.prototype.$container = null;

    user_select.prototype.use_filter = false;

    user_select.prototype.filter_label = '対象済みユーザーをリストから除外する';

    user_select.prototype.filtered_user_ids = [];

    user_select.prototype.unfiltered_user_ids = [];

    user_select.prototype.modal = null;

    user_select.prototype.$modal_body = null;

    user_select.prototype.$modal_list = null;

    user_select.prototype.$modal_list_selected = null;

    user_select.prototype.$modal_list_filter_input = null;

    user_select.prototype.$modal_list_filter_select = null;

    user_select.prototype.$modal_list_filter_checkbox = null;

    user_select.prototype.initialize = function(options) {
      var data;
      this.modal_users = {};
      if ((options != null ? options.use_filter : void 0) != null) {
        this.use_filter = !!options.use_filter;
      }
      this.use_mysearch = App.classes.current_user.is_role_manager() || App.classes.current_user.is_role_work() || App.classes.current_user.is_role_gx();
      if ((options != null ? options.data : void 0) != null) {
        data = options.data;
      } else {
        data = getEmbeddedJSON(this.$el.find('.js-user-select-json-data'));
      }
      if (data.name != null) {
        this.name_attribute = data.name;
      }
      this.default_selected_user_ids = data.selected != null ? data.selected : [];
      this.exclude_user_ids = data.exclude != null ? data.exclude : [];
      this.collection = new App.elements.user_select.collections.users(null, {
        exclude: this.exclude_user_ids
      });
      this.fetch_collection();
      this.selected_collection = new App.elements.user_select.collections.selected_users();
      if (this.use_mysearch) {
        this.mysearch_collection = new App.elements.user_select.collections.search_options();
        this.mysearch_collection.fetch();
      }
      this.build_modal();
      this.$container = this.$el.find('.js-user-select-selected-container');
      if (!this.is_fetched) {
        this.on('fetch_complete', this.set_selected_users);
        return this.on('fetch_complete', this.render_users_on_modal);
      } else {
        this.set_selected_users();
        return this.render_users_on_modal();
      }
    };

    user_select.prototype.build_modal = function() {
      this.modal = new App.elements.modal({
        title: 'ユーザーの選択',
        body: App.elements.user_select.templates.modal.body({
          show_filter_checkbox: this.use_filter,
          use_mysearch: this.use_mysearch
        }),
        decide_button_label: '選択する',
        cancel_button_label: '取り消す'
      });
      this.$modal_body = this.modal.get_body();
      this.$modal_list = this.$modal_body.find('.js-user-select-modal-users-list');
      this.$modal_list_selected = this.$modal_body.find('.js-user-select-modal-selected-users-list');
      this.$modal_list_filter_input = this.$modal_body.find('.js-user-select-modal-filter-input');
      this.$modal_list_filter_select = this.$modal_body.find('.js-user-select-modal-fileter-select');
      this.$modal_list_filter_checkbox = this.$modal_body.find('.js-user-select-modal-filter-checkbox');
      this.$modal_list_all_user_select = this.$modal_body.find('.js-user-select-modal-all-user-select');
      this.modal.on('decide', this.submit);
      this.$modal_list_filter_input.on('keyup', this.filter_users);
      this.$modal_list_filter_select.on('change', this.filter_users);
      this.$modal_list_filter_checkbox.on('change', this.filter_users);
      return this.$modal_list_all_user_select.on('click', this.add_all_users);
    };

    user_select.prototype.set_selected_users = function() {
      var user, user_id, _i, _len, _ref;
      _ref = this.default_selected_user_ids;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        user_id = _ref[_i];
        user_id = parseInt(user_id, 10);
        if (isNaN(user_id)) {
          continue;
        }
        user = this.collection.get(user_id);
        if (user == null) {
          continue;
        }
        user.set('selected', true);
        this.change_modal_user_selected(user);
        this.selected_collection.push(user.clone());
      }
      delete this.default_selected_user_ids;
      return this.render_selected_users();
    };

    user_select.prototype.launch = function() {
      if (!this.is_fetched) {
        this.on('fetch_complete', this.render_users_on_modal);
      } else {
        this.render_users_on_modal();
      }
      this.$modal_list_filter_input.val('');
      if (this.use_mysearch) {
        this.render_mysearch_on_modal();
      }
      this.modal.show();
      this.filter_users();
      return false;
    };

    user_select.prototype.render_users_on_modal = function() {
      var filter_enabled, selected_user_ids;
      selected_user_ids = this.get_selected_user_ids();
      this.filtered_user_ids = this.get_filtered_user_ids();
      this.unfiltered_user_ids = this.get_unfiltered_user_ids(this.el);
      filter_enabled = this.$modal_list_filter_checkbox.is(':checked');
      return this.collection.each((function(_this) {
        return function(user) {
          var modal_user, user_id;
          user_id = parseInt(user.get('id'), 10);
          user.set('selected', _.indexOf(selected_user_ids, user_id) >= 0);
          if (filter_enabled && _this.is_filtered_user_id(user_id)) {
            user.set('visible', false);
          }
          if (_this.modal_users[user_id] != null) {
            return true;
          }
          modal_user = new App.elements.user_select.views.modal_user({
            model: user
          });
          _this.modal_users[user_id] = modal_user;
          _this.$modal_list.append(modal_user.render());
          return user.on('change:selected', _this.change_modal_user_selected);
        };
      })(this));
    };

    user_select.prototype.render_mysearch_on_modal = function() {
      this.$modal_list_filter_select.find('option').remove();
      this.$modal_list_filter_select.append(_.template('<option value="">マイ検索を指定して絞り込む</option>'));
      return this.mysearch_collection.each((function(_this) {
        return function(search_option) {
          return _this.$modal_list_filter_select.append(_.template('<option value="<%= data.get("id") %>"><%- data.get("name") %></option>', {
            data: search_option
          }));
        };
      })(this));
    };

    user_select.prototype.change_modal_user_selected = function(user) {
      var modal_user_selected;
      if (user.get('selected')) {
        modal_user_selected = new App.elements.user_select.views.modal_user_selected({
          model: user
        });
        return this.$modal_list_selected.append(modal_user_selected.render()).append('<span class="js-user-select-nbsp">&nbsp;</span>');
      } else {
        return this.filter_users();
      }
    };

    user_select.prototype.get_selected_user_ids = function() {
      var selected_user_ids;
      selected_user_ids = [];
      this.selected_collection.each((function(_this) {
        return function(selected_user) {
          return selected_user_ids.push(parseInt(selected_user.get('id'), 10));
        };
      })(this));
      return selected_user_ids;
    };

    user_select.prototype.get_filtered_user_ids = function() {
      return [];
    };

    user_select.prototype.get_unfiltered_user_ids = function(el) {
      return [];
    };

    user_select.prototype.is_filtered_user_id = function(user_id) {
      return _.indexOf(this.filtered_user_ids, +user_id) >= 0;
    };

    user_select.prototype.is_unfiltered_user_id = function(user_id) {
      return _.indexOf(this.unfiltered_user_ids, +user_id) >= 0;
    };

    user_select.prototype.submit = function() {
      this.fetch_selected_collection();
      this.render_selected_users();
      this.modal.hide();
      return false;
    };

    user_select.prototype.render_selected_users = function() {
      this.$container.empty();
      return this.selected_collection.each((function(_this) {
        return function(user) {
          var user_view;
          user_view = new App.elements.user_select.views.selected_user({
            model: user,
            name: _this.name_attribute
          });
          return _this.$container.append(user_view.render());
        };
      })(this));
    };

    user_select.prototype.fetch_selected_collection = function() {
      delete this.selected_collection;
      this.selected_collection = new App.elements.user_select.collections.selected_users();
      this.collection.each((function(_this) {
        return function(user) {
          if (user.get('selected')) {
            return _this.selected_collection.push(user.clone());
          }
        };
      })(this));
      return true;
    };

    user_select.prototype.fetch_collection = function() {
      return this.collection.fetch({
        update: true,
        remove: false,
        add: true,
        success: (function(_this) {
          return function() {
            _this.is_fetched = true;
            return _this.trigger('fetch_complete');
          };
        })(this)
      });
    };

    user_select.prototype.filter_users = function() {
      var mysearch_users, search_word_text, search_words, _search_option_id;
      search_word_text = _(this.$modal_list_filter_input.val().replace(/[\s　]+/g, ' ')).trim();
      _search_option_id = this.$modal_list_filter_select.val();
      mysearch_users = [];
      if (_search_option_id) {
        $.ajax("" + App.base_url + "/api/user/select/mysearch/" + _search_option_id, {
          async: false,
          success: (function(_this) {
            return function(response, textStatus, jqXHR) {
              return mysearch_users = response.data;
            };
          })(this),
          error: (function(_this) {
            return function(jqXHR, textStatus, error) {};
          })(this)
        });
      }
      if (search_word_text.length === 0) {
        return this.collection.each((function(_this) {
          return function(user) {
            if (user.is_selected()) {
              return true;
            }
            if (mysearch_users.length > 0) {
              if ($.inArray(user.get('id') * 1, mysearch_users) === -1) {
                user.set('visible', false);
                user.trigger('change:visible');
                return true;
              }
            } else if (_search_option_id) {
              user.set('visible', false);
              user.trigger('change:visible');
              return true;
            }
            return user.set('visible', _this.is_unfiltered_user_id(user.get('id')) || !_this.is_filter_enabled() || !_this.is_filtered_user_id(user.get('id')));
          };
        })(this));
      } else {
        search_words = search_word_text.split(' ');
        return this.collection.each((function(_this) {
          return function(user) {
            var keyword, word, _i, _len;
            if (user.is_selected()) {
              return true;
            }
            if (mysearch_users.length > 0) {
              if ($.inArray(user.get('id') * 1, mysearch_users) === -1) {
                user.set('visible', false);
                user.trigger('change:visible');
                return true;
              }
            } else if (_search_option_id) {
              user.set('visible', false);
              user.trigger('change:visible');
              return true;
            }
            keyword = user.get('keyword');
            for (_i = 0, _len = search_words.length; _i < _len; _i++) {
              word = search_words[_i];
              if (keyword.indexOf(word) < 0) {
                user.set('visible', false);
                user.trigger('change:visible');
                return true;
              }
            }
            return user.set('visible', _this.is_unfiltered_user_id(user.get('id')) || !_this.is_filter_enabled() || !_this.is_filtered_user_id(user.get('id')));
          };
        })(this));
      }
    };

    user_select.prototype.is_filter_enabled = function() {
      return this.use_filter && this.$modal_list_filter_checkbox.is(':checked');
    };

    user_select.prototype.add_all_users = function() {
      this.collection.each((function(_this) {
        return function(user) {
          if (user.is_selected() || !user.get('visible')) {
            return true;
          }
          return user.set('selected', true);
        };
      })(this));
      return false;
    };

    return user_select;

  })(Backbone.View);

  add_route('.*', function() {
    $('.js-_user-select').each(function(index, element) {
      return new App.elements.user_select.views._user_select({
        el: $(element)
      });
    });
    return $('.js-user-select').each(function(index, element) {
      return new App.elements.user_select.views.user_select({
        el: $(element)
      });
    });
  });

  App.elements.wysiwyg = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.elements.wysiwyg.templates.emoji = {
    cell: _.template('<img src="<%= path %>" data-emoji-code="<%= id %>" />')
  };

  App.elements.wysiwyg.config.emoji = {
    start: 0,
    end: 175,
    path_base: "/assets/img/dummy/14x14.png"
  };

  App.elements.wysiwyg.views.wysiwyg = (function(_super) {
    __extends(wysiwyg, _super);

    function wysiwyg() {
      this.initialize = __bind(this.initialize, this);
      return wysiwyg.__super__.constructor.apply(this, arguments);
    }

    wysiwyg.prototype.el = null;

    wysiwyg.prototype.configs = {};

    wysiwyg.prototype.initialize = function(options) {
      var ckeditor;
      if (App.current_user.is_smartphone) {
        return;
      }
      if ((options != null ? options.configs : void 0) != null) {
        this.configs = _.extend(this.options, options.configs);
      }
      ckeditor = CKEDITOR.replace(this.$el[0], this.configs);
      if (this.$el.attr('name')) {
        return ckeditor.name = this.$el.attr('name');
      } else {
        return this.$el.attr('name', ckeditor.name);
      }
    };

    return wysiwyg;

  })(Backbone.View);

  App.elements.wysiwyg.views.wysiwyg_user = (function(_super) {
    __extends(wysiwyg_user, _super);

    function wysiwyg_user() {
      return wysiwyg_user.__super__.constructor.apply(this, arguments);
    }

    wysiwyg_user.prototype.el = null;

    wysiwyg_user.prototype.configs = {
      toolbar: [['Smiley']]
    };

    return wysiwyg_user;

  })(App.elements.wysiwyg.views.wysiwyg);

  App.elements.wysiwyg.views.wysiwyg_admin = (function(_super) {
    __extends(wysiwyg_admin, _super);

    function wysiwyg_admin() {
      return wysiwyg_admin.__super__.constructor.apply(this, arguments);
    }

    wysiwyg_admin.prototype.el = null;

    return wysiwyg_admin;

  })(App.elements.wysiwyg.views.wysiwyg);

  App.elements.wysiwyg.views.wysiwyg_admin_design = (function(_super) {
    __extends(wysiwyg_admin_design, _super);

    function wysiwyg_admin_design() {
      return wysiwyg_admin_design.__super__.constructor.apply(this, arguments);
    }

    wysiwyg_admin_design.prototype.el = null;

    wysiwyg_admin_design.prototype.configs = {
      toolbar: [['Smiley', 'Bold', 'Italic', 'Underline', 'Strike'], ['Link', 'Unlink'], ['FontSize']]
    };

    return wysiwyg_admin_design;

  })(App.elements.wysiwyg.views.wysiwyg);

  add_route('.*', function() {
    return $('[data-wysiwyg="user"]').each((function(_this) {
      return function(index, element) {
        return new App.elements.wysiwyg.views.wysiwyg_user({
          el: element
        });
      };
    })(this));
  });

  add_route('.*', function() {
    return $('[data-wysiwyg="admin"]').each((function(_this) {
      return function(index, element) {
        return new App.elements.wysiwyg.views.wysiwyg_admin({
          el: element
        });
      };
    })(this));
  });

  add_route('.*', function() {
    return $('[data-wysiwyg="admin_design"]').each((function(_this) {
      return function(index, element) {
        return new App.elements.wysiwyg.views.wysiwyg_admin_design({
          el: element
        });
      };
    })(this));
  });

  App.elements.tooltip = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.elements.tooltip.templates.tooltip = _.template('<span class="blnTooltip js-tooltip-content">\n    <span class="blnHeader">\n      <span class="heading"><%= data.title %></span>\n    </span>\n    <span class="blnContent"><%= data.body %></span>\n      <span class="close"><a class="uiAnchor js-tooltip-close-anchor" href="#"><i class="icon-xmark"></i></a></span>\n    </span>\n</span>');

  App.elements.tooltip.views.tooltip = (function(_super) {
    __extends(tooltip, _super);

    function tooltip() {
      this.tooltip_keydown_controlle = __bind(this.tooltip_keydown_controlle, this);
      this.is_active = __bind(this.is_active, this);
      this.all_hide = __bind(this.all_hide, this);
      this.hide = __bind(this.hide, this);
      this.show = __bind(this.show, this);
      this.toggle = __bind(this.toggle, this);
      this.hide_on_document_clicked = __bind(this.hide_on_document_clicked, this);
      this._tooltip_init = __bind(this._tooltip_init, this);
      this.initialize = __bind(this.initialize, this);
      return tooltip.__super__.constructor.apply(this, arguments);
    }

    tooltip.prototype.el = '#main';

    tooltip.prototype.events = {
      'click .js-tooltip-anchor': 'toggle',
      'click .js-tooltip-close-anchor': 'all_hide'
    };

    tooltip.prototype.$tooltip = null;

    tooltip.prototype.initialize = function() {
      return App.$window.on('load', this._tooltip_init);
    };

    tooltip.prototype._tooltip_init = function() {
      var $tooltip_containers;
      $tooltip_containers = $('.js-tooltip-container');
      if (!$tooltip_containers.length) {
        return;
      }
      $tooltip_containers.each((function(_this) {
        return function(index, element) {
          var data_name, tooltip_data;
          data_name = $(element).attr('data-name');
          tooltip_data = getEmbeddedJSON($('#tooltip_' + data_name));
          return $(element).append(App.elements.tooltip.templates.tooltip({
            data: tooltip_data
          }));
        };
      })(this));
      return this.on({
        show: (function(_this) {
          return function() {
            App.$window.on('keydown', _this.tooltip_keydown_controlle);
            return App.$document.on('click', _this.hide_on_document_clicked);
          };
        })(this),
        hide: (function(_this) {
          return function() {
            App.$window.off('keydown', _this.tooltip_keydown_controlle);
            return App.$document.off('click', _this.hide_on_document_clicked);
          };
        })(this)
      });
    };

    tooltip.prototype.hide_on_document_clicked = function(event) {
      if ($(event.target).parents('.js-tooltip-container').length === 0) {
        return this.all_hide();
      }
    };

    tooltip.prototype.toggle = function(event) {
      var left, parallel_position, rect, tooltip_position_class, top, vertical_position, window_width;
      this.$tooltip = $(event.target).parents('.js-tooltip-container').find('.js-tooltip-content');
      if (this.$tooltip.is(':visible')) {
        this.all_hide();
      } else {
        rect = event.target.getBoundingClientRect();
        top = rect.top - this.el.clientTop;
        left = rect.left - this.el.clientLeft;
        window_width = this.el.clientWidth;
        vertical_position = 'bottom';
        parallel_position = 'Left';
        if ((top - (this.$tooltip.height() + 10)) < 0) {
          vertical_position = 'top';
        }
        if ((left + (this.$tooltip.width() + 10)) > window_width) {
          parallel_position = 'Right';
        }
        tooltip_position_class = vertical_position + parallel_position + 'Tale';
        this.$tooltip.removeClass().addClass('blnTooltip').addClass('js-tooltip-content').addClass(tooltip_position_class);
        this.show();
      }
      return false;
    };

    tooltip.prototype.show = function() {
      this.all_hide();
      this.trigger('show');
      this.$tooltip.show();
      return this;
    };

    tooltip.prototype.hide = function() {
      this.trigger('hide');
      this.$tooltip.hide();
      return this;
    };

    tooltip.prototype.all_hide = function() {
      this.trigger('hide');
      this.$el.find('.js-tooltip-content').hide();
      return false;
    };

    tooltip.prototype.is_active = function() {
      if (this.$tooltip.is(':visible')) {
        return true;
      } else {
        return false;
      }
    };

    tooltip.prototype.tooltip_keydown_controlle = function(event) {
      if (this.is_active()) {
        if (event.keyCode === $.ui.keyCode.ESCAPE) {
          return this.hide();
        }
      }
    };

    return tooltip;

  })(Backbone.View);

  App.elements.about_page = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.elements.about_page.views.about_page = (function(_super) {
    __extends(about_page, _super);

    function about_page() {
      this.about_page_modal_show = __bind(this.about_page_modal_show, this);
      this._about_page_init = __bind(this._about_page_init, this);
      this.initialize = __bind(this.initialize, this);
      return about_page.__super__.constructor.apply(this, arguments);
    }

    about_page.prototype.el = '#main';

    about_page.prototype.events = {
      'click .js-about-page-anchor': 'about_page_modal_show'
    };

    about_page.prototype._modals = null;

    about_page.prototype._about_page_data = null;

    about_page.prototype.initialize = function() {
      return App.$window.on('load', this._about_page_init);
    };

    about_page.prototype._about_page_init = function() {
      var $about_page_containers;
      $about_page_containers = $('.js-about-page-container');
      if (!$about_page_containers.length) {
        return;
      }
      this._modals = {};
      return $about_page_containers.each((function(_this) {
        return function(index, element) {
          var data_name;
          data_name = $(element).attr('data-name');
          _this._about_page_data = getEmbeddedJSON($('#about_page_' + data_name));
          _this._modals[data_name] = {};
          return _.each(_this._about_page_data, function(data, i) {
            var modal;
            modal = new App.elements.modal();
            modal.set_title(data.title);
            modal.set_body(data.body);
            if (data.modal_height != null) {
              if (data.modal_height === 'low') {
                modal.set_height_low_class();
              } else {
                modal.set_height_high_class();
              }
            }
            if (parseInt(i) === 1) {
              if (typeof _this._about_page_data[parseInt(i) + 1] === 'object') {
                modal.set_footer_cancel_decide('閉じる', '次へ');
                modal.on('decide', function() {
                  _this._modals[data_name][parseInt(i) + 1].show();
                  return _this._modals[data_name][parseInt(i)].hide();
                });
              } else {
                modal.set_footer_cancel_only('閉じる');
              }
            } else {
              if (typeof _this._about_page_data[parseInt(i) + 1] === 'object') {
                modal.set_footer_cancel_decide('戻る', '次へ');
                modal.on('decide', function() {
                  _this._modals[data_name][parseInt(i) + 1].show();
                  return _this._modals[data_name][parseInt(i)].hide();
                });
              } else {
                modal.set_footer_cancel_decide('戻る', '閉じる');
                modal.on('decide', function() {
                  return _this._modals[data_name][parseInt(i)].hide();
                });
              }
              modal.$footer.find('.js-modal-cancel-button').on('click', function() {
                _this._modals[data_name][parseInt(i) - 1].show();
                return _this._modals[data_name][parseInt(i)].hide();
              });
            }
            return _this._modals[data_name][parseInt(i)] = modal;
          });
        };
      })(this));
    };

    about_page.prototype.about_page_modal_show = function(event) {
      var data_name;
      data_name = $(event.target).parents('.js-about-page-container').attr('data-name');
      return this._modals[data_name][1].show();
    };

    return about_page;

  })(Backbone.View);

  App.elements.url2link = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.elements.url2link.views.url2link = (function(_super) {
    __extends(url2link, _super);

    function url2link() {
      this.show_page_transition_confirm_modal = __bind(this.show_page_transition_confirm_modal, this);
      this.renew_observe_click_url2link = __bind(this.renew_observe_click_url2link, this);
      this.done_url2link = __bind(this.done_url2link, this);
      return url2link.__super__.constructor.apply(this, arguments);
    }

    url2link.prototype.el = null;

    url2link.prototype.modal = null;

    url2link.prototype.has_url = false;

    url2link.prototype.done_url2link = function(string) {
      var regexp;
      if (!string || typeof string !== 'string') {
        return string;
      }
      regexp = new RegExp('(<(?:img)[^<>]+")(http)(s?:\/\/[\-a-zA-Z0-9\_\.,:;\~\^\/?\@&=\+\$%#\!\(\)]+)("(?:[^<>]+)?>)', 'gi');
      string = string.replace(regexp, (function(_this) {
        return function(matches) {
          return matches.replace(/http/, '[[save::http]]').replace(/https/, '[[save::https]]');
        };
      })(this));
      regexp = new RegExp('(http)(s?:\/\/[\-a-zA-Z0-9\_\.,:;\~\^\/?\@&=\+\$%#\!\(\)]+)([\.]{3})', 'gi');
      string = string.replace(regexp, (function(_this) {
        return function(matches) {
          return matches.replace(/http/, '[[save::http]]').replace(/https/, '[[save::https]]');
        };
      })(this));
      string = string.replace(/(<a[^>]+>|<\/a>)/gi, '');
      string = string.replace(/(https?:\/\/[\-a-zA-Z0-9\_\.,:;\~\^\/?\@&=\+\$%#\!\(\)]+)/gi, (function(_this) {
        return function(url) {
          var a_tag_head, a_tag_tail;
          a_tag_head = '<a href="#" class="js-url2link">';
          a_tag_tail = '</a>';
          regexp = new RegExp('^https?://' + location.host);
          if (url.match(regexp)) {
            a_tag_head = '<a href="' + url + '" target="_blank">';
          }
          _this.has_url = true;
          return a_tag_head + url + a_tag_tail;
        };
      })(this));
      return string.replace(/\[\[save::http\]\]/gi, 'http');
    };

    url2link.prototype.renew_observe_click_url2link = function(force) {
      if (this.has_url || force === true) {
        $('.js-url2link').off().on('click', this.show_page_transition_confirm_modal);
        return this.has_url = false;
      }
    };

    url2link.prototype.show_page_transition_confirm_modal = function(event) {
      if (!this.modal) {
        this.modal = new App.elements.modal();
        this.modal.set_title('外部サイトへの移動の確認');
        this.modal.set_footer_cancel_decide('閉じる', '移動する');
      }
      this.target_url = $(event.currentTarget).text();
      this.modal.set_body('下記サイトへ移動しようとしています。<br />よろしいですか？<br /><br />' + this.target_url);
      this.modal.off('decide');
      this.modal.on('decide', (function(_this) {
        return function() {
          window.open(_this.target_url, '_blank');
          return _this.modal.hide();
        };
      })(this));
      this.modal.show();
      return false;
    };

    return url2link;

  })(Backbone.View);

  App.partials.header = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.partials.header.config.notification_balloon = {
    do_read_notification_user_company_url: "" + App.base_url + "/api/notification/do_read_notification_user_company",
    do_read_notification_user_message_url: "" + App.base_url + "/api/notification/do_read_notification_user_message"
  };

  App.partials.header.config.right_side_menu = {
    fade_speed: 100
  };

  App.partials.header.views.notification_company_balloon = (function(_super) {
    __extends(notification_company_balloon, _super);

    function notification_company_balloon() {
      this.show = __bind(this.show, this);
      this.initialize = __bind(this.initialize, this);
      return notification_company_balloon.__super__.constructor.apply(this, arguments);
    }

    notification_company_balloon.prototype.el = '#js-header-notification-user';

    notification_company_balloon.prototype.events = {
      'click #js-header-notification-user-company-anchor': 'show'
    };

    notification_company_balloon.prototype.$ballon = null;

    notification_company_balloon.prototype.$badge = null;

    notification_company_balloon.prototype.initialize = function() {
      this.$ballon = this.$el.find('#js-header-notification-user-company-balloon');
      return this.$badge = this.$el.find('#js-header-notification-user-company-unread-badge');
    };

    notification_company_balloon.prototype.show = function() {
      App.event.global_balloon_mediator.trigger('register', this.$ballon);
      if (this.$badge.length > 0 && this.$badge.is(':visible')) {
        $.ajax({
          type: 'GET',
          url: App.partials.header.config.notification_balloon.do_read_notification_user_company_url,
          cache: false,
          success: (function(_this) {
            return function() {
              return _this.$badge.hide();
            };
          })(this)
        });
      }
      return false;
    };

    return notification_company_balloon;

  })(Backbone.View);

  App.partials.header.views.notification_message_balloon = (function(_super) {
    __extends(notification_message_balloon, _super);

    function notification_message_balloon() {
      this.show = __bind(this.show, this);
      this.initialize = __bind(this.initialize, this);
      return notification_message_balloon.__super__.constructor.apply(this, arguments);
    }

    notification_message_balloon.prototype.el = '#js-header-notification-user';

    notification_message_balloon.prototype.events = {
      'click #js-header-notification-user-message-anchor': 'show'
    };

    notification_message_balloon.prototype.$ballon = null;

    notification_message_balloon.prototype.$badge = null;

    notification_message_balloon.prototype.initialize = function() {
      this.$ballon = this.$el.find('#js-header-notification-user-message-balloon');
      return this.$badge = this.$el.find('#js-header-notification-user-message-unread-badge');
    };

    notification_message_balloon.prototype.show = function() {
      App.event.global_balloon_mediator.trigger('register', this.$ballon);
      if (this.$badge.length > 0 && this.$badge.is(':visible')) {
        $.ajax({
          type: 'GET',
          url: App.partials.header.config.notification_balloon.do_read_notification_user_message_url,
          cache: false,
          success: (function(_this) {
            return function() {
              return _this.$badge.hide();
            };
          })(this)
        });
      }
      return false;
    };

    return notification_message_balloon;

  })(Backbone.View);

  App.partials.header.views.right_side_menu = (function(_super) {
    __extends(right_side_menu, _super);

    function right_side_menu() {
      this.hide = __bind(this.hide, this);
      this.show = __bind(this.show, this);
      this.toggle_menu = __bind(this.toggle_menu, this);
      this.check_click_others = __bind(this.check_click_others, this);
      this.initialize = __bind(this.initialize, this);
      return right_side_menu.__super__.constructor.apply(this, arguments);
    }

    right_side_menu.prototype.el = '.js-header-right-side-drop-down-menu';

    right_side_menu.prototype.events = {
      'click .js-header-right-side-drop-down-menu-anchor': 'toggle_menu'
    };

    right_side_menu.prototype.$container = null;

    right_side_menu.prototype.initialize = function() {
      this.$container = this.$el.find('.js-header-right-side-drop-down-menu-container');
      return App.$document.on('click', this.check_click_others);
    };

    right_side_menu.prototype.check_click_others = function(event) {
      if ((event != null ? event.target : void 0) == null) {
        return true;
      }
      if (this.$el.find(event.target).length <= 0) {
        this.hide();
      }
      return true;
    };

    right_side_menu.prototype.toggle_menu = function() {
      if (this.$container.is(':visible')) {
        this.hide();
      } else {
        this.show();
      }
      return false;
    };

    right_side_menu.prototype.show = function() {
      return this.$container.fadeIn(App.partials.header.config.right_side_menu.fade_speed);
    };

    right_side_menu.prototype.hide = function() {
      return this.$container.fadeOut(App.partials.header.config.right_side_menu.fade_speed);
    };

    return right_side_menu;

  })(Backbone.View);

  App.partials.header.views.header = (function(_super) {
    __extends(header, _super);

    function header() {
      this.initialize = __bind(this.initialize, this);
      return header.__super__.constructor.apply(this, arguments);
    }

    header.prototype.el = '#siteHeader';

    header.prototype.initialize = function() {
      var notification_company_balloon, notification_message_balloon, right_side_menu;
      App.event.global_balloon_mediator.init();
      App.event.global_balloon_mediator.register_document_clicked();
      notification_company_balloon = new App.partials.header.views.notification_company_balloon();
      notification_message_balloon = new App.partials.header.views.notification_message_balloon();
      return right_side_menu = new App.partials.header.views.right_side_menu();
    };

    return header;

  })(Backbone.View);

  add_route('.*', function() {
    if ($('#siteHeader').length > 0) {
      return new App.partials.header.views.header();
    }
  });

  App.pages.admin = {};

  App.pages.admin.aboutsite = {
    views: {},
    routes: {}
  };

  App.pages.admin.aboutsite.views.index = (function(_super) {
    __extends(index, _super);

    function index() {
      this.cb_click_reset_term_user = __bind(this.cb_click_reset_term_user, this);
      this.on_click_reset = __bind(this.on_click_reset, this);
      this.on_click_save = __bind(this.on_click_save, this);
      this.initialize = __bind(this.initialize, this);
      return index.__super__.constructor.apply(this, arguments);
    }

    index.prototype.el = '#siteContainer';

    index.prototype.events = {
      'click #js-btn-save': 'on_click_save',
      'click #js-btn-reset': 'on_click_reset'
    };

    index.prototype._sns_defaults = null;

    index.prototype._modal = null;

    index.prototype.$_form = null;

    index.prototype.$_notice = null;

    index.prototype.initialize = function() {
      this._sns_defaults = getEmbeddedJSON('#js-sns-defaults');
      this._modal = new App.elements.modal({
        title: '納品時の文面に戻す',
        body: nl2br(['すべてのウェルカムメッセージを納品時の文面に戻します。文面を編集していた場合、その内容は失われます。\n', 'よろしいですか？'].join('\n')),
        buttons_type: 'cancel_delete',
        decide_button_label: '納品時の文面に戻す'
      });
      this.$_form = $('#siteContainer form');
      this.$_notice = $('#siteContainer .notice');
      this.listenTo(this._modal, 'decide', this.cb_click_reset_term_user);
      return App.functions.disable_form_multiple_submits(this.$_form);
    };

    index.prototype.on_click_save = function(ev) {
      ev.preventDefault();
      this.$_form.submit();
      return false;
    };

    index.prototype.on_click_reset = function(ev) {
      ev.preventDefault();
      this._modal.show();
      return false;
    };

    index.prototype.cb_click_reset_term_user = function(ev) {
      _.each(['welcome_message_prospective', 'welcome_message_common_employee', 'welcome_message_etc'], (function(_this) {
        return function(name) {
          return $("textarea[name='" + name + "']").val(_this._sns_defaults[name]);
        };
      })(this));
      this.$_notice.fadeIn('500');
      $($.support.checkOn ? 'body' : 'html').animate({
        scrollTop: 0
      }, 300, 'swing');
      return this._modal.hide();
    };

    return index;

  })(Backbone.View);

  App.pages.admin.aboutsite.views.privacy = (function(_super) {
    __extends(privacy, _super);

    function privacy() {
      this.cb_click_reset_term_user = __bind(this.cb_click_reset_term_user, this);
      this.cb_click_delete_decide = __bind(this.cb_click_delete_decide, this);
      this.on_click_reset = __bind(this.on_click_reset, this);
      this.on_click_delete = __bind(this.on_click_delete, this);
      this.on_click_save = __bind(this.on_click_save, this);
      this.initialize = __bind(this.initialize, this);
      return privacy.__super__.constructor.apply(this, arguments);
    }

    privacy.prototype.el = '#siteContainer';

    privacy.prototype.events = {
      'click #js-btn-save': 'on_click_save',
      'click #js-btn-delete': 'on_click_delete',
      'click #js-btn-reset': 'on_click_reset'
    };

    privacy.prototype._sns_defaults = null;

    privacy.prototype._delete_modal = null;

    privacy.prototype._reset_modal = null;

    privacy.prototype.$_form = null;

    privacy.prototype.$_notice = null;

    privacy.prototype.initialize = function() {
      this._sns_defaults = getEmbeddedJSON('#js-sns-defaults');
      this._reset_modal = new App.elements.modal({
        body: nl2br(['プライバシーポリシーを納品時の状態に戻します。', 'すでに入力していた内容は失われますがよろしいですか？', '(納品時の状態に戻しても登録はされません)'].join('\n'))
      });
      this._reset_modal.set_footer_cancel_decide('キャンセル', '初期状態に戻す');
      this._delete_modal = new App.elements.modal({
        title: 'プライバシーポリシー(削除)',
        body: nl2br(['<div class="msgBanner notice"><p><span class="icon"><i class="icon-exclamation"></i></span><span class="txt">削除を実行しようとしています。注意文をご確認ください。</span></p></div>', 'プライバシーポリシーを削除します。削除された情報はもとに戻すことができません。\n', 'よろしいですか？'].join('\n'))
      });
      this._delete_modal.set_footer_cancel_delete('取り消す', '削除する(元に戻せません)');
      this.$_form = $('#siteContainer form');
      this.$_notice = $('#siteContainer .notice');
      this.listenTo(this._reset_modal, 'decide', this.cb_click_reset_term_user);
      this.listenTo(this._delete_modal, 'decide', this.cb_click_delete_decide);
      return App.functions.disable_form_multiple_submits(this.$_form);
    };

    privacy.prototype.on_click_save = function(ev) {
      ev.preventDefault();
      this.$_form.submit();
      return false;
    };

    privacy.prototype.on_click_delete = function(ev) {
      ev.preventDefault();
      this._delete_modal.show();
      return false;
    };

    privacy.prototype.on_click_reset = function(ev) {
      ev.preventDefault();
      this._reset_modal.show();
      return false;
    };

    privacy.prototype.cb_click_delete_decide = function(ev) {
      return $('#form_delete').submit();
    };

    privacy.prototype.cb_click_reset_term_user = function(ev) {
      $('textarea').val(this._sns_defaults.privacy_policy);
      this.$_notice.fadeIn('500');
      $($.support.checkOn ? 'body' : 'html').animate({
        scrollTop: 0
      }, 300, 'swing');
      return this._reset_modal.hide();
    };

    return privacy;

  })(Backbone.View);

  add_route('/admin/aboutsite', function() {
    return new App.pages.admin.aboutsite.views.index();
  });

  add_route('/admin/aboutsite/privacy', function() {
    return new App.pages.admin.aboutsite.views.privacy();
  });

  App.pages.admin.analysis = {
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.analysis.views.common = (function(_super) {
    __extends(common, _super);

    function common() {
      this.select_change_submit = __bind(this.select_change_submit, this);
      this.initialize = __bind(this.initialize, this);
      return common.__super__.constructor.apply(this, arguments);
    }

    common.prototype.el = '#main';

    common.prototype.events = {
      'change #js-admin-analysis-select-user-type': 'select_change_submit',
      'change #js-admin-analysis-select-user-year-and-month': 'select_change_submit'
    };

    common.prototype.initialize = function() {
      var chart, highchart_data;
      this.action = getEmbeddedJSON($('#js-analysis-action'));
      if (this.action === 'access') {
        this.y_label = 'ログインユーザー';
      } else if (this.action === 'announcement' || this.action === 'task' || this.action === 'questionnaire') {
        this.y_label = '対象者';
      } else {
        this.y_label = '該当ユーザー';
      }
      if ($('#js-highchart-data').length) {
        highchart_data = getEmbeddedJSON($('#js-highchart-data'));
        return chart = new Highcharts.Chart({
          chart: {
            renderTo: 'js-highchart-container'
          },
          title: {
            text: ''
          },
          xAxis: {
            labels: {
              rotation: this.action === 'access' && highchart_data.labels.length > 7 ? -70 : 0
            },
            categories: highchart_data.labels
          },
          yAxis: [
            {
              title: {
                text: this.y_label
              },
              stackLabels: {
                enabled: true,
                style: {
                  fontWeight: 'bold',
                  color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                },
                format: '{total}名'
              },
              allowDecimals: false,
              min: 0
            }
          ],
          tooltip: {
            formatter: function() {
              return '<b>' + this.x + '</b><br/>' + this.series.name + ': ' + this.y + '名';
            }
          },
          plotOptions: {
            column: {
              stacking: 'normal',
              dataLabels: {
                enabled: false,
                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
              }
            }
          },
          legend: {
            enabled: false
          },
          series: [
            {
              name: this.y_label,
              type: 'column',
              color: '#c4e5f5',
              data: highchart_data.counts
            }
          ],
          credits: {
            enabled: false
          }
        });
      }
    };

    common.prototype.select_change_submit = function() {
      var $form, action, action_url, content_id, user_type;
      $form = $('#js-admin-analysis-select-form');
      action = $form.attr('data-action');
      user_type = $('#js-admin-analysis-select-user-type').val();
      action_url = "" + App.base_url + "/admin/analysis/" + action;
      content_id = $form.attr('data-content-id');
      if (content_id.length && content_id) {
        action_url += "/" + content_id;
      }
      action_url += user_type === '' ? '/all' : "/" + user_type;
      if ($('#js-admin-analysis-select-user-year-and-month').length) {
        action_url += '/' + $('#js-admin-analysis-select-user-year-and-month').val();
      }
      return location.href = action_url;
    };

    return common;

  })(Backbone.View);

  add_route('/admin/analysis/?.*', function() {
    return new App.pages.admin.analysis.views.common();
  });

  App.pages.admin.calendar = {
    templates: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.calendar.events.show = _.extend({
    modal: {},
    init: function(options) {},
    set_modal: function(name, modal) {
      return this.modal[name] = modal;
    },
    show_edit_modal: function(options) {
      if ((options != null ? options.title : void 0) != null) {
        this.modal['edit'].set_title(options.title);
      }
      if ((options != null ? options.decide_button_label : void 0) != null) {
        this.modal['edit'].set_decide_button_label(options.decide_button_label);
      }
      return this.modal['edit'].show((options != null ? options.data : void 0) != null ? options.data : null);
    },
    show_delete_modal: function(options) {
      if ((options != null ? options.title : void 0) != null) {
        this.modal['delete'].set_title(options.title);
      }
      return this.modal['delete'].show((options != null ? options.data : void 0) != null ? options.data : null);
    }
  }, Backbone.Events);

  App.pages.admin.calendar.views.show = (function(_super) {
    __extends(show, _super);

    function show() {
      this._embed_to_form = __bind(this._embed_to_form, this);
      this["delete"] = __bind(this["delete"], this);
      this.update = __bind(this.update, this);
      this.initialize = __bind(this.initialize, this);
      return show.__super__.constructor.apply(this, arguments);
    }

    show.prototype.el = '#main';

    show.prototype._is_submited = false;

    show.prototype.initialize = function() {
      var $registerView, $scheduleListView, config, max_date, min_date, url2link_class;
      config = getEmbeddedJSON($('#js-embedded-config'));
      min_date = (config != null ? config.min_date : void 0) != null ? config.min_date : null;
      max_date = (config != null ? config.max_date : void 0) != null ? config.max_date : null;
      App.event.global_balloon_mediator.init();
      App.event.global_balloon_mediator.register_document_clicked();
      App.pages.admin.calendar.events.show.init();
      this.calendarView = new App.pages.admin.calendar.views.show.calendar({
        el: '#js-calendar',
        config: config
      });
      $registerView = new App.pages.admin.calendar.views.show.register({
        el: '#js-schedule-buttons',
        config: config
      });
      $scheduleListView = new App.pages.admin.calendar.views.show.schedule_list({
        el: '#js-schedule_list',
        config: config
      });
      this.$edit_form = $('#js-scheduleEditForm');
      App.functions.disable_form_multiple_submits(this.$edit_form);
      this.$delete_form = $('#js-scheduleEditForm');
      App.functions.disable_form_multiple_submits(this.$delete_form);
      this.edit_modal = new App.elements.schedule_modal.views.site({
        config: {
          minDate: min_date != null ? new Date(min_date[0], min_date[1], min_date[2]) : new Date(),
          maxDate: max_date != null ? new Date(max_date[0], max_date[1], max_date[2]) : new Date()
        }
      });
      this.listenTo(this.edit_modal, 'decide', this.update);
      App.pages.admin.calendar.events.show.set_modal('edit', this.edit_modal);
      this.delete_modal = new App.elements.schedule_modal.views["delete"];
      this.listenTo(this.delete_modal, 'decide', this["delete"]);
      App.pages.admin.calendar.events.show.set_modal('delete', this.delete_modal);
      url2link_class = new App.elements.url2link.views.url2link();
      $('tr.js-schedule td.desc p.label').each((function(_this) {
        return function(index, element) {
          return $(element).html(url2link_class.done_url2link($(element).html()));
        };
      })(this));
      return url2link_class.renew_observe_click_url2link();
    };

    show.prototype.update = function() {
      if (this._is_submited) {
        return false;
      }
      this._is_submited = true;
      this._embed_to_form(this.edit_modal, this.$edit_form, ['schedule_id', 'type', 'title', 'start_date', 'end_date', 'content']);
      return this.$edit_form.attr('action', this.edit_modal.get_by_name('schedule_id') ? "" + App.base_url + "/admin/calendar/edit" : "" + App.base_url + "/admin/calendar/create").submit();
    };

    show.prototype["delete"] = function() {
      if (this._is_submited) {
        return false;
      }
      this._is_submited = true;
      this._embed_to_form(this.delete_modal, this.$delete_form, ['schedule_id']);
      return this.$delete_form.attr('action', "" + App.base_url + "/admin/calendar/delete").submit();
    };

    show.prototype._embed_to_form = function(modal, $form, keys) {
      return _.each(keys, (function(_this) {
        return function(name) {
          return $form.append('<input type="hidden" name="' + name + '" value="' + _.escape(modal.get_by_name(name)) + '">');
        };
      })(this));
    };

    return show;

  })(Backbone.View);

  App.pages.admin.calendar.views.show.calendar = (function(_super) {
    __extends(calendar, _super);

    function calendar() {
      this.initialize = __bind(this.initialize, this);
      return calendar.__super__.constructor.apply(this, arguments);
    }

    calendar.prototype.schedules = [];

    calendar.prototype.initialize = function(options) {
      this.config = (options != null ? options.config : void 0) != null ? options.config : {};
      return this.$el.find('.js-schedule').each((function(_this) {
        return function(idx, _el) {
          var $el;
          $el = $(_el);
          return _this.schedules.push(new App.pages.admin.calendar.views.show.calendar.schedule({
            el: $el.context,
            config: _this.config
          }));
        };
      })(this));
    };

    return calendar;

  })(Backbone.View);

  App.pages.admin.calendar.views.show.schedule_list = (function(_super) {
    __extends(schedule_list, _super);

    function schedule_list() {
      this.initialize = __bind(this.initialize, this);
      return schedule_list.__super__.constructor.apply(this, arguments);
    }

    schedule_list.prototype.schedules = [];

    schedule_list.prototype.initialize = function() {
      this.config = (typeof options !== "undefined" && options !== null ? options.config : void 0) != null ? options.config : {};
      return this.$el.find('.js-schedule').each((function(_this) {
        return function(idx, _el) {
          var $el;
          $el = $(_el);
          return _this.schedules.push(new App.pages.admin.calendar.views.show.schedule_list.schedule({
            el: $el.context,
            config: _this.config
          }));
        };
      })(this));
    };

    return schedule_list;

  })(Backbone.View);

  App.pages.admin.calendar.views.show.register = (function(_super) {
    __extends(register, _super);

    function register() {
      this.show_modal = __bind(this.show_modal, this);
      this.initialize = __bind(this.initialize, this);
      return register.__super__.constructor.apply(this, arguments);
    }

    register.prototype.events = {
      'click .js-schedule_add_btn': 'show_modal'
    };

    register.prototype.initialize = function(options) {};

    register.prototype.show_modal = function(ev) {
      var type;
      ev.preventDefault();
      type = $(ev.target).attr('data-schedule-type');
      App.pages.admin.calendar.events.show.show_edit_modal({
        title: type === '2' ? '公開スケジュールの登録' : '非公開スケジュールの登録',
        decide_button_label: '登録する',
        data: {
          type: type
        }
      });
      return false;
    };

    return register;

  })(Backbone.View);

  App.pages.admin.calendar.views.show.calendar.schedule = (function(_super) {
    __extends(schedule, _super);

    function schedule() {
      this.click_anchor = __bind(this.click_anchor, this);
      this.initialize = __bind(this.initialize, this);
      return schedule.__super__.constructor.apply(this, arguments);
    }

    schedule.prototype.events = {
      'click .js-moreEvent': 'click_anchor'
    };

    schedule.prototype.initialize = function(options) {
      var _balloon;
      _balloon = this.$el.find('.blnScheduleList');
      return this.$balloon = new App.elements.schedule_balloon.views.schedule_balloon({
        el: $(_balloon)[0]
      });
    };

    schedule.prototype.click_anchor = function(ev) {
      ev.preventDefault();
      App.event.global_balloon_mediator.trigger('register', this.$balloon);
      return false;
    };

    return schedule;

  })(Backbone.View);

  App.pages.admin.calendar.views.show.schedule_list.schedule = (function(_super) {
    __extends(schedule, _super);

    function schedule() {
      this.click_delete = __bind(this.click_delete, this);
      this.click_edit = __bind(this.click_edit, this);
      this.click_icon = __bind(this.click_icon, this);
      this.initialize = __bind(this.initialize, this);
      return schedule.__super__.constructor.apply(this, arguments);
    }

    schedule.prototype.events = {
      'click .setting .uiAnchor': 'click_icon',
      'click .js-editBtn': 'click_edit',
      'click .js-deleteBtn': 'click_delete'
    };

    schedule.prototype.initialize = function(options) {
      this.config = (options != null ? options.config : void 0) != null ? options.config : {};
      this.$balloon = this.$el.find('.blnOption');
      return this.schedule = getEmbeddedJSON(this.$el.find('.js-embedded-schedule'));
    };

    schedule.prototype.click_icon = function(ev) {
      ev.preventDefault();
      App.event.global_balloon_mediator.trigger('register', this.$balloon);
      return false;
    };

    schedule.prototype.click_edit = function(ev) {
      ev.preventDefault();
      this.$balloon.hide();
      App.pages.admin.calendar.events.show.show_edit_modal({
        title: this.schedule.type === '2' ? '公開スケジュールの編集' : '非公開スケジュールの編集',
        decide_button_label: '編集する',
        data: this.schedule
      });
      return false;
    };

    schedule.prototype.click_delete = function(ev) {
      ev.preventDefault();
      this.$balloon.hide();
      App.pages.admin.calendar.events.show.show_delete_modal({
        title: this.schedule.type === '2' ? '公開スケジュールの削除' : '非公開スケジュールの削除',
        data: this.schedule
      });
      return false;
    };

    return schedule;

  })(Backbone.View);

  add_route('/admin/calendar/show(/([0-9]{6})*)*?', (function(_this) {
    return function() {
      return new App.pages.admin.calendar.views.show();
    };
  })(this));

  App.pages.admin.common = {
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.common.views.globalnavi = (function(_super) {
    __extends(globalnavi, _super);

    function globalnavi() {
      return globalnavi.__super__.constructor.apply(this, arguments);
    }

    globalnavi.prototype.$element = null;

    globalnavi.prototype.$navi = null;

    globalnavi.prototype.active_row = null;

    globalnavi.prototype.on_active = false;

    globalnavi.prototype.timer = null;

    globalnavi.prototype.delay = 500;

    globalnavi.prototype.options = {
      active_class: 'current'
    };

    globalnavi.prototype.initialize = function(element) {
      this.$element = $(element);
      this.$navi = this.$element.find('.navMain');
      this.$navi.find('li').click((function(_this) {
        return function(event) {
          return event.stopPropagation();
        };
      })(this));
      this.$navi.menuAim({
        rowSelector: 'li',
        activate: (function(_this) {
          return function(row) {
            return _this.submenu_active.call(_this, row);
          };
        })(this),
        deactivate: (function(_this) {
          return function(row) {
            return _this.submenu_deactive.call(_this, row);
          };
        })(this),
        enter: (function(_this) {
          return function(row) {
            if (_this.active_row && $(_this.active_row).is(row)) {
              return _this.on_active = true;
            }
          };
        })(this),
        exitMenu: (function(_this) {
          return function(navi) {
            _this.timer = setTimeout(function() {
              return _this.activation_delay();
            }, _this.delay);
            return false;
          };
        })(this),
        submenuDirection: 'below'
      });
      return this;
    };

    globalnavi.prototype.submenu_active = function(row) {
      var $row, $sub;
      this.active_row = row;
      $row = $(row);
      $sub = this.get_subnavi($row);
      $sub.on('mouseenter', (function(_this) {
        return function(event) {
          return _this.on_active = true;
        };
      })(this)).on('mouseleave', (function(_this) {
        return function(event) {
          _this.on_active = false;
          clearTimeout(_this.timer);
          _this.timer = setTimeout(function() {
            return _this.activation_delay();
          }, _this.delay);
          return false;
        };
      })(this)).show();
      $row.addClass(this.options.active_class);
      return this;
    };

    globalnavi.prototype.submenu_deactive = function(row) {
      var $row, $sub;
      clearTimeout(this.timer);
      $row = $(row);
      $sub = this.get_subnavi($row);
      $sub.off('mouseenter').off('mouseleave').hide();
      $row.removeClass(this.options.active_class);
      this.on_active = false;
      this.active_row = null;
      this.$navi.trigger('activate', null);
      return this;
    };

    globalnavi.prototype.get_subnavi = function($row) {
      var sub_id;
      sub_id = $row.data('submenu-id');
      return this.$element.find(sub_id);
    };

    globalnavi.prototype.activation_delay = function() {
      clearTimeout(this.timer);
      if (!this.on_active) {
        return this.submenu_deactive(this.active_row);
      }
    };

    globalnavi.prototype.close = function() {
      var $row, $sub;
      $row = this.$element.find('.' + options.active_class);
      $sub = get_subnavi($row);
      $sub.hide();
      this.$row.removeClass(this.options.active_class);
      this.$active_row = null;
      return this.on_active = false;
    };

    return globalnavi;

  })(Backbone.View);

  App.pages.admin.common.views.naviset = (function(_super) {
    var local_navis;

    __extends(naviset, _super);

    function naviset() {
      return naviset.__super__.constructor.apply(this, arguments);
    }

    local_navis = [];

    $('.navAdminGlobal').each(function(index, element) {
      local_navis[index] = new App.pages.admin.common.views.globalnavi();
      return local_navis[index].initialize(element);
    });

    $(document).on('click', function() {
      var index, _i, _len, _results;
      _results = [];
      for (_i = 0, _len = local_navis.length; _i < _len; _i++) {
        index = local_navis[_i];
        if (local_navis[index]) {
          _results.push(local_navis[index].close());
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    });

    return naviset;

  })(Backbone.View);

  add_route('/admin/?.*', function() {
    new App.pages.admin.common.views.naviset();
    new App.elements.tooltip.views.tooltip();
    return new App.elements.about_page.views.about_page();
  });

  App.pages.admin.company = {};

  App.pages.admin.company.link = {};

  App.pages.admin.company.link.index = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.company.link.index.views.link_list = (function(_super) {
    __extends(link_list, _super);

    function link_list() {
      this.cb_click_modal_decide = __bind(this.cb_click_modal_decide, this);
      this.cb_click_balloon_delete = __bind(this.cb_click_balloon_delete, this);
      this.cb_click_balloon_edit = __bind(this.cb_click_balloon_edit, this);
      this.cb_click_body = __bind(this.cb_click_body, this);
      this.on_click_setting = __bind(this.on_click_setting, this);
      this.initialize = __bind(this.initialize, this);
      return link_list.__super__.constructor.apply(this, arguments);
    }

    link_list.prototype.el = '#js-unitContent';

    link_list.prototype.events = {
      'click .js-setting-anchor': 'on_click_setting',
      'click .js-balloon-edit': 'cb_click_balloon_edit',
      'click .js-balloon-delete': 'cb_click_balloon_delete'
    };

    link_list.prototype._url_root = "" + App.base_url + "/admin/company/link";

    link_list.prototype._balloon = null;

    link_list.prototype._icon = null;

    link_list.prototype._id = null;

    link_list.prototype.$_modal = null;

    link_list.prototype.$_form = null;

    link_list.prototype.initialize = function() {
      this.$_modal = new App.elements.modal({
        body: nl2br(['<div class="msgBanner notice"><p><span class="icon"><i class="icon-exclamation"></i></span><span class="txt">削除を実行しようとしています。注意文をご確認ください。</span></p></div>', 'この関連リンクを削除します。削除すると、リンク名、URLなど、関連リンク情報が全て削除されます。削除された情報は、元に戻すことができません。\n', 'よろしいですか？'].join('\n')),
        decide_button_label: '削除する(元に戻せません)',
        cancel_button_label: '取り消す',
        buttons_type: 'cancel_delete'
      });
      this.listenTo(this.$_modal, 'decide', this.cb_click_modal_decide);
      this.$_form = jQuery('#js-form-delete-site-link');
      App.functions.disable_form_multiple_submits(this.$_form);
      return this.listenTo(App.event, 'document_clicked', this.cb_click_body);
    };

    link_list.prototype.on_click_setting = function(ev) {
      if (this._balloon != null) {
        this._balloon.hide();
      }
      this.$_icon = $(ev.target);
      this._balloon = this.$_icon.parent().next('.blnOption');
      this._balloon.show();
      this._id = this.$_icon.parent().attr('data-site-link-id');
      return ev.preventDefault();
    };

    link_list.prototype.cb_click_body = function(ev) {
      var $_el, _ref;
      if (this._balloon === null) {
        return;
      }
      $_el = $(ev.target);
      while ($_el.get(0).tagName.toLowerCase() !== 'body') {
        if ($_el.attr('data-site-link-id') === ((_ref = this.$_icon) != null ? _ref.attr('data-site-link-id') : void 0)) {
          return;
        }
        $_el = $_el.parent();
      }
      this._balloon.hide();
      return this._balloon = null;
    };

    link_list.prototype.cb_click_balloon_edit = function() {
      location.href = [this._url_root, 'edit', this._id].join('/');
      return false;
    };

    link_list.prototype.cb_click_balloon_delete = function(event) {
      this.company_link_name = $(event.target).parents('.js-admin-site-link-item').find('.title').text();
      this.$_modal.set_title(this.company_link_name + '(削除)');
      return this.$_modal.show();
    };

    link_list.prototype.cb_click_modal_decide = function() {
      var action;
      action = this.$_form.attr('action');
      if ((this._id + 0) > 0) {
        this.$_form.attr('action', [action, this._id].join('/'));
        this.$_form.submit();
      }
      return this.$_modal.hide();
    };

    return link_list;

  })(Backbone.View);

  add_route('/admin/company/link/?', function() {
    return new App.pages.admin.company.link.index.views.link_list();
  });

  App.pages.admin.company.rss = {};

  App.pages.admin.company.rss.index = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.company.rss.index.views.rss_list = (function(_super) {
    __extends(rss_list, _super);

    function rss_list() {
      this.cb_click_modal_decide = __bind(this.cb_click_modal_decide, this);
      this.cb_click_balloon_delete = __bind(this.cb_click_balloon_delete, this);
      this.cb_click_balloon_edit = __bind(this.cb_click_balloon_edit, this);
      this.cb_click_body = __bind(this.cb_click_body, this);
      this.on_click_setting = __bind(this.on_click_setting, this);
      this.initialize = __bind(this.initialize, this);
      return rss_list.__super__.constructor.apply(this, arguments);
    }

    rss_list.prototype.el = '#js-unitContent';

    rss_list.prototype.events = {
      'click .js-setting-anchor': 'on_click_setting',
      'click .js-balloon-edit': 'cb_click_balloon_edit',
      'click .js-balloon-delete': 'cb_click_balloon_delete'
    };

    rss_list.prototype._url_root = "" + App.base_url + "/admin/company/rss";

    rss_list.prototype._balloon = null;

    rss_list.prototype._icon = null;

    rss_list.prototype._id = null;

    rss_list.prototype.$_modal = null;

    rss_list.prototype.$_form = null;

    rss_list.prototype.initialize = function() {
      this.$_modal = new App.elements.modal({
        body: nl2br(['<div class="msgBanner notice"><p><span class="icon"><i class="icon-exclamation"></i></span><span class="txt">削除を実行しようとしています。注意文をご確認ください。</span></p></div>', 'この会社情報RSSを削除します。削除すると、タイトルとURLが削除されます。削除された情報は、元に戻すことができません。\n', 'よろしいですか？'].join('\n')),
        decide_button_label: '削除する(元に戻せません)',
        cancel_button_label: '取り消す',
        buttons_type: 'cancel_delete'
      });
      this.listenTo(this.$_modal, 'decide', this.cb_click_modal_decide);
      this.$_form = jQuery('#js-form-delete-rss');
      App.functions.disable_form_multiple_submits(this.$_form);
      return this.listenTo(App.event, 'document_clicked', this.cb_click_body);
    };

    rss_list.prototype.on_click_setting = function(ev) {
      if (this._balloon != null) {
        this._balloon.hide();
      }
      this.$_icon = $(ev.target);
      this._balloon = this.$_icon.parent().next('.blnOption');
      this._balloon.show();
      this._id = this.$_icon.parent().attr('data-rss-id');
      return ev.preventDefault();
    };

    rss_list.prototype.cb_click_body = function(ev) {
      var $_el, _ref;
      if (this._balloon === null) {
        return;
      }
      $_el = $(ev.target);
      while ($_el.get(0).tagName.toLowerCase() !== 'body') {
        if ($_el.attr('data-rss-id') === ((_ref = this.$_icon) != null ? _ref.attr('data-rss-id') : void 0)) {
          return;
        }
        $_el = $_el.parent();
      }
      this._balloon.hide();
      return this._balloon = null;
    };

    rss_list.prototype.cb_click_balloon_edit = function() {
      location.href = [this._url_root, 'edit', this._id].join('/');
      return false;
    };

    rss_list.prototype.cb_click_balloon_delete = function(event) {
      this.company_link_name = $(event.target).parents('.js-admin-rss-item').find('.title').text();
      this.$_modal.set_title(this.company_link_name + '(削除)');
      return this.$_modal.show();
    };

    rss_list.prototype.cb_click_modal_decide = function() {
      var action;
      action = this.$_form.attr('action');
      if ((this._id + 0) > 0) {
        this.$_form.attr('action', [action, this._id].join('/'));
        this.$_form.submit();
      }
      return this.$_modal.hide();
    };

    return rss_list;

  })(Backbone.View);

  add_route('/admin/company/rss/?', function() {
    return new App.pages.admin.company.rss.index.views.rss_list();
  });

  App.pages.admin.design = {
    views: {},
    routes: {}
  };

  App.pages.admin.design.views.index = (function(_super) {
    __extends(index, _super);

    function index() {
      this.remove_file = __bind(this.remove_file, this);
      this.submit_design_setting = __bind(this.submit_design_setting, this);
      this.show_preview = __bind(this.show_preview, this);
      this.initialize = __bind(this.initialize, this);
      return index.__super__.constructor.apply(this, arguments);
    }

    index.prototype.el = '#main';

    index.prototype.events = {
      'click #js-show-preview-btn': 'show_preview',
      'click #js-show-submit-btn': 'submit_design_setting',
      'click .js-admin-design-form-attach-file-delete': 'remove_file'
    };

    index.prototype.$form = null;

    index.prototype.$ckeditor = null;

    index.prototype.initialize = function() {
      this.$form = $('#js-design-setting-form');
      return this.$ckeditor = CKEDITOR.instances['form_login_message'];
    };

    index.prototype.show_preview = function() {
      this.$form.attr('target', '_blank');
      $('#js-design-post-mode').val('preview');
      this.$form.submit();
      return false;
    };

    index.prototype.submit_design_setting = function() {
      this.$form.attr('target', '');
      $('#js-design-post-mode').val('submit');
      this.$form.submit();
      return false;
    };

    index.prototype.remove_file = function(event) {
      var $file, $file_label;
      $file_label = $(event.target).parents('.js-admin-design-form-attach-file-label');
      $file = $file_label.find('input');
      $file.remove();
      $file_label.prepend('<input type="file" name="' + $file.attr('name') + '">');
      return false;
    };

    return index;

  })(Backbone.View);

  add_route('/admin/design', function() {
    return new App.pages.admin.design.views.index();
  });

  App.pages.admin.elearning = {
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  add_route('/admin/elearning/(?:create|edit|copy)/?.*', function() {
    return new App.pages.admin.work.driver.elements.form.views.form();
  });

  App.pages.admin.invitation = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.invitation.templates.list_modal = {
    body: _.template('<div class="textLead wysiwygContent">\n  <p>この招待メールを再利用することができます。本文をご確認のうえ、<span class="textBtnColor">「確認する」</span>をクリックしてください。</p>\n</div>\n<%= this.items(data) %>', void 0, {
      variable: 'data'
    }),
    items: _.template('<div class="listSelectItem inviteMail" style="height: 100px; overflow-y: auto;">\n  <% if (data.no_list_flg) { %>\n  <p>招待メールの履歴がありません。</p>\n  <% }else{ %>\n  <ul>\n    <% $.each(data.items, function(index, item){ %>\n    <li data-key="<%= index  %>">\n      <a href="#" class="js-admin-past-mail-list-modal-item-link">\n        <span class="item">\n          <span class="date"><%- item.created_at %></span>\n          <span class="name"><%- item.user_name %><%- item.menber_count_other %></span>\n          <span class="text">詳細を見る</span>\n        </span>\n      </a>\n    </li>\n    <% }); %>\n  </ul>\n  <% } %>\n</div>', void 0, {
      variable: 'data'
    })
  };

  App.pages.admin.invitation.templates.item_modal = {
    body: _.template('<div class="boxMailEdit">\n  <div class="label">\n    この招待メールを再利用することができます。本文をご確認のうえ、<span class="textBtnColor">「招待メールを再利用する」</span>をクリックしてください。文面を編集していた場合、その内容は失われます。<br />\n    送信日時：<%- data.item.created_at %>\n  </div>\n  <div class="content">\n    <pre class="preview">\n<%- data.item.subject %>\n<%- data.item.message1 %>\n\n<%- data.item.url %>\n\n<%- data.item.message2 %>\n    </pre>\n  </div>\n</div>', void 0, {
      variable: 'data'
    })
  };

  App.pages.admin.invitation.models.unregistered = (function(_super) {
    __extends(unregistered, _super);

    function unregistered() {
      return unregistered.__super__.constructor.apply(this, arguments);
    }

    unregistered.prototype.urlRoot = "" + App.base_url + "/api/admin/invitation/unregistered/index";

    return unregistered;

  })(Backbone.Model);

  App.pages.admin.invitation.collections.unregistered = (function(_super) {
    __extends(unregistered, _super);

    function unregistered() {
      return unregistered.__super__.constructor.apply(this, arguments);
    }

    unregistered.prototype.url = null;

    unregistered.prototype.model = App.pages.admin.invitation.models.unregistered;

    return unregistered;

  })(Backbone.Collection);

  App.pages.admin.invitation.views.unregistered = (function(_super) {
    __extends(unregistered, _super);

    function unregistered() {
      this._complete_action = __bind(this._complete_action, this);
      this._hide_all_message = __bind(this._hide_all_message, this);
      this._display_error_message = __bind(this._display_error_message, this);
      this._display_done_message = __bind(this._display_done_message, this);
      this.delete_invitation_mail_popup = __bind(this.delete_invitation_mail_popup, this);
      this.resend_invitation_mail_popup = __bind(this.resend_invitation_mail_popup, this);
      this.initialize = __bind(this.initialize, this);
      return unregistered.__super__.constructor.apply(this, arguments);
    }

    unregistered.prototype.el = '.js-tbody';

    unregistered.prototype.events = {
      'click .js-resend-invitation-mail-confirm': 'resend_invitation_mail_popup',
      'click .js-delete-invitation-mail-confirm': 'delete_invitation_mail_popup'
    };

    unregistered.prototype.resend_modal = null;

    unregistered.prototype.delete_modal = null;

    unregistered.prototype.unregistered_invitations_data = null;

    unregistered.prototype.api_base_url = null;

    unregistered.prototype.collection = null;

    unregistered.prototype.$done_message_div = $('#js-done-message');

    unregistered.prototype.$error_message_div = $('#js-error-message');

    unregistered.prototype.initialize = function() {
      this.unregistered_invitations_data = getEmbeddedJSON('#js-unregistered-invitations-data-json');
      this.collection = new App.pages.admin.invitation.collections.unregistered($.map(this.unregistered_invitations_data, (function(_this) {
        return function(invite) {
          return new App.pages.admin.invitation.models.unregistered(invite);
        };
      })(this)));
      if (this.resend_modal == null) {
        this.resend_modal = new App.elements.modal();
      }
      this.resend_modal.set_footer_cancel_decide('取り消す', '再送信する');
      if (this.delete_modal == null) {
        this.delete_modal = new App.elements.modal();
      }
      this.delete_modal.set_title('登録未完了ユーザー(削除)');
      return this.delete_modal.set_footer_cancel_delete('取り消す', '削除する(元に戻せません)');
    };

    unregistered.prototype.resend_invitation_mail_popup = function(event) {
      var full_name, invited_datetime, mail_address, title_head, user_invitation_id;
      this._hide_all_message();
      user_invitation_id = $(event.target).attr('data-id');
      mail_address = this.unregistered_invitations_data[user_invitation_id].mail_address;
      invited_datetime = this.unregistered_invitations_data[user_invitation_id].invited_datetime;
      full_name = this.unregistered_invitations_data[user_invitation_id].full_name;
      title_head = _.trim(full_name).length > (0 != null) ? full_name + ' さん' : mail_address;
      this.resend_modal.set_title('招待メールの再送信');
      this.resend_modal.set_body("<div class=\"boxMailEdit\">\n  <p>この招待メールを再利用して、同じ内容で、同じメールアドレスに送信します。内容を確認のうえ、<span class=\"textBtnColor\">「再送信する」</span>をクリックしてください。</p>\n  <div class=\"content\">\n    <pre class=\"preview\">メールアドレス(姓名)：" + this.unregistered_invitations_data[user_invitation_id].mail_address + "(" + this.unregistered_invitations_data[user_invitation_id].full_name + ")<br />" + this.unregistered_invitations_data[user_invitation_id].body + "</pre>\n  </div>\n</div>");
      this.model = this.collection.get(user_invitation_id);
      this.resend_modal.once('decide', (function(_this) {
        return function() {
          _this.resend_modal.freeze();
          return _this.model.save(null, {
            success: function(model, xhr) {
              if (((xhr != null ? xhr.error : void 0) != null) && xhr.error === 1) {
                return _this._complete_action(_this.resend_modal, 'error', xhr.message);
              } else {
                return _this._complete_action(_this.resend_modal, 'done', xhr.message);
              }
            },
            error: function(model, xhr) {
              return _this._complete_action(_this.resend_modal, 'error', xhr.response.message);
            },
            wait: true,
            silent: true
          });
        };
      })(this));
      return this.resend_modal.show();
    };

    unregistered.prototype.delete_invitation_mail_popup = function(event) {
      var full_name, invited_datetime, mail_address, title_head, user_invitation_id;
      this._hide_all_message();
      user_invitation_id = $(event.target).attr('data-id');
      mail_address = this.unregistered_invitations_data[user_invitation_id].mail_address;
      invited_datetime = this.unregistered_invitations_data[user_invitation_id].invited_datetime;
      full_name = this.unregistered_invitations_data[user_invitation_id].full_name;
      title_head = _.trim(full_name).length > (0 != null) ? full_name + ' さん' : mail_address;
      this.delete_modal.set_body('<div class="msgBanner notice"><p><span class="icon"><i class="icon-exclamation"></i></span><span class="txt">削除を実行しようとしています。注意文をご確認ください。</span></p></div>この登録未完了ユーザーを削除します。削除された情報は元に戻すことができません。<br /><br />よろしいですか？');
      this.model = this.collection.get(user_invitation_id);
      this.delete_modal.on('decide', (function(_this) {
        return function() {
          _this.delete_modal.freeze();
          return _this.model.destroy({
            success: function(model, xhr) {
              if (((xhr != null ? xhr.error : void 0) != null) && xhr.error === 1) {
                return _this._complete_action(_this.delete_modal, 'error', xhr.message);
              } else {
                $('#js-tr-' + model.id).remove();
                return _this._complete_action(_this.delete_modal, 'done', xhr.message);
              }
            },
            error: function(model, xhr) {
              return _this._complete_action(_this.delete_modal, 'error', xhr.response.message);
            }
          }, {
            wait: true
          });
        };
      })(this));
      return this.delete_modal.show();
    };

    unregistered.prototype._display_done_message = function(message) {
      this.$done_message_div.find('span.txt').text(message);
      return this.$done_message_div.show();
    };

    unregistered.prototype._display_error_message = function(message) {
      this.$error_message_div.find('span.txt').text(message);
      return this.$error_message_div.show();
    };

    unregistered.prototype._hide_all_message = function() {
      this.$done_message_div.hide();
      return this.$error_message_div.hide();
    };

    unregistered.prototype._complete_action = function(modal, message_mode, message) {
      if (message_mode === 'error') {
        this._display_error_message(message);
      } else {
        this._display_done_message(message);
      }
      modal.unfreeze();
      modal.hide();
      return App.$html.animate({
        scrollTop: 0
      }, 0);
    };

    return unregistered;

  })(Backbone.View);

  App.pages.admin.invitation.views.create = (function(_super) {
    __extends(create, _super);

    function create() {
      this.pop_mail_undo = __bind(this.pop_mail_undo, this);
      this.pop_mail_reset = __bind(this.pop_mail_reset, this);
      this.copy_invitation_mail = __bind(this.copy_invitation_mail, this);
      this.pop_invitation_detail = __bind(this.pop_invitation_detail, this);
      this.set_list_modal_contents = __bind(this.set_list_modal_contents, this);
      this.pop_list_modal = __bind(this.pop_list_modal, this);
      this.show_detail = __bind(this.show_detail, this);
      this.initialize = __bind(this.initialize, this);
      return create.__super__.constructor.apply(this, arguments);
    }

    create.prototype.el = '#js-admin-invitation-create';

    create.prototype.events = {
      'click #js-invitation-create-show-user-detail': 'show_detail',
      'click #js-past-mail-list-button': 'pop_list_modal',
      'click #js-recover-button': 'pop_mail_reset',
      'click #js-undo-button': 'pop_mail_undo'
    };

    create.prototype.initialize = function() {
      App.functions.disable_form_multiple_submits($('.js-invitation-confirm-form'));
      this.modal = new App.elements.modal();
      this.modal.set_height_low_class();
      return this.invitation_data = getEmbeddedJSON($('#js-admin-invitation-index-past-invitation_data'));
    };

    create.prototype.show_detail = function() {
      $('#js-admin-invitation-create-explanation-container').addClass('steps');
      $('#js-admin-invitation-create-explanation').slideDown(130, (function(_this) {
        return function(event) {
          return $('#js-admin-invitation-create-explanation-label').slideDown(130, function(event) {});
        };
      })(this));
      return false;
    };

    create.prototype.pop_list_modal = function() {
      this.set_list_modal_contents();
      this.modal.show();
      return false;
    };

    create.prototype.set_list_modal_contents = function() {
      var $body;
      if (this.invitation_data.length > 0) {
        this.no_list_flg = false;
      } else {
        this.no_list_flg = true;
      }
      this.modal.set_title("招待メール履歴(一覧)");
      $body = $(App.pages.admin.invitation.templates.list_modal.body({
        items: this.invitation_data,
        no_list_flg: this.no_list_flg
      }));
      $body.find('.js-admin-past-mail-list-modal-item-link').on('click', this.pop_invitation_detail);
      this.modal.set_body($body);
      this.modal.enable_cancel_auto_hide();
      return this.modal.set_footer_cancel_only('取り消す');
    };

    create.prototype.pop_invitation_detail = function(event) {
      var data_key, work_data;
      data_key = $(event.target).parents('li').attr('data-key');
      if (this.invitation_data[data_key] == null) {
        return false;
      }
      work_data = this.invitation_data[data_key];
      this.modal.set_title("招待メール履歴(詳細)");
      this.modal.set_body(App.pages.admin.invitation.templates.item_modal.body({
        item: work_data,
        data_key: data_key
      }));
      this.modal.set_footer_cancel_notice('一覧に戻る', '招待メールを再利用する');
      this.modal.disable_cancel_auto_hide();
      this.modal.on('cancel', this.set_list_modal_contents);
      this.modal.on('decide', (function(_this) {
        return function() {
          _this.copy_invitation_mail(work_data);
          return _this.modal.hide();
        };
      })(this));
      return false;
    };

    create.prototype.copy_invitation_mail = function(work_data) {
      $('.js-editModeBtn').click();
      $('#js-mailEdit01').val(work_data.message1);
      $('#js-mailEdit02').val(work_data.message2);
      return false;
    };

    create.prototype.pop_mail_reset = function(event) {
      var default_message1, default_message2;
      default_message1 = $('#js-default-message1').html();
      default_message2 = $('#js-default-message2').html();
      this.modal.show();
      this.modal.set_title("納品時の文面に戻す");
      this.modal.set_body('招待メールの本文を納品時の文面に戻します。<span class="textBtnColor">「編集する」</span>をクリックして文面を編集していた場合、その内容は失われます。<br /><br />よろしいですか？');
      this.modal.set_footer_cancel_notice('取り消す', '納品時の文面に戻す');
      this.modal.on('decide', (function(_this) {
        return function() {
          $('.js-editModeBtn').click();
          $('#js-mailEdit01').val(default_message1);
          $('#js-mailEdit02').val(default_message2);
          return _this.modal.hide();
        };
      })(this));
      return false;
    };

    create.prototype.pop_mail_undo = function(event) {
      var preview_message1, preview_message2;
      preview_message1 = $('#js-preview-message1').html();
      preview_message2 = $('#js-preview-message2').html();
      this.modal.show();
      this.modal.set_title("編集前の状態に戻す");
      this.modal.set_body('本文を編集開始直前の状態に戻します。<br /><br />よろしいですか？');
      this.modal.set_footer_cancel_notice('取り消す', '編集前の状態に戻す');
      this.modal.on('decide', (function(_this) {
        return function() {
          $('.js-editModeBtn').click();
          $('#js-mailEdit01').val(preview_message1);
          $('#js-mailEdit02').val(preview_message2);
          return _this.modal.hide();
        };
      })(this));
      return false;
    };

    return create;

  })(Backbone.View);

  App.pages.admin.invitation.views.invitation = (function(_super) {
    __extends(invitation, _super);

    function invitation() {
      return invitation.__super__.constructor.apply(this, arguments);
    }

    return invitation;

  })(Backbone.View);

  App.pages.admin.invitation.views.management_invitation = (function(_super) {
    __extends(management_invitation, _super);

    function management_invitation() {
      this.toggle_invite_history_text = __bind(this.toggle_invite_history_text, this);
      this.change_user_type_tab = __bind(this.change_user_type_tab, this);
      this.toggle_invite_mail = __bind(this.toggle_invite_mail, this);
      this.change_view_mode = __bind(this.change_view_mode, this);
      this.change_edit_mode = __bind(this.change_edit_mode, this);
      this.initialize = __bind(this.initialize, this);
      return management_invitation.__super__.constructor.apply(this, arguments);
    }

    management_invitation.prototype.collection = App.pages.admin.invitation.collections.management_invitation;

    management_invitation.prototype.initialize = function() {
      this.$invite_message_1 = $('#invite-message-1');
      this.$invite_message_2 = $('#invite-message-2');
      this.$freeze_invite_message_1 = $('#freeze-invite-message-1');
      this.$freeze_invite_message_2 = $('#freeze-invite-message-2');
      this.$btn_edit_invite_message = $('#btn-edit-invite-message');
      this.$btn_view_invite_message = $('#btn-view-invite-message');
      this.$form_type = $('#form_type');
      return this;
    };

    management_invitation.prototype.events = {
      'click #btn-edit-invite-message': 'change_edit_mode',
      'click #btn-view-invite-message': 'change_view_mode',
      'click .js-user-type-tab-link': 'change_user_type_tab',
      'click .icon-toggle-invite-history': 'toggle_invite_history_text'
    };

    management_invitation.prototype.change_edit_mode = function(evt) {
      this.toggle_invite_mail();
      return false;
    };

    management_invitation.prototype.change_view_mode = function(evt) {
      this.toggle_invite_mail();
      this.$freeze_invite_message_1.html(this.$invite_message_1.val().replace(/\n/g, '<br>'));
      this.$freeze_invite_message_2.html(this.$invite_message_2.val().replace(/\n/g, '<br>'));
      return false;
    };

    management_invitation.prototype.toggle_invite_mail = function(evt) {
      this.$btn_edit_invite_message.toggle();
      this.$btn_view_invite_message.toggle();
      this.$invite_message_1.toggle();
      this.$invite_message_2.toggle();
      this.$freeze_invite_message_1.toggle();
      this.$freeze_invite_message_2.toggle();
      return false;
    };

    management_invitation.prototype.change_user_type_tab = function(evt) {
      var $target_tab, type;
      $target_tab = $(evt.target);
      type = $target_tab.data('user-type');
      return this.$form_type.val($target_tab.data('user-type'));
    };

    management_invitation.prototype.toggle_invite_history_text = function(evt) {
      return false;
    };

    return management_invitation;

  })(App.pages.admin.invitation.views.invitation);

  add_route('/admin/invitation/?.*', function() {
    return new App.pages.admin.invitation.views.management_invitation({
      el: 'div#div-invite-create'
    });
  });

  add_route('/admin/invitation/unregistered/?.*', function() {
    return new App.pages.admin.invitation.views.unregistered();
  });

  add_route('/admin/invitation/create/?.*', function() {
    return new App.pages.admin.invitation.views.create();
  });

  App.pages.admin.mail = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.mail.views.history = (function(_super) {
    __extends(history, _super);

    function history() {
      this.view_mail_send_history_detail = __bind(this.view_mail_send_history_detail, this);
      this.initialize = __bind(this.initialize, this);
      return history.__super__.constructor.apply(this, arguments);
    }

    history.prototype.el = '.js-tbody';

    history.prototype.events = {
      'click .js-resend-from-mail-history-confirm': 'view_mail_send_history_detail'
    };

    history.prototype.mail_send_history_data = null;

    history.prototype.api_base_url = null;

    history.prototype.collection = null;

    history.prototype.$error_message_div = $('#js-error-message');

    history.prototype.initialize = function() {
      return this.mail_send_history_data = getEmbeddedJSON('#js-mail-send-history-data-json');
    };

    history.prototype.view_mail_send_history_detail = function(event) {
      var mail_send_history_id;
      this.modal = new App.elements.modal();
      mail_send_history_id = $(event.target).attr('data-id');
      this.modal.set_title('システムメール送信履歴(詳細)');
      this.modal.set_height_high_class();
      this.modal.set_body("<dl class=\"listMailItem\">\n  <dt class=\"item\">送信元</dt>\n  <dd class=\"content\"><span class=\"punctuation\">：</span>システムメール（" + this.mail_send_history_data[mail_send_history_id].from_mail_address + "）</dd>\n  <dt class=\"item\">宛先</dt>\n  <dd class=\"content\"><span class=\"punctuation\">：</span>" + this.mail_send_history_data[mail_send_history_id].full_name + "（" + this.mail_send_history_data[mail_send_history_id].mail_address + "）</dd>\n  <dt class=\"item\">送信日時</dt>\n  <dd class=\"content\"><span class=\"punctuation\">：</span>" + this.mail_send_history_data[mail_send_history_id].mail_send_datetime + "</dd>\n</dl>\n<div class=\"boxMailEdit\">\n  <div class=\"content\">\n    <pre>" + this.mail_send_history_data[mail_send_history_id].body + "</pre>\n  </div>\n</div>");
      this.modal.set_footer_cancel_only('一覧に戻る');
      return this.modal.show();
    };

    return history;

  })(Backbone.View);

  add_route('/admin/mail/history/?.*', function() {
    return new App.pages.admin.mail.views.history();
  });

  App.pages.admin.profile = {
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.profile.models.profile = (function(_super) {
    __extends(profile, _super);

    function profile() {
      return profile.__super__.constructor.apply(this, arguments);
    }

    profile.prototype.urlRoot = App.base_url + '/admin/profile/api';

    profile.prototype.defaults = {
      id: null,
      name: null,
      input_type: null,
      profile_choices: null,
      is_unused: false,
      default_permission: null
    };

    return profile;

  })(Backbone.Model);

  App.pages.admin.profile.models.basic_profile = (function(_super) {
    __extends(basic_profile, _super);

    function basic_profile() {
      return basic_profile.__super__.constructor.apply(this, arguments);
    }

    return basic_profile;

  })(App.pages.admin.profile.models.profile);

  App.pages.admin.profile.models.communication_profile = (function(_super) {
    __extends(communication_profile, _super);

    function communication_profile() {
      return communication_profile.__super__.constructor.apply(this, arguments);
    }

    communication_profile.prototype.defaults = _.extend(App.pages.admin.profile.models.profile.prototype.defaults, {
      alert_date: null
    });

    return communication_profile;

  })(App.pages.admin.profile.models.profile);

  App.pages.admin.profile.models.management_profile = (function(_super) {
    __extends(management_profile, _super);

    function management_profile() {
      return management_profile.__super__.constructor.apply(this, arguments);
    }

    return management_profile;

  })(App.pages.admin.profile.models.profile);

  App.pages.admin.profile.collections.profile = (function(_super) {
    __extends(profile, _super);

    function profile() {
      return profile.__super__.constructor.apply(this, arguments);
    }

    return profile;

  })(Backbone.Collection);

  App.pages.admin.profile.collections.basic_profile = (function(_super) {
    __extends(basic_profile, _super);

    function basic_profile() {
      return basic_profile.__super__.constructor.apply(this, arguments);
    }

    basic_profile.prototype.model = App.pages.admin.profile.models.basic_profile;

    return basic_profile;

  })(App.pages.admin.profile.collections.profile);

  App.pages.admin.profile.collections.communication_profile = (function(_super) {
    __extends(communication_profile, _super);

    function communication_profile() {
      return communication_profile.__super__.constructor.apply(this, arguments);
    }

    communication_profile.prototype.model = App.pages.admin.profile.models.communication_profile;

    return communication_profile;

  })(App.pages.admin.profile.collections.profile);

  App.pages.admin.profile.collections.management_profile = (function(_super) {
    __extends(management_profile, _super);

    function management_profile() {
      return management_profile.__super__.constructor.apply(this, arguments);
    }

    management_profile.prototype.model = App.pages.admin.profile.models.management_profile;

    return management_profile;

  })(App.pages.admin.profile.collections.profile);

  App.pages.admin.profile.views.index = (function(_super) {
    __extends(index, _super);

    function index() {
      this.show_birthday_public_change_confirm_dialog = __bind(this.show_birthday_public_change_confirm_dialog, this);
      return index.__super__.constructor.apply(this, arguments);
    }

    index.prototype.el = '#main';

    index.prototype.events = {
      'click #js-is-public-birthday': 'show_birthday_public_change_confirm_dialog'
    };

    index.prototype.modal = null;

    index.prototype.templates = {
      title: _.template('ユーザーサイトでの誕生日表示切り替え'),
      body_change_to_public: 'ユーザーサイトで誕生日を表示するように設定を切り替えます。<br />',
      body_change_to_private: 'ユーザーサイトで誕生日を表示しないように設定を切り替えます。<br />',
      body_tail: _.template('よろしいですか？<br />\n<br />\n<p>\n  <span class="textError textSmall">※ 管理サイトにおいてはチェックにかかわらず表示されます。</span>\n  <br />\n  <span class="textError textSmall">※ 表示・非表示の設定は全てのユーザーカテゴリに適用されます。</span>\n</p>'),
      label_public: _.template('<p><span class="labelTagPositive">公開</span></p>'),
      label_private: _.template('<p><span class="labelTagNegative">非公開</span></p>')
    };

    index.prototype.show_birthday_public_change_confirm_dialog = function(event) {
      var body;
      if (this.modal == null) {
        this.modal = new App.elements.modal();
      }
      this.modal.set_title(this.templates.title());
      this.modal.set_footer_cancel_decide('閉じる', '切り替える');
      this.is_public_birthday_checkbox = $(event.target);
      this.is_checked = $(this.is_public_birthday_checkbox).prop('checked') === true ? 1 : 0;
      body = this.is_checked > 0 ? this.templates.body_change_to_public : this.templates.body_change_to_private;
      body += this.templates.body_tail();
      this.modal.set_body(body);
      this.modal.off('decide');
      this.modal.on('decide', (function(_this) {
        return function() {
          _this.modal.show_loader();
          return Backbone.ajax({
            type: 'get',
            url: ("" + App.base_url + "/api/admin/profile/change_birthday_public/") + _this.is_checked,
            success: function(xhr) {
              var $public_or_private_label;
              _this.is_public_birthday_checkbox.prop('checked', _this.is_checked > 0 ? 'checked' : '');
              $public_or_private_label = _this.is_checked > 0 ? _this.templates.label_public : _this.templates.label_private;
              $(_this.is_public_birthday_checkbox).parents('tr.detaListItem').find('td.itemVisibility ').html($public_or_private_label);
              _this.modal.hide_loader();
              return _this.modal.hide();
            },
            async: true,
            cache: false
          });
        };
      })(this));
      return this.modal.show();
    };

    return index;

  })(Backbone.View);

  App.pages.admin.profile.views.profile = (function(_super) {
    __extends(profile, _super);

    function profile() {
      this.set_enabled = __bind(this.set_enabled, this);
      this.get_model_from_event = __bind(this.get_model_from_event, this);
      this.get_row_by_model = __bind(this.get_row_by_model, this);
      this["delete"] = __bind(this["delete"], this);
      this.disable = __bind(this.disable, this);
      this.enable = __bind(this.enable, this);
      this.initialize = __bind(this.initialize, this);
      return profile.__super__.constructor.apply(this, arguments);
    }

    profile.prototype.initialize = function() {
      var collection;
      collection = new this.collection();
      this.$el.find('tbody tr').each((function(_this) {
        return function(index, element) {
          var $row, added_model;
          $row = $(element);
          added_model = collection.push(JSON.parse($row.attr('data-profile')));
          return $row.attr('data-cid', added_model.cid);
        };
      })(this));
      return this.collection = collection;
    };

    profile.prototype.events = {
      'click .used-button:not(.active)': 'enable',
      'click .unused-button:not(.active)': 'disable',
      'click .item-delete-button': 'delete'
    };

    profile.prototype.enable = function(event) {
      return this.set_enabled(this.get_model_from_event(event), true);
    };

    profile.prototype.disable = function(event) {
      return this.set_enabled(this.get_model_from_event(event), false);
    };

    profile.prototype["delete"] = function(event) {
      var model;
      model = this.get_model_from_event(event);
      return model.destroy({
        success: (function(_this) {
          return function() {
            return _this.get_row_by_model(model).remove();
          };
        })(this),
        complete: (function(_this) {
          return function() {
            return log('delete complete', model);
          };
        })(this)
      });
    };

    profile.prototype.get_row_by_model = function(model) {
      return this.$el.find('tbody tr[data-cid="' + model.cid + '"]');
    };

    profile.prototype.get_model_from_event = function(event) {
      var $row;
      $row = $(event.target);
      if (event.target.nodeName.toLowerCase() !== 'tr') {
        $row = $row.parents('tr');
      }
      return this.collection.getByCid($row.attr('data-cid'));
    };

    profile.prototype.set_enabled = function(model, is_enabled) {
      if (is_enabled == null) {
        is_enabled = true;
      }
      if (!model.isNew()) {
        return model.save({
          is_unused: !is_enabled
        }, {
          url: App.base_url + '/admin/profile/set_use/' + model.get('id'),
          success: (function(_this) {
            return function(xhr, result) {
              var $buttons;
              $buttons = _this.get_row_by_model(model).find('.item-usage-buttons');
              if (!model.get('is_unused')) {
                $buttons.find('.used-button').addClass('active');
                return $buttons.find('.unused-button').removeClass('active');
              } else {
                $buttons.find('.used-button').removeClass('active');
                return $buttons.find('.unused-button').addClass('active');
              }
            };
          })(this),
          error: (function(_this) {
            return function() {
              return log('エラーがおきました');
            };
          })(this)
        });
      }
    };

    return profile;

  })(Backbone.View);

  App.pages.admin.profile.views.basic_profile = (function(_super) {
    __extends(basic_profile, _super);

    function basic_profile() {
      return basic_profile.__super__.constructor.apply(this, arguments);
    }

    basic_profile.prototype.el = '#basic-profiles';

    basic_profile.prototype.collection = App.pages.admin.profile.collections.basic_profile;

    return basic_profile;

  })(App.pages.admin.profile.views.profile);

  App.pages.admin.profile.views.communication_profile = (function(_super) {
    __extends(communication_profile, _super);

    function communication_profile() {
      return communication_profile.__super__.constructor.apply(this, arguments);
    }

    communication_profile.prototype.el = '#communication-profiles';

    communication_profile.prototype.collection = App.pages.admin.profile.collections.communication_profile;

    return communication_profile;

  })(App.pages.admin.profile.views.profile);

  App.pages.admin.profile.views.management_profile = (function(_super) {
    __extends(management_profile, _super);

    function management_profile() {
      return management_profile.__super__.constructor.apply(this, arguments);
    }

    management_profile.prototype.el = '#management-profiles';

    management_profile.prototype.collection = App.pages.admin.profile.collections.management_profile;

    return management_profile;

  })(App.pages.admin.profile.views.profile);

  add_route('/admin/profile/?.*', function() {
    new App.pages.admin.profile.views.index();
    new App.pages.admin.profile.views.basic_profile();
    new App.pages.admin.profile.views.communication_profile();
    return new App.pages.admin.profile.views.management_profile();
  });

  App.pages.admin.proxy = {
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.proxy.login = {
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.proxy.login.views.index = (function(_super) {
    __extends(index, _super);

    function index() {
      this.proxy_login_popup = __bind(this.proxy_login_popup, this);
      this.initialize = __bind(this.initialize, this);
      return index.__super__.constructor.apply(this, arguments);
    }

    index.prototype.el = '.js-proxy-login-user-list-tr';

    index.prototype.events = {
      'click .js-proxy-login-confirm': 'proxy_login_popup'
    };

    index.prototype.modal = null;

    index.prototype.initialize = function() {
      if (this.modal == null) {
        this.modal = new App.elements.modal();
      }
      this.modal.set_footer_cancel_decide('閉じる', '代理ログインする');
      return this.modal.set_title('代理ログイン（確認）');
    };

    index.prototype.proxy_login_popup = function(event) {
      var user_id, user_name;
      user_id = $(event.target).attr('data-id');
      user_name = $(event.target).attr('data-name');
      this.modal.set_body(user_name + ' さんとしてログインします。よろしいですか？');
      this.modal.once('decide', (function(_this) {
        return function() {
          var $csrf_hidden, $form, $hidden_input;
          _this.modal.freeze();
          $hidden_input = $('<input>').attr({
            type: 'hidden',
            name: 'user_id',
            value: user_id
          });
          $csrf_hidden = $('<input>').attr({
            type: 'hidden',
            name: App.config.security.csrf_token_key,
            value: fuel_fetch_token()
          });
          return $form = $('<form></form>').attr('method', 'post').attr('action', "" + App.base_url + "/admin/proxy/login/done").append($hidden_input).append($csrf_hidden).appendTo('body').submit();
        };
      })(this));
      return this.modal.show();
    };

    return index;

  })(Backbone.View);

  add_route('/admin/proxy/login/?.*', function() {
    return new App.pages.admin.proxy.login.views.index();
  });

  App.pages.admin.setting = {};

  App.pages.admin.setting.links = {
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.setting.links.models.link = (function(_super) {
    __extends(link, _super);

    function link() {
      this.reget = __bind(this.reget, this);
      this.initialize = __bind(this.initialize, this);
      return link.__super__.constructor.apply(this, arguments);
    }

    link.prototype.urlRoot = "" + App.base_url + "/api/admin/setting/links/index";

    link.prototype.defaults = {
      id: null,
      title: '',
      url: '',
      description: '',
      thumbnail: null,
      is_loaded: false
    };

    link.prototype.interval = 2000;

    link.prototype.initialize = function() {
      return this.reget();
    };

    link.prototype.reget = function() {
      if (!this.get('is_loaded')) {
        return setTimeout((function(_this) {
          return function() {
            return _this.fetch({
              success: function() {
                return _this.reget();
              }
            });
          };
        })(this), this.interval);
      }
    };

    return link;

  })(Backbone.Model);

  App.pages.admin.setting.links.collections.links = (function(_super) {
    __extends(links, _super);

    function links() {
      return links.__super__.constructor.apply(this, arguments);
    }

    links.prototype.url = "" + App.base_url + "/api/admin/setting/links/all";

    links.prototype.model = App.pages.admin.setting.links.models.link;

    return links;

  })(Backbone.Collection);

  App.pages.admin.setting.links.views.index = (function(_super) {
    __extends(index, _super);

    function index() {
      this.set_form_errors = __bind(this.set_form_errors, this);
      this.clear_form_errors = __bind(this.clear_form_errors, this);
      this.clear_form_values = __bind(this.clear_form_values, this);
      this.render_link = __bind(this.render_link, this);
      this.submit_add = __bind(this.submit_add, this);
      this.on_links_reset = __bind(this.on_links_reset, this);
      this.initialize = __bind(this.initialize, this);
      return index.__super__.constructor.apply(this, arguments);
    }

    index.prototype.el = '#admin-setting-links';

    index.prototype.collection = null;

    index.prototype.$container = null;

    index.prototype.$form = null;

    index.prototype.$input_title = null;

    index.prototype.$input_url = null;

    index.prototype.$input_description = null;

    index.prototype.templates = {
      form_errors: _.template('<div class="alert alert-danger js-admin-setting-form-error">\n  <ul>\n  <% $.each(errors, function(index, error){ %>\n    <% $.each(error, function(index, message){ %>\n    <li><%= message %></li>\n    <% }); %>\n  <% }); %>\n  </ul>\n</div>')
    };

    index.prototype.initialize = function() {
      this.$container = this.$el.find('#admin-setting-links-container');
      this.$form = this.$el.find('#admin-setting-link-add-form');
      this.$input_title = this.$form.find('[name="title"]');
      this.$input_url = this.$form.find('[name="url"]');
      this.$input_description = this.$form.find('[name="description"]');
      this.collection = new App.pages.admin.setting.links.collections.links();
      this.collection.on('add', this.on_links_reset);
      this.collection.fetch({
        update: true,
        remove: false,
        add: true
      });
      return this.$form.on('submit', this.submit_add);
    };

    index.prototype.on_links_reset = function(links) {
      return this.render_link(links);
    };

    index.prototype.submit_add = function() {
      var link;
      this.clear_form_errors();
      link = new App.pages.admin.setting.links.models.link();
      link.save({
        title: this.$input_title.val(),
        url: this.$input_url.val(),
        description: this.$input_description.val()
      }, {
        success: (function(_this) {
          return function(link) {
            _this.clear_form_values();
            return _this.render_link(link);
          };
        })(this),
        error: (function(_this) {
          return function(link, xhr) {
            if ((xhr != null ? xhr.response : void 0) != null) {
              return _this.set_form_errors(xhr.response);
            }
          };
        })(this)
      });
      return false;
    };

    index.prototype.render_link = function(link) {
      var link_view;
      link_view = new App.pages.admin.setting.links.views.link({
        model: link
      });
      return this.$container.append(link_view.render());
    };

    index.prototype.clear_form_values = function() {
      this.$input_title.val('');
      return this.$input_url.val('');
    };

    index.prototype.clear_form_errors = function() {
      return this.$form.find('.js-admin-setting-form-error').remove();
    };

    index.prototype.set_form_errors = function(errors) {
      this.clear_form_errors();
      return this.$form.append(this.templates.form_errors({
        errors: errors
      }));
    };

    return index;

  })(Backbone.View);

  App.pages.admin.setting.links.views.link = (function(_super) {
    __extends(link, _super);

    function link() {
      this.destroy_link = __bind(this.destroy_link, this);
      this.set_errors = __bind(this.set_errors, this);
      this.clear_errors = __bind(this.clear_errors, this);
      this.show_input_title = __bind(this.show_input_title, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return link.__super__.constructor.apply(this, arguments);
    }

    link.prototype.el = '<div class="js-admin-setting-links-link"></div>';

    link.prototype.model = null;

    link.prototype.templates = {
      link: _.template('<div class="row-fluid">\n<% if(has_thumbnail){ %>\n  <div class="span2">\n    <img src="<%= model.get(\'thumbnail\') %>" class="span12">\n  </div>\n  <div class="span7">\n<% }else{ %>\n  <div class="span9">\n<% } %>\n    <p>\n      <strong class="js-admin-setting-links-link-title"><%= model.get(\'title\') %></strong>\n      <% title = model.get(\'title\').replace(/"/g, \'&quot;\'); %>\n      <input type="text" value="<%= title %>" class="js-admin-setting-links-link-title-edit-input" style="display:none;">\n      <br>\n      <a href="<%= model.get(\'url\') %>" target="_blank"><%= model.get(\'url\') %></a>\n    </p>\n    <p class="js-admin-setting-links-link-description"><%= model.get(\'description\') %></p>\n    <textarea class="js-admin-setting-links-link-description-edit-input" style="display:none;"><%= model.get(\'description\') %></textarea>\n    </div>\n  <div class="span3" style="text-align: right;">\n    <p>\n      <a href="#" class="btn btn-mini js-admin-setting-links-link-edit-title"><i class="icon-pencil"></i> 編集する</a>\n      <a href="#" class="btn btn-mini btn-primary js-admin-setting-links-link-save-title" style="display:none;">\n        <i class="icon-pencil icon-white"></i> 変更を保存する\n      </a>\n      <a href="#" class="btn btn-mini btn-danger js-admin-setting-links-link-remove"><i class="icon-remove icon-white"></i> 削除する</a>\n    </p>\n  </div>\n</div>\n<hr style="margin: 5px 0;">'),
      errors: _.template('<div class="alert alert-danger js-admin-setting-form-error">\n  <ul>\n    <% $.each(errors, function(index, error){ %>\n      <% $.each(error, function(index, message){ %>\n       <li><%= message %></li>\n      <% }); %>\n    <% }); %>\n  </ul>\n</div>')
    };

    link.prototype.events = {
      'click .js-admin-setting-links-link-edit-title': 'show_input_title',
      'click .js-admin-setting-links-link-remove': 'destroy_link'
    };

    link.prototype.initialize = function() {
      return this.model.on('change', this.render);
    };

    link.prototype.render = function() {
      this.$el.empty().html(this.templates.link({
        model: this.model,
        has_thumbnail: this.model.get('thumbnail') !== null
      }));
      return this.$el;
    };

    link.prototype.show_input_title = function() {
      var $description, $input, $save_button;
      this.$el.find('.js-admin-setting-links-link-title').hide();
      this.$el.find('.js-admin-setting-links-link-edit-title').hide();
      this.$el.find('.js-admin-setting-links-link-remove').hide();
      this.$el.find('.js-admin-setting-links-link-description').hide();
      $input = this.$el.find('.js-admin-setting-links-link-title-edit-input');
      $description = this.$el.find('.js-admin-setting-links-link-description-edit-input');
      $save_button = this.$el.find('.js-admin-setting-links-link-save-title');
      $input.show();
      $description.show();
      $save_button.show();
      return $save_button.off('click').on('click', (function(_this) {
        return function() {
          _this.clear_errors();
          return _this.model.save({
            title: $input.val(),
            description: $description.val()
          }, {
            success: function() {
              return _this.render();
            },
            error: function(link, xhr) {
              if ((xhr != null ? xhr.response : void 0) != null) {
                return _this.set_errors(xhr.response);
              }
            }
          });
        };
      })(this));
    };

    link.prototype.clear_errors = function() {
      return this.$el.find('.js-admin-setting-form-error').remove();
    };

    link.prototype.set_errors = function(errors) {
      this.clear_errors();
      return this.$el.prepend(this.templates.errors({
        errors: errors
      }));
    };

    link.prototype.destroy_link = function() {
      return this.model.destroy({
        success: (function(_this) {
          return function() {
            return _this.remove();
          };
        })(this)
      });
    };

    return link;

  })(Backbone.View);

  add_route('/admin/setting/links/?', function() {
    return new App.pages.admin.setting.links.views.index();
  });

  App.pages.admin.setting.rss = {
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.setting.rss.models.rss = (function(_super) {
    __extends(rss, _super);

    function rss() {
      return rss.__super__.constructor.apply(this, arguments);
    }

    rss.prototype.urlRoot = "" + App.base_url + "/api/admin/setting/rss/index";

    rss.prototype.defaults = {
      id: null,
      title: '',
      url: ''
    };

    return rss;

  })(Backbone.Model);

  App.pages.admin.setting.rss.collections.rsses = (function(_super) {
    __extends(rsses, _super);

    function rsses() {
      return rsses.__super__.constructor.apply(this, arguments);
    }

    rsses.prototype.url = "" + App.base_url + "/api/admin/setting/rss/all";

    rsses.prototype.model = App.pages.admin.setting.rss.models.rss;

    return rsses;

  })(Backbone.Collection);

  App.pages.admin.setting.rss.views.index = (function(_super) {
    __extends(index, _super);

    function index() {
      this.set_form_errors = __bind(this.set_form_errors, this);
      this.clear_form_errors = __bind(this.clear_form_errors, this);
      this.clear_form_values = __bind(this.clear_form_values, this);
      this.render_rss = __bind(this.render_rss, this);
      this.submit_add = __bind(this.submit_add, this);
      this.on_rsses_reset = __bind(this.on_rsses_reset, this);
      this.initialize = __bind(this.initialize, this);
      return index.__super__.constructor.apply(this, arguments);
    }

    index.prototype.el = '#admin-setting-rss';

    index.prototype.collection = null;

    index.prototype.$container = null;

    index.prototype.$form = null;

    index.prototype.$input_title = null;

    index.prototype.$input_url = null;

    index.prototype.templates = {
      form_errors: _.template('<div class="alert alert-danger js-admin-setting-form-error">\n  <ul>\n  <% $.each(errors, function(index, error){ %>\n    <% $.each(error, function(index, message){ %>\n    <li><%= message %></li>\n    <% }); %>\n  <% }); %>\n  </ul>\n</div>')
    };

    index.prototype.initialize = function() {
      this.$container = this.$el.find('#admin-setting-rss-container');
      this.$form = this.$el.find('#admin-setting-rss-add-form');
      this.$input_title = this.$form.find('[name="title"]');
      this.$input_url = this.$form.find('[name="url"]');
      this.collection = new App.pages.admin.setting.rss.collections.rsses();
      this.collection.on('add', this.on_rsses_reset);
      this.collection.fetch({
        update: true,
        remove: false,
        add: true
      });
      return this.$form.on('submit', this.submit_add);
    };

    index.prototype.on_rsses_reset = function(rss) {
      return this.render_rss(rss);
    };

    index.prototype.submit_add = function() {
      var rss;
      this.clear_form_errors();
      rss = new App.pages.admin.setting.rss.models.rss();
      rss.save({
        title: this.$input_title.val(),
        url: this.$input_url.val()
      }, {
        success: (function(_this) {
          return function(rss) {
            _this.clear_form_values();
            return _this.render_rss(rss);
          };
        })(this),
        error: (function(_this) {
          return function(rss, xhr) {
            if ((xhr != null ? xhr.response : void 0) != null) {
              return _this.set_form_errors(xhr.response);
            }
          };
        })(this)
      });
      return false;
    };

    index.prototype.render_rss = function(rss) {
      var rss_view;
      rss_view = new App.pages.admin.setting.rss.views.rss({
        model: rss
      });
      return this.$container.append(rss_view.render());
    };

    index.prototype.clear_form_values = function() {
      this.$input_title.val('');
      return this.$input_url.val('');
    };

    index.prototype.clear_form_errors = function() {
      return this.$form.find('.js-admin-setting-form-error').remove();
    };

    index.prototype.set_form_errors = function(errors) {
      this.clear_form_errors();
      return this.$form.append(this.templates.form_errors({
        errors: errors
      }));
    };

    return index;

  })(Backbone.View);

  App.pages.admin.setting.rss.views.rss = (function(_super) {
    __extends(rss, _super);

    function rss() {
      this.destroy_rss = __bind(this.destroy_rss, this);
      this.set_errors = __bind(this.set_errors, this);
      this.clear_errors = __bind(this.clear_errors, this);
      this.show_input_title = __bind(this.show_input_title, this);
      this.render = __bind(this.render, this);
      return rss.__super__.constructor.apply(this, arguments);
    }

    rss.prototype.el = '<div class="js-admin-setting-rss-rsses-rss"></div>';

    rss.prototype.model = null;

    rss.prototype.templates = {
      rss: _.template('<div class="row-fluid">\n  <div class="span9">\n    <p>\n      <strong class="js-admin-setting-rss-rss-title"><%= model.get(\'title\') %></strong>\n      <% title = model.get(\'title\').replace(/"/g, \'&quot;\'); %>\n      <input type="text" value="<%= title %>" class="js-admin-setting-rss-rss-title-edit-input" style="display:none;">\n      <br>\n      <a href="<%= model.get(\'url\') %>" target="_blank"><%= model.get(\'url\') %></a>\n    </p>\n  </div>\n  <div class="span3" style="text-align: right;">\n    <p>\n      <a href="#" class="btn btn-mini js-admin-setting-rss-rss-edit-title"><i class="icon-pencil"></i> タイトルを編集する</a>\n      <a href="#" class="btn btn-mini btn-primary js-admin-setting-rss-rss-save-title" style="display:none;">\n        <i class="icon-pencil icon-white"></i> タイトルの変更を保存する\n      </a>\n      <a href="#" class="btn btn-mini btn-danger js-admin-setting-rss-rss-remove"><i class="icon-remove icon-white"></i> 削除する</a>\n    </p>\n  </div>\n</div>\n<hr style="margin: 5px 0;">'),
      errors: _.template('<div class="alert alert-danger js-admin-setting-form-error">\n  <ul>\n    <% $.each(errors, function(index, error){ %>\n      <% $.each(error, function(index, message){ %>\n       <li><%= message %></li>\n      <% }); %>\n    <% }); %>\n  </ul>\n</div>')
    };

    rss.prototype.events = {
      'click .js-admin-setting-rss-rss-edit-title': 'show_input_title',
      'click .js-admin-setting-rss-rss-remove': 'destroy_rss'
    };

    rss.prototype.render = function() {
      this.$el.empty().html(this.templates.rss({
        model: this.model
      }));
      return this.$el;
    };

    rss.prototype.show_input_title = function() {
      var $input, $save_button;
      this.$el.find('.js-admin-setting-rss-rss-title').hide();
      this.$el.find('.js-admin-setting-rss-rss-edit-title').hide();
      this.$el.find('.js-admin-setting-rss-rss-remove').hide();
      $input = this.$el.find('.js-admin-setting-rss-rss-title-edit-input');
      $save_button = this.$el.find('.js-admin-setting-rss-rss-save-title');
      $input.show();
      $save_button.show();
      return $save_button.off('click').on('click', (function(_this) {
        return function() {
          _this.clear_errors();
          return _this.model.save({
            title: $input.val()
          }, {
            success: function() {
              return _this.render();
            },
            error: function(rss, xhr) {
              if ((xhr != null ? xhr.response : void 0) != null) {
                return _this.set_errors(xhr.response);
              }
            }
          });
        };
      })(this));
    };

    rss.prototype.clear_errors = function() {
      return this.$el.find('.js-admin-setting-form-error').remove();
    };

    rss.prototype.set_errors = function(errors) {
      this.clear_errors();
      return this.$el.prepend(this.templates.errors({
        errors: errors
      }));
    };

    rss.prototype.destroy_rss = function() {
      return this.model.destroy({
        success: (function(_this) {
          return function() {
            return _this.remove();
          };
        })(this)
      });
    };

    return rss;

  })(Backbone.View);

  add_route('/admin/setting/rss/?', function() {
    return new App.pages.admin.setting.rss.views.index();
  });

  App.pages.admin.startup = {
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.startup.models.index = (function(_super) {
    __extends(index, _super);

    function index() {
      return index.__super__.constructor.apply(this, arguments);
    }

    index.prototype.urlRoot = "" + App.base_url + "/api/admin/startup/index";

    return index;

  })(Backbone.Model);

  App.pages.admin.startup.collections.index = (function(_super) {
    __extends(index, _super);

    function index() {
      return index.__super__.constructor.apply(this, arguments);
    }

    index.prototype.url = null;

    index.prototype.model = App.pages.admin.startup.models.index;

    return index;

  })(Backbone.Collection);

  App.pages.admin.startup.views.index = (function(_super) {
    __extends(index, _super);

    function index() {
      this._enable_button_to_next_page = __bind(this._enable_button_to_next_page, this);
      this._add_visited_css_class = __bind(this._add_visited_css_class, this);
      this._get_btnAction_id_by_btnAction_data_id = __bind(this._get_btnAction_id_by_btnAction_data_id, this);
      this.submit_to_next_page = __bind(this.submit_to_next_page, this);
      this.btnAction_controlle = __bind(this.btnAction_controlle, this);
      this.show_more_setting = __bind(this.show_more_setting, this);
      this.initialize = __bind(this.initialize, this);
      return index.__super__.constructor.apply(this, arguments);
    }

    index.prototype.el = '.unitSection';

    index.prototype.events = {
      'click #js-admin-startup-more-setting-show': 'show_more_setting',
      'click a.js-admin-startup-btnAction-controlle': 'btnAction_controlle',
      'submit #js-admin-startup-next-page-form': 'submit_to_next_page'
    };

    index.prototype.modal = null;

    index.prototype.btnAction_collection = null;

    index.prototype.btnAction_css_class_name = 'btnAction';

    index.prototype.btnAction_visited_css_class_name = 'btnVisited';

    index.prototype.btnAction_data_id_prefix = 'startup-setting-';

    index.prototype.site_startup_completed = 0;

    index.prototype.initialize = function() {
      var admin_startup_setting_button_clicked_list;
      this.site_startup_completed = getEmbeddedJSON('#js-site-startup-completed').site_startup_completed;
      admin_startup_setting_button_clicked_list = getEmbeddedJSON('#js-admin-startup-setting-button-clicked-list-json');
      this.btnAction_collection = new App.pages.admin.startup.collections.index();
      $('.' + this.btnAction_css_class_name).each((function(_this) {
        return function(index, element) {
          var btnAction_id, btnAction_model;
          btnAction_id = _this._get_btnAction_id_by_btnAction_data_id($(element).attr('data-id'));
          btnAction_model = new App.pages.admin.startup.models.index({
            id: parseInt(btnAction_id),
            is_required: parseInt($(element).attr('data-is-required')),
            is_clicked: 0
          });
          if (_this.site_startup_completed || $.inArray(parseInt(btnAction_id), admin_startup_setting_button_clicked_list) >= 0) {
            btnAction_model.set('is_clicked', 1);
          }
          if (btnAction_model.get('is_clicked') > 0) {
            _this._add_visited_css_class($(element));
          }
          return _this.btnAction_collection.add(btnAction_model);
        };
      })(this));
      this._enable_button_to_next_page();
      if (this.modal == null) {
        this.modal = new App.elements.modal();
      }
      this.modal.set_title('未確認のサイト設定(必須項目)があります');
      this.modal.set_body('スタートアップを完了するには、サイト設定（必須事項）をすべてご確認いただく必要があります。<br /><br /><span class="textBtnColor">「1.ユーザー利用規約を確認する」</span>、<span class="textBtnColor">「2、セキュリティ設定を確認する」</span>など、ご確認いただきたい項目のボタンはクリックすると灰色になりますが、1つでも緑色で表示されたまま<span class="textBtnColor">「確認する」</span>ボタンをクリックすると、このメッセージが表示されます。<br /><br />すべてのボタンを灰色にしてから、スタートアップ終了ページに進んでください。');
      return this.modal.set_footer_cancel_only('閉じる');
    };

    index.prototype.show_more_setting = function(event) {
      $('#js-admin-startup-more-setting').show();
      return false;
    };

    index.prototype.btnAction_controlle = function(event) {
      var btnAction_a_tag, btnAction_id, btnAction_model;
      btnAction_a_tag = $(event.currentTarget);
      if (!btnAction_a_tag.hasClass(this.btnAction_visited_css_class_name)) {
        btnAction_id = this._get_btnAction_id_by_btnAction_data_id(btnAction_a_tag.attr('data-id'));
        btnAction_model = this.btnAction_collection.get(btnAction_id);
        return btnAction_model.save(null, {
          success: (function(_this) {
            return function(btnAction_model, xhr) {
              btnAction_model.set('is_clicked', 1);
              _this._add_visited_css_class(btnAction_a_tag);
              return _this._enable_button_to_next_page();
            };
          })(this),
          silent: true
        });
      }
    };

    index.prototype.submit_to_next_page = function() {
      if ($('#admin-startup-next-page-button').hasClass('btnBasic')) {
        this.modal.show();
        return false;
      }
    };

    index.prototype._get_btnAction_id_by_btnAction_data_id = function(btnAction_data_id) {
      return btnAction_data_id.substr(this.btnAction_data_id_prefix.length);
    };

    index.prototype._add_visited_css_class = function(element) {
      return element.addClass(this.btnAction_visited_css_class_name);
    };

    index.prototype._enable_button_to_next_page = function() {
      var button_to_enable;
      button_to_enable = true;
      if (!this.site_startup_completed) {
        _.each(this.btnAction_collection.where({
          is_required: 1
        }), (function(_this) {
          return function(btnAction_model) {
            if (btnAction_model.get('is_clicked') <= 0) {
              return button_to_enable = false;
            }
          };
        })(this));
      }
      if (button_to_enable === true) {
        return $('#admin-startup-next-page-button').removeClass('btnBasic');
      }
    };

    return index;

  })(Backbone.View);

  App.pages.admin.startup.views.confirm = (function(_super) {
    __extends(confirm, _super);

    function confirm() {
      this.form_submit_for_startup_complete = __bind(this.form_submit_for_startup_complete, this);
      return confirm.__super__.constructor.apply(this, arguments);
    }

    confirm.prototype.el = '.unitSection';

    confirm.prototype.events = {
      'click #js-admin-startup-complete-button': 'form_submit_for_startup_complete'
    };

    confirm.prototype.form_submit_for_startup_complete = function() {
      return $('#js-admin-startup-complete-form').submit();
    };

    return confirm;

  })(Backbone.View);

  add_route('/admin/startup/?(index)?', function() {
    return new App.pages.admin.startup.views.index();
  });

  add_route('/admin/startup/confirm/?.*', function() {
    return new App.pages.admin.startup.views.confirm();
  });

  App.pages.admin.terms = {
    views: {},
    routes: {}
  };

  App.pages.admin.terms.views.user = (function(_super) {
    __extends(user, _super);

    function user() {
      this.cb_click_reset_term_user = __bind(this.cb_click_reset_term_user, this);
      this.on_click_reset = __bind(this.on_click_reset, this);
      this.on_click_save = __bind(this.on_click_save, this);
      this.initialize = __bind(this.initialize, this);
      return user.__super__.constructor.apply(this, arguments);
    }

    user.prototype.el = '#siteContainer';

    user.prototype.events = {
      'click #js-btn-save': 'on_click_save',
      'click #js-btn-reset': 'on_click_reset'
    };

    user.prototype._sns_defaults = null;

    user.prototype._modal = null;

    user.prototype.$_form = null;

    user.prototype.$_notice = null;

    user.prototype.initialize = function() {
      this._sns_defaults = getEmbeddedJSON('#js-sns-defaults');
      this._modal = new App.elements.modal({
        title: '納品時の文面に戻す',
        body: nl2br(['利用規約を納品時の状態に戻します。文面を編集していた場合、その内容は失われます。\n', 'よろしいですか？'].join('\n')),
        buttons_type: 'cancel_delete',
        decide_button_label: '納品時の文面に戻す'
      });
      this.$_form = $('#siteContainer form');
      this.$_notice = $('#siteContainer .notice');
      this.listenTo(this._modal, 'decide', this.cb_click_reset_term_user);
      return App.functions.disable_form_multiple_submits(this.$_form);
    };

    user.prototype.on_click_save = function(ev) {
      ev.preventDefault();
      this.$_form.submit();
      return false;
    };

    user.prototype.on_click_reset = function(ev) {
      ev.preventDefault();
      this._modal.show();
      return false;
    };

    user.prototype.cb_click_reset_term_user = function(ev) {
      $('textarea').val(this._sns_defaults.term_user);
      this.$_notice.fadeIn('500');
      $($.support.checkOn ? 'body' : 'html').animate({
        scrollTop: 0
      }, 300, 'swing');
      return this._modal.hide();
    };

    return user;

  })(Backbone.View);

  add_route('/admin/terms', function() {
    return new App.pages.admin.terms.views.user();
  });

  App.pages.admin.top = {
    template: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.top.template.schedule = {
    body: _.template('<div class="boxLabeled">\n  <p class="label" id=\'js-schedule-form-title-label\'>\n     <i class="required">*</i><label for="form_title">タイトル</label>\n  </p>\n  <p class="content">\n    <input placeholder="タイトル" name="title" type="text" id="js-schedule-form-title">\n  </p>\n</div>\n<div class="boxLabeled">\n    <p class="label" id=\'js-schedule-form-date-label\'>\n      <i class="required">*</i>期間\n    </p>\n    <div class="content">\n      <input placeholder="年/月/日" size="6" name="start_date" rel="datepicker" value="" type="text" id="js-schedule-form-start-date" class="hasDatepicker">\n      -\n      <input placeholder="年/月/日" size="6" name="end_date" rel="datepicker" value="" type="text" id="js-schedule-form-end-date" class="hasDatepicker">\n</div>\n<div class="boxLabeled">\n    <p class="label">備考</p>\n    <p class="content"><textarea rows="4" class="js-input-field" name="content" id="js-schedule-form-content"></textarea></p>\n</div>', void 0, {
      variable: 'data'
    }),
    error_banner: _.template('<div class="msgBanner error">\n    <p>\n      <span class="icon"><i class="icon-exclamation"></i></span>\n      <span class="txt">入力内容に誤りがあります</span>\n    </p>\n</div>', void 0, {
      variable: 'data'
    }),
    error_message: _.template('<em class="error">：<% _.each(data, function(value, key){  %> <%= value %><% }); %></em>', void 0, {
      variable: 'data'
    })
  };

  App.pages.admin.top.models.schedule = (function(_super) {
    __extends(schedule, _super);

    function schedule() {
      this.save_schedule = __bind(this.save_schedule, this);
      this.is_type_unpublic_schedule = __bind(this.is_type_unpublic_schedule, this);
      this.is_type_public_schedule = __bind(this.is_type_public_schedule, this);
      this.set_type_unpublic_schedule = __bind(this.set_type_unpublic_schedule, this);
      this.set_type_public_schedule = __bind(this.set_type_public_schedule, this);
      return schedule.__super__.constructor.apply(this, arguments);
    }

    schedule.prototype.urlRoot = App.base_url + '/api/schedule/save';

    schedule.prototype.defaults = {
      id: null,
      type: 0,
      start_date: '',
      end_date: '',
      title: '',
      content: ''
    };

    schedule.prototype.set_type_public_schedule = function() {
      return this.set('type', 2);
    };

    schedule.prototype.set_type_unpublic_schedule = function() {
      return this.set('type', 3);
    };

    schedule.prototype.is_type_public_schedule = function() {
      return +this.get('type') === 2;
    };

    schedule.prototype.is_type_unpublic_schedule = function() {
      return +this.get('type') === 3;
    };

    schedule.prototype.save_schedule = function(container) {
      this.set('title', container.find('#js-schedule-form-title').val());
      this.set('start_date', container.find('#js-schedule-form-start-date').val());
      this.set('end_date', container.find('#js-schedule-form-end-date').val());
      this.set('content', container.find('#js-schedule-form-content').val());
      container.find('.boxLabeled .error').remove();
      return this.save({}, {
        success: (function(_this) {
          return function() {
            return location.href = App.base_url + '/admin';
          };
        })(this),
        error: (function(_this) {
          return function(xhr, error) {
            if (container.find('.error').length === 0) {
              container.find('.dlgContainer').prepend(App.elements.form.schedule.templates.error_banner());
            }
            if (error.response != null) {
              if (error.response.title != null) {
                container.find('#js-schedule-form-title-label').append(App.elements.form.schedule.templates.error_message(error.response.title));
              }
              if ((error.response.start_date != null) || (error.response.end_date != null)) {
                return container.find('#js-schedule-form-date-label').append(App.elements.form.schedule.templates.error_message($.extend(true, error.response.start_date, error.response.end_date)));
              }
            }
          };
        })(this)
      });
    };

    return schedule;

  })(Backbone.Model);

  App.pages.admin.top.views.index = (function(_super) {
    __extends(index, _super);

    function index() {
      this.initialize = __bind(this.initialize, this);
      return index.__super__.constructor.apply(this, arguments);
    }

    index.prototype.el = '#main';

    index.prototype.initialize = function(options) {
      var access_user_count_data, chart, config, max_date, min_date;
      config = getEmbeddedJSON($('#js-embedded-config'));
      min_date = (config != null ? config.min_date : void 0) != null ? config.min_date : null;
      max_date = (config != null ? config.max_date : void 0) != null ? config.max_date : null;
      this.scheduleView = new App.pages.admin.top.views.index.public_schedule({
        el: '#js-admin-top-schedule',
        config: config
      });
      this.notificationAdminView = new App.pages.admin.top.views.index.notification_admin({
        el: '#js-admin-top-notification-admin'
      });
      if ($('#js-access-user-count-data').length) {
        access_user_count_data = getEmbeddedJSON($('#js-access-user-count-data'));
        return chart = new Highcharts.Chart({
          chart: {
            renderTo: 'js-highchart-container'
          },
          title: {
            text: ''
          },
          xAxis: {
            categories: access_user_count_data.labels
          },
          yAxis: [
            {
              title: {
                text: "ログインユーザー"
              },
              stackLabels: {
                enabled: true,
                style: {
                  fontWeight: 'bold',
                  color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                },
                format: '{total}名'
              },
              allowDecimals: false,
              min: 0
            }
          ],
          tooltip: {
            formatter: function() {
              return '<b>' + this.x + '</b><br/>' + this.series.name + ': ' + this.y + '名';
            }
          },
          plotOptions: {
            column: {
              stacking: 'normal',
              dataLabels: {
                enabled: false,
                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
              }
            }
          },
          legend: {
            enabled: false
          },
          series: [
            {
              name: "ログインユーザー",
              type: 'column',
              color: '#c4e5f5',
              data: access_user_count_data.counts
            }
          ],
          credits: {
            enabled: false
          }
        });
      }
    };

    return index;

  })(Backbone.View);

  App.pages.admin.top.views.index.public_schedule = (function(_super) {
    __extends(public_schedule, _super);

    function public_schedule() {
      this._embed_to_form = __bind(this._embed_to_form, this);
      this.submit = __bind(this.submit, this);
      this.show_modal = __bind(this.show_modal, this);
      this.initialize = __bind(this.initialize, this);
      return public_schedule.__super__.constructor.apply(this, arguments);
    }

    public_schedule.prototype.events = {
      'click .js-schedule_add_btn': 'show_modal'
    };

    public_schedule.prototype._is_submited = false;

    public_schedule.prototype.initialize = function(options) {
      var max_date, min_date, _ref, _ref1;
      min_date = (options != null ? (_ref = options.config) != null ? _ref.min_date : void 0 : void 0) ? options.config.min_date : null;
      max_date = (options != null ? (_ref1 = options.config) != null ? _ref1.max_date : void 0 : void 0) ? options.config.max_date : null;
      this.$form = $('#js-scheduleEditForm');
      this.modal = new App.elements.schedule_modal.views.site({
        buttons_type: 'cancel_decide',
        config: {
          minDate: min_date != null ? new Date(min_date[0], min_date[1], min_date[2]) : new Date(),
          maxDate: max_date != null ? new Date(max_date[0], max_date[1], max_date[2]) : new Date()
        }
      });
      this.listenTo(this.modal, 'decide', this.submit);
    };

    public_schedule.prototype.show_modal = function(ev) {
      var type;
      ev.preventDefault();
      type = $(ev.target).attr('data-schedule-type');
      this.modal.set_title(type === '2' ? '公開スケジュールの登録' : '非公開スケジュールの登録');
      this.modal.show({
        type: type
      });
      return false;
    };

    public_schedule.prototype.submit = function() {
      if (this._is_submited) {
        return false;
      }
      this._is_submited = true;
      this._embed_to_form(this.modal, this.$form, ['schedule_id', 'type', 'title', 'start_date', 'end_date', 'content']);
      return this.$form.submit();
    };

    public_schedule.prototype._embed_to_form = function(modal, $form, keys) {
      return _.each(keys, (function(_this) {
        return function(name) {
          return $form.append('<input type="hidden" name="' + name + '" value="' + _.escape(modal.get_by_name(name)) + '">');
        };
      })(this));
    };

    return public_schedule;

  })(Backbone.View);

  App.pages.admin.top.views.index.notification_admin = (function(_super) {
    __extends(notification_admin, _super);

    function notification_admin() {
      this.notification_admin_complete = __bind(this.notification_admin_complete, this);
      this.change = __bind(this.change, this);
      this.initialize = __bind(this.initialize, this);
      return notification_admin.__super__.constructor.apply(this, arguments);
    }

    notification_admin.prototype.events = {
      'click .js-labelTagPositive': 'change',
      'click .js-admin-top-notification-admin-complete': 'notification_admin_complete'
    };

    notification_admin.prototype.initialize = function() {
      this.$form = this.$el.find('#js-admin-top-notificatioin-mail-form');
      return App.functions.disable_form_multiple_submits(this.$form);
    };

    notification_admin.prototype.change = function(ev) {
      $(ev.target).find('input').attr('checked', 'checked');
      return this.$form.submit();
    };

    notification_admin.prototype.notification_admin_complete = function(ev) {
      var form;
      form = $(ev.currentTarget).parent().parent();
      form.submit();
      return false;
    };

    return notification_admin;

  })(Backbone.View);

  add_route('/admin(/top)?/?', function() {
    return new App.pages.admin.top.views.index();
  });

  App.pages.admin.user = {
    config: {},
    templates: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.user.templates.search = _.template('<form action="" method="post" accept-charset="utf-8" class="js-user-search-form">\n<table class="tableDataList form">\n<tbody>\n	<tr>\n		<th><p>名前</p></th>\n		<td><p><input type="text" name="search_option[name]" class="js-user-search-name" value="<%= posts.name %>"></p></td>\n	</tr>\n	<tr>\n		<th><p>権限</p></th>\n		<td>\n		<p>\n		<% $.each(contents.roles, function(role_value, role_name){  %>\n			<span class="item">\n				<label>\n				<input type="radio" name="search_option[role]" value="<%= role_value %>" class="js-user-search-role"\n					<% if(typeof(posts.role) != \'undefined\' && role_value == posts.role){ %>\n						checked = "checked"\n					<% } %>\n\n				 >\n				<%= role_name %>\n				</label>\n			</span>\n		<% }) %>\n		</p>\n		</td>\n	</tr>\n	<tr>\n		<th><p>メールアドレス</p></th>\n		<td><p><input type="text" name="search_option[mail]" class="js-user-search-mail" value="<%= posts.mail %>"></p></td>\n	</tr>\n	<tr>\n		<th><p>生年月</p></th>\n		<td>\n		<p>\n			<select name="search_option[birth_year]" class="js-user-search-birth-year">\n			<% $.each(contents.birth_years, function(year_value, year_name){ %>\n			<option value="<%= year_value %>"\n				<% if(year_value == posts.birth_year){ %>\n					selected = "selected"\n				<% } %> ><%= year_name %></option>\n			<% }) %>\n			</select>\n\n			<select name="search_option[birth_month]" class="js-user-search-birth-month">\n			<% $.each(contents.birth_months, function(month_value, month_name){ %>\n			<option value="<%= month_value %>"\n				<% if(month_value == posts.birth_month){ %>\n					selected = "selected"\n				<% } %> ><%= month_name %></option>\n			<% }); %>\n			</select>\n		</p>\n		</td>\n	</tr>\n	<tr>\n		<th><p>プロフィール写真</p></th>\n		<td>\n		<p>\n			<% $.each(contents.is_saved_profile_images, function(value, name){ %>\n			<span class="item">\n				<label>\n				<input name="search_option[is_saved_profile_image]" type="radio" value="<%= value %>" class="js-user-search-have-profile-image">\n				<%= name %>\n				</label>\n			</span>\n			<% }) %>\n		</p>\n		</td>\n	</tr>\n	<tr>\n		<th><p>ステータス</p></th>\n		<td><p><label><input name="search_option[is_absence]" value="1" type="checkbox" class="js-user-search-is-absent"\n			<% if(typeof(posts.is_absence) != \'undefined\' && posts.is_absence > 0){ %>\n				checked="checked"\n			<% } %> >長期不在中</label></p></td>\n	</tr>\n</tbody>\n</table>\n</form>');

  App.pages.admin.user.events.feed = _.extend({
    modal: {},
    current: null,
    init: function(options) {},
    set_modal: function(name, modal) {
      return this.modal[name] = modal;
    },
    show_modal: function(name, data) {
      var url2link_class;
      this.current = data;
      if (this.modal[name] != null) {
        this.modal[name].show(data);
      }
      url2link_class = new App.elements.url2link.views.url2link();
      return url2link_class.renew_observe_click_url2link(true);
    },
    get_modal: function(name) {
      if (this.modal[name] != null) {
        return this.modal[name];
      }
    },
    set_modal_options: function(type, options) {
      if ((options != null ? options.title : void 0) != null) {
        this.modal[type].set_title(options.title);
      }
      if ((options != null ? options.body : void 0) != null) {
        this.modal[type].set_body(options.body);
      }
      if ((options != null ? options.set_cancel_button_label : void 0) != null) {
        this.modal[type].set_cancel_button_label(options.set_cancel_button_label);
      }
      if ((options != null ? options.set_decide_button_label : void 0) != null) {
        return this.modal[type].set_decide_button_label(options.set_decide_button_label);
      }
    },
    get_current: function() {
      return this.current;
    },
    show_edit_modal: function(data) {
      return this.show_modal('edit', data);
    },
    show_delete_modal: function(data) {
      return this.show_modal('delete', data);
    }
  }, Backbone.Events);

  App.pages.admin.user.models.memo = (function(_super) {
    __extends(memo, _super);

    function memo() {
      return memo.__super__.constructor.apply(this, arguments);
    }

    memo.prototype.urlRoot = App.base_url + '/api/admin/user/memo/index';

    memo.prototype.defaults = {
      id: null,
      user_name: '',
      body: '',
      created_at: ''
    };

    return memo;

  })(Backbone.Model);

  App.pages.admin.user.collections.memos = (function(_super) {
    __extends(memos, _super);

    function memos() {
      return memos.__super__.constructor.apply(this, arguments);
    }

    memos.prototype.url = App.base_url + '/api/admin/user/memo/index';

    memos.prototype.model = App.pages.admin.user.models.memo;

    return memos;

  })(Backbone.Collection);

  App.pages.admin.user.views.index = (function(_super) {
    __extends(index, _super);

    function index() {
      this.hide_loader_and_hide_download_dialog = __bind(this.hide_loader_and_hide_download_dialog, this);
      this.polling_check_completed_excel_file = __bind(this.polling_check_completed_excel_file, this);
      this.form_send_for_download = __bind(this.form_send_for_download, this);
      this.click_multi_download = __bind(this.click_multi_download, this);
      this.form_send = __bind(this.form_send, this);
      this.click_sort_anchor = __bind(this.click_sort_anchor, this);
      this.click_mysearch_save = __bind(this.click_mysearch_save, this);
      this.click_mysearch = __bind(this.click_mysearch, this);
      this.click_seatch_option = __bind(this.click_seatch_option, this);
      this.click_mainform = __bind(this.click_mainform, this);
      this.initialize = __bind(this.initialize, this);
      return index.__super__.constructor.apply(this, arguments);
    }

    index.prototype.el = '#main';

    index.prototype.events = {
      'click #js-mainFormButton': 'click_mainform',
      'click #js-searchOptionButton': 'click_seatch_option',
      'click #js-mySearchButton': 'click_mysearch',
      'click #js-mySearchAnchor': 'click_mysearch_save',
      'click #js-MultiDownload': 'click_multi_download',
      'click .js-sortAnchor': 'click_sort_anchor'
    };

    index.prototype.mysearch_modal = null;

    index.prototype.save_modal = null;

    index.prototype.my_searches = [];

    index.prototype.download_request_count = 0;

    index.prototype.initialize = function() {
      App.event.global_balloon_mediator.init();
      App.event.global_balloon_mediator.register_document_clicked();
      if (this.download_modal == null) {
        this.download_modal = new App.elements.modal();
        this.download_modal.set_title('ユーザーの一覧(ダウンロード)');
        this.download_modal.set_body('検索条件に該当するユーザーのプロフィールをダウンロードします。<br /><br /><span class="textBtnColor">「Excel ファイルダウンロード」</span>をクリックすると、Excel のダウンロードが開始します。<br />※ダウンロードが開始されるまでに時間がかかる場合がありますが、その際は何度もクリックせずそのままお待ちください。<br /><br />ダウンロードできるファイル形式は、.xlsx形式です。お手元で開けない場合は、お手数ですがサポートデスクまでご連絡ください。');
        this.download_modal.set_footer_cancel_action('閉じる', 'Excel ファイルダウンロード');
        this.download_modal.on('decide', (function(_this) {
          return function() {
            _this.form_send_for_download(_this.$download_form);
            _this.download_request_count++;
            _this.download_modal.show_loader();
            return setTimeout(function() {
              return _this.polling_check_completed_excel_file();
            }, _this.download_request_count > 1 ? 3000 : 300);
          };
        })(this));
      }
      this.$main_form = this.$el.find('#js-mainForm');
      this.$main_form.on('submit', function() {
        $(this).append(_.template('<input type="hidden" name="<%= csrf_key %>" value="<%= csrf_token %>">', {
          csrf_key: App.config.security.csrf_token_key,
          csrf_token: fuel_fetch_token()
        }));
        return true;
      });
      this.$sort_form = this.$el.find('#js-sortForm');
      this.$sort_form.on('submit', function() {
        $(this).append(_.template('<input type="hidden" name="<%= csrf_key %>" value="<%= csrf_token %>">', {
          csrf_key: App.config.security.csrf_token_key,
          csrf_token: fuel_fetch_token()
        }));
        return true;
      });
      this.$search_option_form = this.$el.find('#js-searchOptionForm');
      this.$search_option_form.on('submit', function() {
        $(this).append(_.template('<input type="hidden" name="<%= csrf_key %>" value="<%= csrf_token %>">', {
          csrf_key: App.config.security.csrf_token_key,
          csrf_token: fuel_fetch_token()
        }));
        return true;
      });
      this.$delete_form = this.$el.find('#js-mySearchDeleteForm');
      this.$delete_form.on('submit', function() {
        $(this).append(_.template('<input type="hidden" name="<%= csrf_key %>" value="<%= csrf_token %>">', {
          csrf_key: App.config.security.csrf_token_key,
          csrf_token: fuel_fetch_token()
        }));
        return true;
      });
      this.$download_form = this.$el.find('#js-admin-user-index-multi-download-form');
      this.$download_form.on('submit', function() {
        $(this).append(_.template('<input type="hidden" name="<%= csrf_key %>" value="<%= csrf_token %>">', {
          csrf_key: App.config.security.csrf_token_key,
          csrf_token: fuel_fetch_token()
        }));
        return true;
      });
      return this.$excel_tmp_filename = $('#js-down-load-excel-tmp-filename').val();
    };

    index.prototype.click_mainform = function(ev) {
      ev.preventDefault();
      return this.form_send(this.$main_form);
    };

    index.prototype.click_seatch_option = function(ev) {
      ev.preventDefault();
      return this.form_send(this.$search_option_form);
    };

    index.prototype.click_mysearch = function(ev) {
      ev.preventDefault();
      if (this.mysearch_modal === null) {
        this.mysearch_modal = new App.elements.modal();
        this.mysearch_modal.set_title('マイ検索');
        this.mysearch_modal.set_body($('#js-mySearchLoadBody').html());
        this.mysearch_modal.set_height_high_class();
        if (this.mysearch_modal.$modal_body.find('table tbody tr').length > 0) {
          this.mysearch_modal.set_footer_cancel_decide('閉じる', '検索する');
        } else {
          this.mysearch_modal.set_footer_cancel_only('閉じる');
        }
        this.mysearch_modal.on('decide', (function(_this) {
          return function() {
            var $form;
            if ($form = _this.mysearch_modal.$modal_body.find('#js-mySearchLoadForm')) {
              $form.append(_.template('<input type="hidden" name="<%= csrf_key %>" value="<%= csrf_token %>">', {
                csrf_key: App.config.security.csrf_token_key,
                csrf_token: fuel_fetch_token()
              }));
            }
            return _this.form_send($form);
          };
        })(this));
        _.each(this.mysearch_modal.$modal_body.find('table tbody tr'), (function(_this) {
          return function(_el) {
            return _this.my_searches.push(new App.pages.admin.user.views.index.my_search({
              el: $(_el).context,
              delete_form: _this.$delete_form
            }));
          };
        })(this));
      }
      this.mysearch_modal.$modal_body.find('input[type="radio"]').first().prop('checked', true);
      return this.mysearch_modal.show();
    };

    index.prototype.click_mysearch_save = function(ev) {
      var $form;
      ev.preventDefault();
      if (this.save_modal === null) {
        this.save_modal = new App.elements.modal();
        this.save_modal.set_title('マイ検索　登録');
        this.save_modal.set_body($('#js-mySearchBody').html());
        if ($form = this.save_modal.$modal_body.find('#js-mySearchForm')) {
          $form.on('submit', function() {
            return $(this).append(_.template('<input type="hidden" name="<%= csrf_key %>" value="<%= csrf_token %>">', {
              csrf_key: App.config.security.csrf_token_key,
              csrf_token: fuel_fetch_token()
            }));
          });
        }
        this.save_modal.set_footer_decide_only('登録する');
        this.save_modal.on('decide', (function(_this) {
          return function() {
            return _this.form_send($form);
          };
        })(this));
      }
      return this.save_modal.show();
    };

    index.prototype.click_sort_anchor = function(ev) {
      var el, sort;
      ev.preventDefault();
      el = $(ev.target);
      sort = el.attr('data-sort');
      this.$sort_form.append(_.template('<input type="hidden" name="sort" value="<%= value %>">', {
        value: sort
      }));
      return this.form_send(this.$sort_form);
    };

    index.prototype.form_send = function($form) {
      App.functions.disable_form_multiple_submits($form);
      return $form.submit();
    };

    index.prototype.click_multi_download = function(submit_event) {
      return this.download_modal.show();
    };

    index.prototype.form_send_for_download = function($form) {
      return $form.submit();
    };

    index.prototype.polling_check_completed_excel_file = function() {
      return Backbone.ajax({
        type: 'get',
        url: "" + App.base_url + "/api/admin/user/check_completed_excel_file/" + this.$excel_tmp_filename,
        success: (function(_this) {
          return function(xhr) {
            if (xhr.result === 1) {
              return setTimeout(function() {
                return _this.hide_loader_and_hide_download_dialog();
              }, parseInt(xhr.wait_milli_seconds));
            } else {
              return setTimeout(function() {
                return _this.polling_check_completed_excel_file();
              }, 300);
            }
          };
        })(this),
        async: false,
        cache: false
      });
    };

    index.prototype.hide_loader_and_hide_download_dialog = function() {
      this.download_modal.hide_loader();
      return this.download_modal.hide();
    };

    return index;

  })(Backbone.View);

  App.pages.admin.user.views.feed = (function(_super) {
    __extends(feed, _super);

    function feed() {
      this["delete"] = __bind(this["delete"], this);
      this.initialize = __bind(this.initialize, this);
      return feed.__super__.constructor.apply(this, arguments);
    }

    feed.prototype.el = '.feedContainer';

    feed.prototype._is_submited = false;

    feed.prototype.articles = [];

    feed.prototype.comments = [];

    feed.prototype.initialize = function() {
      var action, chart, config, histogram_data, url2link_class;
      config = getEmbeddedJSON($('#js-embedded-config'));
      App.event.global_balloon_mediator.init();
      App.event.global_balloon_mediator.register_document_clicked();
      App.pages.admin.user.events.feed.init();
      this.$el.find('.js-boxFeedArticle').each((function(_this) {
        return function(idx, _el) {
          var $el;
          $el = $(_el);
          return _this.articles.push(new App.pages.admin.user.views.feed.article({
            el: $el.context,
            model: new App.elements.timeline.models.feed({
              id: $el.attr('data-feed_id')
            }),
            config: config
          }));
        };
      })(this));
      this.$el.find('.js-boxFeedComment').each((function(_this) {
        return function(idx, _el) {
          var $el;
          $el = $(_el);
          return _this.comments.push(new App.pages.admin.user.views.feed.comment({
            el: $el.context,
            config: config
          }));
        };
      })(this));
      this.$delete_form = $('#js-mainForm');
      this.photo_modal = new App.elements.modal({
        modal_classes: ['dialogPhoto', 'multiplePhotoModal'],
        set_buttons: false
      });
      this.photo_modal.disable_auto_set_top();
      this.photo_modal.add_container_class('jsModal-Photo');
      this.photo_modal.add_header_heding_class('postModule');
      this.photo_modal.set_title('', 'title textContent');
      App.pages.admin.user.events.feed.set_modal('photo', this.photo_modal);
      this.delete_modal = new App.elements.modal({
        title: '削除',
        body: '',
        decide_button_label: '削除する',
        buttons_type: 'cancel_delete'
      });
      this.listenTo(this.delete_modal, 'show', (function(_this) {
        return function() {
          return _this._is_submited = false;
        };
      })(this));
      this.listenTo(this.delete_modal, 'decide', this["delete"]);
      App.pages.admin.user.events.feed.set_modal('delete', this.delete_modal);
      action = getEmbeddedJSON($('#js-analysis-action'));
      if (action === 'access') {
        this.y_label = 'ログインユーザー';
      } else if (action === 'announcement' || action === 'task' || action === 'questionnaire') {
        this.y_label = '対象者';
      } else {
        this.y_label = '該当ユーザー';
      }
      this.rotation = action === 'access' && $('#js-highchart-data').length > 7 ? -70 : 0;
      if ($('#js-histogram-data').length) {
        histogram_data = getEmbeddedJSON($('#js-histogram-data'));
        chart = new Highcharts.Chart({
          chart: {
            renderTo: 'js-highchart-container'
          },
          title: {
            text: ''
          },
          xAxis: {
            categories: histogram_data.labels
          },
          yAxis: [
            {
              title: {
                text: this.y_label
              },
              stackLabels: {
                enabled: true,
                style: {
                  fontWeight: 'bold',
                  color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                },
                format: '{total}名'
              },
              allowDecimals: false,
              min: 0
            }
          ],
          tooltip: {
            formatter: function() {
              return '<b>' + this.x + '</b><br/>' + this.series.name + ': ' + this.y + '名';
            }
          },
          plotOptions: {
            column: {
              stacking: 'normal',
              dataLabels: {
                enabled: false,
                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
              }
            }
          },
          series: [
            {
              name: '本人を含む',
              type: 'column',
              data: histogram_data.counts_in_this,
              color: '#F0D273'
            }, {
              name: "他ユーザー",
              type: 'column',
              color: '#c4e5f5',
              data: histogram_data.counts_other
            }
          ],
          credits: {
            enabled: false
          }
        });
      }
      url2link_class = new App.elements.url2link.views.url2link();
      $('.feedSection').find('.wysiwygContent').each((function(_this) {
        return function(index, element) {
          return $(element).html(url2link_class.done_url2link($(element).html()));
        };
      })(this));
      $('.feedSection').find('p.title').each((function(_this) {
        return function(index, element) {
          return $(element).html(url2link_class.done_url2link($(element).html()));
        };
      })(this));
      return url2link_class.renew_observe_click_url2link();
    };

    feed.prototype["delete"] = function() {
      var data;
      if (this._is_submited) {
        return false;
      }
      this._is_submited = true;
      data = App.pages.admin.user.events.feed.get_current();
      if (!data) {
        return;
      }
      return this.$delete_form.append('<input type="hidden" name="id" value="' + data['id'] + '">').append('<input type="hidden" name="redirect_to" value="' + location.pathname + '">').attr('action', data['url']).submit();
    };

    return feed;

  })(Backbone.View);

  App.pages.admin.user.views.search = (function(_super) {
    __extends(search, _super);

    function search() {
      this.search = __bind(this.search, this);
      this.open_user_search_dialog = __bind(this.open_user_search_dialog, this);
      this.initialize = __bind(this.initialize, this);
      return search.__super__.constructor.apply(this, arguments);
    }

    search.prototype.initialize = function() {};

    search.prototype.events = {
      'click .js-search-user-modal': 'open_user_search_dialog'
    };

    search.prototype.open_user_search_dialog = function(event) {
      var modal, posts, search_contents, template;
      search_contents = getEmbeddedJSON($('#js-user-search-contents-json'), {
        roles: null,
        birth_years: null,
        birth_months: null,
        is_saved_profile_images: null
      });
      posts = getEmbeddedJSON($('#js-user-posted-search-option-json'), {
        name: null,
        role: null,
        birth_years: null,
        birth_months: null,
        is_saved_profile_images: null
      });
      template = App.pages.admin.user.templates.search({
        contents: search_contents,
        posts: posts
      });
      modal = new App.elements.modal();
      modal.add_container_class('jsModal-User-Search-Dialog');
      modal.set_title('検索オプション');
      modal.set_body(template);
      modal.set_decide_button_label('検索');
      modal.on('decide', this.search);
      return modal.show();
    };

    search.prototype.search = function(evt) {
      return $('form.js-user-search-form').submit();
    };

    return search;

  })(Backbone.View);

  App.pages.admin.user.views.memo_app = (function(_super) {
    __extends(memo_app, _super);

    function memo_app() {
      this.delete_memo_confirm = __bind(this.delete_memo_confirm, this);
      this.delete_memo = __bind(this.delete_memo, this);
      this.save_memo = __bind(this.save_memo, this);
      this.init_memo_elements = __bind(this.init_memo_elements, this);
      this.regist_memo = __bind(this.regist_memo, this);
      this.show_memo = __bind(this.show_memo, this);
      this.initialize = __bind(this.initialize, this);
      return memo_app.__super__.constructor.apply(this, arguments);
    }

    memo_app.prototype.el = 'js-user-list';

    memo_app.prototype.regist_modal = null;

    memo_app.prototype.list_modal = null;

    memo_app.prototype.delete_confirm_modal = null;

    memo_app.prototype.events = {
      'click .js-regist-memo': 'regist_memo',
      'click .js-show-memo': 'show_memo'
    };

    memo_app.prototype.templates = {
      regist_memo_modal: _.template('<div class="boxLabeled js-memo-modal">\n  <p class="label">メモ<em class="error js-alert"></em></p>\n  <p class="content"><textarea name="body" class="js-memo-body" rows="6" placeholder="このメモは、ユーザーサイトに公開されません。"></textarea></p>\n</div>'),
      list_memo_modal: _.template('<div class="boxDataList adminMemo" id="js-memo-div-<%= memo.get(\'id\') %>">\n  <dl>\n    <dt>登録日時</dt>\n    <dd class="set">:</dd>\n    <dd><%= memo.get(\'created_at\') %></dd>\n  </dl>\n  <dl>\n    <dt>メモ</dt>\n    <dd class="set">:</dd>\n    <dd><%= memo.get(\'body\') %></dd>\n  </dl>\n  <dl>\n    <dt></dt>\n    <dd></dd>\n    <dd class="delete"><% if(!is_role_gx) { %><a href="#" class="js-memo-destroy" data-id="<%= memo.get(\'id\') %>"><i class="icon-garbage"></i></a><% } %></dd>\n  </dl>\n</div>')
    };

    memo_app.prototype.user_id = null;

    memo_app.prototype.memo_id = null;

    memo_app.prototype.$memo_body = null;

    memo_app.prototype.user_memo_collection = null;

    memo_app.prototype.$user_memo_p = null;

    memo_app.prototype.$user_memo_note = null;

    memo_app.prototype.memo_max_length_at_list = 50;

    memo_app.prototype.initialize = function() {
      if (this.regist_modal == null) {
        this.regist_modal = new App.elements.modal();
      }
      this.regist_modal.set_title('メモの登録');
      this.regist_modal.set_body(this.templates.regist_memo_modal());
      this.regist_modal.set_decide_button_label('登録する');
      this.regist_modal.on('decide', this.save_memo);
      if (this.list_modal == null) {
        this.list_modal = new App.elements.modal();
      }
      this.list_modal.set_height_high_class();
      this.list_modal.set_footer_cancel_only('閉じる');
      if (this.delete_confirm_modal == null) {
        this.delete_confirm_modal = new App.elements.modal();
      }
      this.delete_confirm_modal.set_title('管理者メモ削除（確認）');
      this.delete_confirm_modal.set_body('<div class="msgBanner notice"><p><span class="icon"><i class="icon-exclamation"></i></span><span class="txt">削除を実行しようとしています。注意文をご確認ください。</span></p></div>このメモを削除します。削除されたメモは、元に戻すことができません。<br /><br />よろしいですか？');
      this.delete_confirm_modal.set_footer_cancel_delete('取り消す', '削除する(元に戻せません)');
      this.delete_confirm_modal.on('decide', this.delete_memo);
      return this.user_memo_collection = new App.pages.admin.user.collections.memos();
    };

    memo_app.prototype.show_memo = function(event) {
      this.init_memo_elements(event);
      this.list_modal.set_body('');
      this.user_memo_collection.fetch({
        url: "" + App.base_url + "/api/admin/user/memo/index/" + this.user_id,
        success: (function(_this) {
          return function(model) {
            if (_this.user_memo_collection.length <= 0) {
              return _this.list_modal.$modal_body.append('管理メモの登録はありません');
            }
            _this.url2link_class = new App.elements.url2link.views.url2link();
            _this.user_memo_collection.forEach(function(user_memo_model) {
              user_memo_model.set({
                body: _this.url2link_class.done_url2link(user_memo_model.get('body'))
              }, {
                silent: true
              });
              _this.list_modal.$modal_body.append(_this.templates.list_memo_modal({
                memo: user_memo_model,
                is_role_gx: App.classes.current_user.is_role_gx()
              }));
              _this.user_name = user_memo_model.get('user_name');
              return _this.list_modal.set_title(_this.user_name + 'さんについて(メモ)');
            });
            return _this.url2link_class.renew_observe_click_url2link();
          };
        })(this),
        error: (function(_this) {
          return function(model, xhr) {
            return _this.list_modal.$modal_body.append('管理メモデータの取得に失敗しました');
          };
        })(this),
        cache: false
      });
      this.list_modal.$modal_body.on('click', 'a.js-memo-destroy', this.delete_memo_confirm);
      return this.list_modal.show();
    };

    memo_app.prototype.regist_memo = function(event) {
      this.init_memo_elements(event);
      this.$memo_body.val('');
      $('.js-alert').text('');
      return this.regist_modal.show();
    };

    memo_app.prototype.init_memo_elements = function(event) {
      this.user_id = $(event.target).parents('tr').data('user-id');
      this.$memo_body = $('.js-memo-body');
      this.$user_memo_p = $('#js-user-memo-p-' + this.user_id);
      this.$user_memo_td = $('#js-user-memo-td-' + this.user_id);
      this.$user_memo_note = $('#js-user-memo-td-' + this.user_id).find('.none');
      return this.$user_memo_btn_show_memo = $('#js-user-memo-td-' + this.user_id).find('.js-show-memo');
    };

    memo_app.prototype.save_memo = function() {
      var memo;
      this.regist_modal.show_loader();
      memo = new App.pages.admin.user.models.memo({
        user_id: this.user_id,
        body: this.$memo_body.val()
      });
      return memo.save({}, {
        success: (function(_this) {
          return function(memo, xhr) {
            var date, date_md, memo_content;
            if (parseInt(xhr.error) === 1) {
              $('.js-alert').text(':' + xhr.message);
              return _this.regist_modal.hide_loader();
            } else {
              _this.user_memo_collection.add(memo);
              memo_content = _this.$memo_body.val();
              if (memo_content.length > _this.memo_max_length_at_list) {
                memo_content = _(memo_content).truncate(_this.memo_max_length_at_list) + '...';
              }
              date = new Date();
              date_md = parseInt(date.getMonth() + 1) + '/' + date.getDate();
              _this.$user_memo_p.html(date_md + ' ' + nl2br(memo_content));
              _this.$user_memo_note.hide();
              _this.$user_memo_btn_show_memo.show();
              _this.regist_modal.hide_loader();
              return _this.regist_modal.hide();
            }
          };
        })(this),
        error: (function(_this) {
          return function(model, xhr) {
            $('.js-alert').text(':' + xhr.response);
            return _this.regist_modal.hide_loader();
          };
        })(this),
        slient: true,
        wait: true
      });
    };

    memo_app.prototype.delete_memo = function(event) {
      this.delete_confirm_modal.show_loader();
      return this.user_memo_collection.get(this.memo_id).destroy({
        url: "" + App.base_url + "/api/admin/user/memo/index/" + this.memo_id,
        success: (function(_this) {
          return function(model) {
            var collection_length, memo_content, prev_model;
            _this.delete_confirm_modal.hide_loader();
            $('#js-memo-div-' + _this.memo_id).remove();
            _this.delete_confirm_modal.hide();
            collection_length = _this.user_memo_collection.length;
            if (collection_length <= 0) {
              $('#js-user-memo-td-' + _this.user_id).find('.none').show();
            }
            if (collection_length <= 0) {
              $('#js-user-memo-p-' + _this.user_id).text('');
              $('#js-user-memo-td-' + _this.user_id).find('.none').show();
              _this.delete_confirm_modal.hide_loader();
              _this.list_modal.hide();
              return _this.$user_memo_btn_show_memo.hide();
            } else {
              prev_model = _this.user_memo_collection.at(collection_length - 1);
              memo_content = prev_model.get('body');
              if (memo_content.length > _this.memo_max_length_at_list) {
                memo_content = _(memo_content).truncate(_this.memo_max_length_at_list) + '...';
              }
              _this.$user_memo_btn_show_memo.show();
              $('#js-user-memo-p-' + _this.user_id).html(prev_model.get('created_at_short') + ' ' + memo_content);
              return _this.delete_confirm_modal.hide_loader();
            }
          };
        })(this)
      });
    };

    memo_app.prototype.delete_memo_confirm = function(event) {
      this.memo_id = $(event.currentTarget).attr('data-id');
      return this.delete_confirm_modal.show();
    };

    return memo_app;

  })(Backbone.View);

  App.pages.admin.user.views.memo_list = (function(_super) {
    __extends(memo_list, _super);

    function memo_list() {
      this.reset_memos = __bind(this.reset_memos, this);
      this.add_memo = __bind(this.add_memo, this);
      this.reset = __bind(this.reset, this);
      this.initialize = __bind(this.initialize, this);
      return memo_list.__super__.constructor.apply(this, arguments);
    }

    memo_list.prototype.user_id = null;

    memo_list.prototype.collection = App.pages.admin.user.collections.memos;

    memo_list.prototype.el = '.js-memo-list-container';

    memo_list.prototype.table = null;

    memo_list.prototype.templates = {
      table: _.template('<table class="tableDataGrid adminDlgEachQst"></table>')
    };

    memo_list.prototype.initialize = function(obj) {
      this.$el.empty();
      this.user_id = obj.user_id;
      return this.reset();
    };

    memo_list.prototype.reset = function() {
      this.collection = new this.collection();
      this.collection.url = App.base_url + '/api/admin/user/memo/index/' + this.user_id;
      return this.collection.fetch({
        dataType: 'json',
        success: (function(_this) {
          return function(memos) {
            return _this.reset_memos(memos);
          };
        })(this)
      });
    };

    memo_list.prototype.add_memo = function(memo) {
      var memo_view;
      memo_view = new App.pages.admin.user.views.memo({
        model: memo
      });
      if (this.table === null) {
        this.table = $(this.templates.table());
      }
      return $(this.table).append(memo_view.render());
    };

    memo_list.prototype.reset_memos = function(memos) {
      memos.forEach((function(_this) {
        return function(memo) {
          return _this.add_memo(memo);
        };
      })(this));
      return this.$el.html(this.table);
    };

    return memo_list;

  })(Backbone.View);

  App.pages.admin.user.views.memo = (function(_super) {
    __extends(memo, _super);

    function memo() {
      this.clear = __bind(this.clear, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return memo.__super__.constructor.apply(this, arguments);
    }

    memo.prototype.el = null;

    memo.prototype.model = null;

    memo.prototype.templates = {
      row: _.template('<tr>\n  <td class="span1">\n    <a class="btn btn-small js-memo-destroy">\n      <span class="labelTagNotice">削除する</span>\n    </a>\n  </td>\n  <td><%= memo.get(\'body\') %></td>\n</tr>')
    };

    memo.prototype.events = {
      'click a.js-memo-destroy': 'clear'
    };

    memo.prototype.initialize = function(obj) {
      this.model = obj.model;
      this.listenTo(this.model, 'destroy', this.remove);
      return this.$el = $(this.templates.row({
        memo: this.model,
        base_url: App.base_url
      }));
    };

    memo.prototype.render = function() {
      return this.$el;
    };

    memo.prototype.clear = function(event) {
      return this.model.destroy();
    };

    return memo;

  })(Backbone.View);

  App.pages.admin.user.views.decline_possibility = (function(_super) {
    __extends(decline_possibility, _super);

    function decline_possibility() {
      this.decline_possibility_setting_change = __bind(this.decline_possibility_setting_change, this);
      return decline_possibility.__super__.constructor.apply(this, arguments);
    }

    decline_possibility.prototype.events = {
      'click .js-labelTagPositive': 'decline_possibility_setting_change'
    };

    decline_possibility.prototype.decline_possibility_setting_change = function(event) {
      $(event.target).find('input').attr('checked', 'checked');
      return $('#js-admin-user-index-change-decline_possibility_setting-form').submit();
    };

    return decline_possibility;

  })(Backbone.View);

  App.pages.admin.user.views.feed.article = (function(_super) {
    __extends(article, _super);

    function article() {
      this.open_modal = __bind(this.open_modal, this);
      this.get_model_from_collection = __bind(this.get_model_from_collection, this);
      this.show_next_modal = __bind(this.show_next_modal, this);
      this.show_previous_modal = __bind(this.show_previous_modal, this);
      this.click_image = __bind(this.click_image, this);
      this.click_delete_image = __bind(this.click_delete_image, this);
      this.click_delete = __bind(this.click_delete, this);
      this.click_setting = __bind(this.click_setting, this);
      this.click_star = __bind(this.click_star, this);
      this.initialize = __bind(this.initialize, this);
      return article.__super__.constructor.apply(this, arguments);
    }

    article.prototype.events = {
      'click .photo img': 'click_image',
      'click .js-articleBoxFeedFooter .js-uiAnchorStar': 'click_star',
      'click .js-articleBoxFeedFooter .js-uiAnchorComment': 'click_comment',
      'click .js-articleBoxFeedFooter .js-uiAnchorSetting': 'click_setting',
      'click .js-articleBoxFeedFooter .js-deleteBtn': 'click_delete'
    };

    article.prototype.feed_image = null;

    article.prototype.photos = [];

    article.prototype.files = [];

    article.prototype.collections = {};

    article.prototype.current_modal_feed_image = null;

    article.prototype.current_modal_feed_image_feed_id = null;

    article.prototype.initialize = function(options) {
      var feed_id;
      this.config = (options != null ? options.config : void 0) != null ? options.config : {};
      this.$star_balloon = this.$el.find('.js-articleBoxFeedFooter .star .blnMember');
      this.$menu_balloon = this.$el.find('.js-articleBoxFeedFooter .blnOption');
      this.$el.find('.js-boxPhotoItem').each((function(_this) {
        return function(idx, _el) {
          var $el;
          $el = $(_el);
          return _this.photos.push(new App.pages.admin.user.views.feed.photo({
            el: $el.context
          }));
        };
      })(this));
      this.$el.find('.js-listFileItem').each((function(_this) {
        return function(idx, _el) {
          var $el;
          $el = $(_el);
          return _this.files.push(new App.pages.admin.user.views.feed.file({
            el: $el.context
          }));
        };
      })(this));
      feed_id = this.model.get('id');
      this.collections[feed_id] = new Backbone.Collection();
      return $('#js-feed-' + feed_id).find('div.boxPhotoItem').each((function(_this) {
        return function(index, element) {
          return _this.collections[feed_id].push(new App.elements.timeline.models.feed_image({
            id: $(element).find('p.photo img').attr('data-feed_image_id')
          }));
        };
      })(this));
    };

    article.prototype.click_star = function(ev) {
      ev.preventDefault();
      App.event.global_balloon_mediator.trigger('register', this.$star_balloon);
      return false;
    };

    article.prototype.click_setting = function(ev) {
      ev.preventDefault();
      App.event.global_balloon_mediator.trigger('register', this.$menu_balloon);
      return false;
    };

    article.prototype.click_delete = function(ev) {
      ev.preventDefault();
      this.$menu_balloon.hide();
      App.pages.admin.user.events.feed.set_modal_options('delete', {
        title: '投稿(削除)',
        body: '<div class="msgBanner notice"><p><span class="icon"><i class="icon-exclamation"></i></span><span class="txt">削除を実行しようとしています。注意文をご確認ください。</span></p></div> この投稿を削除します。削除すると、この投稿へのコメント、スター、写真、ファイルが削除されます。削除された情報は元に戻すことができません。<br /> <br /> よろしいですか？',
        set_decide_button_label: '削除する(元に戻せません)'
      });
      App.pages.admin.user.events.feed.show_delete_modal({
        url: "" + App.base_url + "/admin/user/feed/delete",
        id: this.$el.attr('data-feed_id')
      });
      return false;
    };

    article.prototype.click_delete_image = function(obj) {
      this.photo_menu_balloon.hide();
      App.pages.admin.user.events.feed.set_modal_options('delete', {
        title: '画像の削除',
        body: 'この動作はもとに戻せませんが本当によろしいですか？'
      });
      App.pages.admin.user.events.feed.show_delete_modal({
        url: "" + App.base_url + "/admin/user/feed_image/delete",
        id: this.feed_image.get('id')
      });
      return false;
    };

    article.prototype.click_image = function(ev) {
      var feed_id, feed_image_id, _el, _feed_images;
      ev.preventDefault();
      _el = ev.target;
      if (_el) {
        _feed_images = [];
        feed_id = $(_el).attr('data-feed_id');
        feed_image_id = $(_el).attr('data-feed_image_id');
        this.feed_image = this.collections[feed_id].get({
          id: feed_image_id
        });
        this.current_modal_feed_image_feed_id = feed_id;
        this.feed_image.urlRoot = "" + App.base_url + "/api/admin/timeline/feed_image";
        return this.feed_image.fetch({
          cache: false,
          success: (function(_this) {
            return function() {
              var $comments_container, comment_url_base, modal, star_url_base, _comments_view, _stars_view;
              modal = App.pages.admin.user.events.feed.get_modal('photo');
              modal.set_title(_this.feed_image.get('file').get('caption'));
              modal.set_body(App.elements.timeline.templates.feed_image_modal.container({
                base_url: App.base_url,
                model: _this.feed_image,
                has_multiple: _this.collections[_this.current_modal_feed_image_feed_id].length > 1,
                show_related_icons: false
              }));
              if (_this.config.user_id === _this.feed_image.get('user_id') && !_this.config.is_role_gaiax) {
                _this.photo_menu_balloon = new App.elements.setting_balloon.views.setting_balloon({
                  links: [
                    {
                      path: '#',
                      label: '削除する',
                      event_name: 'delete'
                    }
                  ]
                });
                modal.$el.find('.js-timeline-image-feed-feed-image-modal-feed-article-data-container').append(_this.photo_menu_balloon.$el);
                _this.listenTo(_this.photo_menu_balloon, 'delete', _this.click_delete_image);
              }
              star_url_base = "" + App.elements.timeline.config.api_base_url + "/feed_image_star";
              comment_url_base = "" + App.elements.timeline.config.api_base_url + "/feed_image_comment";
              if (typeof stars_view !== "undefined" && stars_view !== null) {
                _stars_view.remove();
              }
              _stars_view = new App.elements.timeline.views.star({
                model: _this.feed_image,
                url: "" + star_url_base + "/" + (_this.feed_image.get('id'))
              });
              modal.$el.find('.js-timeline-image-feed-feed-image-modal-feed-article-star').append(_stars_view.$el);
              if (typeof _comments_view !== "undefined" && _comments_view !== null) {
                _comments_view.remove();
              }
              _comments_view = new App.elements.timeline.views.comments({
                collection: _this.feed_image.get('comments'),
                model: _this.feed_image,
                url: "" + comment_url_base + "/" + (_this.feed_image.get('id'))
              });
              $comments_container = modal.$el.find('.js-timeline-image-feed-feed-image-modal-comments-container');
              _comments_view.set_scroll_target($comments_container);
              $comments_container.append(_comments_view.$el);
              _comments_view.render_comments(App.elements.timeline.config.comments.initial_count, false);
              _this.current_modal_feed_image = _this.feed_image;
              modal.$el.off('click', '.prev a');
              modal.$el.on('click', '.prev a', _this.show_previous_modal);
              modal.$el.off('click', '.next a');
              modal.$el.on('click', '.next a', _this.show_next_modal);
              return App.pages.admin.user.events.feed.show_modal('photo');
            };
          })(this),
          error: (function(_this) {
            return function() {};
          })(this)
        });
      }
    };

    article.prototype.show_previous_modal = function() {
      var model;
      model = this.get_model_from_collection('previous');
      $('#js-feed-image-' + model.get('id')).click();
      return false;
    };

    article.prototype.show_next_modal = function() {
      var model;
      model = this.get_model_from_collection('next');
      $('#js-feed-image-' + model.get('id')).click();
      return false;
    };

    article.prototype.get_model_from_collection = function(method) {
      var index, model;
      if (method == null) {
        method = 'next';
      }
      index = this.collections[this.current_modal_feed_image_feed_id].indexOf(this.current_modal_feed_image);
      if (method === 'next') {
        index++;
      } else {
        index--;
      }
      model = this.collections[this.current_modal_feed_image_feed_id].at(index);
      if (!model || model === 'undefined') {
        if (method === 'next') {
          model = this.collections[this.current_modal_feed_image_feed_id].at(0);
        } else {
          model = this.collections[this.current_modal_feed_image_feed_id].at(this.collections[this.current_modal_feed_image_feed_id].length - 1);
        }
      }
      return model;
    };

    article.prototype.open_modal = function(feed_image) {};

    return article;

  })(Backbone.View);

  App.pages.admin.user.views.feed.comment = (function(_super) {
    __extends(comment, _super);

    function comment() {
      this.click_delete = __bind(this.click_delete, this);
      this.click_setting = __bind(this.click_setting, this);
      this.click_star = __bind(this.click_star, this);
      this.initialize = __bind(this.initialize, this);
      return comment.__super__.constructor.apply(this, arguments);
    }

    comment.prototype.events = {
      'click .js-commentBoxFeedFooter .js-uiAnchorStar': 'click_star',
      'click .js-commentBoxFeedFooter .js-uiAnchorComment': 'click_comment',
      'click .js-commentBoxFeedFooter .js-uiAnchorSetting': 'click_setting',
      'click .js-commentBoxFeedFooter .js-deleteBtn': 'click_delete'
    };

    comment.prototype.initialize = function(options) {
      this.config = (options != null ? options.config : void 0) != null ? options.config : {};
      this.$star_balloon = this.$el.find('.js-commentBoxFeedFooter .star .blnMember');
      return this.$menu_balloon = this.$el.find('.js-commentBoxFeedFooter .blnOption');
    };

    comment.prototype.click_star = function(ev) {
      ev.preventDefault();
      App.event.global_balloon_mediator.trigger('register', this.$star_balloon);
      return false;
    };

    comment.prototype.click_setting = function(ev) {
      ev.preventDefault();
      App.event.global_balloon_mediator.trigger('register', this.$menu_balloon);
      return false;
    };

    comment.prototype.click_delete = function(ev) {
      ev.preventDefault();
      this.$menu_balloon.hide();
      App.pages.admin.user.events.feed.set_modal_options('delete', {
        title: 'コメントの削除',
        body: 'この動作はもとに戻せませんが本当によろしいですか？'
      });
      App.pages.admin.user.events.feed.show_delete_modal({
        url: "" + App.base_url + "/admin/user/comment/delete",
        id: this.$el.attr('data-comment_id')
      });
      return false;
    };

    return comment;

  })(Backbone.View);

  App.pages.admin.user.views.feed.photo = (function(_super) {
    __extends(photo, _super);

    function photo() {
      this.click_delete = __bind(this.click_delete, this);
      this.click_setting = __bind(this.click_setting, this);
      this.click_star = __bind(this.click_star, this);
      this.initialize = __bind(this.initialize, this);
      return photo.__super__.constructor.apply(this, arguments);
    }

    photo.prototype.events = {
      'click .js-photoFooter .js-uiAnchorStar': 'click_star',
      'click .js-photoFooter .js-uiAnchorComment': 'click_comment',
      'click .js-photoFooter .js-uiAnchorSetting': 'click_setting',
      'click .js-photoFooter .js-deleteBtn': 'click_delete'
    };

    photo.prototype.initialize = function(options) {
      this.config = (options != null ? options.config : void 0) != null ? options.config : {};
      this.$star_balloon = this.$el.find('.js-photoFooter .star .blnMember');
      return this.$menu_balloon = this.$el.find('.js-photoFooter .blnOption');
    };

    photo.prototype.click_star = function(ev) {
      ev.preventDefault();
      App.event.global_balloon_mediator.trigger('register', this.$star_balloon);
      return false;
    };

    photo.prototype.click_setting = function(ev) {
      ev.preventDefault();
      App.event.global_balloon_mediator.trigger('register', this.$menu_balloon);
      return false;
    };

    photo.prototype.click_delete = function(ev) {
      ev.preventDefault();
      this.$menu_balloon.hide();
      App.pages.admin.user.events.feed.set_modal_options('delete', {
        title: '画像の削除',
        body: 'この動作はもとに戻せませんが本当によろしいですか？'
      });
      App.pages.admin.user.events.feed.show_delete_modal({
        url: "" + App.base_url + "/admin/user/feed_image/delete",
        id: this.$el.attr('data-feed_image_id')
      });
      return false;
    };

    return photo;

  })(Backbone.View);

  App.pages.admin.user.views.feed.file = (function(_super) {
    __extends(file, _super);

    function file() {
      this.click_delete = __bind(this.click_delete, this);
      this.click_setting = __bind(this.click_setting, this);
      this.initialize = __bind(this.initialize, this);
      return file.__super__.constructor.apply(this, arguments);
    }

    file.prototype.events = {
      'click .js-uiAnchorSetting': 'click_setting',
      'click .js-deleteBtn': 'click_delete'
    };

    file.prototype.initialize = function(options) {
      this.config = (options != null ? options.config : void 0) != null ? options.config : {};
      return this.$menu_balloon = this.$el.find('.blnOption');
    };

    file.prototype.click_setting = function(ev) {
      ev.preventDefault();
      App.event.global_balloon_mediator.trigger('register', this.$menu_balloon);
      return false;
    };

    file.prototype.click_delete = function(ev) {
      ev.preventDefault();
      this.$menu_balloon.hide();
      App.pages.admin.user.events.feed.set_modal_options('delete', {
        title: 'ファイルの削除',
        body: 'この動作はもとに戻せませんが本当によろしいですか？'
      });
      App.pages.admin.user.events.feed.show_delete_modal({
        url: "" + App.base_url + "/admin/user/feed_file/delete",
        id: this.$el.attr('data-feed_file_id')
      });
      return false;
    };

    return file;

  })(Backbone.View);

  App.pages.admin.user.views.index.my_search = (function(_super) {
    __extends(my_search, _super);

    function my_search() {
      this.form_send = __bind(this.form_send, this);
      this.delete_mysearch = __bind(this.delete_mysearch, this);
      this.click_delete = __bind(this.click_delete, this);
      this.click_setting = __bind(this.click_setting, this);
      this.initialize = __bind(this.initialize, this);
      return my_search.__super__.constructor.apply(this, arguments);
    }

    my_search.prototype.events = {
      'click .js-uiAnchorSetting': 'click_setting',
      'click .js-deleteBtn': 'click_delete'
    };

    my_search.prototype.initialize = function(options) {
      var search_option_id;
      this.$delete_form = (options != null ? options.delete_form : void 0) != null ? options.delete_form : {};
      search_option_id = this.$el.find('input[name="search_option_id"]').first().val();
      this.$menu_balloon = this.$el.find('.blnOption');
      this.delete_modal = new App.elements.modal({
        title: 'マイ検索の削除',
        body: 'この動作はもとに戻せませんが本当によろしいですか？',
        decide_button_label: '削除する',
        buttons_type: 'cancel_delete'
      });
      return this.listenTo(this.delete_modal, 'decide', (function(_this) {
        return function() {
          return _this.delete_mysearch(search_option_id);
        };
      })(this));
    };

    my_search.prototype.click_setting = function(ev) {
      ev.preventDefault();
      App.event.global_balloon_mediator.trigger('register', this.$menu_balloon);
      return false;
    };

    my_search.prototype.click_delete = function(ev) {
      ev.preventDefault();
      this.$menu_balloon.hide();
      this.delete_modal.show();
      return false;
    };

    my_search.prototype.delete_mysearch = function(search_option_id) {
      this.$delete_form.append(_.template('<input type="hidden" name="search_option_id" value="<%= search_option_id %>">', {
        search_option_id: search_option_id
      }));
      return this.form_send(this.$delete_form);
    };

    my_search.prototype.form_send = function($form) {
      App.functions.disable_form_multiple_submits($form);
      return $form.submit();
    };

    return my_search;

  })(Backbone.View);

  App.pages.admin.user.calendar = {
    templates: {},
    events: {},
    elements: {},
    views: {},
    routes: {}
  };

  App.pages.admin.user.calendar.events.show = _.extend({
    modal: {},
    init: function(options) {},
    set_modal: function(name, modal) {
      return this.modal[name] = modal;
    },
    show_edit_modal: function(data) {
      return this.modal['edit'].show(data);
    },
    show_delete_modal: function(data) {
      return this.modal['delete'].show(data);
    }
  }, Backbone.Events);

  App.pages.admin.user.calendar.views.show = (function(_super) {
    __extends(show, _super);

    function show() {
      this.is_decline_possibility_user = __bind(this.is_decline_possibility_user, this);
      this._embed_to_form = __bind(this._embed_to_form, this);
      this["delete"] = __bind(this["delete"], this);
      this.update = __bind(this.update, this);
      this.initialize = __bind(this.initialize, this);
      return show.__super__.constructor.apply(this, arguments);
    }

    show.prototype.el = '#main';

    show.prototype.events = {
      'click #js-is-decline-possibility-user-check': 'is_decline_possibility_user'
    };

    show.prototype._is_submited = false;

    show.prototype.initialize = function() {
      var config, max_date, min_date, url2link_class;
      config = getEmbeddedJSON($('#js-embedded-config'));
      min_date = (config != null ? config.min_date : void 0) != null ? config.min_date : null;
      max_date = (config != null ? config.max_date : void 0) != null ? config.max_date : null;
      App.event.global_balloon_mediator.init();
      App.event.global_balloon_mediator.register_document_clicked();
      App.pages.admin.user.calendar.events.show.init();
      this.listView = new App.pages.admin.user.calendar.views.show.schedule_list({
        el: '#js-schedule_list',
        config: config
      });
      this.$edit_form = $('#js-mainForm');
      this.$delete_form = $('#js-mainForm');
      this.edit_modal = new App.elements.schedule_modal.views.user({
        config: {
          minDate: min_date != null ? new Date(min_date[0], min_date[1], min_date[2]) : new Date(),
          maxDate: max_date != null ? new Date(max_date[0], max_date[1], max_date[2]) : new Date()
        }
      });
      this.listenTo(this.edit_modal, 'decide', this.update);
      App.pages.admin.user.calendar.events.show.set_modal('edit', this.edit_modal);
      this.delete_modal = new App.elements.schedule_modal.views["delete"];
      this.listenTo(this.delete_modal, 'decide', this["delete"]);
      App.pages.admin.user.calendar.events.show.set_modal('delete', this.delete_modal);
      this.user_model = new App.pages.admin.user.show.models.user({
        user_id: config.user_id
      });
      if (!this.is_decline_possibility_user_check_info_modal) {
        this.is_decline_possibility_user_check_info_modal = new App.elements.modal();
        this.is_decline_possibility_user_check_info_modal.set_footer_cancel_only('閉じる');
        this.is_decline_possibility_user_check_info_modal.on('cancel', (function(_this) {
          return function() {
            return location.href = location.href;
          };
        })(this));
      }
      url2link_class = new App.elements.url2link.views.url2link();
      $('tr.js-schedule td.desc p.label').each((function(_this) {
        return function(index, element) {
          return $(element).html(url2link_class.done_url2link($(element).html()));
        };
      })(this));
      return url2link_class.renew_observe_click_url2link();
    };

    show.prototype.update = function() {
      if (this._is_submited) {
        return false;
      }
      this._is_submited = true;
      this._embed_to_form(this.edit_modal, this.$edit_form, ['schedule_id', 'user_id', 'type', 'title', 'start_date', 'end_date', 'content']);
      return this.$edit_form.attr('action', this.edit_modal.get_by_name('schedule_id') ? "" + App.base_url + "/admin/user/calendar/edit" : "" + App.base_url + "/admin/user/calendar/create").submit();
    };

    show.prototype["delete"] = function() {
      if (this._is_submited) {
        return false;
      }
      this._is_submited = true;
      this._embed_to_form(this.delete_modal, this.$delete_form, ['schedule_id', 'user_id']);
      return this.$delete_form.attr('action', "" + App.base_url + "/admin/user/calendar/delete").submit();
    };

    show.prototype._embed_to_form = function(modal, $form, keys) {
      return _.each(keys, (function(_this) {
        return function(name) {
          return $form.append('<input type="hidden" name="' + name + '" value="' + _.escape(modal.get_by_name(name)) + '">');
        };
      })(this));
    };

    show.prototype.is_decline_possibility_user = function(event) {
      var is_decline_possibility;
      is_decline_possibility = $(event.currentTarget).is(':checked') === true ? 1 : 0;
      if (is_decline_possibility) {
        this.is_decline_possibility_user_check_info_modal.set_title('辞退予備群(任意登録)');
        this.is_decline_possibility_user_check_info_modal.set_body('サイト利用傾向に関わらず、このユーザーを辞退予備群として登録しました。');
      } else {
        this.is_decline_possibility_user_check_info_modal.set_title('辞退予備群(任意登録の解除)');
        this.is_decline_possibility_user_check_info_modal.set_body('このユーザーを辞退予備群から解除しました。');
      }
      this.user_model.urlRoot = ("" + App.base_url + "/api/admin/user/change_decline_possibility/") + this.user_model.get('user_id') + '/' + is_decline_possibility;
      return this.user_model.save({}, {
        'complete': this.is_decline_possibility_user_check_info_modal.show(),
        'async': false
      });
    };

    return show;

  })(Backbone.View);

  App.pages.admin.user.calendar.views.show.schedule_list = (function(_super) {
    __extends(schedule_list, _super);

    function schedule_list() {
      this.click_demand = __bind(this.click_demand, this);
      this.initialize = __bind(this.initialize, this);
      return schedule_list.__super__.constructor.apply(this, arguments);
    }

    schedule_list.prototype.events = {
      'click #js-schedule_list-demandBtn': 'click_demand'
    };

    schedule_list.prototype.schedules = [];

    schedule_list.prototype.initialize = function(options) {
      this.config = (options != null ? options.config : void 0) != null ? options.config : {};
      return this.$el.find('.js-schedule').each((function(_this) {
        return function(idx, _el) {
          var $el;
          $el = $(_el);
          return _this.schedules.push(new App.pages.admin.user.calendar.views.show.schedule_list.schedule({
            el: $el.context,
            config: _this.config
          }));
        };
      })(this));
    };

    schedule_list.prototype.click_demand = function(ev) {
      ev.preventDefault();
      App.pages.admin.user.calendar.events.show.show_edit_modal({
        type: this.config.type,
        user_id: this.config.user_id
      });
      return false;
    };

    return schedule_list;

  })(Backbone.View);

  App.pages.admin.user.calendar.views.show.schedule_list.schedule = (function(_super) {
    __extends(schedule, _super);

    function schedule() {
      this.click_delete = __bind(this.click_delete, this);
      this.click_edit = __bind(this.click_edit, this);
      this.click_icon = __bind(this.click_icon, this);
      this.initialize = __bind(this.initialize, this);
      return schedule.__super__.constructor.apply(this, arguments);
    }

    schedule.prototype.events = {
      'click .setting .uiAnchor': 'click_icon',
      'click .js-editBtn': 'click_edit',
      'click .js-deleteBtn': 'click_delete'
    };

    schedule.prototype.initialize = function(options) {
      this.config = (options != null ? options.config : void 0) != null ? options.config : {};
      this.$balloon = this.$el.find('.blnOption');
      return this.row = getEmbeddedJSON(this.$el.find('.js-embedded-row'));
    };

    schedule.prototype.click_icon = function(ev) {
      ev.preventDefault();
      App.event.global_balloon_mediator.trigger('register', this.$balloon);
      return false;
    };

    schedule.prototype.click_edit = function(ev) {
      ev.preventDefault();
      this.$balloon.hide();
      App.pages.admin.user.calendar.events.show.show_edit_modal(this.row);
      return false;
    };

    schedule.prototype.click_delete = function(ev) {
      ev.preventDefault();
      this.$balloon.hide();
      App.pages.admin.user.calendar.events.show.show_delete_modal(this.row);
      return false;
    };

    return schedule;

  })(Backbone.View);

  add_route('/admin/user/calendar/[0-9]+?(/([0-9]{6})*)*?', (function(_this) {
    return function() {
      return new App.pages.admin.user.calendar.views.show();
    };
  })(this));

  add_route('/admin/user/calendar/show/[0-9]+?(/([0-9]{6})*)*?', (function(_this) {
    return function() {
      return new App.pages.admin.user.calendar.views.show();
    };
  })(this));

  App.pages.admin.user["import"] = {
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.user["import"].views.download = (function(_super) {
    __extends(download, _super);

    function download() {
      this.form_send_for_import = __bind(this.form_send_for_import, this);
      this.click_multi_import = __bind(this.click_multi_import, this);
      this.hide_loader_and_hide_download_dialog = __bind(this.hide_loader_and_hide_download_dialog, this);
      this.polling_check_completed_excel_file = __bind(this.polling_check_completed_excel_file, this);
      this.form_send_for_download = __bind(this.form_send_for_download, this);
      this.click_multi_download = __bind(this.click_multi_download, this);
      this.initialize = __bind(this.initialize, this);
      return download.__super__.constructor.apply(this, arguments);
    }

    download.prototype.el = '#main';

    download.prototype.events = {
      'click #js-MultiDownloadForImport': 'click_multi_download',
      'click button#js-UserDataMultiImport': 'click_multi_import'
    };

    download.prototype.download_request_count = 0;

    download.prototype.initialize = function() {
      App.event.global_balloon_mediator.init();
      App.event.global_balloon_mediator.register_document_clicked();
      if (this.download_modal == null) {
        this.download_modal = new App.elements.modal();
        this.download_modal.set_title('ユーザー情報一括変更(ダウンロード)');
        this.download_modal.set_body('選択したユーザーカテゴリに該当するユーザーのプロフィールをダウンロードします。<br /><br /><span class="textBtnColor">「Excel ファイルダウンロード」</span>をクリックすると、Excel のダウンロードが開始します。<br />※ダウンロードが開始されるまでに時間がかかる場合がありますが、その際は何度もクリックせずそのままお待ちください。<br /><br />ダウンロードできるファイル形式は、.xlsx形式です。お手元で開けない場合は、お手数ですがサポートデスクまでご連絡ください。');
        this.download_modal.set_footer_cancel_action('閉じる', 'Excel ファイルダウンロード');
        this.download_modal.on('decide', (function(_this) {
          return function() {
            _this.form_send_for_download(_this.$download_form);
            _this.download_request_count++;
            _this.download_modal.show_loader();
            return setTimeout(function() {
              return _this.polling_check_completed_excel_file();
            }, _this.download_request_count > 1 ? 3000 : 300);
          };
        })(this));
      }
      this.$download_form = this.$el.find('#js-admin-user-multi-import-download-form');
      this.$download_form.on('submit', function() {
        $(this).append(_.template('<input type="hidden" name="<%= csrf_key %>" value="<%= csrf_token %>">', {
          csrf_key: App.config.security.csrf_token_key,
          csrf_token: fuel_fetch_token()
        }));
        return true;
      });
      this.$excel_tmp_filename = $('#js-down-load-excel-tmp-filename').val();
      this.$import_form = this.$el.find('#js-admin-user-multi-import-confirm-form-submit');
      return this.$import_form.on('submit', function() {
        $(this).append(_.template('<input type="hidden" name="<%= csrf_key %>" value="<%= csrf_token %>">', {
          csrf_key: App.config.security.csrf_token_key,
          csrf_token: fuel_fetch_token()
        }));
        return true;
      });
    };

    download.prototype.click_multi_download = function(submit_event) {
      return this.download_modal.show();
    };

    download.prototype.form_send_for_download = function($form) {
      return $form.submit();
    };

    download.prototype.polling_check_completed_excel_file = function() {
      return Backbone.ajax({
        type: 'get',
        url: "" + App.base_url + "/api/admin/user/check_completed_excel_file/" + this.$excel_tmp_filename,
        success: (function(_this) {
          return function(xhr) {
            if (xhr.result === 1) {
              return setTimeout(function() {
                return _this.hide_loader_and_hide_download_dialog();
              }, parseInt(xhr.wait_milli_seconds));
            } else {
              return setTimeout(function() {
                return _this.polling_check_completed_excel_file();
              }, 300);
            }
          };
        })(this),
        async: false,
        cache: false
      });
    };

    download.prototype.hide_loader_and_hide_download_dialog = function() {
      this.download_modal.hide_loader();
      return this.download_modal.hide();
    };

    download.prototype.click_multi_import = function(submit_event) {
      return this.form_send_for_import(this.$import_form);
    };

    download.prototype.form_send_for_import = function($form) {
      App.functions.disable_form_multiple_submits($form);
      return $form.submit();
    };

    return download;

  })(Backbone.View);

  add_route('/admin/user/multi/import/?.*', function() {
    return new App.pages.admin.user["import"].views.download();
  });

  App.pages.admin.user.message_room = {
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.user.message_room.views.message_item = (function(_super) {
    __extends(message_item, _super);

    function message_item() {
      this["delete"] = __bind(this["delete"], this);
      this.show_modal = __bind(this.show_modal, this);
      this.click_delete = __bind(this.click_delete, this);
      this.click_icon = __bind(this.click_icon, this);
      this.initialize = __bind(this.initialize, this);
      return message_item.__super__.constructor.apply(this, arguments);
    }

    message_item.prototype.el = null;

    message_item.prototype.$form = null;

    message_item.prototype.$balloon = null;

    message_item.prototype.events = {
      'click .js-setting-balloon-anchor': 'click_icon',
      'click .js-deleteBtn': 'click_delete'
    };

    message_item.prototype.initialize = function(options) {
      this.$balloon = this.$el.find('.blnOption');
      this.$form = $(options.form);
      this.modal = new App.elements.modal({
        decide_button_label: '削除',
        buttons_type: 'cancel_delete',
        title: 'メッセージの削除',
        body: 'メッセージを削除します。よろしいですか？'
      });
      return this.listenTo(this.modal, 'decide', this["delete"]);
    };

    message_item.prototype.click_icon = function(ev) {
      ev.preventDefault();
      App.event.global_balloon_mediator.trigger('register', this.$balloon);
      return false;
    };

    message_item.prototype.click_delete = function(ev) {
      ev.preventDefault();
      return this.show_modal(this.$el);
    };

    message_item.prototype.show_modal = function($target) {
      this.$balloon.hide();
      return this.modal.show();
    };

    message_item.prototype["delete"] = function() {
      var action, message_id;
      this.$balloon.hide();
      message_id = this.$el.attr('data-message-id');
      action = "" + App.base_url + "/admin/user/message/delete";
      if ((message_id + 0) > 0) {
        this.$form.attr('action', [action, message_id].join('/'));
        this.$form.submit();
      }
      return this.modal.hide();
    };

    return message_item;

  })(Backbone.View);

  App.pages.admin.user.message_room.views.message_list = (function(_super) {
    __extends(message_list, _super);

    function message_list() {
      this.show_user_balloon = __bind(this.show_user_balloon, this);
      this.initialize = __bind(this.initialize, this);
      return message_list.__super__.constructor.apply(this, arguments);
    }

    message_list.prototype.el = '.unit_admin_message_show';

    message_list.prototype.events = {
      'click #js-userBalloonAnchor': 'show_user_balloon'
    };

    message_list.prototype.initialize = function() {
      var $form, url2link_class;
      App.event.global_balloon_mediator.init();
      App.event.global_balloon_mediator.register_document_clicked();
      $form = this.$el.find('form#js-messageEditForm');
      this.$el.find('#js-messageListContainer li.listItem.messageSection').each((function(_this) {
        return function(idx, _el) {
          return new App.pages.admin.user.message_room.views.message_item({
            el: _el,
            form: $form
          });
        };
      })(this));
      url2link_class = new App.elements.url2link.views.url2link();
      this.$el.find('.wysiwygContent').each((function(_this) {
        return function(index, element) {
          return $(element).html(url2link_class.done_url2link($(element).html()));
        };
      })(this));
      return url2link_class.renew_observe_click_url2link();
    };

    message_list.prototype.show_user_balloon = function(ev) {
      App.event.global_balloon_mediator.trigger('register', $('#js-userBalloon'));
      return false;
    };

    return message_list;

  })(Backbone.View);

  add_route('/admin/user/message/room/?.*', function() {
    return new App.pages.admin.user.message_room.views.message_list();
  });

  add_route('/admin/user/index/?.*', function() {
    return new App.pages.admin.user.views.index({
      el: '#main'
    });
  });

  add_route('/admin/user/?.*', function() {
    new App.pages.admin.user.views.feed({
      el: 'div.feedContainer'
    });
    new App.pages.admin.user.views.memo_app({
      el: 'table.js-user-list'
    });
    return new App.pages.admin.user.views.decline_possibility({
      el: 'div.attach'
    });
  });

  App.pages.admin.user.search_option = {
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.user.search_option.events.index = _.extend({
    mainViwe: null,
    current: null,
    search_option_item_num: null,
    search_option_item_max: null,
    init: (function(_this) {
      return function(options) {
        _this.mainView = options != null ? options.mainView : void 0;
        _this.errorMsgBanner = _this.mainView.$el.find('.msgBanner');
        _this.search_option_item_num = 1;
        return _this.search_option_item_max = options != null ? options.search_option_item_max : void 0;
      };
    })(this),
    collapse: (function(_this) {
      return function(search_option) {
        var _ref;
        if ((_ref = _this.current) != null) {
          _ref.hide();
        }
        _this.current = search_option;
        return _this.current.show();
      };
    })(this),
    append: (function(_this) {
      return function(data) {
        _this.errorMsgBanner.hide();
        if (_this.search_option_item_num >= _this.search_option_item_max) {
          _this.errorMsgBanner.text('検索オプションはユーザーカテゴリを含め' + _this.search_option_item_max + '個以上指定はできません。').show();
          return;
        }
        _this.search_option_item_num++;
        return _this.mainView.append(data);
      };
    })(this),
    remove: (function(_this) {
      return function($el) {
        _this.search_option_item_num--;
        return $el.remove();
      };
    })(this)
  }, Backbone.Events);

  App.pages.admin.user.search_option.views.index = (function(_super) {
    __extends(index, _super);

    function index() {
      this.append = __bind(this.append, this);
      this.search = __bind(this.search, this);
      this.back = __bind(this.back, this);
      this.initialize = __bind(this.initialize, this);
      return index.__super__.constructor.apply(this, arguments);
    }

    index.prototype.el = '#main';

    index.prototype.events = {
      'click #js-btnBack': 'back',
      'click #js-btnSubmit': 'search'
    };

    index.prototype._is_submited = false;

    index.prototype.initialize = function() {
      var config, errors, search_option_items, selected_items, _ref, _ref1;
      config = getEmbeddedJSON($('#js-embedded-config'));
      selected_items = getEmbeddedJSON($('#js-embedded-selected'));
      errors = getEmbeddedJSON($('#js-embedded-errors'));
      App.pages.admin.user.search_option.events.index.init({
        mainView: this,
        search_option_item_max: (config != null ? (_ref = config.app) != null ? _ref.search_option_item_max : void 0 : void 0) != null ? config != null ? (_ref1 = config.app) != null ? _ref1.search_option_item_max : void 0 : void 0 : 5
      });
      this.tableDataList = new App.pages.admin.user.search_option.views.index.tableDataList({
        el: '#js-tableDataList',
        config: config
      });
      this.$el.find('.conditionAdd').each((function(_this) {
        return function(idx, _el) {
          var $el, _view;
          $el = $(_el);
          return _view = new App.pages.admin.user.search_option.views.index.conditionAdd({
            el: $el.context,
            config: config
          });
        };
      })(this));
      this.$main_form = this.$el.find('#js-mainForm');
      this.$sub_form = this.$el.find('#js-subForm');
      this.current = 1;
      search_option_items = config != null ? config.search_option_items : void 0;
      return _.each(selected_items, (function(_this) {
        return function(selected_item, idx) {
          var data, field_name, type, values, _ref2;
          type = selected_item['type'];
          field_name = selected_item['field_name'];
          values = selected_item['values'];
          data = (_ref2 = search_option_items[type]) != null ? _ref2[field_name] : void 0;
          if (!data) {
            return;
          }
          data = $.extend({}, data, selected_item);
          if ((errors != null ? errors[field_name] : void 0) != null) {
            data.error = errors != null ? errors[field_name] : void 0;
          }
          return App.pages.admin.user.search_option.events.index.append(data);
        };
      })(this));
    };

    index.prototype.back = function(ev) {
      ev.preventDefault();
      return this.$sub_form.submit();
    };

    index.prototype.search = function(ev) {
      ev.preventDefault();
      return this.$main_form.submit();
    };

    index.prototype.append = function(data) {
      var _template_form, _template_label, _template_setting;
      if (!data) {
        return;
      }
      _template_label = '<th>\n  <p><%= data.title %></p>\n  <p><i class="required">*</i>\n    <%= data.name %>\n  </p>\n</th>';
      switch (data.input_type_name) {
        case 'text':
        case 'textarea':
        case 'address':
        case 'telephone':
          _template_form = '<td>\n  <% if (data.error) { %><p><span class="textError"><%= data.error.values %></span></p><% } %>\n  <p>\n    <input type="text" name="search_option_item[<%= data.idx %>][values]" value="<%= data.values %>">\n  </p>\n</td>';
          break;
        case 'radio':
        case 'select':
          _template_form = '<td>\n  <% if (data.error) { %><p><span class="textError"><%= data.error.values %></span></p><% } %>\n  <% _.each(data.choices, function(choice, idx) { %>\n    <p>\n      <label><input type="radio" name="search_option_item[<%= data.idx %>][values]" value="<%= choice.id %>"<% if (data.values == choice.id) { %> checked="checked"<% } %>><%= choice.choice %></label>\n    </p>\n  <% }); %>\n</td>';
          break;
        case 'checkbox':
          _template_form = '<td>\n  <% if (data.error) { %><p><span class="textError"><%= data.error.values %></span></p><% } %>\n  <% _.each(data.choices, function(choice, idx) { %>\n    <p>\n      <label><input type="checkbox" name="search_option_item[<%= data.idx %>][values][]" value="<%= choice.id %>"<% if ($.inArray(choice.id, data.values) >= 0) { %> checked="checked"<% } %>><%= choice.choice %></label>\n    </p>\n  <% }); %>\n</td>';
          break;
        case 'date':
          _template_form = '<td>\n  <% if (data.error) { %><p><span class="textError"><%= data.error.values %></span></p><% } %>\n  <p class="noval">\n    <label class="bullet"><input type="radio" name="search_option_item[<%= data.idx %>][values][select]" value="month"<% if (data.values && data.values[\'select\'] == \'month\') { %> checked="checked"<% } %>></label>\n    <select name="search_option_item[<%= data.idx %>][values][month]" class="js-search_option_date_select">\n      <option value="">選択してください</option>\n      <% _.each([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], function(month, idx) { %>\n      <option value="<%= month %>"<% if (data.values && data.values[\'select\'] == \'month\' && data.values[\'month\'] == month) { %> selected="selected"<% } %>><%= month %>月</option>\n      <% }); %>\n    </select>\n  </p>\n  <p class="noval">\n    <label class="bullet"><input type="radio" name="search_option_item[<%= data.idx %>][values][select]" value="term"<% if (data.values && data.values[\'select\'] == \'term\') { %> checked="checked"<% } %>></label>\n    <input type="text" rel="datepicker" class="inputField startDate hasDatepicker js-search_option_date_input" placeholder="年/月/日" size="6" name="search_option_item[<%= data.idx %>][values][start_ymd]" value="<%= data.values && data.values[\'select\'] == \'term\' ? data.values[\'start_ymd\'] : \'\' %>">～<input type="text" rel="datepicker" class="inputField endDate hasDatepicker js-search_option_date_input" placeholder="年/月/日" size="6" name="search_option_item[<%= data.idx %>][values][end_ymd]" value="<%= data.values && data.values[\'select\'] == \'term\' ? data.values[\'end_ymd\'] : \'\' %>">\n  </p>\n</td>';
      }
      _template_setting = '<td>\n  <div class="setting">\n    <a class="uiAnchor" href="#">\n      <i class="icon-delete" title="削除する"></i>\n    </a>\n  </div>\n  <input type="hidden" name="search_option_item[<%= data.idx %>][type]" value="<%= data.type %>">\n  <input type="hidden" name="search_option_item[<%= data.idx %>][field_name]" value="<%= data.field_name %>">\n</td>';
      data.idx = this.current++;
      return this.tableDataList.append(_.template([_template_label, _template_form, _template_setting].join("")), data);
    };

    return index;

  })(Backbone.View);

  App.pages.admin.user.search_option.views.index.tableDataList = (function(_super) {
    __extends(tableDataList, _super);

    function tableDataList() {
      this.append = __bind(this.append, this);
      this.initialize = __bind(this.initialize, this);
      return tableDataList.__super__.constructor.apply(this, arguments);
    }

    tableDataList.prototype.initialize = function(options) {
      return this.config = (options != null ? options.config : void 0) != null ? options.config : {};
    };

    tableDataList.prototype.append = function(template, data) {
      var item;
      item = new App.pages.admin.user.search_option.views.index.tableDataList.item({
        template: template,
        data: data
      });
      return this.$el.find('tbody').append(item.render().el);
    };

    return tableDataList;

  })(Backbone.View);

  App.pages.admin.user.search_option.views.index.conditionAdd = (function(_super) {
    __extends(conditionAdd, _super);

    function conditionAdd() {
      this.add = __bind(this.add, this);
      this.hide = __bind(this.hide, this);
      this.show = __bind(this.show, this);
      this.collapse = __bind(this.collapse, this);
      this.initialize = __bind(this.initialize, this);
      return conditionAdd.__super__.constructor.apply(this, arguments);
    }

    conditionAdd.prototype.events = {
      'click .content a': 'collapse',
      'click button': 'add'
    };

    conditionAdd.prototype.initialize = function(options) {
      var _ref;
      this.config = (options != null ? (_ref = options.config) != null ? _ref.search_option_items : void 0 : void 0) != null ? options.config.search_option_items : {};
      this.$select = this.$el.find('select');
      this.search_option_attr = this.$select.attr('data-search_option_attr');
      return this;
    };

    conditionAdd.prototype.collapse = function(ev) {
      ev.preventDefault();
      return App.pages.admin.user.search_option.events.index.collapse(this);
    };

    conditionAdd.prototype.show = function() {
      return this.$el.find('.options').show();
    };

    conditionAdd.prototype.hide = function() {
      return this.$el.find('.options').hide();
    };

    conditionAdd.prototype.add = function(ev) {
      var data, _ref, _ref1;
      data = (_ref = this.config) != null ? (_ref1 = _ref[this.search_option_attr]) != null ? _ref1[this.$select.val()] : void 0 : void 0;
      if (!data) {
        return;
      }
      data.type = this.search_option_attr;
      data.field_name = this.$select.val();
      switch (data.input_type_name) {
        case 'radio':
        case 'select':
          data.values = data.choices[0].id;
          break;
        case 'checkbox':
          data.values = $.map(data.choices, function(_choice, _idx) {
            return _choice.id;
          });
          break;
        case 'date':
          data.values = {
            select: 'month'
          };
      }
      return App.pages.admin.user.search_option.events.index.append(data);
    };

    return conditionAdd;

  })(Backbone.View);

  App.pages.admin.user.search_option.views.index.tableDataList.item = (function(_super) {
    __extends(item, _super);

    function item() {
      this.check_term = __bind(this.check_term, this);
      this.check_month = __bind(this.check_month, this);
      this.click_delete = __bind(this.click_delete, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return item.__super__.constructor.apply(this, arguments);
    }

    item.prototype.datepicker_options = App.elements.datepicker.views.datepicker.options;

    item.prototype.tagName = 'tr';

    item.prototype.events = {
      'click .setting .uiAnchor': 'click_delete',
      'focus .js-search_option_date_select': 'check_month',
      'focus .js-search_option_date_input': 'check_term'
    };

    item.prototype.initialize = function(options) {
      this.template = (options != null ? options.template : void 0) != null ? options.template : '';
      this.data = (options != null ? options.data : void 0) != null ? options.data : {};
      return this.datepicker_options.minDate = null;
    };

    item.prototype.render = function() {
      this.$el.html(this.template({
        data: this.data
      }));
      this.$el.find('[rel="datepicker"]').removeClass('hasDatepicker').removeData('datepicker').unbind().datepicker(this.datepicker_options);
      return this;
    };

    item.prototype.click_delete = function(ev) {
      ev.preventDefault();
      return App.pages.admin.user.search_option.events.index.remove(this.$el);
    };

    item.prototype.check_month = function(ev) {
      return this.$el.find('[type="radio"]' + '[name^="search_option_item"]').val(['month']);
    };

    item.prototype.check_term = function(ev) {
      return this.$el.find('[type="radio"]' + '[name^="search_option_item"]').val(['term']);
    };

    return item;

  })(Backbone.View);

  add_route('/admin/user/search_option/?.*', function() {
    return new App.pages.admin.user.search_option.views.index();
  });

  App.pages.admin.user.show = {
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.user.show.models.user = (function(_super) {
    __extends(user, _super);

    function user() {
      return user.__super__.constructor.apply(this, arguments);
    }

    user.prototype.urlRoot = App.base_url + '/api/admin/user/';

    user.prototype.defaults = {
      id: null,
      is_decline_possibility: 0,
      created_at: ''
    };

    return user;

  })(Backbone.Model);

  App.pages.admin.user.show.views.profile_edit = (function(_super) {
    __extends(profile_edit, _super);

    function profile_edit() {
      this.form_send = __bind(this.form_send, this);
      this.enter_key_control = __bind(this.enter_key_control, this);
      this.delete_email_address_confirm = __bind(this.delete_email_address_confirm, this);
      this.edit_profile = __bind(this.edit_profile, this);
      this.edit_account = __bind(this.edit_account, this);
      this.delete_user = __bind(this.delete_user, this);
      this.rejected_cancel = __bind(this.rejected_cancel, this);
      this.login_rejected = __bind(this.login_rejected, this);
      this.edit_role = __bind(this.edit_role, this);
      this.initialize = __bind(this.initialize, this);
      return profile_edit.__super__.constructor.apply(this, arguments);
    }

    profile_edit.prototype.el = '.unitContent';

    profile_edit.prototype.events = {
      'click .js-admin-edit-role': 'edit_role',
      'click .js-admin-login-rejected': 'login_rejected',
      'click .js-admin-login-rejected-is-cancel': 'rejected_cancel',
      'click .js-admin-delete-user': 'delete_user',
      'click .js-admin-edit-account': 'edit_account',
      'click .js-admin-edit-profile': 'edit_profile'
    };

    profile_edit.prototype.edit_role_modal = null;

    profile_edit.prototype.login_rejected_modal = null;

    profile_edit.prototype.rejected_cancel_modal = null;

    profile_edit.prototype.delete_user_modal = null;

    profile_edit.prototype.edit_account_modal = null;

    profile_edit.prototype.edit_profile_modal = null;

    profile_edit.prototype.email_address_delete_confirm_modal = null;

    profile_edit.prototype.confirm_modal = null;

    profile_edit.prototype.form = null;

    profile_edit.prototype.target_user = null;

    profile_edit.prototype.initialize = function() {
      var url2link_class;
      this.target_user = getEmbeddedJSON($('#account_operation_target_user'));
      if (this.edit_role_modal == null) {
        this.edit_role_modal = new App.elements.modal();
      }
      this.edit_role_modal.set_title('権限の変更');
      this.edit_role_modal.set_footer_cancel_decide('取り消す', '変更する');
      this.edit_role_modal.on('decide', (function(_this) {
        return function() {
          var $form;
          if ($form = _this.edit_role_modal.$modal_body.find('#role_form')) {
            _this.edit_role_modal.show_loader();
            return _this.form_send($form);
          }
        };
      })(this));
      if (this.login_rejected_modal == null) {
        this.login_rejected_modal = new App.elements.modal();
      }
      this.login_rejected_modal.set_title('ログイン停止');
      this.login_rejected_modal.set_body($('#account_operation').html() + this.target_user.user_name + 'さんをログイン停止にします。このユーザーはサイトにログインできなくなり、メールも配信されなくなります。<br><br>ただしログイン停止中でも、ユーザーサイトでは停止中のユーザーのプロフィールや投稿が、他のユーザーと同じように表示されます。他のユーザーは、このユーザーがログイン停止になったことを知ることはできません。<br><br>また、ログインを停止してもこのユーザーの情報は削除されず、<span class="textBtnColor">「ログイン停止の解除」</span>を行うことで、再度サイトを利用できるようになります。<br><br>よろしいですか？');
      this.login_rejected_modal.set_footer_cancel_decide('取り消す', 'ログイン停止にする');
      this.login_rejected_modal.on('decide', (function(_this) {
        return function() {
          var $form;
          if ($form = _this.login_rejected_modal.$modal_body.find('#account_operation_form')) {
            _this.login_rejected_modal.show_loader();
            $form.attr('action', $('.js-admin-login-rejected').data('url'));
            return _this.form_send($form);
          }
        };
      })(this));
      if (this.rejected_cancel_modal == null) {
        this.rejected_cancel_modal = new App.elements.modal();
      }
      this.rejected_cancel_modal.set_title('ログイン停止解除');
      this.rejected_cancel_modal.set_body($('#account_operation').html() + this.target_user.user_name + ' さんのログイン停止状態を解除します。このユーザーはサイトにログインできるようになり、サイトからメールも配信されるようになります。<br /><br />よろしいですか？');
      this.rejected_cancel_modal.set_footer_cancel_decide('取り消す', 'ログイン停止を解除する');
      this.rejected_cancel_modal.on('decide', (function(_this) {
        return function() {
          var $form;
          if ($form = _this.rejected_cancel_modal.$modal_body.find('#account_operation_form')) {
            _this.rejected_cancel_modal.show_loader();
            $form.attr('action', $('.js-admin-login-rejected-is-cancel').data('url'));
            return _this.form_send($form);
          }
        };
      })(this));
      if (this.delete_user_modal == null) {
        this.delete_user_modal = new App.elements.modal();
      }
      this.user_name = $('#account_operation').html() + this.target_user.user_name;
      this.delete_user_modal.set_title(this.target_user.user_name + 'さんのユーザー情報(削除)');
      this.delete_user_modal.set_body($('#account_operation').html() + '<div class="msgBanner notice"><p><span class="icon"><i class="icon-exclamation"></i></span><span class="txt">削除を実行しようとしています。注意文をご確認ください。</span></p></div>このユーザーを削除します。削除すると、プロフィール、投稿、コメント、提出物、アンケートなど、このユーザーに関連したすべての情報が削除されます。削除された情報は、元に戻すことができません。 <br><br>また、参加しているイベントやコミュニティでこのユーザー以外に主催者がいない場合、最も古くから参加していたメンバーが自動的に主催者になります。<br><br>なお、サイトから削除されたユーザーは、課金対象から除外されます。<br><br>よろしいですか？');
      this.delete_user_modal.set_footer_cancel_delete('取り消す', '削除する(元に戻せません)');
      this.delete_user_modal.on('decide', (function(_this) {
        return function() {
          var $form;
          if ($form = _this.delete_user_modal.$modal_body.find('#account_operation_form')) {
            _this.delete_user_modal.show_loader();
            $form.attr('action', $('.js-admin-delete-user').data('url'));
            return _this.form_send($form);
          }
        };
      })(this));
      if (this.edit_account_modal == null) {
        this.edit_account_modal = new App.elements.modal();
      }
      if (this.edit_profile_modal == null) {
        this.edit_profile_modal = new App.elements.modal();
      }
      this.edit_profile_modal.set_footer_cancel_decide('キャンセル', '編集する');
      if (this.confirm_modal == null) {
        this.confirm_modal = new App.elements.modal();
      }
      if (this.email_address_delete_confirm_modal == null) {
        this.email_address_delete_confirm_modal = new App.elements.modal();
      }
      this.email_address_delete_confirm_modal.set_title('削除確認');
      this.email_address_delete_confirm_modal.set_decide_button_label('削除');
      this.email_address_delete_confirm_modal.set_footer_cancel_delete();
      url2link_class = new App.elements.url2link.views.url2link();
      $('td.js-profile-textarea div.js-profile-data').each((function(_this) {
        return function(index, element) {
          return $(element).html(url2link_class.done_url2link($(element).html()));
        };
      })(this));
      return url2link_class.renew_observe_click_url2link();
    };

    profile_edit.prototype.edit_role = function(event) {
      Backbone.ajax({
        type: 'get',
        url: "" + App.base_url + "/api/admin/user/is_over_sns_user_limit",
        success: (function(_this) {
          return function(xhr) {
            var show_html;
            show_html = $('#role_edit').html();
            show_html += 'このユーザーの権限を、下記のように変更します。<br><br>&nbsp; &nbsp;(変更前)無料ユーザー(期間限定) <br>&nbsp; &nbsp;(変更後)一般ユーザー<br><br>変更した場合、課金対象としてカウントされます。<br><br>よろしいですか？';
            if (xhr.is_over_sns_user_limit) {
              show_html += '<br />(パック人数を超過します)';
            }
            _this.edit_role_modal.set_body(show_html);
            return _this.edit_role_modal.show();
          };
        })(this)
      });
      return false;
    };

    profile_edit.prototype.login_rejected = function(event) {
      return this.login_rejected_modal.show();
    };

    profile_edit.prototype.rejected_cancel = function(event) {
      return this.rejected_cancel_modal.show();
    };

    profile_edit.prototype.delete_user = function(event) {
      return this.delete_user_modal.show();
    };

    profile_edit.prototype.edit_account = function(event) {
      var has_profile_image;
      this.account_field = $(event.target).data('accountField');
      this.account_field_name = $(event.target).data('accountFieldName');
      switch (this.account_field) {
        case 'email_address':
          this.edit_account_modal.set_title(this.account_field_name + '(入力)');
          this.edit_account_modal.set_body($('#account_edit_' + this.account_field).html());
          this.edit_account_modal.set_footer_cancel_decide('取り消す', '確認する');
          this.edit_account_modal.on('decide', (function(_this) {
            return function() {
              _this.confirm_modal.set_title(_this.account_field_name + '(確認)');
              _this.confirm_modal.set_body('入力したメールアドレスに、認証メールを送信します。<br /><br />よろしいですか？');
              _this.confirm_modal.set_footer_cancel_decide('取り消す', '送信する');
              _this.confirm_modal.on('decide', function() {
                var $form;
                if ($form = _this.edit_account_modal.$modal_body.find('#account_form_email_address_add')) {
                  _this.confirm_modal.show_loader();
                  return _this.form_send($form);
                }
              });
              return _this.confirm_modal.show();
            };
          })(this));
          this.edit_account_modal.show();
          this.edit_account_modal.$modal_body.find('#account_form_email_address_add').keypress((function(_this) {
            return function(event) {
              return _this.enter_key_control(event, _this.edit_account_modal);
            };
          })(this));
          $('.js-delete-email-address-button').on('click', this.delete_email_address_confirm);
          break;
        case 'name':
        case 'birthday':
        case 'profile_image':
          this.edit_account_modal.set_title(this.account_field_name + '(入力)');
          this.edit_account_modal.set_body($('#account_edit_' + this.account_field).html());
          this.edit_account_modal.set_footer_cancel_decide('取り消す', '確認する');
          this.edit_account_modal.$modal_body.find('.js-file-input-container').each((function(_this) {
            return function(index, input) {
              return new App.elements.file_input.views.file_input({
                el: input,
                accept_files: false,
                accept_images: true,
                dummy_path: '/assets/img/theme/common/profile_x140.png'
              });
            };
          })(this));
          has_profile_image = $(event.target).data('hasProfileImage');
          if (has_profile_image < 1) {
            this.edit_account_modal.$modal_body.find('.js-file-input-container-thumbnail-delete-button').hide();
          }
          this.edit_account_modal.on('decide', (function(_this) {
            return function() {
              _this.confirm_modal.set_title(_this.account_field_name + '(確認)');
              if (_this.account_field === 'name') {
                _this.message_body = '姓名を編集します。';
              } else if (_this.account_field === 'profile_image') {
                _this.message_body = 'プロフィール写真を編集します。';
              } else if (_this.account_field === 'birthday') {
                _this.message_body = '生年月日を編集します。';
              } else {
                _this.message_body = 'プロフィールを編集します。';
              }
              _this.confirm_modal.set_body(_this.message_body + '<br /><br />よろしいですか？');
              _this.confirm_modal.set_footer_cancel_decide('取り消す', '編集する');
              _this.confirm_modal.on('decide', function() {
                var $form;
                if ($form = _this.edit_account_modal.$modal_body.find('#account_form_' + _this.account_field)) {
                  _this.confirm_modal.show_loader();
                  return _this.form_send($form);
                }
              });
              return _this.confirm_modal.show();
            };
          })(this));
          this.edit_account_modal.show();
          this.edit_account_modal.$modal_body.find('#account_form_' + this.account_field).keypress((function(_this) {
            return function(event) {
              return _this.enter_key_control(event, _this.edit_account_modal);
            };
          })(this));
          break;
        default:
          return false;
      }
      return false;
    };

    profile_edit.prototype.edit_profile = function(event) {
      var $cloned_profile_edit, edit_input_id;
      this.profile_id = $(event.target).data('profileId');
      this.profile_name = $(event.target).data('profileName');
      $cloned_profile_edit = $('#profile_edit_' + this.profile_id).clone().show();
      this.edit_profile_modal.set_title(this.profile_name + '(入力)');
      this.edit_profile_modal.set_body($cloned_profile_edit);
      this.edit_profile_modal.set_footer_cancel_decide('取り消す', '確認する');
      this.edit_profile_modal.on('decide', (function(_this) {
        return function() {
          _this.confirm_modal.set_title(_this.profile_name + '(確認)');
          _this.confirm_modal.set_body('プロフィールを編集します。<br /><br />よろしいですか？');
          _this.confirm_modal.set_footer_cancel_decide('取り消す', '編集する');
          _this.confirm_modal.on('decide', function() {
            var $form;
            if ($form = _this.edit_profile_modal.$modal_body.find('#profile_form_' + _this.profile_id)) {
              _this.confirm_modal.show_loader();
              return _this.form_send($form);
            }
          });
          return _this.confirm_modal.show();
        };
      })(this));
      this.edit_profile_modal.show();
      this.edit_profile_modal.$modal_body.find('#profile_form_' + this.profile_id).keypress((function(_this) {
        return function(event) {
          return _this.enter_key_control(event, _this.edit_profile_modal);
        };
      })(this));
      if ($($cloned_profile_edit).find('.hasDatepicker').length) {
        edit_input_id = 'profile_edit_' + this.profile_id + '_input';
        $($cloned_profile_edit).find('.hasDatepicker').attr('id', edit_input_id);
        $('#' + edit_input_id).removeClass('hasDatepicker').removeData('datepicker').unbind().datepicker(App.elements.datepicker.views.datepicker.options);
      }
      return false;
    };

    profile_edit.prototype.delete_email_address_confirm = function(event) {
      var delete_email_address, delete_email_address_id;
      delete_email_address_id = $(event.currentTarget).data('deleteEmailAddressId');
      delete_email_address = $(event.currentTarget).data('deleteEmailAddress');
      this.email_address_delete_confirm_modal.set_body(delete_email_address + 'を削除します。よろしいですか？');
      this.email_address_delete_confirm_modal.on('decide', (function(_this) {
        return function() {
          var $form;
          if ($form = _this.edit_account_modal.$modal_body.find('#account_form_email_address_delete_' + delete_email_address_id)) {
            _this.email_address_delete_confirm_modal.show_loader();
            return _this.form_send($form);
          }
        };
      })(this));
      return this.email_address_delete_confirm_modal.show();
    };

    profile_edit.prototype.enter_key_control = function(event, modal_obj) {
      if (event.target.tagName.toUpperCase() === 'INPUT' && ((event.which && event.which === 13) || (event.keyCode && event.keyCode === 13))) {
        modal_obj.trigger('decide');
        return false;
      }
    };

    profile_edit.prototype.form_send = function($form) {
      App.functions.disable_form_multiple_submits($form);
      $form.append(_.template('<input type="hidden" name="<%= csrf_key %>" value="<%= csrf_token %>">', {
        csrf_key: App.config.security.csrf_token_key,
        csrf_token: fuel_fetch_token()
      }));
      return $form.submit();
    };

    return profile_edit;

  })(Backbone.View);

  add_route('/admin/user/show/?.*', function() {
    return new App.pages.admin.user.show.views.profile_edit();
  });

  App.pages.admin.work = {};

  App.pages.admin.work.driver = {};

  App.pages.admin.work.driver.elements = {};

  App.pages.admin.work.driver.elements.form = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {},
    data: {}
  };

  App.pages.admin.work.driver.elements.form.templates.user_select = {
    selected_user: _.template('<td>\n  <p><%- model.get(\'name\') %></p>\n  <input type="hidden" name="<%= name %>" value="<%= model.get(\'id\') %>">\n</td>\n<td class="delete">\n  <p>\n    <a class="uiAnchor" href="#">\n      <i class="icon-garbage js-community-elements-form-user-select-selected-user-delete"></i>\n    </a>\n  </p>\n</td>')
  };

  App.pages.admin.work.driver.elements.form.templates.assign = {
    main: _.template('<% if((\'id\' in data) && data.id){ %>\n  <input type="hidden" name="assigns[<%= data.number %>][id]" value="<%= data.id %>">\n<% } %>\n<%= this.header(data) %>\n<%= this.dates(data) %>\n<%= this.user_select(data) %>', void 0, {
      variable: 'data'
    }),
    header: _.template('    <div class="headingType3">\n      <h2 class="heading js-admin-work-driver-elements-form-assign-header-title">\n      	<% if(data.work_name == \'announcement\') { %>\n          確認期限\n        <% } else if(data.work_name == \'questionnaire\') { %>\n          回答期限\n<% } else { %>\n          提出期限\n<% } %> \n        <% if(data.page_name == \'edit\') { %>\n          (<%= data.number + 1 %>)\n        <% } %>\n      </h2>\n        <ul class="nav">\n          <li>\n            <% if (!data.id && data.number != 0) { %>\n            <a class="btnM btnBasic js-admin-work-driver-elements-form-assign-delete-button" href="#">削除する</a>\n            <% } %>\n          </li>\n        </ul>\n    </div>', void 0, {
      variable: 'data'
    }),
    user_select: _.template('<div class="boxLabeled memberManage js-admin-work-driver-elements-form-user-select">\n  <div class="label">\n    <i class="required">*</i>\n    <span class="text">\n      対象者 \n      <% if((\'errors\' in data) && (\'users\' in data.errors)){ %><%= this.errors({errors: data.errors.users}) %><% } %>\n    </span>\n    <span class="button">\n      <a class="btnM js-admin-work-driver-elements-form-user-select-launch-button" href="#">対象者を追加する</a>\n    </span>\n  </div>\n  <div class="content">\n    <%= this.table(data) %>\n  </div>\n</div>', void 0, {
      variable: 'data'
    }),
    table: _.template('<table class="tableDataList js-admin-work-driver-elements-form-user-select-list-selected-container">\n  <thead>\n    <tr>\n      <th colspan="2">姓名</th>\n    </tr>\n  </thead>\n  <tbody>\n  </tbody>\n</table>', void 0, {
      variable: 'data'
    }),
    dates: _.template('<div class="boxLabeled">\n    <p class="label">\n      <i class="required">*</i><%= data.announce_date_show_name %>～<%= data.limit_date_show_name %>\n      <span class="boxBtnTooltip js-about-page-container" data-name="admin_<%= data.work_name %>_date">\n        <a class="uiAnchor js-about-page-anchor" href="#">?</a>\n      </span>\n      <% if(\'errors\' in data){ %>\n        <% var error_messages = []; %>\n        <% if(\'announce_date\' in data.errors){ error_messages.push(data.errors.announce_date)} %>\n        <% if(\'limit_date\' in data.errors){ error_messages.push(data.errors.limit_date)} %>\n        <% if( error_messages.length > 0 ){ %><%= this.errors({errors: error_messages}) %><% } %>\n      <% } %>\n    </p>\n    <div class="content">\n      <input placeholder="<%= data.announce_date_show_name %>" size="6" name="assigns[<%= data.number %>][announce_date]" rel="datepicker"\n        value="<%= data.announce_date %>" type="text" id="assigns-<%= data.number %>-announce-date"\n        class="js-admin-work-driver-elements-form-assign-date-input-announce-date">\n      -\n      <input placeholder="<%= data.limit_date_show_name %>" size="6" name="assigns[<%= data.number %>][limit_date]" rel="datepicker"\n        value="<%= data.limit_date %>" type="text" id="assigns-<%= data.number %>-limit-date"\n        class="js-admin-work-driver-elements-form-assign-date-input-limit-date">\n    </div>\n</div>', void 0, {
      variable: 'data'
    }),
    errors: _.template('<em class="error">：<%= (_.isArray(data.errors) ? data.errors : [data.errors]).join(\',\') %></em>', void 0, {
      variable: 'data'
    }),
    remove_modal: _.template('<div class="msgBanner notice">\n  <p>\n    <span class="icon"><i class="icon-exclamation"></i></span>\n    <span class="txt">削除した内容を復活させる事はできません</span>\n  </p>\n</div>\n<div class="textLead wysiwygContent">\n  <p>この期間を削除しますか？</p>\n</div>', void 0, {
      variable: 'data'
    })
  };

  App.pages.admin.work.driver.elements.form.templates.file_input = _.template('<input type="file" name="files[]">\n<a href="#" class="js-admin-work-driver-elements-form-attach-file-delete">\n  <i class="icon-xmark"></i>\n</a>');

  App.pages.admin.work.driver.elements.form.templates.remove_temporary_file = _.template('<input type="hidden" name="delete_file_temporary_ids[]" value="<%= id %>">');

  App.pages.admin.work.driver.elements.form.views.selected_user = (function(_super) {
    __extends(selected_user, _super);

    function selected_user() {
      return selected_user.__super__.constructor.apply(this, arguments);
    }

    selected_user.prototype.el = '<tr></tr>';

    selected_user.prototype.events = {
      'click .js-community-elements-form-user-select-selected-user-delete': 'remove_self'
    };

    selected_user.prototype.template = App.pages.admin.work.driver.elements.form.templates.user_select.selected_user;

    return selected_user;

  })(App.elements.user_select.views.selected_user);

  App.pages.admin.work.driver.elements.form.views.user_select = (function(_super) {
    __extends(user_select, _super);

    function user_select() {
      this.get_unfiltered_user_ids = __bind(this.get_unfiltered_user_ids, this);
      this.get_filtered_user_ids = __bind(this.get_filtered_user_ids, this);
      this.add_user_row = __bind(this.add_user_row, this);
      this.render_all_selected_users = __bind(this.render_all_selected_users, this);
      this.render_selected_users = __bind(this.render_selected_users, this);
      this.initialize = __bind(this.initialize, this);
      return user_select.__super__.constructor.apply(this, arguments);
    }

    user_select.INITIAL_MAX_SHOW = 20;

    user_select.prototype.el = null;

    user_select.prototype.events = {
      'click .js-admin-work-driver-elements-form-user-select-launch-button': 'launch'
    };

    user_select.prototype.$selected_container = null;

    user_select.prototype.$tbody = null;

    user_select.prototype.show_more = null;

    user_select.prototype.use_filter = true;

    user_select.prototype.filter_label = '対象済みユーザーをリストから除外する';

    user_select.prototype.initialize = function() {
      if (document.URL.indexOf("create") >= 0) {
        this.use_filter = false;
      }
      user_select.__super__.initialize.apply(this, arguments);
      this.$selected_container = this.$el.find('.js-admin-work-driver-elements-form-user-select-list-selected-container');
      this.$tbody = this.$selected_container.find('tbody');
      this.show_more = new App.elements.show_more.views.show_more({
        label: 'もっと見る'
      });
      this.show_more.hide();
      this.show_more.on('clicked', this.render_all_selected_users);
      return this.$selected_container.after(this.show_more.$el);
    };

    user_select.prototype.render_selected_users = function() {
      this.$tbody.find('tr').not('.js-community-elements-form-users-list-container-creator-row').remove();
      if (this.selected_collection.length > App.pages.admin.work.driver.elements.form.views.user_select.INITIAL_MAX_SHOW) {
        this.show_more.show();
      } else {
        this.show_more.hide();
      }
      return this.selected_collection.each((function(_this) {
        return function(user, index) {
          return _this.add_user_row(user, index < App.pages.admin.work.driver.elements.form.views.user_select.INITIAL_MAX_SHOW);
        };
      })(this));
    };

    user_select.prototype.render_all_selected_users = function() {
      this.$tbody.find('tr').show();
      return this.show_more.hide();
    };

    user_select.prototype.add_user_row = function(user, show) {
      var $user_view, user_view;
      if (show == null) {
        show = true;
      }
      user_view = new App.pages.admin.work.driver.elements.form.views.selected_user({
        model: user,
        name: this.name_attribute
      });
      $user_view = user_view.render();
      if (!show) {
        $user_view.hide();
      }
      return this.$tbody.append($user_view);
    };

    user_select.prototype.get_filtered_user_ids = function() {
      return App.pages.admin.work.driver.elements.form.data.main_view.get_registered_user_ids();
    };

    user_select.prototype.get_unfiltered_user_ids = function(el) {
      return App.pages.admin.work.driver.elements.form.data.main_view.get_registered_user_ids(el);
    };

    return user_select;

  })(App.elements.user_select.views.user_select);

  App.pages.admin.work.driver.elements.form.views.assign = (function(_super) {
    __extends(assign, _super);

    function assign() {
      this.remove_assign = __bind(this.remove_assign, this);
      this.check_remove_assign = __bind(this.check_remove_assign, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return assign.__super__.constructor.apply(this, arguments);
    }

    assign.prototype.el = '<div class="js-work-assign-group"></div>';

    assign.prototype.events = {
      'click .js-admin-work-driver-elements-form-assign-delete-button': 'check_remove_assign'
    };

    assign.prototype.options = {};

    assign.prototype.modal = null;

    assign.prototype.$announce_date = null;

    assign.prototype.$limit_date = null;

    assign.prototype.exclude_user_ids = null;

    assign.prototype.initialize = function(options) {
      if (options != null) {
        this.options = options;
      }
      this.exclude_user_ids = getEmbeddedJSON($('#js-user-exclude-data'));
      return this.render();
    };

    assign.prototype.render = function() {
      var user_select_data;
      this.$el.html(App.pages.admin.work.driver.elements.form.templates.assign.main(_.extend({
        id: null,
        number: 0,
        announce_date: '',
        limit_date: '',
        announce_date_show_name: 'yyyy/mm/dd',
        limit_date_show_name: 'yyyy/mm/dd'
      }, this.options)));
      this.$announce_date = this.$el.find('.js-admin-work-driver-elements-form-assign-date-input-announce-date');
      this.$limit_date = this.$el.find('.js-admin-work-driver-elements-form-assign-date-input-limit-date');
      new App.elements.datepicker.views.datepicker({
        el: this.$el.find('[rel="datepicker"]')
      });
      new App.elements.form.datetime.views.datetime({
        el: this.$el
      });
      user_select_data = {
        name: "assigns[" + this.options.number + "][users][]"
      };
      if ((this.options.users != null) && _.isArray(this.options.users)) {
        user_select_data.selected = this.options.users;
      }
      if (this.exclude_user_ids != null) {
        user_select_data.exclude = this.exclude_user_ids;
      }
      new App.pages.admin.work.driver.elements.form.views.user_select({
        el: this.$el.find('.js-admin-work-driver-elements-form-user-select'),
        data: user_select_data
      });
      return this.$el;
    };

    assign.prototype.check_remove_assign = function() {
      if ((this.options.id != null) && this.options.id > 0) {
        if (!this.modal) {
          this.modal = new App.elements.modal();
          this.modal.set_title('期間の削除');
          this.modal.set_footer_cancel_delete('キャンセル', '削除する');
          this.modal.on('decide', (function(_this) {
            return function() {
              _this.modal.hide();
              return _this.remove_assign();
            };
          })(this));
        }
        this.modal.set_body(App.pages.admin.work.driver.elements.form.templates.assign.remove_modal());
        this.modal.show();
      } else {
        this.remove_assign();
      }
      return false;
    };

    assign.prototype.remove_assign = function() {
      this.$el.remove();
      this.trigger('assign_removed');
      return false;
    };

    return assign;

  })(Backbone.View);

  App.pages.admin.work.driver.elements.form.views.file_input = (function(_super) {
    __extends(file_input, _super);

    function file_input() {
      this.remove_me = __bind(this.remove_me, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return file_input.__super__.constructor.apply(this, arguments);
    }

    file_input.prototype.el = '<li></li>';

    file_input.prototype.events = {
      'click .js-admin-work-driver-elements-form-attach-file-delete': 'remove_me'
    };

    file_input.prototype.initialize = function() {
      return this.render();
    };

    file_input.prototype.render = function() {
      this.$el.html(App.pages.admin.work.driver.elements.form.templates.file_input());
      return this.$el;
    };

    file_input.prototype.remove_me = function() {
      this.remove();
      this.trigger('removed');
      return false;
    };

    return file_input;

  })(Backbone.View);

  App.pages.admin.work.driver.elements.form.views.form = (function(_super) {
    __extends(form, _super);

    function form() {
      this.insert_profile = __bind(this.insert_profile, this);
      this.change_type = __bind(this.change_type, this);
      this.get_registered_user_ids = __bind(this.get_registered_user_ids, this);
      this.remove_temporary_file = __bind(this.remove_temporary_file, this);
      this.check_file_inputs = __bind(this.check_file_inputs, this);
      this.get_files_count = __bind(this.get_files_count, this);
      this.add_attach_file_input = __bind(this.add_attach_file_input, this);
      this.reset_assign_numbers = __bind(this.reset_assign_numbers, this);
      this.get_assigns_count = __bind(this.get_assigns_count, this);
      this.assign_removed = __bind(this.assign_removed, this);
      this.add_assign = __bind(this.add_assign, this);
      this.initialize = __bind(this.initialize, this);
      return form.__super__.constructor.apply(this, arguments);
    }

    form.INITIAL_MAX_SHOW = 20;

    form.prototype.el = '#js-admin-work-driver-elements-form';

    form.prototype.events = {
      'click #js-admin-work-driver-elements-form-attach-file-add-button': 'add_attach_file_input',
      'click .js-admin-work-driver-elements-form-attach-temporary-file-delete': 'remove_temporary_file',
      'click #js-admin-work-driver-elements-form-add-new-assign-button': 'add_assign',
      'click .js-admin-work-driver-elements-form-type': 'change_type',
      'click #js-admin-work-driver-elements-form-profile-insert-button': 'insert_profile'
    };

    form.prototype.$assign_container = null;

    form.prototype.$files_container = null;

    form.prototype.$add_file_button_container = null;

    form.prototype.assign_number = 0;

    form.prototype.files_limit = 9;

    form.prototype.file_inputs_initial_count = 2;

    form.prototype.router = null;

    form.prototype.announce_date_show_name = 'yyyy/mm/dd';

    form.prototype.limit_date_show_name = 'yyyy/mm/dd';

    form.prototype.work_name = 'task';

    form.prototype.page_name = 'create';

    form.prototype.ckeditor = null;

    form.prototype.initialize = function() {
      var assign_data, options, _i, _len, _ref;
      App.functions.submit_once_form($('form'));
      this.ckeditor = CKEDITOR.instances['form_message'];
      this.$assign_container = $('#js-admin-work-driver-elements-form-assign-group-container');
      this.$files_container = $('#js-admin-work-driver-elements-form-attach-file-list-container');
      this.$add_file_button_container = $('#js-admin-work-driver-elements-form-attach-file-add-button-container');
      options = getEmbeddedJSON($('#js-admin-work-driver-elements-form-option-json'));
      if (options == null) {
        return false;
      }
      this.files_limit = +options.files_limit;
      this.file_inputs_initial_count = +options.file_inputs_initial_count;
      if ((options != null ? options.announce_date_show_name : void 0) != null) {
        this.announce_date_show_name = options.announce_date_show_name;
      }
      if ((options != null ? options.limit_date_show_name : void 0) != null) {
        this.limit_date_show_name = options.limit_date_show_name;
      }
      if ((options != null ? options.work_name : void 0) != null) {
        this.work_name = options.work_name;
      }
      if ((options != null ? options.page_name : void 0) != null) {
        this.page_name = options.page_name;
      }
      if ((options.assigns != null) && _.isArray(options.assigns)) {
        _ref = options.assigns;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          assign_data = _ref[_i];
          this.add_assign(assign_data);
        }
      }
      while (this.get_files_count() < this.file_inputs_initial_count) {
        this.add_attach_file_input();
      }
      this.check_file_inputs();
      if (this.$assign_container.find('.js-work-assign-group').length <= 0) {
        this.add_assign();
      }
      this.router = new App.pages.admin.work.driver.elements.form.routes.route();
      this.router.on('route:new_assign', this.add_assign);
      Backbone.history.start();
      return App.pages.admin.work.driver.elements.form.data.main_view = this;
    };

    form.prototype.add_assign = function(assign_data) {
      var assign_view;
      if (assign_data == null) {
        assign_data = {};
      }
      assign_view = new App.pages.admin.work.driver.elements.form.views.assign(_.extend(assign_data, {
        number: this.assign_number++,
        announce_date_show_name: this.announce_date_show_name,
        limit_date_show_name: this.limit_date_show_name,
        work_name: this.work_name,
        page_name: this.page_name
      }));
      assign_view.on('assign_removed', this.assign_removed);
      this.$assign_container.append(assign_view.$el);
      return false;
    };

    form.prototype.assign_removed = function() {
      if (this.get_assigns_count() === 0) {
        return this.add_assign();
      } else {

      }
    };

    form.prototype.get_assigns_count = function() {
      return this.$assign_container.find('.js-work-assign-group').length;
    };

    form.prototype.reset_assign_numbers = function() {
      var number;
      number = 1;
      return this.$assign_container.find('.js-work-assign-group').each((function(_this) {
        return function(index, assign) {
          var $title;
          $title = $(assign).find('.js-admin-work-driver-elements-form-assign-header-title');
          if (work_name === 'announcement') {
            return $title.text("確認期限(" + (number++) + ")");
          } else {
            return $title.text("提出期限(" + (number++) + ")");
          }
        };
      })(this));
    };

    form.prototype.add_attach_file_input = function() {
      var file_input_view;
      if (this.get_files_count() >= this.files_limit) {
        return false;
      }
      file_input_view = new App.pages.admin.work.driver.elements.form.views.file_input();
      file_input_view.once('removed', this.check_file_inputs);
      this.$files_container.append(file_input_view.$el);
      this.check_file_inputs();
      return false;
    };

    form.prototype.get_files_count = function() {
      return this.$files_container.find('li:visible').length;
    };

    form.prototype.check_file_inputs = function() {
      var files_count;
      files_count = this.get_files_count();
      if (files_count >= this.files_limit) {
        return this.$add_file_button_container.hide();
      } else {
        this.$add_file_button_container.show();
        if (files_count < this.file_inputs_initial_count) {
          return this.add_attach_file_input();
        }
      }
    };

    form.prototype.remove_temporary_file = function(event) {
      var $li, temp_file_id;
      $li = $(event.target).parents('li');
      temp_file_id = +$li.attr('data-temp-file-id');
      $li.append(App.pages.admin.work.driver.elements.form.templates.remove_temporary_file({
        id: temp_file_id
      }));
      $li.fadeOut((function(_this) {
        return function() {
          return _this.check_file_inputs();
        };
      })(this));
      return false;
    };

    form.prototype.get_registered_user_ids = function(el) {
      var selectors, target_user_ids;
      target_user_ids = [];
      selectors = ['.js-admin-work-driver-elements-form-user-select', '.js-admin-work-driver-elements-form-user-select-list-selected-container', 'tbody', 'tr', 'td', 'input[type="hidden"][name^="assigns"]'];
      if (el) {
        $(el).parents('.js-work-assign-group').find(selectors.join(' ')).each((function(_this) {
          return function(index, element) {
            return target_user_ids.push(+$(element).val());
          };
        })(this));
      } else {
        this.$assign_container.find('.js-work-assign-group').find(selectors.join(' ')).each((function(_this) {
          return function(index, element) {
            return target_user_ids.push(+$(element).val());
          };
        })(this));
      }
      return _(target_user_ids).uniq();
    };

    form.prototype.change_type = function(event) {
      var $current_tab, $el, $new_tab, $target, message, registered_user_ids;
      $el = $(this.el);
      $target = $(event.currentTarget);
      $current_tab = $el.find('.navTabMenu label.current');
      $new_tab = $target.parent('label');
      $current_tab.removeClass('current');
      $new_tab.addClass('current');
      message = this.ckeditor.getData().replace(/&nbsp;/ig, ' ').replace(/<br\s?\/?>/ig, "\n").replace(/^[\s\u3000]*(.*?)[\s\u3000]*$/g, "$1");
      registered_user_ids = this.get_registered_user_ids();
      if (message.length !== 0 || registered_user_ids.length !== 0) {
        if (confirm('ユーザーカテゴリ変更をします。本文と対象者の情報は引き継げないためリセットされます。カテゴリ変更しますか？')) {
          $('#form___mode__').val('change');
          $el.attr('action', $target.attr('data-url'));
          return $el.submit();
        } else {
          $new_tab.removeClass('current');
          $current_tab.addClass('current');
          $target.prop('checked', false);
          return $current_tab.children().prop('checked', true);
        }
      } else {
        $('#form___mode__').val('change');
        $el.attr('action', $target.attr('data-url'));
        return $el.submit();
      }
    };

    form.prototype.insert_profile = function(event) {
      var $selected_value, label, label_end, label_start;
      label_start = '[###';
      label_end = '###]';
      $selected_value = $('#js-admin-work-driver-elements-form-profile-insert-list').val();
      label = label_start + $selected_value + label_end;
      this.ckeditor.insertText(label);
      return false;
    };

    return form;

  })(Backbone.View);

  App.pages.admin.work.driver.elements.form.views.form_confirm = (function(_super) {
    __extends(form_confirm, _super);

    function form_confirm() {
      this.pop_preview = __bind(this.pop_preview, this);
      this.initialize = __bind(this.initialize, this);
      return form_confirm.__super__.constructor.apply(this, arguments);
    }

    form_confirm.prototype.el = '#js-admin-work-driver-elements-form-confirm';

    form_confirm.prototype.events = {
      'click #js-admin-work-driver-elements-form-confirm-see-preview-button': 'pop_preview'
    };

    form_confirm.prototype.modal = null;

    form_confirm.prototype.preview_data = null;

    form_confirm.prototype.initialize = function() {
      this.modal = new App.elements.modal();
      return this.preview_data = getEmbeddedJSON($('#js-admin-work-driver-elements-form-confirm-preview'));
    };

    form_confirm.prototype.pop_preview = function() {
      this.modal.set_title(this.preview_data.title);
      this.modal.set_body(this.preview_data.message);
      this.modal.set_footer_decide_only('閉じる');
      this.modal.set_height_low_class();
      this.modal.on('decide', this.modal.hide);
      this.modal.show();
      return false;
    };

    return form_confirm;

  })(Backbone.View);

  App.pages.admin.work.driver.elements.form.routes.route = (function(_super) {
    __extends(route, _super);

    function route() {
      return route.__super__.constructor.apply(this, arguments);
    }

    route.prototype.routes = {
      'new_assign': 'new_assign'
    };

    return route;

  })(Backbone.Router);

  App.pages.admin.work.driver.index = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.work.driver.index.templates.list_modal = {
    body: _.template('<div class="textLead wysiwygContent">\n  <p>登録された<%= data.show_name %>を再利用することができます。再利用したい<%= data.show_name %>の<span class="textBtnColor">「詳細を見る」</span>をクリックしてください。</p>\n</div>\n<%= this.items(data) %>', void 0, {
      variable: 'data'
    }),
    items: _.template('<div class="listSelectItem copyArticle">\n  <ul>\n    <% $.each(data.items, function(index, item){ %>\n    <li data-key="<%= index %>">\n      <a href="#" class="js-admin-work-index-list-modal-item-link">\n        <span class="item">\n          <span class="title"><%- item.title %></span>\n          <span class="text">詳細を見る</span>\n        </span>\n      </a>\n    </li>\n    <% }); %>\n  </ul>\n</div>', void 0, {
      variable: 'data'
    })
  };

  App.pages.admin.work.driver.index.templates.item_modal = {
    body: _.template('<div class="textLead wysiwygContent">\n  <p>この<%= data.show_name %>を再利用して、新規<%= data.show_name %>を登録します。内容を確認のうえ、<span class="textBtnColor">「コピーして登録ページへ」</span>をクリックしてください。</p>\n</div>\n\n<div class="boxLabeled">\n  <p class="label">タイトル</p>\n  <p class="content"><%- data.item.title %></p>\n</div>\n<% if(_(data.item.message).trim().length > 0){ %>\n<div class="boxLabeled">\n  <p class="label">本文</p>\n  <p class="content"><%= data.item.message %></p>\n</div>\n<% } %>\n<% if(data.item.files.length > 0){ %>\n<div class="boxLabeled">\n  <p class="label">添付ファイル</p>\n  <ul class="content listBullet">\n    <% $.each(data.item.files, function(index, file){ %>\n      <li>\n        <a href="<%- file.url %>" target="_blank">\n          <%- file.name %>\n        </a>\n      </li>\n    <% }) %>\n  </ul>\n</div>\n<% } %>\n<% if(data.optional_template){ %>\n  <%= data.optional_template(data) %>\n<% } %>', void 0, {
      variable: 'data'
    })
  };

  App.pages.admin.work.driver.index.views.index = (function(_super) {
    __extends(index, _super);

    function index() {
      this.copy_work = __bind(this.copy_work, this);
      this.pop_work_detail = __bind(this.pop_work_detail, this);
      this.set_list_modal_contents = __bind(this.set_list_modal_contents, this);
      this.pop_list_modal = __bind(this.pop_list_modal, this);
      this.initialize = __bind(this.initialize, this);
      return index.__super__.constructor.apply(this, arguments);
    }

    index.prototype.el = '#js-admin-work-driver-index-container';

    index.prototype.events = {
      'click #js-admin-work-driver-index-copy-link': 'pop_list_modal'
    };

    index.prototype.base_url = null;

    index.prototype.api_base_url = null;

    index.prototype.modal = null;

    index.prototype.works_data = [];

    index.prototype.show_name = '';

    index.prototype.optional_template = null;

    index.prototype.initialize = function(options) {
      if (options != null) {
        if (options.base_url != null) {
          this.base_url = options.base_url;
        }
        if (options.api_base_url != null) {
          this.api_base_url = options.api_base_url;
        }
        if (options.show_name != null) {
          this.show_name = options.show_name;
        }
      }
      if ((options != null ? options.optional_template : void 0) && typeof options.optional_template === 'function') {
        this.optional_template = options.optional_template;
      }
      this.modal = new App.elements.modal();
      this.modal.set_height_low_class();
      return this.works_data = getEmbeddedJSON($('#js-admin-work-driver-index-works-data'));
    };

    index.prototype.pop_list_modal = function() {
      this.set_list_modal_contents();
      this.modal.show();
      return false;
    };

    index.prototype.set_list_modal_contents = function() {
      var $body;
      this.modal.set_title("コピーして" + this.show_name + "を登録(一覧)");
      $body = $(App.pages.admin.work.driver.index.templates.list_modal.body({
        items: this.works_data,
        show_name: this.show_name
      }));
      $body.find('.js-admin-work-index-list-modal-item-link').on('click', this.pop_work_detail);
      this.modal.set_body($body);
      this.modal.enable_cancel_auto_hide();
      return this.modal.set_footer_cancel_only_left('取り消す');
    };

    index.prototype.pop_work_detail = function(event) {
      var data_key, work_data;
      data_key = $(event.target).parents('li').attr('data-key');
      if (this.works_data[data_key] == null) {
        return false;
      }
      work_data = this.works_data[data_key];
      this.modal.set_title("コピーして" + this.show_name + "を登録(詳細)");
      this.modal.set_body(App.pages.admin.work.driver.index.templates.item_modal.body({
        item: work_data,
        show_name: this.show_name,
        optional_template: this.optional_template
      }));
      this.modal.set_footer_cancel_decide('一覧に戻る', 'コピーして登録ページヘ');
      this.modal.disable_cancel_auto_hide();
      this.modal.on('cancel', this.set_list_modal_contents);
      this.modal.on('decide', (function(_this) {
        return function() {
          return _this.copy_work(work_data);
        };
      })(this));
      return false;
    };

    index.prototype.copy_work = function(work_data) {
      location.href = "" + this.base_url + "/copy/" + work_data.id;
      if ((work_data.work_name != null) && work_data.work_name === 'announcement') {
        location.href = "" + this.base_url + "/copy/" + work_data.id + "/" + work_data.type_name;
      }
      return false;
    };

    return index;

  })(Backbone.View);

  App.pages.admin.work.driver.show = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.work.driver.show.templates.show = {
    modal: {
      has_data_confirm: _.template('<div class="textLead wysiwygContent">\n  <p><%- data.message %></p>\n</div>\n<table class="tableFixHead">\n  <tbody>\n    <tr>\n      <th>\n        <% if(data.pop_type.match(/^remind\.?.*$/)){ %>\n          催促する\n        <% } else if(data.pop_type == \'pending_approval\') { %>\n          承認する\n        <% } else if(data.pop_type == \'reset_pending_task\') { %>\n          承認待ちに戻す\n        <% } %>\n        対象者</th>\n      <td>\n        <%- data.user_name %>\n      </td>\n    </tr>\n    <tr>\n      <th>ステータス</th>\n      <td>\n        <% if(data.status === \'finished\'){ %>\n          <span class="labelTagPositive">完了</span>\n        <% } %>\n        <% if ( data.status === \'pending_approval\'){ %>\n          <span class="labelTagAttention">承認待ち</span>\n        <% } %>\n        <% if ( data.status === \'unfinished_unread\' || data.status === \'unfinished_read\'){ %>\n          <span class="labelTagNotice">未完了</span>\n        <% } %>\n      </td>\n    </tr>\n  </tbody>\n</table>\n<%= this.modal_form(data) %>', void 0, {
        variable: 'data'
      }),
      multiple_confirm: _.template('<table class="tableFixHead">\n  <tbody>\n    <tr>\n      <th>対象ユーザー</th>\n      <td>\n        <%- _(data.users).map(function(user){ return user.user_name; }).join(\'、\') %>\n      </td>\n    </tr>\n    <% if (data.user_statuses) { %>\n    <tr>\n      <th>ステータス</th>\n      <td>\n      <% if (_.indexOf(data.user_statuses, \'finished\') >= 0) { %>\n        <span class="labelTagPositive">完了</span>\n      <% } %>\n      <% if (_.indexOf(data.user_statuses, \'pending_approval\') >= 0) { %>\n        <span class="labelTagAttention">承認待ち</span>\n      <% } %>\n      <% if (_.indexOf(data.user_statuses, \'unfinished_unread\') >= 0\n          || _.indexOf(data.user_statuses, \'unfinished_read\') >= 0\n      ) { %>\n        <span class="labelTagNotice">未完了</span>\n      <% } %>\n      </td>\n    </tr>\n    <% } %>\n  </tbody>\n</table>\n<%= this.modal_form(_.extend(data, {hidden: this.multiple_hidden(data)})) %>', void 0, {
        variable: 'data'
      }),
      multiple_hidden: _.template('<% $.each(data.users, function(index, user_data){ %>\n<input type="hidden" name="work_user_ids[]" value="<%- user_data.work_user_id %>">\n<% }); %>', void 0, {
        variable: 'data'
      }),
      modal_form: _.template('<form action="<%- data.url %>" method="post" enctype="multipart/form-data">\n  <div class="boxLabeled"<% if(((data.work_name === \'announcement\' || data.work_name === \'questionnaire\') && data.pop_type == \'finish\') || (data.pop_type == \'reset_pending\') || (data.pop_type == \'reset_pending_task\') ||  (data.pop_type == \'finish_announcement\') || (data.pop_type == \'finish_questionnaire\')){ %> style="display:none;"<% } %>>\n    <p class="label js-admin-work-driver-show-change-status-modal-message">\n      <span class="val">個別連絡(Bcc)</span>\n    </p>\n    <p class="content">\n      <textarea rows="6" placeholder="<%- data.placeholder_text %>"></textarea>\n    </p>\n    <p class="content">\n      <input name="file" type="file">\n      <a href="#" class="js-admin-work-driver-show-change-status-modal-file-remove-link">\n        <i class="icon-xmark"></i>\n      </a>\n    </p>\n  </div>\n  <% if(\'hidden\' in data){ %><%= data.hidden %><% } %>\n</form>', void 0, {
        variable: 'data'
      }),
      stop_confirm: _.template('<div class="textLead wysiwygContent">\n  <p>\n  <%= message %>\n\n  <br /><br />よろしいですか？\n  </p>\n</div>'),
      delete_confirm: _.template('<div class="msgBanner notice">\n  <p>\n    <span class="icon"><i class="icon-exclamation"></i></span>\n    <span class="txt">削除を実行しようとしています。注意文をご確認ください。</span>\n  </p>\n</div>\n<div class="textLead wysiwygContent">\n  <% if(work_name === \'announcement\') { %>\n    このおしらせを削除します。削除すると、タイトル、おしらせ本文、添付ファイル、対象者など、このおしらせに関連したすべての情報が削除されます。削除された情報は、元に戻すことができません。\n  <% } else if(work_name === \'questionnaire\') { %>\n    このアンケートを削除します。削除すると、タイトル、説明文、添付ファイル、対象者、回答など、このアンケートに関連したすべての情報が削除されます。削除された情報は、元に戻すことができません。\n  <% }else { %>\n    この提出物を削除します。削除すると、タイトル、説明文、添付ファイル、対象者、提出されたファイルなど、この提出物に関連したすべての情報が削除されます。削除された情報は、元に戻すことができません。\n  <% } %>   \n  <br /><br />\n  よろしいですか？ \n</div>'),
      work_stop_confirm: _.template('      <div class="textLead wysiwygContent">\n      <% if(work_name === \'announcement\') { %>\n        このおしらせの受付を終了します。受付を終了すると、対象者は、連絡情報の確認はできますが、確認報告ができなくなります。\n      <% } else if(work_name === \'questionnaire\') { %>\nこのアンケートの受付を終了します。受付を終了すると、対象者はアンケートの内容確認はできますが、回答はできなくなります。\n      <% }else { %>\n       この提出物の受付を終了します。受付を終了すると、対象者は課題情報の確認はできますが、ファイルやコメントの提出ができなくなります。\n      <% } %>\n       <br /><br />\n       よろしいですか？ \n      </div>'),
      error: _.template('<div class="alert alert-danger js-modal-error">\n  <% if(!(errors instanceof Array)){ errors = [errors] }; %>\n  <ul class="unstyled">\n  <% $.each(errors, function(index, error){ %>\n    <% if(error instanceof Object){ %>\n      <% $.each(error, function(index, err){ %>\n      <li><%- err %></li>\n      <% }); %>\n    <% }else{ %>\n    <li><%- error %></li>\n    <% } %>\n  <% }); %>\n  </ul>\n</div>'),
      alert: _.template('<div class="msgBanner error">\n  <p>\n    <span class="icon"><i class="icon-exclamation"></i></span>\n    <span class="txt"><%= message %></span>\n  </p>\n</div>')
    }
  };

  App.pages.admin.work.driver.show.templates.admin_message_modal = _.template('<div class="boxDataList">\n  <dl>\n    <dt>送信日時</dt>\n    <dd>:</dd>\n    <dd><%- data.created_at %></dd>\n  </dl>\n  <dl>\n    <dt>送信ユーザー</dt>\n    <dd>:</dd>\n    <dd><%- data.user_name %></dd>\n  </dl>\n  <% if(data.message){ %>\n  <dl>\n    <dt>メッセージ内容</dt>\n    <dd>:</dd>\n    <dd>\n      <%= data.message %>\n    </dd>\n  </dl>\n  <% } %>\n  <% if(data.file_name && data.file_path){ %>\n  <dl>\n    <dt>添付ファイル</dt>\n    <dd>:</dd>\n    <dd>\n      <a href="<%- data.file_path %>">\n        <%- data.file_name %>\n      </a>\n    </dd>\n  </dl>\n  <% } %>\n</div>', void 0, {
    variable: 'data'
  });

  App.pages.admin.work.driver.show.config.messages = {
    remind: {
      title: '以下のユーザーに対して催促しますか？',
      button: '催促する',
      not_select: '未完了のユーザーが1人も選ばれていません。いずれか1人を選んでください。'
    },
    approve: {
      title: '以下の内容で承認してもよろしいですか？',
      button: '承認する',
      not_select: '承認待ちのユーザーが1人も選ばれていません。いずれか1人を選んでください。'
    },
    reset_unfinished: {
      title: '未完了にしてもよろしいですか？',
      button: '未完了にする',
      not_select: '完了のユーザーが1人も選ばれていません。いずれか1人を選んでください。'
    },
    reintroduction: {
      title: '再提出を指示してもよろしいですか？',
      button: '再提出を指示する',
      not_select: '承認待ちのユーザーが1人も選ばれていません。いずれか1人を選んでください。'
    },
    finish: {
      title: '以下の内容で完了にしてもよろしいですか？',
      button: '完了にする',
      not_select: '未完了のユーザーが1人も選ばれていません。いずれか1人を選んでください。'
    },
    reset_pending: {
      title: '以下の内容で承認待ちに戻してもよろしいですか？',
      button: '承認待ちに戻す',
      not_select: '完了のユーザーが1人も選ばれていません。いずれか1人を選んでください。'
    },
    assign: {
      stop: {
        title: '',
        body: 'を終了しますか？',
        button: '終了する'
      },
      reopen: {
        title: '以下の提出期間を再開してよろしいですか？',
        body: 'を再開しますか？',
        button: '再開する'
      },
      "delete": {
        title: '以下の提出期間を削除してもよろしいですか？',
        body: 'を削除しますか？',
        button: '削除する'
      }
    }
  };

  App.pages.admin.work.driver.show.config.available_statuses = {
    remind: ['unfinished_unread', 'unfinished_read'],
    reintroduction: ['pending_approval', 'finished'],
    finish: ['unfinished_unread', 'unfinished_read', 'pending_approval'],
    reset_pending: ['finished']
  };

  App.pages.admin.work.driver.show.models.admin_message = (function(_super) {
    __extends(admin_message, _super);

    function admin_message() {
      this.initialize = __bind(this.initialize, this);
      this.urlRoot = __bind(this.urlRoot, this);
      return admin_message.__super__.constructor.apply(this, arguments);
    }

    admin_message.prototype.api_base_url = null;

    admin_message.prototype.urlRoot = function() {
      return "" + this.api_base_url + "/user/admin/message/index";
    };

    admin_message.prototype.defaults = {
      id: null
    };

    admin_message.prototype.initialize = function(options) {
      if (options != null ? options.api_base_url : void 0) {
        return this.api_base_url = options.api_base_url;
      }
    };

    return admin_message;

  })(Backbone.Model);

  App.pages.admin.work.driver.show.models.assign = (function(_super) {
    __extends(assign, _super);

    function assign() {
      this.initialize = __bind(this.initialize, this);
      this.urlRoot = __bind(this.urlRoot, this);
      return assign.__super__.constructor.apply(this, arguments);
    }

    assign.prototype.api_base_url = null;

    assign.prototype.urlRoot = function() {
      return "" + this.api_base_url + "/assign/index";
    };

    assign.prototype.defaults = {
      id: null,
      stop: false
    };

    assign.prototype.initialize = function(data, options) {
      if ((options != null ? options.api_base_url : void 0) != null) {
        return this.api_base_url = options.api_base_url;
      }
    };

    return assign;

  })(Backbone.Model);

  App.pages.admin.work.driver.show.models.work = (function(_super) {
    __extends(work, _super);

    function work() {
      this.initialize = __bind(this.initialize, this);
      this.urlRoot = __bind(this.urlRoot, this);
      return work.__super__.constructor.apply(this, arguments);
    }

    work.prototype.api_base_url = null;

    work.prototype.urlRoot = function() {
      return "" + this.api_base_url + "/show/work";
    };

    work.prototype.defaults = {
      id: null
    };

    work.prototype.initialize = function(data, options) {
      if ((options != null ? options.api_base_url : void 0) != null) {
        return this.api_base_url = options.api_base_url;
      }
    };

    return work;

  })(Backbone.Model);

  App.pages.admin.work.driver.show.views.show = (function(_super) {
    __extends(show, _super);

    function show() {
      this.pop_preview = __bind(this.pop_preview, this);
      this.pop_admin_message_modal = __bind(this.pop_admin_message_modal, this);
      this.toggle_all_checkboxes = __bind(this.toggle_all_checkboxes, this);
      this.unfilter = __bind(this.unfilter, this);
      this.change_export_button_link = __bind(this.change_export_button_link, this);
      this.filter = __bind(this.filter, this);
      this.filter_finished = __bind(this.filter_finished, this);
      this.filter_pending_approval = __bind(this.filter_pending_approval, this);
      this.filter_unsent = __bind(this.filter_unsent, this);
      this.pop_delete_work_confirm = __bind(this.pop_delete_work_confirm, this);
      this.get_all_assign_dates_for_message = __bind(this.get_all_assign_dates_for_message, this);
      this.pop_stop_work_confirm = __bind(this.pop_stop_work_confirm, this);
      this.pop_delete_assign_confirm = __bind(this.pop_delete_assign_confirm, this);
      this.pop_assign_confirm = __bind(this.pop_assign_confirm, this);
      this.setup_assign_modal = __bind(this.setup_assign_modal, this);
      this.get_assign_data_from_event = __bind(this.get_assign_data_from_event, this);
      this.get_tbody_data = __bind(this.get_tbody_data, this);
      this.pop_reopen_assign_confirm = __bind(this.pop_reopen_assign_confirm, this);
      this.pop_stop_assign_confirm = __bind(this.pop_stop_assign_confirm, this);
      this.pop_alert_modal = __bind(this.pop_alert_modal, this);
      this.pop_multiple_confirm = __bind(this.pop_multiple_confirm, this);
      this.pop_admin_message_destroy_confirm = __bind(this.pop_admin_message_destroy_confirm, this);
      this.pop_finish_work_confirm = __bind(this.pop_finish_work_confirm, this);
      this.pop_reset_pending_work_confirm = __bind(this.pop_reset_pending_work_confirm, this);
      this.pop_reintroduction_work_confirm = __bind(this.pop_reintroduction_work_confirm, this);
      this.pop_remind_work_confirm = __bind(this.pop_remind_work_confirm, this);
      this.clear_modal_errors = __bind(this.clear_modal_errors, this);
      this.set_modal_errors = __bind(this.set_modal_errors, this);
      this.pop_modal = __bind(this.pop_modal, this);
      this.pop_row_modal = __bind(this.pop_row_modal, this);
      this.refresh_page = __bind(this.refresh_page, this);
      this.get_row_data = __bind(this.get_row_data, this);
      this.modal_canceled = __bind(this.modal_canceled, this);
      this.modal_decided = __bind(this.modal_decided, this);
      this.change_filter_select = __bind(this.change_filter_select, this);
      this.initialize = __bind(this.initialize, this);
      return show.__super__.constructor.apply(this, arguments);
    }

    show.prototype.el = '#js-admin-work-driver-show-container';

    show.prototype.events = {
      'change #js-admin-work-driver-show-filter-select': 'change_filter_select',
      'click .js-admin-work-driver-show-remind-button': 'pop_remind_work_confirm',
      'click .js-admin-work-driver-show-reintroduction-button': 'pop_reintroduction_work_confirm',
      'click .js-admin-work-driver-show-reset-pending-button': 'pop_reset_pending_work_confirm',
      'click .js-admin-work-driver-show-finish-button': 'pop_finish_work_confirm',
      'click .js-admin-message-destroy': 'pop_admin_message_destroy_confirm',
      'click #js-admin-work-driver-show-multiple-run': 'pop_multiple_confirm',
      'click .js-admin-work-driver-show-stop-button': 'pop_stop_assign_confirm',
      'click .js-admin-work-driver-show-reopen-button': 'pop_reopen_assign_confirm',
      'click .js-admin-work-driver-show-delete-button': 'pop_delete_assign_confirm',
      'click #js-admin-work-driver-show-work-stop-work': 'pop_stop_work_confirm',
      'click #js-admin-work-driver-show-work-delete-work': 'pop_delete_work_confirm',
      'change .js-work-admin-driver-show-data-table-toggle-all-checkbox': 'toggle_all_checkboxes',
      'click .js-admin-work-driver-show-admin-message-show-link': 'pop_admin_message_modal',
      'click #js-admin-work-driver-show-see-preview-button': 'pop_preview'
    };

    show.prototype.base_url = null;

    show.prototype.api_base_url = null;

    show.prototype.use_pending_approval = true;

    show.prototype.modal = null;

    show.prototype.current_modal_errors = [];

    show.prototype.$table = null;

    show.prototype.$rows = null;

    show.prototype.$filter_select = null;

    show.prototype.$title_container = null;

    show.prototype.$export_button = null;

    show.prototype.$toggle_checkbox = null;

    show.prototype.$all_checkboxes = null;

    show.prototype.export_base_url = '';

    show.prototype.router = null;

    show.prototype.work_data = null;

    show.prototype.work_id = 0;

    show.prototype.on_submit = null;

    show.prototype.$multiple_select = null;

    show.prototype.$rows = null;

    show.prototype.messages = App.pages.admin.work.driver.show.config.messages;

    show.prototype.unfinish_messages_key = 'reset_unfinished';

    show.prototype.finish_messages_key = 'finish';

    show.prototype.templates = App.pages.admin.work.driver.show.templates.show;

    show.prototype.work_key = '';

    show.prototype.lang_key = '';

    show.prototype.admin_message_modal = null;

    show.prototype.available_statuses = App.pages.admin.work.driver.show.config.available_statuses;

    show.prototype.preview_data = null;

    show.prototype.initialize = function(options) {
      var work_data;
      this.work_key = options.work_key;
      this.lang_key = "work/" + this.work_key;
      if ((options != null ? options.base_url : void 0) != null) {
        this.base_url = options.base_url;
      }
      if ((options != null ? options.api_base_url : void 0) != null) {
        this.api_base_url = options.api_base_url;
      }
      if ((options != null ? options.use_pending_approval : void 0) != null) {
        this.use_pending_approval = !!options.use_pending_approval;
      }
      if (((options != null ? options.finish_messages_key : void 0) != null) && (this.messages[options.finish_messages_key] != null)) {
        this.finish_messages_key = options.finish_messages_key;
      }
      if (((options != null ? options.unfinish_messages_key : void 0) != null) && (this.messages[options.unfinish_messages_key] != null)) {
        this.unfinish_messages_key = options.unfinish_messages_key;
      }
      work_data = getEmbeddedJSON($('#js-admin-work-driver-show-work-data-json'));
      if (work_data == null) {
        return false;
      }
      this.work_id = work_data.work_id;
      this.work_name = work_data.work_name;
      this.$multiple_select = $('#js-admin-work-driver-show-multiple-action-select');
      this.$table = $('#js-work-admin-driver-show-data-table');
      this.$rows = this.$table.find('tr.js-admin-work-driver-show-data-table-row');
      this.$filter_select = $('#js-admin-work-driver-show-filter-select');
      this.$title_container = $('#js-admin-work-driver-show-work-title-container');
      this.$export_button = $('#js-admin-work-driver-show-export-download-button');
      this.export_base_url = this.$export_button.length > 0 ? this.$export_button.attr('href').replace(/\/$/, '') : '';
      this.$toggle_checkbox = this.$table.find('.js-work-admin-driver-show-data-table-toggle-all-checkbox');
      this.$all_checkboxes = this.$table.find('tbody td.check input[type="checkbox"]');
      this.modal = new App.elements.modal();
      this.modal.add_container_class('jsModal-Dialog');
      this.modal.set_height_low_class();
      this.modal.on('cancel', this.modal_canceled);
      this.modal.on('decide', this.modal_decided);
      this.router = new App.pages.admin.work.driver.show.routes.filter();
      this.router.on('route:filter_unsent', this.filter_unsent);
      this.router.on('route:filter_pending_approval', this.filter_pending_approval);
      this.router.on('route:filter_finished', this.filter_finished);
      this.router.on('route:unfilter', this.unfilter);
      Backbone.history.start();
      if (Backbone.history.fragment) {
        this.$filter_select.val(Backbone.history.fragment);
      }
      return this.preview_data = getEmbeddedJSON($('#js-admin-work-driver-show-preview'));
    };

    show.prototype.change_filter_select = function() {
      this.router.navigate(this.$filter_select.val(), {
        trigger: true
      });
      return false;
    };

    show.prototype.modal_decided = function() {
      if (_.isFunction(this.on_submit)) {
        return this.on_submit();
      }
    };

    show.prototype.modal_canceled = function() {
      return this.on_submit = null;
    };

    show.prototype.get_row_data = function($row) {
      var $json;
      if (($row != null ? $row[0] : void 0) == null) {
        return false;
      }
      if ($row[0]._work_user_data) {
        return $row[0]._work_user_data;
      }
      $json = $row.find('.js-admin-work-driver-show-data-table-row-data-json');
      if (!($json.length > 0)) {
        return false;
      }
      $row[0]._work_user_data = getEmbeddedJSON($json);
      return $row[0]._work_user_data;
    };

    show.prototype.refresh_page = function() {
      return location.reload();
    };

    show.prototype.pop_row_modal = function($row, messages, url_base, pop_type) {
      var data, url;
      data = this.get_row_data($row);
      url = "" + (url_base.replace(/\/$/, '')) + "/" + data.work_user_id;
      data.url = url;
      data.message = messages.message;
      data.placeholder_text = messages.placeholder_text;
      data.pop_type = pop_type;
      return this.pop_modal(data, messages.title, messages.button_label);
    };

    show.prototype.pop_modal = function(data, title, button_label, template) {
      var $file, $file_clone, $form, $textarea;
      if (template == null) {
        template = 'has_data_confirm';
      }
      this.modal.set_title(this.$title_container.text() + title);
      this.modal.set_body(this.templates.modal[template](data));
      if (data.pop_type.match(/^remind\.?.*$/)) {
        this.modal.set_footer_cancel_attention('取り消す', button_label);
      } else {
        this.modal.set_footer_cancel_decide('取り消す', button_label);
      }
      this.modal.set_height_high_class();
      $form = this.modal.$el.find('form');
      $file = $form.find('input[type="file"]');
      $file_clone = $file.clone();
      $textarea = $form.find('textarea');
      $form.find('.js-admin-work-driver-show-change-status-modal-file-remove-link').on('click', (function(_this) {
        return function() {
          var file_value;
          $file.val('');
          file_value = $file.val();
          if (((file_value != null ? file_value.length : void 0) != null) && file_value.length > 0) {
            $file.replaceWith($file_clone.clone());
          }
          return false;
        };
      })(this));
      this.on_submit = (function(_this) {
        return function() {
          var file_form, form_data, key, obj, upload_data;
          _this.clear_modal_errors();
          _this.modal.show_loader();
          file_form = new App.elements.file_form({
            el: $form,
            input: $file,
            url: data.url
          });
          upload_data = [];
          form_data = $form.serializeArray();
          for (key in form_data) {
            obj = form_data[key];
            if (obj.name !== 'message') {
              upload_data.push(obj);
            }
          }
          upload_data.push({
            name: 'message',
            value: _.trim($textarea.val())
          });
          return file_form.upload(upload_data, function(event, response) {
            var errors, response_json, response_object;
            errors = null;
            response_object = response.response();
            if ((response_object != null ? response_object.result : void 0) != null) {
              response_json = response_object.result;
              if ((response_json.result != null) && response_json.result) {
                _this.refresh_page();
                return;
              } else if (response_json.errors != null) {
                errors = response_json.errors;
              }
            }
            _this.set_modal_errors(response_json.errors);
            return _this.modal.hide_loader();
          }, function(event, response) {
            var errors, file_error, _ref, _ref1;
            errors = null;
            file_error = null;
            if ((response != null ? response.jqXHR : void 0) != null) {
              if ((response.jqXHR.status != null) && response.jqXHR.status === 413) {
                file_error = 'ファイル容量が大きすぎます。10MB以内のファイルしかアップロードできません';
              } else if ((response != null ? (_ref = response.jqXHR) != null ? (_ref1 = _ref.response) != null ? _ref1.errors : void 0 : void 0 : void 0) != null) {
                errors = response.jqXHR.response.errors;
              }
            }
            _this.set_modal_errors(errors, null, file_error ? [file_error] : null);
            return _this.modal.hide_loader();
          });
        };
      })(this);
      return this.modal.show();
    };

    show.prototype.set_modal_errors = function(errors, message_errors, file_errors) {
      var $modal_errors_container, error, field;
      if (errors == null) {
        errors = null;
      }
      if (message_errors == null) {
        message_errors = null;
      }
      if (file_errors == null) {
        file_errors = null;
      }
      file_errors = [];
      message_errors = [];
      if (errors && _.isObject(errors)) {
        for (field in errors) {
          error = errors[field];
          if (field.match(/^message\.?.*$/)) {
            message_errors.push(error);
          } else if (field.match(/^file\.?.*$/)) {
            file_errors.push(error);
          }
        }
      }
      $modal_errors_container = this.modal.$el.find('.js-admin-work-driver-show-change-status-modal-message');
      if (message_errors.length === 0 && file_errors.length === 0) {
        message_errors.push('予期せぬエラーが発生しました。お手数ですがもう一度やり直してください。');
      }
      if (message_errors.length > 0) {
        errors = new App.elements.errors.views.errors({
          errors: message_errors
        });
        $modal_errors_container.append(errors.$el);
        this.current_modal_errors.push(errors);
      }
      if (file_errors.length > 0) {
        errors = new App.elements.errors.views.errors({
          errors: file_errors
        });
        $modal_errors_container.append(errors.$el);
        return this.current_modal_errors.push(errors);
      }
    };

    show.prototype.clear_modal_errors = function() {
      var error, _i, _len, _ref;
      _ref = this.current_modal_errors;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        error = _ref[_i];
        error.remove();
      }
      return this.current_modal_errors = [];
    };

    show.prototype.pop_remind_work_confirm = function(event, work_type) {
      var $pop_type, $row;
      this.work_type = work_type;
      $row = $(event.target).parents('tr');
      $pop_type = 'remind_' + this.work_name;
      this.pop_row_modal($row, this.messages.remind, "" + this.api_base_url + "/user/remind", $pop_type);
      return false;
    };

    show.prototype.pop_reintroduction_work_confirm = function(event, work_type) {
      var $pop_type, $row;
      this.work_type = work_type;
      $row = $(event.target).parents('tr');
      $pop_type = 'reintroduction_' + this.work_name;
      this.pop_row_modal($row, this.messages[this.unfinish_messages_key], "" + this.api_base_url + "/user/reintroduction", $pop_type);
      return false;
    };

    show.prototype.pop_reset_pending_work_confirm = function(event, work_type) {
      var $pop_type, $row;
      this.work_type = work_type;
      $row = $(event.target).parents('tr');
      $pop_type = 'reset_pending_' + this.work_name;
      this.pop_row_modal($row, this.messages.reset_pending, "" + this.api_base_url + "/user/reset_pending", $pop_type);
      return false;
    };

    show.prototype.pop_finish_work_confirm = function(event, work_type) {
      var $pop_type, $row;
      this.work_type = work_type;
      $row = $(event.target).parents('tr');
      $pop_type = 'finish_' + this.work_name;
      this.pop_row_modal($row, this.messages[this.finish_messages_key], "" + this.api_base_url + "/user/finish", $pop_type);
      return false;
    };

    show.prototype.pop_admin_message_destroy_confirm = function(event) {
      var $container, $link, $original_container, admin_message_id;
      $link = $(event.target);
      if (event.target.tagName.toLowerCase() !== 'a') {
        $link = $link.parents('a');
      }
      admin_message_id = parseInt($link.attr('data-id'), 10);
      if (isNaN(admin_message_id)) {
        return false;
      }
      $original_container = $link.parents('.js-admin-message-container');
      $container = $original_container.clone();
      $container.find('.js-admin-message-destroy').remove();
      this.modal.set_title('管理者メッセージの削除');
      this.modal.set_body('この管理者メッセージを削除してもよいですか？');
      this.modal.set_footer_cancel_delete('キャンセル', '削除する');
      this.modal.clear_height_classes();
      this.on_submit = (function(_this) {
        return function() {
          var admin_message;
          admin_message = new App.pages.admin.work.driver.show.models.admin_message({
            api_base_url: _this.api_base_url
          });
          admin_message.id = admin_message_id;
          return admin_message.destroy({
            success: function() {
              $original_container.remove();
              return _this.modal.hide();
            }
          });
        };
      })(this);
      this.modal.show();
      return false;
    };

    show.prototype.pop_multiple_confirm = function() {
      var button_text, cnt, messages_key, mode, not_select_error_message, title, url, user_status, user_statuses, users;
      url = "" + this.api_base_url + "/show/multiple/";
      mode = this.$multiple_select.val();
      switch (mode) {
        case 'remind':
        case 'reintroduction':
        case 'finish':
        case 'reset_pending':
          messages_key = mode;
          if (mode === 'reintroduction') {
            messages_key = this.unfinish_messages_key;
          } else if (mode === 'finish') {
            messages_key = this.finish_messages_key;
          }
          url += mode;
          title = this.messages[messages_key].title;
          button_text = this.messages[messages_key].button_label;
          not_select_error_message = this.messages[messages_key].not_select;
          break;
        default:
          return false;
      }
      users = {};
      cnt = 0;
      user_statuses = [];
      this.$rows.each((function(_this) {
        return function(index, row) {
          var $action, $row, data, target_statuses;
          $row = $(row);
          if (!$(row).is(':visible') || $row.find('td.check input[type="checkbox"]:checked').length <= 0) {
            return true;
          }
          $action = $row.find('td.cell-action');
          target_statuses = _this.available_statuses[mode];
          data = _this.get_row_data($row);
          if (_.indexOf(target_statuses, data.status) < 0) {
            return true;
          }
          users[data.work_user_id] = data;
          user_statuses.push(data.status);
          return cnt++;
        };
      })(this));
      user_statuses = _(user_statuses).uniq();
      user_status = user_statuses.length >= 1 ? user_statuses[0] : null;
      if (cnt <= 0) {
        this.pop_alert_modal(not_select_error_message, this.messages[messages_key].title);
        return false;
      }
      this.pop_modal({
        users: users,
        url: url,
        user_status: user_status,
        user_statuses: user_statuses,
        pop_type: mode,
        work_name: this.work_name
      }, title, button_text, 'multiple_confirm');
      return false;
    };

    show.prototype.pop_alert_modal = function(message, title) {
      this.modal.set_title(this.$title_container.text() + title);
      this.modal.set_body(App.pages.admin.work.driver.show.templates.show.modal.alert({
        message: message
      }));
      this.modal.set_footer_cancel_only('閉じる');
      this.modal.set_height_low_class();
      return this.modal.show();
    };

    show.prototype.pop_stop_assign_confirm = function(event) {
      this.pop_assign_confirm(event, {
        stop: true
      }, 'stop');
      return false;
    };

    show.prototype.pop_reopen_assign_confirm = function(event) {
      this.pop_assign_confirm(event, {
        reopen: true
      }, 'reopen');
      return false;
    };

    show.prototype.get_tbody_data = function($tbody) {
      var _ref;
      if (($tbody != null ? (_ref = $tbody[0]) != null ? _ref._work_assign_data : void 0 : void 0) != null) {
        return $tbody[0]._work_assign_data;
      }
      $tbody[0]._work_assign_data = getEmbeddedJSON($tbody.find('.js-admin-work-driver-show-data-table-assign-tbody-data-json'));
      return $tbody[0]._work_assign_data;
    };

    show.prototype.get_assign_data_from_event = function(event) {
      var $assign_tbody, $link, $rows, assign_data, users_data, work_assign_id;
      $link = $(event.target);
      if (event.target.tagName.toLowerCase() !== 'a') {
        $link = $link.parents('a');
      }
      work_assign_id = +$link.attr('data-id');
      $assign_tbody = this.$table.find('tbody[data-id="' + work_assign_id + '"]');
      assign_data = this.get_tbody_data($assign_tbody);
      users_data = [];
      $rows = $assign_tbody.find('tr');
      $rows.filter(':has(.check)').each((function(_this) {
        return function(index, row) {
          return users_data.push(_this.get_row_data($(row)));
        };
      })(this));
      return {
        assign_data: assign_data,
        users_data: users_data,
        work_assign_id: work_assign_id
      };
    };

    show.prototype.setup_assign_modal = function(message_key, data, use_delete_button, work_name) {
      if (use_delete_button == null) {
        use_delete_button = false;
      }
      this.modal.set_title(this.$title_container.text() + this.messages.assign[message_key].title);
      this.modal.set_body(this.templates.modal.stop_confirm({
        title: this.$title_container.text(),
        assign_data: data.assign_data,
        users_data: data.users_data,
        message: this.messages.assign[message_key].body,
        use_pending_approval: this.use_pending_approval,
        work_name: this.work_name
      }));
      if (use_delete_button) {
        this.modal.set_footer_cancel_delete('キャンセル', this.messages.assign[message_key].button);
      } else {
        this.modal.set_footer_cancel_decide('取り消す', this.messages.assign[message_key].button);
      }
      return this.modal.set_height_low_class();
    };

    show.prototype.pop_assign_confirm = function(event, param, message_key) {
      var data;
      data = this.get_assign_data_from_event(event);
      this.setup_assign_modal(message_key, data);
      this.on_submit = (function(_this) {
        return function() {
          var assign;
          _this.modal.show_loader();
          assign = new App.pages.admin.work.driver.show.models.assign({
            id: data.work_assign_id
          }, {
            api_base_url: _this.api_base_url
          });
          return assign.save(param, {
            success: function() {
              return _this.refresh_page();
            },
            error: function() {
              return _this.modal.hide_loader();
            }
          });
        };
      })(this);
      this.modal.show();
      return false;
    };

    show.prototype.pop_delete_assign_confirm = function(event) {
      var data;
      data = this.get_assign_data_from_event(event);
      this.setup_assign_modal('delete', data, true);
      this.on_submit = (function(_this) {
        return function() {
          var assign;
          _this.modal.show_loader();
          assign = new App.pages.admin.work.driver.show.models.assign({
            id: data.work_assign_id
          }, {
            api_base_url: _this.api_base_url
          });
          return assign.destroy({
            success: function() {
              return _this.refresh_page();
            },
            error: function() {
              return _this.modal.hide_loader();
            }
          });
        };
      })(this);
      this.modal.set_height_low_class();
      this.modal.show();
      return false;
    };

    show.prototype.pop_stop_work_confirm = function(work_name) {
      var assign_count, has_open_assign;
      has_open_assign = false;
      assign_count = 0;
      this.$table.find('tbody[data-id]').each((function(_this) {
        return function(index, tbody) {
          var data;
          data = _this.get_tbody_data($(tbody));
          assign_count++;
          if (!data.is_stopped) {
            has_open_assign = true;
            return false;
          }
        };
      })(this));
      if (assign_count === 0) {
        this.pop_alert_modal('期間がひとつもありません');
        return false;
      }
      if (!has_open_assign) {
        this.pop_alert_modal('全ての期間が既に受付を終了しています');
        return false;
      }
      this.modal.set_title(this.$title_container.text() + '(受付終了)');
      this.modal.set_body(this.templates.modal.work_stop_confirm({
        title: this.$title_container.text(),
        assign_dates: this.get_all_assign_dates_for_message(),
        work_name: this.work_name
      }));
      this.modal.set_footer_cancel_decide('取り消す', '受付を終了する');
      this.modal.set_height_low_class();
      this.on_submit = (function(_this) {
        return function() {
          var work;
          _this.modal.show_loader();
          work = new App.pages.admin.work.driver.show.models.work({
            id: _this.work_id
          }, {
            api_base_url: _this.api_base_url
          }, {
            work_name: _this.work_name
          });
          return work.save({}, {
            success: function() {
              return _this.refresh_page();
            },
            error: function() {
              return _this.modal.hide_loader();
            }
          });
        };
      })(this);
      this.modal.show();
      return false;
    };

    show.prototype.get_all_assign_dates_for_message = function() {
      var assign_dates;
      assign_dates = [];
      this.$table.find('tbody[data-id]').each((function(_this) {
        return function(index, tbody) {
          var assign_data;
          assign_data = _this.get_tbody_data($(tbody));
          return assign_dates.push("<li>「" + assign_data.announce_date + "～" + assign_data.limit_date + "」</li>");
        };
      })(this));
      return assign_dates.join('');
    };

    show.prototype.pop_delete_work_confirm = function(work_name) {
      this.modal.set_title(this.$title_container.text() + '(削除)');
      this.modal.set_body(this.templates.modal.delete_confirm({
        title: this.$title_container.text(),
        assign_dates: this.get_all_assign_dates_for_message(),
        work_name: this.work_name
      }));
      this.modal.set_footer_cancel_delete('取り消す', '削除する(元に戻せません)');
      this.modal.set_height_low_class();
      this.on_submit = (function(_this) {
        return function() {
          var work;
          _this.modal.show_loader();
          work = new App.pages.admin.work.driver.show.models.work({
            id: _this.work_id
          }, {
            api_base_url: _this.api_base_url
          });
          return work.destroy({
            success: function() {
              return location.href = _this.base_url;
            },
            error: function() {
              return _this.modal.hide_loader();
            }
          });
        };
      })(this);
      this.modal.show();
      return false;
    };

    show.prototype.filter_unsent = function() {
      this.change_export_button_link('unfinished');
      this.filter('unfinished_unread', 'unfinished_read');
      return false;
    };

    show.prototype.filter_pending_approval = function() {
      this.change_export_button_link('pending_approval');
      this.filter('pending_approval');
      return false;
    };

    show.prototype.filter_finished = function() {
      this.change_export_button_link('finished');
      this.filter('finished');
      return false;
    };

    show.prototype.filter = function() {
      var before_action, statuses;
      statuses = arguments;
      before_action = null;
      this.$rows.each((function(_this) {
        return function(index, row) {
          var $row, data;
          $row = $(row);
          data = _this.get_row_data($row);
          if (data !== false) {
            if (_.indexOf(statuses, data.status) >= 0) {
              $row.show();
              return before_action = 'show';
            } else {
              $row.hide();
              return before_action = 'hide';
            }
          } else if (before_action != null) {
            return $row[before_action]();
          }
        };
      })(this));
      return false;
    };

    show.prototype.change_export_button_link = function(status) {
      this.$export_button.attr('href', "" + this.export_base_url + "/" + status);
      return this;
    };

    show.prototype.unfilter = function() {
      this.change_export_button_link('');
      this.$rows.show();
      return false;
    };

    show.prototype.toggle_all_checkboxes = function() {
      return this.$all_checkboxes.filter(':visible').prop('checked', this.$toggle_checkbox.is(':checked'));
    };

    show.prototype.pop_admin_message_modal = function(event) {
      var $container, $link, data, url2link_class;
      $link = $(event.target);
      $container = $link.parents('.js-admin-work-driver-show-admin-message-show-link-container');
      data = getEmbeddedJSON($container.find('.js-admin-work-driver-show-admin-message-data'));
      if (data === null) {
        return false;
      }
      if (!this.admin_message_modal) {
        this.admin_message_modal = new App.elements.modal();
        this.admin_message_modal.set_title('管理メッセージ');
        this.admin_message_modal.set_footer_cancel_only('閉じる');
        this.admin_message_modal.set_height_high_class();
      }
      url2link_class = new App.elements.url2link.views.url2link();
      data.message = url2link_class.done_url2link(nl2br(data.message));
      this.admin_message_modal.set_body(App.pages.admin.work.driver.show.templates.admin_message_modal(data));
      this.admin_message_modal.show();
      url2link_class.renew_observe_click_url2link();
      return false;
    };

    show.prototype.pop_preview = function() {
      this.modal.set_title(this.preview_data.title);
      this.modal.set_body(this.preview_data.message);
      this.modal.set_footer_decide_only('閉じる');
      this.modal.set_height_low_class();
      this.modal.on('decide', this.modal.hide);
      return this.modal.show();
    };

    return show;

  })(Backbone.View);

  App.pages.admin.work.driver.show.routes.filter = (function(_super) {
    __extends(filter, _super);

    function filter() {
      return filter.__super__.constructor.apply(this, arguments);
    }

    filter.prototype.routes = {
      'unsent': 'filter_unsent',
      'pending_approval': 'filter_pending_approval',
      'finished': 'filter_finished',
      'unfilter': 'unfilter',
      '.*': 'unfilter'
    };

    return filter;

  })(Backbone.Router);

  App.pages.admin.work.announcement = {};

  add_route('/admin/work/announcement/(?:create|edit|copy)/?.*', function() {
    new App.pages.admin.work.driver.elements.form.views.form();
    return new App.pages.admin.work.driver.elements.form.views.form_confirm();
  });

  add_route('/admin/work/announcement(/index)?/?', function() {
    return new App.pages.admin.work.driver.index.views.index({
      base_url: "" + App.base_url + "/admin/work/announcement",
      api_base_url: "" + App.base_url + "/api/admin/work/announcement",
      show_name: 'おしらせ'
    });
  });

  App.pages.admin.work.announcement.show = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.work.announcement.show.config.messages = {
    stop_work: {
      title: 'おしらせの受付終了'
    },
    delete_work: {
      title: 'おしらせの削除'
    },
    remind: {
      title: '(催促)',
      message: '以下の対象者に対して催促します',
      button_label: '催促する',
      placeholder_text: 'この内容は、催促する対象者のみ確認することができます。',
      not_select: 'ステータスが「未完了」の対象者が選択されていません。'
    },
    reset_unfinished: {
      title: '(未完了に戻す)',
      message: '以下の対象者のおしらせを未完了に戻します。',
      button_label: '未完了に戻す',
      not_select: 'ステータスが「完了」の対象者が選択されていません。'
    },
    finish: {
      title: '(完了にする)',
      message: '以下の対象者のおしらせを完了にします。',
      button_label: '完了にする',
      not_select: 'ステータスが「未完了」の対象者が選択されていません。'
    },
    assign: {
      stop: {
        title: '(受付終了)',
        body: 'このおしらせの受付を終了します。受付を終了すると、対象者は、連絡情報の確認はできますが、確認報告ができなくなります。',
        button: '受付を終了する'
      },
      reopen: {
        title: '(受付再開)',
        body: 'このおしらせの受付を再開します。受付を再開すると、未完了の対象者は確認報告ができるようになります。',
        button: '受付を再開する'
      },
      "delete": {
        title: '(期限削除)',
        body: '<div class="msgBanner notice"><p><span class="icon"><i class="icon-exclamation"></i></span><span class="txt">削除を実行しようとしています。注意文をご確認ください。</span></p></div>このおしらせを削除します。削除すると、タイトル、おしらせ本文、添付ファイル、対象者など、このおしらせの期限に関連したすべての情報が削除されます。削除された情報は元に戻すことができません。',
        button: '削除する(元に戻せません)'
      }
    }
  };

  App.pages.admin.work.announcement.show.views.show = (function(_super) {
    __extends(show, _super);

    function show() {
      return show.__super__.constructor.apply(this, arguments);
    }

    show.prototype.messages = App.pages.admin.work.announcement.show.config.messages;

    return show;

  })(App.pages.admin.work.driver.show.views.show);

  add_route('/admin/work/announcement/show/?.*', function() {
    return new App.pages.admin.work.announcement.show.views.show({
      work_key: 'announcement',
      base_url: "" + App.base_url + "/admin/work/announcement",
      api_base_url: "" + App.base_url + "/api/admin/work/announcement",
      use_pending_approval: false
    });
  });

  App.pages.admin.work.questionnaire = {};

  App.pages.admin.work.questionnaire.elements = {};

  App.pages.admin.work.questionnaire.elements.form = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.work.questionnaire.elements.form.templates.question_modal = {
    delete_body: _.template('<div class="msgBanner notice"><p><span class="icon"><i class="icon-exclamation"></i></span><span class="txt">削除を実行しようとしています。注意文をご確認ください。</span></p></div>\nこの質問を削除します。削除すると、この質問に回答したユーザーが存在する場合、回答内容も削除されます。削除された内容は、元に戻すことができません。<br /><br />\n削除は、「削除する」をクリックして確認ページに遷移した後、内容を確認いただき、「編集する」をクリックした時点で行われます。<br /><br />\nよろしいですか？')
  };

  App.pages.admin.work.questionnaire.elements.form.views.form = (function(_super) {
    var $container, $document_body, $form;

    __extends(form, _super);

    function form() {
      this.reassign_sort_order = __bind(this.reassign_sort_order, this);
      this._update_question_view = __bind(this._update_question_view, this);
      this._update_question_settion_info = __bind(this._update_question_settion_info, this);
      this.execute_sort_question = __bind(this.execute_sort_question, this);
      this.end_sort_question_mode = __bind(this.end_sort_question_mode, this);
      this.cancel_sort_question_mode = __bind(this.cancel_sort_question_mode, this);
      this.start_sort_question = __bind(this.start_sort_question, this);
      this.before_submit = __bind(this.before_submit, this);
      this.get_new_question = __bind(this.get_new_question, this);
      this.create_question = __bind(this.create_question, this);
      this.add_question = __bind(this.add_question, this);
      this.check_and_add_question = __bind(this.check_and_add_question, this);
      this.restore_questions = __bind(this.restore_questions, this);
      this.initialize = __bind(this.initialize, this);
      return form.__super__.constructor.apply(this, arguments);
    }

    form.prototype.el = '#questionnaire-questions';

    form.prototype.events = {
      'click #questionnaire-questions-add-button': 'create_question',
      'click #js-question-sort-button': 'start_sort_question'
    };

    form.prototype.questions = null;

    $container = null;

    $form = null;

    $document_body = null;

    form.prototype.templates = {
      no_question: _.template('<tr>\n<td class="itemName"><p>質問項目が未作成です。</p></td>\n<td class="itemSample"></td>\n<td class="itemSetting"></td>\n<td class="itemSort"></td>\n</tr>')
    };

    form.prototype.initialize = function() {
      var body_element;
      body_element = !document.uniqueID && !window.opera && !window.sidebar && window.localStorage && typeof window.orientation === "undefined" ? 'body' : 'html';
      this.$document_body = $(body_element);
      this.questions = [];
      this.$container = $('#questionnaire-questions-container');
      this.$form = $('#admin-work-elements-form');
      this.$form.on('submit', this.before_submit);
      this.questions_params = getEmbeddedJSON($('#questionnaire-questions-params'));
      this.questions_errors = getEmbeddedJSON($('#questionnaire-questions-errors'));
      this.questions_mode = $('#js-questions-mode').val();
      if (this.questions_params != null) {
        this.restore_questions(this.questions_params, this.questions_errors, this.questions_mode);
      }
      this.check_and_add_question();
      this.$el.find('.sortCancelBtn').on('click', this.cancel_sort_question_mode);
      return this.$el.find('.sortModeEndBtn').on('click', this.execute_sort_question);
    };

    form.prototype.restore_questions = function(questions_params, questions_errors, questions_mode) {
      var errors, index, num, params, _results;
      num = 0;
      _results = [];
      for (index in questions_params) {
        params = questions_params[index];
        errors = null;
        if ((questions_errors != null ? questions_errors[index] : void 0) != null) {
          errors = questions_errors[index];
        }
        this.add_question(num, params, errors, questions_mode);
        _results.push(num++);
      }
      return _results;
    };

    form.prototype.check_and_add_question = function() {
      if (this.$container.find('.js-questionnaire-question').length <= 0) {
        this.$container.append(this.templates.no_question());
      }
      if (this.questions.length <= 1) {
        return this.$document_body.find('#js-question-sort-button').addClass('disabled');
      } else if (this.$document_body.find('#js-question-sort-button').hasClass('disabled')) {
        return this.$document_body.find('#js-question-sort-button').removeClass('disabled');
      }
    };

    form.prototype.add_question = function(num, params, errors, questions_mode) {
      if (params == null) {
        params = null;
      }
      if (errors == null) {
        errors = null;
      }
      if (questions_mode == null) {
        questions_mode = null;
      }
      this.$container.append(this.get_new_question(num, params, errors, questions_mode).$el);
      this.reassign_sort_order();
      return false;
    };

    form.prototype.create_question = function() {
      var questionnaire_id;
      questionnaire_id = $('#js-hidden-questionnaire_id').val();
      $form = $('form#js-admin-work-driver-elements-form');
      $form.attr('action', App.base_url + '/admin/work/questionnaire/question/create/');
      $form.submit();
      return false;
    };

    form.prototype.get_new_question = function(num, params, errors, questions_mode) {
      var question;
      if (params == null) {
        params = null;
      }
      if (errors == null) {
        errors = null;
      }
      if (questions_mode == null) {
        questions_mode = null;
      }
      question = new App.pages.admin.work.questionnaire.elements.form.views.question({
        number: num,
        params: params,
        errors: errors,
        questions_mode: questions_mode
      });
      question.on('destroy', (function(_this) {
        return function(delete_question) {
          var key, _i, _len, _ref, _results;
          _ref = _this.questions;
          _results = [];
          for (key = _i = 0, _len = _ref.length; _i < _len; key = ++_i) {
            question = _ref[key];
            if (question.cid === delete_question.cid) {
              _this.questions.splice(key, 1);
              if (_this.questions.length <= 1) {
                _this.$document_body.find('#js-question-sort-button').addClass('disabled');
              }
              _this._update_question_settion_info();
              _this._update_question_view();
              break;
            } else {
              _results.push(void 0);
            }
          }
          return _results;
        };
      })(this));
      this.questions.push(question);
      return question;
    };

    form.prototype.before_submit = function() {
      var question, sort_order, _i, _len, _ref;
      sort_order = 0;
      _ref = this.questions;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        question = _ref[_i];
        question.set_sort_order(sort_order++);
      }
      return true;
    };

    form.prototype.start_sort_question = function(event) {
      if (this.questions.length <= 1) {
        return false;
      }
      this.position = $(event.target).offset().top - 40;
      return this.$document_body.animate({
        scrollTop: this.position
      }, 300, (function(_this) {
        return function() {
          $('#questionnaire-questions').addClass('sorting');
          _this.$el.find('.sortCancelBtn').show();
          _this.$document_body.find('.boxSectionSetProf').addClass('disabled');
          _this.$document_body.find('.js-about-page-container').hide();
          _this.$document_body.find('.js-questionnaire-questions-add-button').hide();
          return _this.$document_body.find('#js-question-sort-button').hide();
        };
      })(this));
    };

    form.prototype.cancel_sort_question_mode = function() {
      this._update_question_view();
      return this.end_sort_question_mode();
    };

    form.prototype.end_sort_question_mode = function(event) {
      $('#questionnaire-questions').removeClass('sorting');
      this.$el.find('.sortCancelBtn').hide();
      this.$document_body.find('.boxSectionSetProf').removeClass('disabled');
      this.$document_body.find('.js-about-page-container').show();
      this.$document_body.find('#js-question-sort-button').show();
      if ($('#questionnaire-questions-container .js-questionnaire-question').length < 20) {
        this.$document_body.find('.js-questionnaire-questions-add-button').show();
      }
      return false;
    };

    form.prototype.execute_sort_question = function() {
      return this._update_question_settion_info();
    };

    form.prototype._update_question_settion_info = function() {
      this.reassign_sort_order();
      $form = $('#js-admin-work-driver-elements-form');
      App.functions.fuel_set_token($form);
      Backbone.ajax({
        type: 'post',
        url: "" + App.base_url + "/api/admin/work/questionnaire/edit/questions",
        data: $form.serialize(),
        success: (function(_this) {
          return function(xhr) {
            return _this.end_sort_question_mode();
          };
        })(this),
        error: (function(_this) {
          return function(xhr) {
            return setTimeout(function() {
              return _this.execute_sort_question();
            }, 3000);
          };
        })(this),
        async: false,
        cache: false
      });
      return false;
    };

    form.prototype._update_question_view = function() {
      return Backbone.ajax({
        type: 'get',
        url: "" + App.base_url + "/api/admin/work/questionnaire/edit/questions",
        success: (function(_this) {
          return function(xhr) {
            if (xhr != null ? xhr.questions : void 0) {
              _this.questions_params = xhr.questions;
              _this.$container.html('');
              return _this.restore_questions(_this.questions_params, null, _this.questions_mode);
            }
          };
        })(this),
        error: (function(_this) {
          return function(xhr) {
            return setTimeout(function() {
              return _this._update_question_view();
            }, 3000);
          };
        })(this),
        async: false,
        cache: false
      });
    };

    form.prototype.reassign_sort_order = function() {
      return $('#questionnaire-questions-container').find('tr.js-questionnaire-question').each((function(_this) {
        return function(index, element) {
          return $(element).find('.itemSample input:hidden').each(function(idx, hidden) {
            var sort_order;
            if ($(hidden).attr('name').match(/questions\[[0-9]+\]\[sort_order\]/)) {
              sort_order = $(hidden).attr('name').replace(/questions\[([0-9]+)\]\[sort_order\]/, '$1');
              return $(hidden).attr('value', sort_order);
            }
          });
        };
      })(this));
    };

    return form;

  })(Backbone.View);

  App.pages.admin.work.questionnaire.elements.form.views.question = (function(_super) {
    __extends(question, _super);

    function question() {
      this.set_sort_order = __bind(this.set_sort_order, this);
      this.down_question = __bind(this.down_question, this);
      this.up_question = __bind(this.up_question, this);
      this.change_input_type = __bind(this.change_input_type, this);
      this.get_input_type_name_by_input_value = __bind(this.get_input_type_name_by_input_value, this);
      this.get_input_type = __bind(this.get_input_type, this);
      this.hide_balloon = __bind(this.hide_balloon, this);
      this.delete_question_complete = __bind(this.delete_question_complete, this);
      this.remove_question = __bind(this.remove_question, this);
      this.delete_question_confirm = __bind(this.delete_question_confirm, this);
      this.edit_question = __bind(this.edit_question, this);
      this._edit_question = __bind(this._edit_question, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return question.__super__.constructor.apply(this, arguments);
    }

    question.current_number = 0;

    question.prototype.el = '<tr class="js-questionnaire-question"></tr>';

    question.prototype.events = {
      'change input.js-questionnaire-question-input-type-input': 'change_input_type',
      'click .js-questionnaire-question-remove-button': 'remove_question',
      'click .js-questionnaire-question-up-button': 'up_question',
      'click .js-questionnaire-question-down-button': 'down_question',
      'click .js-questionnaire-question-edit-link': 'edit_question'
    };

    question.prototype.templates = {
      input_container: _.template('<td class="itemName"><p></p></td>\n<td class="itemSample"><p></p></td>\n<td class="itemSetting"></td>\n<td class="itemSort"><p></p></td>'),
      control_group: _.template('<div class="control-group" style="margin-bottom: 5px;">\n  <label class="control-label"><%= label %></label>\n  <div class="controls"><%= controls %><%= errors %></div>\n</div>'),

      /*
      input_type: _.template('''
        <label class="radio inline">
          <input type="radio" class="js-questionnaire-question-input-type-input"
            name="questions[<%= number %>][type]" value="<%= value %>"<% if(checked){ %> checked="checked"<% } %>>
          <%= label %>
        </label>
      ''')
       */
      items: {
        name_input: _.template('<td class="itemName"><p><a href="#" class="js-questionnaire-question-edit-link"><%= value %></a></p></td>'),
        name_confirm: _.template('<td class="itemName"><p><%= value %></p></td>'),
        sample: _.template('<td class="itemSample"><%= input_field %></td>'),
        setting: _.template('<td class="itemSetting"></td>'),
        hidden_basic: _.template('<input type="hidden" name="questions[<%= number %>][id]" value="<%= params.id %>">\n<input type="hidden" name="questions[<%= number %>][name]" value="<%= params.name %>">\n<input type="hidden" name="questions[<%= number %>][description]" value="<%= params.description %>">\n<input type="hidden" name="questions[<%= number %>][is_required]" value="<%= params.is_required %>">\n<input type="hidden" name="questions[<%= number %>][sort_order]" value="<%= params.sort_order %>">\n<input type="hidden" name="questions[<%= number %>][type]" value="<%= params.type %>">'),
        hidden_text: _.template('<input type="hidden" name="questions[<%= number %>][input_format]" value="<%= params.input_format %>">'),
        hidden_choice: _.template('<input type="hidden" name="questions[<%= number %>][has_choice_limit]" value="<%= params.has_choice_limit %>">\n<input type="hidden" name="questions[<%= number %>][question_choices][<%= choice_number %>][id]" value="<%= choice.id %>">\n<input type="hidden" name="questions[<%= number %>][question_choices][<%= choice_number %>][choice]" value="<%= choice.choice %>">\n<input type="hidden" name="questions[<%= number %>][question_choices][<%= choice_number %>][limit_count]" value="<%= choice.limit_count %>">\n<input type="hidden" name="questions[<%= number %>][question_choices][<%= choice_number %>][sort_order]" value="<%= choice.sort_order %>">')
      },

      /*
        item_setting: _.template('''
          <td class="itemSetting"><div class="setting"><a href="#" class="uiAnchor"><i class="icon-setting"></i></a></div></td>
        ''')
        required: _.template('''
          <input type="checkbox" name="questions[<%= number %>][is_required]" value="1"<% if(checked){ %> checked="checked"<% } %>> 入力必須にする
        ''')
       */
      sample_fields: {
        text: _.template('<input type="text" disabled="disabled">'),
        textarea: _.template('<textarea rows="6" disabled="disabled"></textarea>'),
        radio: _.template('<p><label class="bullet"><input type="radio" disabled="disabled"><%= choice %></label></p>'),
        checkbox: _.template('<p><label class="bullet"><input type="checkbox" disabled="disabled"><%= choice %></label></p>'),
        date: _.template('<input type="text" placeholder="年/月/日" size="6" value="" disabled="disabled">')
      }
    };

    question.prototype.id = null;

    question.prototype.number = null;

    question.prototype.choice_number = null;

    question.prototype.questions_mode = null;

    question.prototype.$container = null;

    question.prototype.$input_types = null;

    question.prototype.input_type_option_views = {};

    question.prototype.current_input_type_option_view = null;

    question.prototype.input_types = [
      {
        label: 'テキスト（1行入力）',
        type: 'text',
        value: 0
      }, {
        label: 'テキストエリア（複数行入力）',
        type: 'textarea',
        value: 1
      }, {
        label: 'ラジオボタン（単一選択）',
        type: 'radio',
        value: 2
      }, {
        label: 'チェックボックス（複数選択）',
        type: 'checkbox',
        value: 3
      }, {
        label: '日付',
        type: 'date',
        value: 4
      }
    ];

    question.prototype.initialize = function(options) {
      var errors, params;
      this.number = (options != null ? options.number : void 0) != null ? options.number : null;
      if ((options != null ? options.id : void 0) != null) {
        this.id = options.id;
      }
      if ((options != null ? options.questions_mode : void 0) != null) {
        this.questions_mode = options.questions_mode;
      }
      params = (options != null ? options.params : void 0) != null ? options.params : null;
      errors = (options != null ? options.errors : void 0) != null ? options.errors : null;
      return this.render(params, errors);
    };

    question.prototype.render = function(params, errors) {
      var $setting, $sort, $sort_step_down, $sort_step_up, $sort_td, choice, choice_number, hidden, hidden_basic, hidden_choice, input_field, input_type, links, sample, selected_input_type, _i, _j, _len, _len1, _ref, _ref1;
      if (params == null) {
        params = null;
      }
      if (errors == null) {
        errors = null;
      }
      if (this.input_type_option_views != null) {
        delete this.input_type_option_views;
        this.input_type_option_views = {};
      }
      if (this.questions_mode === 'confirm') {
        this.$el.append(this.templates.items.name_confirm({
          number: this.number,
          value: (params != null ? params.name : void 0) != null ? params.name : null
        }));
      } else {
        this.$el.append(this.templates.items.name_input({
          number: this.number,
          value: (params != null ? params.name : void 0) != null ? params.name : null
        }));
      }
      input_field = '';
      selected_input_type = null;
      _ref = this.input_types;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        input_type = _ref[_i];
        sample = '';
        if (((params != null ? params.type : void 0) != null) && parseInt(params.type, 10) === parseInt(input_type.value, 10)) {
          hidden_basic = this.templates.items.hidden_basic({
            number: this.number,
            params: params
          });
          hidden = hidden_basic;
          selected_input_type = input_type.type;
          hidden_choice = null;
          choice_number = 0;
          switch (selected_input_type) {
            case 'checkbox':
            case 'radio':
              _ref1 = params.question_choices;
              for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
                choice = _ref1[_j];
                input_field += this.templates.sample_fields[selected_input_type]({
                  choice: choice.choice
                });
                hidden_choice = this.templates.items.hidden_choice({
                  number: this.number,
                  type: selected_input_type,
                  choice_number: choice_number,
                  params: params,
                  choice: choice
                });
                hidden += hidden_choice;
                choice_number++;
              }
              break;
            default:
              input_field = this.templates.sample_fields[selected_input_type]({
                number: this.number,
                params: params
              });
              hidden += this.templates.items.hidden_text({
                number: this.number,
                params: params
              });
          }
          sample = this.templates.items.sample({
            input_field: input_field + hidden
          });
          this.$el.append(sample);
          $setting = $('<td class="itemSetting"></td>');
          if (this.questions_mode === 'confirm') {
            this.$el.append($setting);
          } else {
            links = [];
            links.push({
              path: '#',
              label: '編集する',
              event_name: 'edit'
            });
            links.push({
              path: '#',
              label: '削除する',
              event_name: 'delete'
            });
            this.balloon = new App.elements.setting_balloon.views.setting_balloon({
              links: links
            });
            this.balloon.on('edit', (function(_this) {
              return function() {
                return _this._edit_question(_this.balloon.$el);
              };
            })(this));
            this.balloon.on('delete', this.delete_question_confirm);
            $setting.append(this.balloon.$el);
            this.$el.append($setting);
          }
          $sort_td = $('<td class="itemSort" data-number="' + this.number + '"></td>');
          $sort_step_up = $('<a id="js-sortBtn-up-' + this.number + '" class="btnM btnBasic block js-sortBtn js-sortBtn-up" href="#" data-step="1" data-index="' + this.number + '">↑</a>');
          $sort_step_down = $('<a id="js-sortBtn-down-' + this.number + '" class="btnM btnBasic block js-sortBtn js-sortBtn-down" href="#" data-step="1" data-index="' + this.number + '">↓</a>');
          $sort_step_up.on('click', this.up_question);
          $sort_step_down.on('click', this.down_question);
          $sort = $sort_td.append($sort_step_up).append($sort_step_down);
          if (this.questions_mode === 'form') {
            this.$el.append($sort);
          }
          if (this.number === 0) {
            $sort_step_up.addClass('disabled');
          }
          $sort_step_down.addClass('disabled');
          if (this.number > 0) {
            $('#js-sortBtn-down-' + (parseInt(this.number, 10) - 1)).removeClass('disabled');
          }
        }
      }
      return this.$el;
    };

    question.prototype._edit_question = function(target) {
      var $form, number;
      number = $(target).parents('.js-questionnaire-question').find('.itemSort').attr('data-number');
      $form = $('form#js-admin-work-driver-elements-form');
      $form.attr('action', App.base_url + '/admin/work/questionnaire/question/edit/' + number);
      $form.submit();
      return false;
    };

    question.prototype.edit_question = function(event) {
      return this._edit_question(event.currentTarget);
    };

    question.prototype.delete_question_confirm = function() {
      if (this.modal == null) {
        this.modal = new App.elements.modal();
        this.modal.add_container_class('jsModal-Dialog');
        this.modal.set_title('質問の削除');
        this.modal.set_body(App.pages.admin.work.questionnaire.elements.form.templates.question_modal.delete_body());
        this.modal.set_footer_cancel_delete(null, '削除する(まだ実行されません)');
      }
      this.modal.show();
      this.modal.on('cancel', this.hide_balloon);
      return this.modal.on('decide', (function(_this) {
        return function() {
          return _this.remove_question();
        };
      })(this));
    };

    question.prototype.remove_question = function() {
      this.remove();
      this.trigger('destroy', this);
      this.delete_question_complete(this.modal);
      return false;
    };

    question.prototype.delete_question_complete = function(modal) {
      if (modal == null) {
        modal = null;
      }
      if (modal != null) {
        modal.hide();
      }
      if ($('#questionnaire-questions-container .js-questionnaire-question').length < 20) {
        return $('#questionnaire-questions .js-questionnaire-questions-add-button').show();
      }
    };

    question.prototype.hide_balloon = function() {
      if (this.balloon) {
        return this.balloon.hide();
      }
    };

    question.prototype.get_input_type = function() {
      return this.$input_types.filter(':checked').val();
    };

    question.prototype.get_input_type_name_by_input_value = function(input_value) {
      var input_type, _i, _len, _ref;
      _ref = this.input_types;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        input_type = _ref[_i];
        if (input_type.value === parseInt(input_value, 10)) {
          return input_type.type;
        }
      }
      return null;
    };

    question.prototype.change_input_type = function() {
      var input_type_name;
      if (this.current_input_type_option_view != null) {
        this.input_type_option_views[this.current_input_type_option_view].hide();
      }
      input_type_name = this.get_input_type_name_by_input_value(this.get_input_type());
      if (input_type_name != null) {
        this.input_type_option_views[input_type_name].show();
        this.current_input_type_option_view = input_type_name;
      }
      return false;
    };

    question.prototype.up_question = function(event) {
      var $prev_question;
      if ($(event.currentTarget).hasClass('disabled')) {
        return false;
      }
      $prev_question = this.$el.prev('.js-questionnaire-question');
      $prev_question.before(this.$el);
      this.$sort_btn = this.$el.find('.js-sortBtn');
      this.new_index_num = parseInt(this.$sort_btn.attr('data-index'), 10) - parseInt(this.$sort_btn.attr('data-step'), 10);
      this.$sort_btn.attr('data-index', this.new_index_num);
      this.$el.find('.itemSort').attr('data-number', this.new_index_num);
      this.regexp = new RegExp('^questions\[[0-9]+\]', 'gi');
      this.$el.find('.itemSample input:hidden').each((function(_this) {
        return function(index, element) {
          return $(element).attr('name', $(element).attr('name').replace(_this.regexp, 'questions[' + _this.new_index_num + ']'));
        };
      })(this));
      this.$prev_sort_btn = $prev_question.find('.js-sortBtn');
      this.new_index_num = parseInt(this.$prev_sort_btn.attr('data-index'), 10) + parseInt(this.$prev_sort_btn.attr('data-step'), 10);
      this.$prev_sort_btn.attr('data-index', this.new_index_num);
      $prev_question.find('.itemSort').attr('data-number', this.new_index_num);
      $prev_question.find('.itemSample input:hidden').each((function(_this) {
        return function(index, element) {
          return $(element).attr('name', $(element).attr('name').replace(_this.regexp, 'questions[' + _this.new_index_num + ']'));
        };
      })(this));
      if (parseInt(this.$sort_btn.attr('data-index'), 10) === 0) {
        this.$el.find('.js-sortBtn-up').addClass('disabled');
        $prev_question.find('.js-sortBtn-up').removeClass('disabled');
      }
      if ($('#questionnaire-questions-container .js-questionnaire-question').length === parseInt(this.$prev_sort_btn.attr('data-index'), 10) + 1) {
        $prev_question.find('.js-sortBtn-down').addClass('disabled');
        this.$el.find('.js-sortBtn-down').removeClass('disabled');
      }
      return false;
    };

    question.prototype.down_question = function(event) {
      var $next_question;
      if ($(event.currentTarget).hasClass('disabled')) {
        return false;
      }
      $next_question = this.$el.next('.js-questionnaire-question');
      $next_question.after(this.$el);
      this.$sort_btn = this.$el.find('.js-sortBtn');
      this.new_index_num = parseInt(this.$sort_btn.attr('data-index'), 10) + parseInt(this.$sort_btn.attr('data-step'), 10);
      this.$sort_btn.attr('data-index', this.new_index_num);
      this.$el.find('.itemSort').attr('data-number', this.new_index_num);
      this.regexp = new RegExp('^questions\[[0-9]+\]', 'gi');
      this.$el.find('.itemSample input:hidden').each((function(_this) {
        return function(index, element) {
          return $(element).attr('name', $(element).attr('name').replace(_this.regexp, 'questions[' + _this.new_index_num + ']'));
        };
      })(this));
      this.$next_sort_btn = $next_question.find('.js-sortBtn');
      this.new_index_num = parseInt(this.$next_sort_btn.attr('data-index'), 10) - parseInt(this.$next_sort_btn.attr('data-step'), 10);
      this.$next_sort_btn.attr('data-index', this.new_index_num);
      $next_question.find('.itemSort').attr('data-number', this.new_index_num);
      $next_question.find('.itemSample input:hidden').each((function(_this) {
        return function(index, element) {
          return $(element).attr('name', $(element).attr('name').replace(_this.regexp, 'questions[' + _this.new_index_num + ']'));
        };
      })(this));
      if ($('#questionnaire-questions-container .js-questionnaire-question').length === parseInt(this.$sort_btn.attr('data-index'), 10) + 1) {
        this.$el.find('.js-sortBtn-down').addClass('disabled');
        $next_question.find('.js-sortBtn-down').removeClass('disabled');
      }
      if (parseInt(this.$next_sort_btn.attr('data-index'), 10) === 0) {
        $next_question.find('.js-sortBtn-up').addClass('disabled');
        this.$el.find('.js-sortBtn-up').removeClass('disabled');
      }
      return false;
    };

    question.prototype.set_sort_order = function(sort_order) {
      var input_type_view, _i, _len, _ref;
      if (sort_order == null) {
        sort_order = 0;
      }
      this.$el.find('.js-questionnaire-question-sort-order').val(sort_order);
      _ref = this.input_type_option_views;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        input_type_view = _ref[_i];
        if ((input_type_view.set_choices_sort_order != null) && typeof input_type_view.set_choices_sort_order === 'function') {
          input_type_view.set_choices_sort_order();
        }
      }
      return true;
    };

    return question;

  })(Backbone.View);

  App.pages.admin.work.questionnaire.elements.form.views.question.options = {};

  App.pages.admin.work.questionnaire.elements.form.views.question.options.driver = (function(_super) {
    __extends(driver, _super);

    function driver() {
      this.show = __bind(this.show, this);
      this.hide = __bind(this.hide, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      this.format_option_body_arguments = __bind(this.format_option_body_arguments, this);
      return driver.__super__.constructor.apply(this, arguments);
    }

    driver.prototype.el = '<div></div>';

    driver.prototype.container_template = _.template('<div class="control-group js-questionnaire-question-input-type-option">\n  <label class="control-label"><%= label %></label>\n  <div class="controls">\n    <%= option_body %>\n    <%= errors %>\n  </div>\n</div>');

    driver.prototype.name = '';

    driver.prototype.template = _.template('');

    driver.prototype.number = null;

    driver.prototype.value = null;

    driver.prototype.format_option_body_arguments = function(params) {
      var object;
      if (params == null) {
        params = null;
      }
      object = {
        number: this.number,
        value: this.value
      };
      object.default_value = (params != null ? params["default"] : void 0) != null ? params["default"] : null;
      return object;
    };

    driver.prototype.initialize = function(options) {
      if ((options != null ? options.number : void 0) != null) {
        this.number = options.number;
      }
      if ((options != null ? options.value : void 0) != null) {
        this.value = options.value;
      }
      return this.render((options != null ? options.params : void 0) != null ? options.params : null, (options != null ? options.errors : void 0) != null ? options.errors : null);
    };

    driver.prototype.render = function(params, errors) {
      if (params == null) {
        params = null;
      }
      if (errors == null) {
        errors = null;
      }
      this.$el.html(this.container_template({
        label: this.name,
        option_body: this.template(this.format_option_body_arguments(params)),
        errors: errors != null ? (new App.elements.errors.views.errors({
          errors: errors
        })).get_html() : null
      }));
      return this.$el;
    };

    driver.prototype.hide = function() {
      this.$el.hide();
      return this.$el;
    };

    driver.prototype.show = function() {
      this.$el.show();
      return this.$el;
    };

    return driver;

  })(Backbone.View);

  App.pages.admin.work.questionnaire.elements.form.views.question.options.driver_selectable = (function(_super) {
    __extends(driver_selectable, _super);

    function driver_selectable() {
      this.set_choices_sort_order = __bind(this.set_choices_sort_order, this);
      this.down_choice = __bind(this.down_choice, this);
      this.up_choice = __bind(this.up_choice, this);
      this.remove_choice = __bind(this.remove_choice, this);
      this.add_choice = __bind(this.add_choice, this);
      this.get_choices_count = __bind(this.get_choices_count, this);
      this.render = __bind(this.render, this);
      return driver_selectable.__super__.constructor.apply(this, arguments);
    }

    driver_selectable.prototype.DEFAULT_CHOICES_COUNT = 2;

    driver_selectable.prototype.name = '選択肢を設定';

    driver_selectable.prototype.input_type_name = null;

    driver_selectable.prototype.template = _.template('<div class="js-questionnaire-question-choices-choice-container">\n</div>\n<a href="#" class="js-questionnaire-question-choices-add-choice-button btn">\n  <i class="icon-plus"></i> 選択肢を追加する\n</a>');

    driver_selectable.prototype.choice_template = _.template('<div class="row-fluid js-questionnaire-question-choices-choice">\n  <div class="span5">\n    <input type="hidden" name="questions[<%= number %>][input_type][<%= input_type_name %>][choices][<%= choice_number %>][sort_order]"\n      value="" class="js-questionnaire-question-choices-choice-sort-order">\n    <input type="hidden" name="questions[<%= number %>][input_type][<%= input_type_name %>][choices][<%= choice_number %>][id]" value="<%= id %>">\n    <input type="text" class="span12" name="questions[<%= number %>][input_type][<%= input_type_name %>][choices][<%= choice_number %>][choice]" value="<%= choice %>">\n  </div>\n  <div class="span3">\n    上限人数：<input type="text" class="input-mini" name="questions[<%= number %>][input_type][<%= input_type_name %>][choices][<%= choice_number %>][limit_count]" value="<%= limit_count %>">\n  </div>\n  <div class="span4">\n    <a href="#" class="btn btn-mini js-questionnaire-question-choices-up-choice-button">\n      <i class="icon-arrow-up"></i>\n    </a>\n    <a href="#" class="btn btn-mini js-questionnaire-question-choices-down-choice-button">\n      <i class="icon-arrow-down"></i>\n    </a>\n    <a href="#" class="btn btn-mini btn-danger js-questionnaire-question-choices-remove-choice-button">\n      <i class="icon-remove icon-white"></i>\n    </a>\n  </div>\n</div>');

    driver_selectable.prototype.events = {
      'click .js-questionnaire-question-choices-add-choice-button': 'add_choice',
      'click .js-questionnaire-question-choices-remove-choice-button': 'remove_choice',
      'click .js-questionnaire-question-choices-up-choice-button': 'up_choice',
      'click .js-questionnaire-question-choices-down-choice-button': 'down_choice'
    };

    driver_selectable.prototype.$choices_container = null;

    driver_selectable.prototype.choice_number = 0;

    driver_selectable.prototype.render = function(params, errors) {
      var choice, choices_count, index, _ref;
      if (params == null) {
        params = null;
      }
      if (errors == null) {
        errors = null;
      }
      driver_selectable.__super__.render.apply(this, arguments);
      this.$choices_container = this.$el.find('.js-questionnaire-question-choices-choice-container');
      if ((params != null ? params.choices : void 0) != null) {
        _ref = params.choices;
        for (index in _ref) {
          choice = _ref[index];
          this.add_choice({
            id: choice.id != null ? choice.id : null,
            choice: choice.choice != null ? choice.choice : null,
            limit_count: choice.limit_count != null ? choice.limit_count : null
          });
        }
      }
      choices_count = this.get_choices_count();
      this.choice_number = choices_count;
      while (choices_count++ < this.DEFAULT_CHOICES_COUNT) {
        this.add_choice();
      }
      return this.$el;
    };

    driver_selectable.prototype.get_choices_count = function() {
      return this.$choices_container.find('.js-questionnaire-question-choices-choice').length;
    };

    driver_selectable.prototype.add_choice = function(params) {
      var choice, id, limit_count;
      if (params != null) {
        choice = params.choice != null ? params.choice : null;
        limit_count = params.limit_count != null ? params.limit_count : null;
        id = params.id != null ? params.id : null;
      }
      this.$choices_container.append(this.choice_template({
        id: id,
        number: this.number,
        input_type_name: this.input_type_name,
        choice_number: this.choice_number++,
        choice: choice,
        limit_count: limit_count
      }));
      return false;
    };

    driver_selectable.prototype.remove_choice = function(event) {
      var $btn, $choice;
      if (!confirm('削除してもよろしいですか？')) {
        return false;
      }
      $btn = $(event.target);
      $choice = $btn.parents('.js-questionnaire-question-choices-choice');
      $choice.remove();
      if (this.get_choices_count() <= 0) {
        this.add_choice();
      }
      return false;
    };

    driver_selectable.prototype.up_choice = function(event) {
      var $choice;
      $choice = $(event.target).parents('.js-questionnaire-question-choices-choice');
      $choice.prev('.js-questionnaire-question-choices-choice').before($choice);
      return false;
    };

    driver_selectable.prototype.down_choice = function(event) {
      var $choice;
      $choice = $(event.target).parents('.js-questionnaire-question-choices-choice');
      $choice.next('.js-questionnaire-question-choices-choice').after($choice);
      return false;
    };

    driver_selectable.prototype.set_choices_sort_order = function() {
      var sort_order;
      sort_order = 0;
      this.$el.find('.js-questionnaire-question-choices-choice').each((function(_this) {
        return function(index, element) {
          var $choice;
          $choice = $(element);
          return $choice.find('.js-questionnaire-question-choices-choice-sort-order').val(sort_order++);
        };
      })(this));
      return true;
    };

    return driver_selectable;

  })(App.pages.admin.work.questionnaire.elements.form.views.question.options.driver);

  App.pages.admin.work.questionnaire.elements.form.views.question.options.text = (function(_super) {
    __extends(text, _super);

    function text() {
      return text.__super__.constructor.apply(this, arguments);
    }

    text.prototype.name = 'デフォルト値';

    text.prototype.template = _.template('<input type="text" class="span12" name="questions[<%= number %>][input_type][text][default]" value="<%= default_value %>">');

    return text;

  })(App.pages.admin.work.questionnaire.elements.form.views.question.options.driver);

  App.pages.admin.work.questionnaire.elements.form.views.question.options.textarea = (function(_super) {
    __extends(textarea, _super);

    function textarea() {
      return textarea.__super__.constructor.apply(this, arguments);
    }

    textarea.prototype.name = 'デフォルト値';

    textarea.prototype.template = _.template('<textarea class="span12" rows="3" name="questions[<%= number %>][input_type][textarea][default]"><%= default_value %></textarea>');

    return textarea;

  })(App.pages.admin.work.questionnaire.elements.form.views.question.options.driver);

  App.pages.admin.work.questionnaire.elements.form.views.question.options.radio = (function(_super) {
    __extends(radio, _super);

    function radio() {
      return radio.__super__.constructor.apply(this, arguments);
    }

    radio.prototype.input_type_name = 'radio';

    return radio;

  })(App.pages.admin.work.questionnaire.elements.form.views.question.options.driver_selectable);

  App.pages.admin.work.questionnaire.elements.form.views.question.options.checkbox = (function(_super) {
    __extends(checkbox, _super);

    function checkbox() {
      return checkbox.__super__.constructor.apply(this, arguments);
    }

    checkbox.prototype.input_type_name = 'checkbox';

    return checkbox;

  })(App.pages.admin.work.questionnaire.elements.form.views.question.options.driver_selectable);

  App.pages.admin.work.questionnaire.elements.form.views.question.options.date = (function(_super) {
    __extends(date, _super);

    function date() {
      this.render = __bind(this.render, this);
      return date.__super__.constructor.apply(this, arguments);
    }

    date.prototype.name = 'デフォルト値';

    date.prototype.template = _.template('<input type="text" rel="datepicker" class="span12" name="questions[<%= number %>][input_type][date][default]" value="<%= default_value %>">');

    date.prototype.render = function(params, errors) {
      if (params == null) {
        params = null;
      }
      if (errors == null) {
        errors = null;
      }
      date.__super__.render.apply(this, arguments);
      new App.elements.datepicker.views.datepicker({
        el: this.$el.find('[rel="datepicker"]')
      });
      return this.$el;
    };

    return date;

  })(App.pages.admin.work.questionnaire.elements.form.views.question.options.driver);

  add_route('/admin/work/questionnaire/(?:create|edit|copy)/?.*', function() {
    var $form;
    new App.pages.admin.work.driver.elements.form.views.form();
    new App.pages.admin.work.questionnaire.elements.form.views.form();
    $form = $('form');
    return $form.one('submit', (function(_this) {
      return function() {
        $form.on('submit', function() {
          return false;
        });
        return true;
      };
    })(this));
  });

  add_route('/admin/work/questionnaire(/index)?/?', function() {
    return new App.pages.admin.work.driver.index.views.index({
      base_url: "" + App.base_url + "/admin/work/questionnaire",
      api_base_url: "" + App.base_url + "/api/admin/work/questionnaire",
      show_name: 'アンケート',
      optional_template: _.template('<div class="boxLabeled">\n  <p class="label">質問項目</p>\n  <% _.each(item.questions, function(question_name) { %>\n    <p class="content"><%- question_name %></p>\n  <% }); %>\n</div>')
    });
  });

  App.pages.admin.work.questionnaire.question = {};

  App.pages.admin.work.questionnaire.question.elements = {};

  App.pages.admin.work.questionnaire.question.elements.form = {
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.work.questionnaire.question.elements.form.models.choice = (function(_super) {
    __extends(choice, _super);

    function choice() {
      return choice.__super__.constructor.apply(this, arguments);
    }

    choice.prototype.urlRoot = 'aip';

    choice.prototype.inputView = null;

    choice.prototype.defaults = {
      index: 0,
      value: ''
    };

    return choice;

  })(Backbone.Model);

  App.pages.admin.work.questionnaire.question.elements.form.collections.choices = (function(_super) {
    __extends(choices, _super);

    function choices() {
      this.check_input_val = __bind(this.check_input_val, this);
      return choices.__super__.constructor.apply(this, arguments);
    }

    choices.prototype.model = App.pages.admin.work.questionnaire.question.elements.form.models.choice;

    choices.prototype.check_input_val = function() {
      return _.each(this.models, (function(_this) {
        return function(model, i) {
          var val, view;
          view = model.inputView;
          val = '';
          if (view && (val = view.get_value()) !== '') {
            return model.set({
              'value': val
            });
          }
        };
      })(this));
    };

    return choices;

  })(Backbone.Collection);

  App.pages.admin.work.questionnaire.question.elements.form.views.form = (function(_super) {
    __extends(form, _super);

    function form() {
      this.set_preview_label = __bind(this.set_preview_label, this);
      this.initialize = __bind(this.initialize, this);
      return form.__super__.constructor.apply(this, arguments);
    }

    form.prototype.el = '.js-fieldTypeArea';

    form.prototype.question = null;

    form.prototype.$type = $('.js-type');

    form.prototype.events = {
      'change .js-type': 'change_type'
    };

    form.prototype.initialize = function() {
      var $field_type_area, $main, $preview_area, active_id, prevew_section;
      if (this.$el.length <= 0) {
        return false;
      }
      $main = jQuery('#main');
      $preview_area = $main.find('.previewArea');
      this.set_preview_label($main, $preview_area);
      $field_type_area = $main.find('.js-fieldTypeArea');
      active_id = $field_type_area.find('input:checked').val();
      if (!active_id) {
        active_id = $field_type_area.find('input:eq(0)').attr('checked', true).val();
      }
      active_id -= 0;
      prevew_section = new App.pages.admin.work.questionnaire.question.elements.form.views.preview($preview_area, {
        active_id: active_id
      });
      return $field_type_area.find('input').on('change', (function(_this) {
        return function(event) {
          var value;
          value = event.target.value - 0;
          return prevew_section.change_field(value);
        };
      })(this));
    };

    form.prototype.set_preview_label = function($main, $preview_area) {
      var $input_field, $label, $label_node, $note, $requiredArea, label_val, note_val, value;
      $input_field = $main.find('.jsFieldLabelArea .js-input-field');
      $label = $preview_area.find('.js-fieldLabel');
      $label_node = $label.find('span');
      $input_field.on('change', (function(_this) {
        return function(event) {
          var value;
          value = event.target.value;
          return $label_node.text(value ? value : '質問');
        };
      })(this));
      label_val = $input_field.val();
      $label_node.text(label_val ? label_val : '質問');
      $input_field = $main.find('.jsFieldNoteArea .js-input-field');
      $note = $preview_area.find('.js-fieldNote');
      $input_field.on('change', (function(_this) {
        return function(event) {
          var text, value;
          value = event.target.value;
          $note.text(value ? value : '説明文');
          text = $note[0].innerHTML;
          return $note.html(text.replace(/(\r\n|\n\r|\r|\n)/g, "<br>"));
        };
      })(this));
      note_val = $input_field.val().replace(/(\r\n|\n\r|\r|\n)/g, "<br>");
      $note.html(note_val ? note_val : '説明文');
      $requiredArea = $main.find('.js-isRequiredArea');
      $input_field = $requiredArea.find('input');
      $input_field.on('change', (function(_this) {
        return function(event) {
          var checked, value;
          value = event.target.value - 0;
          checked = event.target.checked;
          if (checked === true && value > 0) {
            return $label.find('i').show();
          } else {
            return $label.find('i').hide();
          }
        };
      })(this));
      value = $requiredArea.find('input:checked').val() - 0;
      if (value > 0) {
        return $label.find('i').show();
      }
    };

    return form;

  })(Backbone.View);

  App.pages.admin.work.questionnaire.question.elements.form.views.preview = (function(_super) {
    __extends(preview, _super);

    function preview() {
      this.toggle_preview_limit = __bind(this.toggle_preview_limit, this);
      this.change_field = __bind(this.change_field, this);
      this._limit_init = __bind(this._limit_init, this);
      this._show_delete_dialog = __bind(this._show_delete_dialog, this);
      this._add_choice_event_listener = __bind(this._add_choice_event_listener, this);
      this._create_choice_preview_view = __bind(this._create_choice_preview_view, this);
      this._create_choice_model = __bind(this._create_choice_model, this);
      this._choices_init = __bind(this._choices_init, this);
      this._field_type_init = __bind(this._field_type_init, this);
      this._set_active_id = __bind(this._set_active_id, this);
      this.initialize = __bind(this.initialize, this);
      return preview.__super__.constructor.apply(this, arguments);
    }

    preview.prototype.active_id = null;

    preview.prototype.$preview = null;

    preview.prototype.$type = null;

    preview.prototype.$choices = null;

    preview.prototype.$limit = null;

    preview.prototype.contens = null;

    preview.prototype.$globalOptionAddress = null;

    preview.prototype.$globalOptionTelephone = null;

    preview.prototype.$delete_modal = null;

    preview.prototype.btnProgress = false;

    preview.prototype.options = {
      preview: '.js-preview-question',
      content: '.content',
      contentWidth: 740,
      speed: 0,
      active_id: 0,
      activeClass: 'active',
      inputTypeArea: '.js-optionInputTypeArea',
      choicesArea: '.js-optionChoicesArea',
      limitArea: '.js-optionChoiceLimitArea',
      choiceWapper: '.contentWapper',
      choiceSection: '.choiceInputSection',
      choicePrevArea: '.choicesContent',
      choicePrevSection: '.choiceView',
      choicePrevLimitSection: '.choiceLimitView',
      choicePrevExtraSection: '.choiceExtraView',
      addChoiceBtn: '.js-addChoiceBtn',
      globalOptionAddressArea: '.js-optionGlobalOptionAddressArea',
      globalOptionTelephoneArea: '.js-optionGlobalOptionTelephoneArea'
    };

    preview.prototype.collection = null;

    preview.prototype.initialize = function($element, options) {
      var $preview_area;
      $preview_area = $element instanceof jQuery === true ? $element : jQuery($element);
      options = this.options = jQuery.extend(this.options, options);
      this.btnProgress = true;
      this.$delete_modal = new App.elements.modal();
      this.$delete_modal.add_container_class('jsModal-Dialog');
      this.$delete_modal.set_title('削除の確認');
      this.$delete_modal.set_body('選択肢を削除しようとしています。既にこの選択肢を選択しているユーザーが存在する場合、ユーザーが選択したデータも削除されます。よろしいですか？');
      this.$delete_modal.set_footer_cancel_delete(null, '削除');
      this.active_id = options.active_id;
      this.$preview = $preview_area.find(options.preview);
      this.$type = $preview_area.find(options.inputTypeArea);
      this.$choices = $preview_area.find(options.choicesArea);
      this.$limit = $preview_area.find(options.limitArea);
      this._field_type_init();
      this._choices_init();
      this._limit_init();
      this.contens = [];
      this.$preview.find(options.content).each((function(_this) {
        return function(i, $element) {
          var $content, num;
          $content = jQuery($element);
          if ($content.attr('id')) {
            num = +$content.attr('id').split('js-previewContent-')[1];
            if (num === _this.active_id) {
              $content.show();
            } else {
              $content.hide();
            }
            return _this.contens[num] = $content;
          }
        };
      })(this));
      if (this.active_id === 0) {
        this.$type.show();
      } else if (this.active_id === 2 || this.active_id === 3) {
        this.$choices.show();
        this.$limit.show();
      }
      return this.btnProgress = false;
    };

    preview.prototype._set_active_id = function(id) {
      this.active_id = id - 0;
      return this;
    };

    preview.prototype._field_type_init = function() {
      var $type, value;
      $type = this.$type;
      value = $type.find('input:checked').val();
      if (!value) {
        $type.find('input:eq(0)').attr('checked', true);
      }
      return this;
    };

    preview.prototype._choices_init = function() {
      var $add_btn, $wapper, collection, options, preview_areas;
      options = this.options;
      collection = this.collection = new App.pages.admin.work.questionnaire.question.elements.form.collections.choices();
      $add_btn = this.$choices.find(options.addChoiceBtn);
      $wapper = this.$choices.find(options.choiceWapper);
      preview_areas = [];
      $wapper.find(options.choiceSection).each((function(_this) {
        return function(i, element) {
          var input_model, input_view;
          input_model = _this._create_choice_model();
          input_view = new App.pages.admin.work.questionnaire.question.elements.form.views.choice_input({
            model: input_model,
            el: element
          });
          return _this._add_choice_event_listener(input_view);
        };
      })(this));
      this.$preview.find(options.choicePrevArea).each((function(_this) {
        return function(i, choice_preview_area) {
          var $preview_area, count, k, model_count, new_preview_model, new_preview_view, type, _i, _results;
          type = i;
          $preview_area = preview_areas[i] = jQuery(choice_preview_area);
          model_count = collection.length;
          count = 0;
          $preview_area.find(options.choicePrevSection).each(function(j, element) {
            var preview_model, preview_view;
            preview_model = collection.models[j];
            preview_view = new App.pages.admin.work.questionnaire.question.elements.form.views.choice({
              model: preview_model,
              el: element,
              option: {
                inputType: type
              }
            });
            return count = j;
          });
          count += 1;
          if (model_count > count) {
            _results = [];
            for (k = _i = count; count <= model_count ? _i <= model_count : _i >= model_count; k = count <= model_count ? ++_i : --_i) {
              new_preview_model = collection.models[k];
              new_preview_view = _this._create_choice_preview_view(new_preview_model, type);
              _results.push(new_preview_view.$el.insertBefore($preview_area.find(_this.options.choicePrevExtraSection)));
            }
            return _results;
          }
        };
      })(this));
      collection.check_input_val();
      return $add_btn.on('click', (function(_this) {
        return function(event) {
          var input_view, model, pre_view, type, _i, _ref;
          if (_this.btnProgress === false) {
            _this.btnProgress = true;
            model = _this._create_choice_model();
            input_view = new App.pages.admin.work.questionnaire.question.elements.form.views.choice_input({
              model: model
            });
            _this._add_choice_event_listener(input_view);
            for (type = _i = 0, _ref = preview_areas.length - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; type = 0 <= _ref ? ++_i : --_i) {
              pre_view = _this._create_choice_preview_view(model, type);
              pre_view.$el.hide().insertBefore(preview_areas[type].find(_this.options.choicePrevExtraSection)).slideDown(200);
            }
            input_view.$el.css({
              position: 'relative',
              left: '100%',
              opacity: 0
            }).appendTo($wapper).animate({
              left: 0,
              opacity: 1
            }, {
              duration: 200,
              complete: function() {
                return input_view.$el.removeAttr('style');
              }
            });
            _this._limit_init();
            _this.btnProgress = false;
          }
          return false;
        };
      })(this));
    };

    preview.prototype._create_choice_model = function() {
      var model;
      model = new App.pages.admin.work.questionnaire.question.elements.form.models.choice({
        index: this.collection.length
      });
      this.collection.add(model);
      return model;
    };

    preview.prototype._create_choice_preview_view = function(model, type) {
      var view;
      view = new App.pages.admin.work.questionnaire.question.elements.form.views.choice({
        model: model,
        option: {
          inputType: type
        }
      });
      return view;
    };

    preview.prototype._add_choice_event_listener = function(input_view) {
      return input_view.listenTo(input_view, 'delete', this._show_delete_dialog);
    };

    preview.prototype._show_delete_dialog = function(choice) {
      var $question_id, $questionnaire_id;
      $questionnaire_id = $('#form_questionnaire_id').val();
      $question_id = $('#form_id').val();
      if ($questionnaire_id !== '' && $question_id !== '') {
        this.$delete_modal.on('decide', (function(_this) {
          return function() {
            choice._dispose();
            return _this.$delete_modal.hide();
          };
        })(this));
        return this.$delete_modal.show();
      } else {
        return choice._dispose();
      }
    };

    preview.prototype._limit_init = function() {
      var value;
      value = this.$limit.find('input:checked').val();
      if (!value) {
        this.$limit.find('input:eq(1)').attr('checked', true);
      }
      this.$limit.find('input').on('change', (function(_this) {
        return function(event) {
          value = event.target.value - 0;
          return _this.toggle_preview_limit(value);
        };
      })(this));
      this.toggle_preview_limit(this.$limit.find('input:checked').val() - 0);
      return this;
    };

    preview.prototype.change_field = function(active_id) {
      var $choices, $limit, $next, $old, $preview, $type, old_id, options, position;
      options = this.options;
      old_id = this.active_id;
      position = options.contentWidth;
      $type = this.$type;
      $choices = this.$choices;
      $preview = this.$preview;
      $limit = this.$limit;
      if (old_id === active_id) {
        return false;
      } else if (active_id < old_id) {
        position *= -1;
      }
      $old = this.contens[old_id];
      $next = this.contens[active_id];
      $next.css({
        position: 'absolute',
        display: 'block',
        left: position,
        top: 0
      });
      if (old_id === 0) {
        $type.hide();
      } else if ((old_id === 2 && active_id !== 3) || (old_id === 3 && active_id !== 2)) {
        $choices.hide();
        $limit.hide();
      }
      return $preview.css('height', $next.height()).animate({
        left: -position
      }, options.speed, (function(_this) {
        return function(event) {
          $old.hide();
          $next.show().removeAttr('style');
          $preview.removeAttr('style');
          _this._set_active_id(active_id);
          if (active_id === 0) {
            return $type.slideDown(100);
          } else if ((active_id === 2 && old_id !== 3) || (active_id === 3 && old_id !== 2)) {
            $choices.slideDown(100);
            return $limit.slideDown(100);
          }
        };
      })(this));
    };

    preview.prototype.toggle_preview_limit = function(value) {
      this.$preview.find(this.options.choicePrevArea).each((function(_this) {
        return function(type, choice_preview_area) {
          var $preview_area;
          $preview_area = jQuery(choice_preview_area);
          return $preview_area.find(_this.options.choicePrevSection).each(function(j, element) {
            var $preview_limit_section;
            $preview_limit_section = $(element).find(_this.options.choicePrevLimitSection);
            if (value) {
              return $preview_limit_section.show(100);
            } else {
              return $preview_limit_section.hide(100);
            }
          });
        };
      })(this));
      return this;
    };

    return preview;

  })(Backbone.View);

  App.pages.admin.work.questionnaire.question.elements.form.views.choice = (function(_super) {
    __extends(choice, _super);

    function choice() {
      this._dispose = __bind(this._dispose, this);
      this._remove = __bind(this._remove, this);
      this._change_value = __bind(this._change_value, this);
      this._setting = __bind(this._setting, this);
      this._render = __bind(this._render, this);
      this.initialize = __bind(this.initialize, this);
      return choice.__super__.constructor.apply(this, arguments);
    }

    choice.prototype.model = App.pages.admin.work.questionnaire.question.elements.form.models.choice;

    choice.prototype.$label = null;

    choice.prototype.option = {
      el: '<p class="choiceView"></p>',
      inputType: 0,
      label: '.labelName',
      defaultText: '選択肢を入力してください'
    };

    choice.prototype.template = _.template('<label class="bullet">\n  <input type="<%= type %>" disabled="disabled">\n  <span class="labelName"><%= text %></span>\n  <span class="choiceLimitView" style="display: none;">\n    (<input class="" type="number" min="0" name="<%= type %>_choice_limit_num[]">人まで選択可能。)\n  </span>\n</label>');

    choice.prototype.initialize = function(arg) {
      if (arg && arg.option && typeof arg.option === 'object') {
        this.option = jQuery.extend({}, this.option, arg.option);
      }
      this.inputType = this.option.inputType ? 'checkbox' : 'radio';
      if (!this.$el.context) {
        this._render();
      }
      this._setting();
      this.listenTo(this.model, 'change:value', this._change_value);
      return this.listenTo(this.model, 'destroy', this._remove);
    };

    choice.prototype._render = function() {
      var $el;
      $el = jQuery(this.option.el).append(this.template({
        type: this.inputType,
        text: this.option.defaultText
      }));
      this.setElement($el);
      return $el;
    };

    choice.prototype._setting = function() {
      this.$label = this.$el.find(this.option.label);
      return this;
    };

    choice.prototype._change_value = function(model) {
      var value;
      value = model.get('value');
      if (!value) {
        value = this.option.defaultText;
      }
      this.$el.find(this.option.label).text(value);
      return this;
    };

    choice.prototype._remove = function(model) {
      return this.$el.slideUp(300, (function(_this) {
        return function(event) {
          return _this.remove();
        };
      })(this));
    };

    choice.prototype._dispose = function() {};

    return choice;

  })(Backbone.View);

  App.pages.admin.work.questionnaire.question.elements.form.views.choice_input = (function(_super) {
    __extends(choice_input, _super);

    function choice_input() {
      this.get_value = __bind(this.get_value, this);
      this._dispose = __bind(this._dispose, this);
      this._setting = __bind(this._setting, this);
      this._render = __bind(this._render, this);
      return choice_input.__super__.constructor.apply(this, arguments);
    }

    choice_input.prototype.option = {
      el: '<p class="content choiceInputSection"></p>',
      field: 'input.choiceLabelInput',
      deleteBtn: '.js-deleteBtn'
    };

    choice_input.prototype.template = _.template('<span class="addItem">\n  <input type="text" class="choiceLabelInput" name="question_choices[]" size="25">\n  <input type="hidden" name="choice_ids[]" value="">\n  <a class="uiAnchor js-deleteBtn" href=""><i class="icon-xmark"></i></a>\n</span>');

    choice_input.prototype._render = function() {
      var $el, id;
      id = this.model.get('index');
      $el = jQuery(this.option.el).append(this.template({
        id: id
      }));
      this.setElement($el);
      return this;
    };

    choice_input.prototype._setting = function() {
      var $btn, $el, $input;
      $el = this.$el;
      $input = this.$field = this.$el.find(this.option.field);
      $btn = this.$el.find(this.option.deleteBtn);
      $input.on('change', (function(_this) {
        return function(event) {
          return _this.model.set({
            'value': event.target.value
          });
        };
      })(this));
      $btn.on('click', (function(_this) {
        return function(event) {
          _this.trigger('delete', _this);
          return false;
        };
      })(this));
      this.model.inputView = this;
      return this;
    };

    choice_input.prototype._dispose = function() {
      return this.model.destroy({
        success: (function(_this) {
          return function(model) {};
        })(this),
        error: (function(_this) {
          return function(model) {
            return false;
          };
        })(this)
      });
    };

    choice_input.prototype.get_value = function() {
      return this.$field.val();
    };

    return choice_input;

  })(App.pages.admin.work.questionnaire.question.elements.form.views.choice);

  add_route('/admin/work/questionnaire/question/(?:create|edit)/?.*', function() {
    var $form;
    new App.pages.admin.work.questionnaire.question.elements.form.views.form();
    $form = $('form');
    return $form.one('submit', (function(_this) {
      return function() {
        $form.on('submit', function() {
          return false;
        });
        return true;
      };
    })(this));
  });

  App.pages.admin.work.questionnaire.show = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.work.questionnaire.show.config.messages = {
    stop_work: {
      title: 'アンケートの受付終了'
    },
    delete_work: {
      title: 'アンケートの削除'
    },
    remind: {
      title: '(催促)',
      message: '以下の対象者に対して催促します',
      button_label: '催促する',
      not_select: 'ステータスが「未完了」の対象者が選択されていません。',
      placeholder_text: 'この内容は、催促する対象者のみ確認することができます。'
    },
    reset_unfinished: {
      title: '(未完了に戻す)',
      message: '下記の対象者のアンケートを未完了に戻します。',
      button_label: '未完了に戻す',
      not_select: 'ステータスが「完了」の対象者が選択されていません。'
    },
    finish: {
      title: '(完了にする)',
      message: '下記の対象者のアンケートを完了します。',
      button_label: '完了にする',
      not_select: 'ステータスが「未完了」の対象者が選択されていません。'
    },
    assign: {
      stop: {
        title: '(受付終了)',
        body: 'このアンケートの受付を終了します。受付を終了すると、対象者はアンケートの内容確認はできますが、回答はできなくなります。',
        button: '受付を終了する'
      },
      reopen: {
        title: '(受付再開)',
        body: 'この提出物の受付を再開します。受付を再開すると、未完了の対象者は提出ができるようになります。',
        button: '受付を再開する'
      },
      "delete": {
        title: '(期限削除)',
        body: '<div class="msgBanner notice"><p><span class="icon"><i class="icon-exclamation"></i></span><span class="txt">削除を実行しようとしています。注意文をご確認ください。</span></p></div>このアンケートの期限を削除します。削除すると、タイトル、説明文、添付ファイル、質問、対象者、回答など、このアンケートの期限に関連したすべての情報が削除されます。削除された情報は元に戻すことができません。',
        button: '削除する(元に戻せません)'
      }
    }
  };

  App.pages.admin.work.questionnaire.show.views.show = (function(_super) {
    __extends(show, _super);

    function show() {
      return show.__super__.constructor.apply(this, arguments);
    }

    show.prototype.messages = App.pages.admin.work.questionnaire.show.config.messages;

    return show;

  })(App.pages.admin.work.driver.show.views.show);

  add_route('/admin/work/questionnaire/show/?.*', function() {
    return new App.pages.admin.work.questionnaire.show.views.show({
      work_key: 'questionnaire',
      base_url: "" + App.base_url + "/admin/work/questionnaire",
      api_base_url: "" + App.base_url + "/api/admin/work/questionnaire"
    });
  });

  App.pages.admin.work.questionnaire.summary = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.work.questionnaire.summary.models.QuestionnairUserAnswer = (function(_super) {
    __extends(QuestionnairUserAnswer, _super);

    function QuestionnairUserAnswer() {
      return QuestionnairUserAnswer.__super__.constructor.apply(this, arguments);
    }

    QuestionnairUserAnswer.prototype.defaults = {
      id: null,
      full_name: null,
      icon_url: null,
      permalink: null
    };

    return QuestionnairUserAnswer;

  })(Backbone.Model);

  App.pages.admin.work.questionnaire.summary.collections.QuestionnairUserAnswer = (function(_super) {
    __extends(QuestionnairUserAnswer, _super);

    function QuestionnairUserAnswer() {
      this.find_by = __bind(this.find_by, this);
      return QuestionnairUserAnswer.__super__.constructor.apply(this, arguments);
    }

    QuestionnairUserAnswer.prototype.model = App.pages.admin.work.questionnaire.summary.models.QuestionnairUserAnswer;

    QuestionnairUserAnswer.prototype.url = "" + App.base_url + "/api/admin/work/questionnaire/summary/choice";

    QuestionnairUserAnswer.prototype.parse = function(res, xhr) {
      return res.data;
    };

    QuestionnairUserAnswer.prototype.find_by = function(id) {
      return this.fetch({
        reset: true,
        url: [this.url, id].join('/')
      });
    };

    return QuestionnairUserAnswer;

  })(Backbone.Collection);

  App.pages.admin.work.questionnaire.summary.views.show = (function(_super) {
    __extends(show, _super);

    function show() {
      return show.__super__.constructor.apply(this, arguments);
    }

    show.prototype.el = '#js-admin-work-questionnaire-summary-content';

    show.prototype.balloon = new App.elements.MemberBalloon({
      placement: 'right'
    });

    show.prototype.events = {
      'click .omitBox': 'on_omit'
    };

    show.prototype.exclude_count = 3;

    show.prototype.initialize = function() {
      var url2link_class;
      this.collections = new App.pages.admin.work.questionnaire.summary.collections.QuestionnairUserAnswer();
      this.listenTo(this.collections, 'reset', this.render);
      this.listenTo(this.collections, 'error', this.render_error);
      url2link_class = new App.elements.url2link.views.url2link();
      $('td.js-question-type-textarea').each((function(_this) {
        return function(index, element) {
          return $(element).html(url2link_class.done_url2link($(element).html()));
        };
      })(this));
      return url2link_class.renew_observe_click_url2link();
    };

    show.prototype.on_omit = function(evt) {
      var _el;
      _el = $(evt.target);
      this.balloon.setElement(_el);
      this.balloon.resetData();
      return this.collections.find_by(_el.attr('id').replace(/choice_id\-/, ''));
    };

    show.prototype.render = function() {
      _(this.collections.filter((function(_this) {
        return function(model) {
          return model.get('idx') > _this.exclude_count;
        };
      })(this))).forEach((function(_this) {
        return function(model) {
          return _this.balloon.addData(model);
        };
      })(this));
      this.balloon.show();
      return this;
    };

    show.prototype.render_error = function(err, xhr, options) {};

    return show;

  })(Backbone.View);

  add_route('/admin/work/questionnaire/summary/?.*', function() {
    return new App.pages.admin.work.questionnaire.summary.views.show({
      work_key: 'questionnaire',
      base_url: "" + App.base_url + "/admin/work/questionnaire",
      api_base_url: "" + App.base_url + "/api/admin/work/questionnaire"
    });
  });

  App.pages.admin.work.questionnaire.user = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.work.questionnaire.user.answer = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.work.questionnaire.user.answer.views.answer = (function(_super) {
    __extends(answer, _super);

    function answer() {
      return answer.__super__.constructor.apply(this, arguments);
    }

    answer.prototype.el = null;

    answer.prototype.events = {
      'click .omitBox': 'on_omit'
    };

    answer.prototype.initialize = function() {
      var url2link_class;
      url2link_class = new App.elements.url2link.views.url2link();
      $('.unitContent .wysiwygContent').each((function(_this) {
        return function(index, element) {
          return $(element).html(url2link_class.done_url2link($(element).html()));
        };
      })(this));
      return url2link_class.renew_observe_click_url2link();
    };

    return answer;

  })(Backbone.View);

  add_route('/admin/work/questionnaire/user/answer/?.*', function() {
    return new App.pages.admin.work.questionnaire.user.answer.views.answer();
  });

  App.pages.admin.work.task = {};

  add_route('/admin/work/task/(?:create|edit|copy)/?.*', function() {
    return new App.pages.admin.work.driver.elements.form.views.form();
  });

  add_route('/admin/work/task(/index)?/?', function() {
    return new App.pages.admin.work.driver.index.views.index({
      base_url: "" + App.base_url + "/admin/work/task",
      api_base_url: "" + App.base_url + "/api/admin/work/task",
      optional_template: _.template('<div class="boxLabeled">\n  <p class="label">提出方法</p>\n  <p class="content"><%= item.send_type %></p>\n</div>'),
      show_name: '提出物'
    });
  });

  App.pages.admin.work.task.show = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.admin.work.task.show.templates.answer_modal = _.template('<div class="boxMailEdit">\n  <div class="content">\n    <pre class="preview"><%= data.message %></pre>\n  </div>\n</div>', void 0, {
    variable: 'data'
  });

  App.pages.admin.work.task.show.config.available_statuses = {
    remind: ['unfinished_unread', 'unfinished_read'],
    reintroduction: ['pending_approval'],
    finish: ['unfinished_unread', 'unfinished_read', 'pending_approval'],
    reset_pending: ['finished']
  };

  App.pages.admin.work.task.show.config.messages = {
    stop_work: {
      title: '提出物の受付終了'
    },
    delete_work: {
      title: '提出物の削除'
    },
    remind: {
      title: '(催促)',
      message: '下記の対象者に催促します。',
      button_label: '催促する',
      not_select: 'ステータスが「未完了」の対象者が選択されていません。',
      placeholder_text: 'この内容は、催促する対象者のみ確認することができます。'
    },
    reset_unfinished: {
      title: '提出物提出ステータスの変更',
      message: '以下のユーザーの提出ステータスを未完了に変更します',
      button_label: '未完了にする',
      not_select: 'ステータスが「完了」の対象者が選択されていません。'
    },
    finish: {
      title: '提出物提出ステータスの変更',
      message: '以下のユーザーの回答ステータスを完了に変更します',
      button_label: '完了にする',
      not_select: '未完了のユーザーが1人も選ばれていません。いずれか1人を選んでください。'
    },
    approve: {
      title: '(承認)',
      message: '下記の対象者の提出物を承認します。',
      button_label: '承認する',
      placeholder_text: 'この内容は、承認する対象者のみ確認することができます。',
      not_select: 'ステータスが「完了」の対象者が選択されていません。'
    },
    reset_pending: {
      title: '(承認待ちに戻す)',
      message: '下記の対象者の提出物を承認待ちに戻します。',
      button_label: '承認待ちに戻す',
      not_select: 'ステータスが「完了」の対象者が選択されていません。',
      placeholder_text: ''
    },
    reintroduction: {
      title: '(再提出を依頼)',
      message: '下記の対象者に再提出を依頼します。',
      button_label: '再提出を依頼する',
      not_select: 'ステータスが「承認待ち」の対象者が選択されていません。'
    },
    assign: {
      stop: {
        title: '(受付終了)',
        body: 'この提出物の受付を終了します。受付を終了すると、対象者は課題情報の確認はできますが、ファイルやコメントの提出ができなくなります。',
        button: '受付を終了する'
      },
      reopen: {
        title: '(受付再開)',
        body: 'この提出物の受付を再開します。受付を再開すると、未完了の対象者は提出ができるようになります。',
        button: '受付を再開する'
      },
      "delete": {
        title: '(期限削除)',
        body: '<div class="msgBanner notice"><p><span class="icon"><i class="icon-exclamation"></i></span><span class="txt">削除を実行しようとしています。注意文をご確認ください。</span></p></div>この提出物の期限を削除します。削除すると、対象者、提出されたファイルなど、この提出物の期限に関連したすべての情報が削除されます。削除された情報は元に戻すことができません。',
        button: '削除する(元に戻せません)'
      }
    }
  };

  App.pages.admin.work.task.show.views.answer = (function(_super) {
    __extends(answer, _super);

    function answer() {
      this.open_modal = __bind(this.open_modal, this);
      this.initialize = __bind(this.initialize, this);
      return answer.__super__.constructor.apply(this, arguments);
    }

    answer.prototype.el = null;

    answer.prototype.events = {
      'click .js-admin-work-task-answer-show-message-detail-link': 'open_modal'
    };

    answer.prototype.modal = null;

    answer.prototype.message = null;

    answer.prototype.initialize = function() {
      var data;
      data = getEmbeddedJSON(this.$el.find('.js-admin-work-task-answer-show-message-detail-data'));
      if ((data != null ? data.message : void 0) != null) {
        this.message = data.message;
      }
      if (!this.message) {
        return this.$el.find('.js-admin-work-task-answer-show-message-detail-link').remove();
      }
    };

    answer.prototype.open_modal = function() {
      if (!this.message) {
        return false;
      }
      this.url2link_class = new App.elements.url2link.views.url2link();
      if (this.modal == null) {
        this.modal = new App.elements.modal();
        this.modal.set_title('提出内容');
        this.message = this.url2link_class.done_url2link(this.message);
        this.modal.set_body(App.pages.admin.work.task.show.templates.answer_modal({
          message: this.message
        }));
        this.modal.set_footer_cancel_only('閉じる');
        this.modal.set_height_high_class();
      }
      this.modal.show();
      this.url2link_class.renew_observe_click_url2link();
      return false;
    };

    return answer;

  })(Backbone.View);

  App.pages.admin.work.task.show.views.show = (function(_super) {
    __extends(show, _super);

    function show() {
      return show.__super__.constructor.apply(this, arguments);
    }

    show.prototype.messages = App.pages.admin.work.task.show.config.messages;

    show.prototype.available_statuses = App.pages.admin.work.task.show.config.available_statuses;

    return show;

  })(App.pages.admin.work.driver.show.views.show);

  add_route('/admin/work/task/show/?.*', function() {
    new App.pages.admin.work.task.show.views.show({
      work_key: 'task',
      base_url: "" + App.base_url + "/admin/work/task",
      api_base_url: "" + App.base_url + "/api/admin/work/task",
      finish_messages_key: 'approve',
      unfinish_messages_key: 'reintroduction'
    });
    return $('.js-admin-work-task-answer-show-message-detail').each((function(_this) {
      return function(index, element) {
        return new App.pages.admin.work.task.show.views.answer({
          el: element
        });
      };
    })(this));
  });

  App.pages.auth = {};

  App.pages.auth.login = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.auth.login.views.login = (function(_super) {
    __extends(login, _super);

    function login() {
      this.check_keydown = __bind(this.check_keydown, this);
      this.move = __bind(this.move, this);
      this.reissue = __bind(this.reissue, this);
      this.login = __bind(this.login, this);
      this.initialize = __bind(this.initialize, this);
      return login.__super__.constructor.apply(this, arguments);
    }

    login.prototype.el = '.js-auth-login-container';

    login.prototype.width = 0;

    login.prototype.$login_email = null;

    login.prototype.$reissue_email = null;

    login.prototype.to = 'reissue';

    login.prototype.router = null;

    login.prototype.initialize = function() {
      this.$login_email = this.$el.find('input[name="email"]');
      this.$reissue_email = this.$el.find('input[name="email_reissue"]');
      this.width = this.$el.parent().width();
      App.$document.on('keydown', this.check_keydown);
      App.functions.disable_form_multiple_submits(this.$el.find('form'));
      this.router = new App.pages.auth.login.routes.login();
      this.router.on('route:login', this.login);
      this.router.on('route:reissue', this.reissue);
      return Backbone.history.start();
    };

    login.prototype.login = function() {
      return this.move(0, (function(_this) {
        return function() {
          _this.$login_email.focus();
          return _this.to = 'reissue';
        };
      })(this));
    };

    login.prototype.reissue = function() {
      return this.move(-this.width, (function(_this) {
        return function() {
          _this.$reissue_email.focus();
          return _this.to = 'login';
        };
      })(this));
    };

    login.prototype.move = function(left, cb) {
      if (cb == null) {
        cb = null;
      }
      if (typeof cb !== 'function') {
        return this.$el.stop().animate({
          left: left
        }, 'swing');
      } else {
        return this.$el.stop().animate({
          left: left
        }, 'swing', cb);
      }
    };

    login.prototype.check_keydown = function(event) {
      if (event.keyCode != null) {
        if (event.keyCode === 9 && $(':focus').hasClass('js-auth-login-container-move-link')) {
          if (this.to === 'reissue' && !event.shiftKey) {
            this.router.navigate('reissue', {
              trigger: true
            });
            return false;
          } else if (this.to === 'login' && event.shiftKey) {
            this.router.navigate('login', {
              trigger: true
            });
            return false;
          }
        } else if (event.keyCode === 9 && $(':focus').attr('id') === 'input-email' && event.shiftKey) {
          return false;
        } else if (event.keyCode === 9 && $(':focus').attr('id') === 'input-password' && !event.shiftKey) {
          $('input[name="remember_me"]').focus();
          return false;
        } else if (event.keyCode === 9 && $(':focus').attr('name') === 'remember_me' && !event.shiftKey) {
          $('button[value="login"]').focus();
          return false;
        } else if (event.keyCode === 9 && $(':focus').attr('value') === 'login' && !event.shiftKey) {
          $('.js-auth-login-container-move-link:first').focus();
          return false;
        } else if (event.keyCode === 9 && $(':focus').attr('name') === 'email_reissue' && event.shiftKey) {
          $('.js-auth-login-container-move-link:last').focus();
          return false;
        }
      }
    };

    return login;

  })(Backbone.View);

  App.pages.auth.login.routes.login = (function(_super) {
    __extends(login, _super);

    function login() {
      return login.__super__.constructor.apply(this, arguments);
    }

    login.prototype.routes = {
      'login': 'login',
      'reissue': 'reissue',
      '.*': 'login'
    };

    return login;

  })(Backbone.Router);

  add_route('/auth/login/?', function() {
    return new App.pages.auth.login.views.login();
  });

  App.pages.calendar = {
    templates: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.calendar.events.show = _.extend({
    modal: {},
    init: function(options) {},
    set_modal: function(name, modal) {
      return this.modal[name] = modal;
    },
    show_edit_modal: function(data) {
      this.url = (data != null ? data.schedule_id : void 0) != null ? "" + App.base_url + "/calendar/edit" : "" + App.base_url + "/calendar/create";
      return this.modal['edit'].show(data);
    },
    show_create_modal: function(data) {
      this.url = "" + App.base_url + "/calendar/create";
      return this.modal['create'].show(data);
    },
    show_delete_modal: function(data) {
      this.url = "" + App.base_url + "/calendar/delete";
      return this.modal['delete'].show(data);
    }
  }, Backbone.Events);

  App.pages.calendar.views.show = (function(_super) {
    __extends(show, _super);

    function show() {
      this._embed_to_form = __bind(this._embed_to_form, this);
      this["delete"] = __bind(this["delete"], this);
      this.update = __bind(this.update, this);
      this.create = __bind(this.create, this);
      this.initialize = __bind(this.initialize, this);
      return show.__super__.constructor.apply(this, arguments);
    }

    show.prototype.el = '#main';

    show.prototype.initialize = function() {
      var config, max_date, min_date, url2link_class;
      config = getEmbeddedJSON($('#js-embedded-config'));
      min_date = (config != null ? config.min_date : void 0) != null ? config.min_date : null;
      max_date = (config != null ? config.max_date : void 0) != null ? config.max_date : null;
      App.event.global_balloon_mediator.init();
      App.event.global_balloon_mediator.register_document_clicked();
      App.pages.calendar.events.show.init();
      this.headerView = new App.pages.calendar.views.show.header({
        el: '#js-heading',
        config: config
      });
      this.calendarView = new App.pages.calendar.views.show.calendar({
        el: '#js-calendar',
        config: config
      });
      this.listView = new App.pages.calendar.views.show.schedule_list({
        el: '#js-schedule_list',
        config: config
      });
      this.$edit_form = $('#js-scheduleEditForm');
      App.functions.disable_form_multiple_submits(this.$edit_form);
      this.$delete_form = $('#js-scheduleDeleteForm');
      App.functions.disable_form_multiple_submits(this.$delete_form);
      this.create_modal = new App.elements.schedule_modal.views.user({
        title: '長期不在の登録',
        decide_button_label: '登録する',
        buttons_type: 'cancel_decide',
        config: {
          minDate: min_date != null ? new Date(min_date[0], min_date[1], min_date[2]) : new Date(),
          maxDate: max_date != null ? new Date(max_date[0], max_date[1], max_date[2]) : new Date()
        }
      });
      this.listenTo(this.create_modal, 'decide', this.create);
      App.pages.calendar.events.show.set_modal('edit', this.edit_modal);
      App.pages.calendar.events.show.set_modal('create', this.create_modal);
      this.edit_modal = new App.elements.schedule_modal.views.user({
        title: '長期不在の編集',
        decide_button_label: '編集する',
        buttons_type: 'cancel_decide',
        config: {
          minDate: min_date != null ? new Date(min_date[0], min_date[1], min_date[2]) : new Date(),
          maxDate: max_date != null ? new Date(max_date[0], max_date[1], max_date[2]) : new Date()
        }
      });
      this.listenTo(this.edit_modal, 'decide', this.update);
      App.pages.calendar.events.show.set_modal('edit', this.edit_modal);
      this.delete_modal = new App.elements.schedule_modal.views["delete"]({
        title: '長期不在(削除)',
        body: '<div class="msgBanner notice"><p><span class="icon"><i class="icon-exclamation"></i></span><span class="txt">削除を実行しようとしています。注意文をご確認ください。</span></p></div>この長期不在を削除します。削除すると、タイトル、不在期間など、この長期不在に関連したすべての情報が削除されます。削除された情報は、元に戻すことができません。<br /><br />よろしいですか？',
        decide_button_label: '削除する(元に戻せません)',
        cancel_button_label: '取り消す',
        buttons_type: 'cancel_delete'
      });
      this.listenTo(this.delete_modal, 'decide', this["delete"]);
      App.pages.calendar.events.show.set_modal('delete', this.delete_modal);
      url2link_class = new App.elements.url2link.views.url2link();
      $('tr.js-schedule td.desc p.label').each((function(_this) {
        return function(index, element) {
          return $(element).html(url2link_class.done_url2link($(element).html()));
        };
      })(this));
      return url2link_class.renew_observe_click_url2link();
    };

    show.prototype.create = function() {
      this._embed_to_form(this.create_modal, this.$edit_form, ['schedule_id', 'type', 'title', 'start_date', 'end_date', 'content']);
      return this.$edit_form.attr('action', App.pages.calendar.events.show.url).submit();
    };

    show.prototype.update = function() {
      this._embed_to_form(this.edit_modal, this.$edit_form, ['schedule_id', 'type', 'title', 'start_date', 'end_date', 'content']);
      return this.$edit_form.attr('action', App.pages.calendar.events.show.url).submit();
    };

    show.prototype["delete"] = function() {
      this._embed_to_form(this.delete_modal, this.$delete_form, ['schedule_id']);
      return this.$delete_form.attr('action', App.pages.calendar.events.show.url).submit();
    };

    show.prototype._embed_to_form = function(modal, $form, keys) {
      return _.each(keys, (function(_this) {
        return function(name) {
          return $form.append('<input type="hidden" name="' + name + '" value="' + _.escape(modal.get_by_name(name)) + '">');
        };
      })(this));
    };

    return show;

  })(Backbone.View);

  App.pages.calendar.views.show.header = (function(_super) {
    __extends(header, _super);

    function header() {
      this.show_modal = __bind(this.show_modal, this);
      this.initialize = __bind(this.initialize, this);
      return header.__super__.constructor.apply(this, arguments);
    }

    header.prototype.events = {
      'click #js-header-demandBtn': 'show_modal'
    };

    header.prototype.initialize = function(options) {
      return this.config = (options != null ? options.config : void 0) != null ? options.config : {};
    };

    header.prototype.show_modal = function(ev) {
      ev.preventDefault();
      App.pages.calendar.events.show.show_create_modal({
        type: this.config.type
      });
      return false;
    };

    return header;

  })(Backbone.View);

  App.pages.calendar.views.show.calendar = (function(_super) {
    __extends(calendar, _super);

    function calendar() {
      this.initialize = __bind(this.initialize, this);
      return calendar.__super__.constructor.apply(this, arguments);
    }

    calendar.prototype.schedules = [];

    calendar.prototype.initialize = function(options) {
      this.config = (options != null ? options.config : void 0) != null ? options.config : {};
      return this.$el.find('.js-schedule').each((function(_this) {
        return function(idx, _el) {
          var $el;
          $el = $(_el);
          return _this.schedules.push(new App.pages.calendar.views.show.calendar.schedule({
            el: $el.context,
            config: _this.config
          }));
        };
      })(this));
    };

    return calendar;

  })(Backbone.View);

  App.pages.calendar.views.show.schedule_list = (function(_super) {
    __extends(schedule_list, _super);

    function schedule_list() {
      this.show_modal = __bind(this.show_modal, this);
      this.initialize = __bind(this.initialize, this);
      return schedule_list.__super__.constructor.apply(this, arguments);
    }

    schedule_list.prototype.events = {
      'click #js-schedule_list-demandBtn': 'show_modal'
    };

    schedule_list.prototype.schedules = [];

    schedule_list.prototype.initialize = function(options) {
      this.config = (options != null ? options.config : void 0) != null ? options.config : {};
      return this.$el.find('.js-schedule').each((function(_this) {
        return function(idx, _el) {
          var $el;
          $el = $(_el);
          return _this.schedules.push(new App.pages.calendar.views.show.schedule_list.schedule({
            el: $el.context,
            config: _this.config
          }));
        };
      })(this));
    };

    schedule_list.prototype.show_modal = function(ev) {
      ev.preventDefault();
      App.pages.calendar.events.show.show_create_modal({
        type: this.config.type
      });
      return false;
    };

    return schedule_list;

  })(Backbone.View);

  App.pages.calendar.views.show.calendar.schedule = (function(_super) {
    __extends(schedule, _super);

    function schedule() {
      this.click_anchor = __bind(this.click_anchor, this);
      this.initialize = __bind(this.initialize, this);
      return schedule.__super__.constructor.apply(this, arguments);
    }

    schedule.prototype.events = {
      'click .js-moreEvent': 'click_anchor'
    };

    schedule.prototype.initialize = function(options) {
      var _balloon;
      _balloon = this.$el.find('.blnScheduleList');
      return this.$balloon = new App.elements.schedule_balloon.views.schedule_balloon({
        el: $(_balloon)[0]
      });
    };

    schedule.prototype.click_anchor = function(ev) {
      ev.preventDefault();
      App.event.global_balloon_mediator.trigger('register', this.$balloon);
      return false;
    };

    return schedule;

  })(Backbone.View);

  App.pages.calendar.views.show.schedule_list.schedule = (function(_super) {
    __extends(schedule, _super);

    function schedule() {
      this.click_delete = __bind(this.click_delete, this);
      this.click_edit = __bind(this.click_edit, this);
      this.click_icon = __bind(this.click_icon, this);
      this.initialize = __bind(this.initialize, this);
      return schedule.__super__.constructor.apply(this, arguments);
    }

    schedule.prototype.events = {
      'click .setting .uiAnchor': 'click_icon',
      'click .js-editBtn': 'click_edit',
      'click .js-deleteBtn': 'click_delete'
    };

    schedule.prototype.initialize = function(options) {
      this.config = (options != null ? options.config : void 0) != null ? options.config : {};
      this.$balloon = this.$el.find('.blnOption');
      return this.schedule = getEmbeddedJSON(this.$el.find('.js-embedded-schedule'));
    };

    schedule.prototype.click_icon = function(ev) {
      ev.preventDefault();
      App.event.global_balloon_mediator.trigger('register', this.$balloon);
      return false;
    };

    schedule.prototype.click_edit = function(ev) {
      ev.preventDefault();
      this.$balloon.hide();
      App.pages.calendar.events.show.show_edit_modal(this.schedule);
      return false;
    };

    schedule.prototype.click_delete = function(ev) {
      ev.preventDefault();
      this.$balloon.hide();
      App.pages.calendar.events.show.show_delete_modal(this.schedule);
      return false;
    };

    return schedule;

  })(Backbone.View);

  add_route('/calendar/show(/([0-9]{6})*)*?', (function(_this) {
    return function() {
      return new App.pages.calendar.views.show();
    };
  })(this));

  add_route('((?!/auth/?).)*', function() {
    $('a.js-show-more-on-content-link').each(function(index, element) {
      return new App.elements.show_more.views.content_body_show_more({
        el: element
      });
    });
    $('a.js-show-more-on-modal-link').each(function(index, element) {
      return new App.elements.show_more.views.content_body_show_more_modal({
        el: element
      });
    });
    return new App.elements.url2link.views.url2link();
  });

  App.pages.communication = {};

  App.pages.communication.show = {};

  add_route('/communication/profile/?$|^/communication/(profile/)?[0-9]+/?', function() {
    var profile_id;
    profile_id = getEmbeddedJSON($('.js-communication-profile-data-json')).profile_id;
    App.elements.timeline.config.api_base_url = "" + App.base_url + "/api/communication/" + profile_id;
    App.elements.timeline.config.show_related = true;
    return new App.elements.timeline.views.timeline({
      show_communication_link_mores: false
    });
  });

  App.pages.community = {};

  App.pages.community.elements = {};

  App.pages.community.elements.form = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.community.elements.form.templates.user_select = {
    selected_user: _.template('<td>\n  <p><%- model.get(\'name\') %></p>\n  <input type="hidden" name="<%= name %>" value="<%= model.get(\'id\') %>">\n</td>\n<td>\n  <% if(model.get(\'is_owner\') || model.get(\'is_manager\')){ %>\n  <p>主催者</p>\n  <% }else{ %>\n  <p>一般ユーザー</p>\n  <% } %>\n</td>\n<td>\n  <% if(model.get(\'is_manager\')){ %>\n    <input type="hidden" name="user_types[<%= model.get(\'id\') %>]" value="1">\n  <% }else{ %>\n  <p>\n    主催者に\n    <span class="choice">\n      <label>\n        <input type="radio" name="user_types[<%= model.get(\'id\') %>]" value="1"<% if(model.get(\'is_owner\')){ %> checked="checked"<% } %>>する\n      </label>\n      <label>\n        <input type="radio" name="user_types[<%= model.get(\'id\') %>]" value="0"<% if(!model.get(\'is_owner\')){ %> checked="checked"<% } %>>しない\n      </label>\n      <span></span>\n    </span>\n  </p>\n  <% } %>\n</td>\n<td class="delete">\n  <p>\n    <a class="uiAnchor" href="#">\n      <i class="icon-garbage js-community-elements-form-user-select-selected-user-delete"></i>\n    </a>\n  </p>\n</td>')
  };

  App.pages.community.elements.form.views.selected_user = (function(_super) {
    __extends(selected_user, _super);

    function selected_user() {
      return selected_user.__super__.constructor.apply(this, arguments);
    }

    selected_user.prototype.el = '<tr></tr>';

    selected_user.prototype.events = {
      'click .js-community-elements-form-user-select-selected-user-delete': 'remove_self'
    };

    selected_user.prototype.template = App.pages.community.elements.form.templates.user_select.selected_user;

    return selected_user;

  })(App.elements.user_select.views.selected_user);

  App.pages.community.elements.form.views.user_select = (function(_super) {
    __extends(user_select, _super);

    function user_select() {
      this.add_user_row = __bind(this.add_user_row, this);
      this.render_all_selected_users = __bind(this.render_all_selected_users, this);
      this.render_selected_users = __bind(this.render_selected_users, this);
      this.initialize = __bind(this.initialize, this);
      return user_select.__super__.constructor.apply(this, arguments);
    }

    user_select.INITIAL_MAX_SHOW = 20;

    user_select.prototype.el = '.js-community-elements-form-user-select';

    user_select.prototype.events = {
      'click .js-community-elements-form-user-select-launch-button': 'launch'
    };

    user_select.prototype.$selected_container = null;

    user_select.prototype.$tbody = null;

    user_select.prototype.owner_ids = [];

    user_select.prototype.manager_ids = [];

    user_select.prototype.show_more = null;

    user_select.prototype.show_ids = [];

    user_select.prototype.initialize = function() {
      var owners;
      user_select.__super__.initialize.apply(this, arguments);
      this.$selected_container = this.$el.find('.js-community-elements-form-user-select-list-selected-container');
      this.$tbody = this.$selected_container.find('tbody');
      owners = getEmbeddedJSON(this.$el.find('.js-community-elements-form-user-select-community-owners'));
      if ((owners != null ? owners.owners : void 0) != null) {
        this.owner_ids = owners.owners;
      }
      if ((owners != null ? owners.managers : void 0) != null) {
        this.manager_ids = owners.managers;
      }
      this.show_more = new App.elements.show_more.views.show_more({
        label: 'もっと見る'
      });
      this.show_more.hide();
      this.show_more.on('clicked', this.render_all_selected_users);
      return this.$selected_container.after(this.show_more.$el);
    };

    user_select.prototype.render_selected_users = function() {
      this.$tbody.find('tr').not('.js-community-elements-form-users-list-container-creator-row').remove();
      this.show_ids = [];
      if (this.selected_collection.length > App.pages.community.elements.form.views.user_select.INITIAL_MAX_SHOW) {
        this.show_more.show();
      } else {
        this.show_more.hide();
      }
      return this.selected_collection.each((function(_this) {
        return function(user, index) {
          return _this.add_user_row(user, index >= App.pages.community.elements.form.views.user_select.INITIAL_MAX_SHOW);
        };
      })(this));
    };

    user_select.prototype.render_all_selected_users = function() {
      this.$tbody.find('tr').show();
      return this.show_more.hide();
    };

    user_select.prototype.add_user_row = function(user, hidden) {
      var $user_row, user_id, user_view;
      if (hidden == null) {
        hidden = false;
      }
      user_id = parseInt(user.get('id'), 10);
      if (_.indexOf(this.show_ids, user_id) >= 0) {
        return true;
      }
      user.set('is_owner', _.indexOf(this.owner_ids, user_id) >= 0);
      user.set('is_manager', _.indexOf(this.manager_ids, user_id) >= 0);
      user_view = new App.pages.community.elements.form.views.selected_user({
        model: user,
        name: this.name_attribute
      });
      $user_row = user_view.render();
      if (hidden) {
        $user_row.hide();
      }
      this.$tbody.append($user_row);
      return this.show_ids.push(user_id);
    };

    return user_select;

  })(App.elements.user_select.views.user_select);

  App.pages.community.elements.form.views.image = (function(_super) {
    __extends(image, _super);

    function image() {
      this.show_input_area = __bind(this.show_input_area, this);
      this.initialize = __bind(this.initialize, this);
      return image.__super__.constructor.apply(this, arguments);
    }

    image.prototype.events = {
      'click .js-file-input-container-thumbnail-delete-button': 'show_input_area'
    };

    image.prototype.initialize = function() {
      this.$file_area = this.$el.find('.js-file-input-container-file-area');
      return this.$input_area = this.$el.find('.js-file-input-container-input-area');
    };

    image.prototype.show_input_area = function() {
      this.$file_area.hide();
      return this.$input_area.show();
    };

    return image;

  })(Backbone.View);

  add_route('/community/(create|edit/[0-9]+)/?', function() {
    var $form, $limited_radio;
    if ($('.js-community-elements-form-user-select').length > 0) {
      new App.pages.community.elements.form.views.user_select();
    }
    $('.js-file-input-container').each((function(_this) {
      return function(index, input) {
        new App.elements.file_input.views.file_input({
          el: input,
          accept_files: false,
          accept_images: true
        });
        return new App.pages.community.elements.form.views.image({
          el: input
        });
      };
    })(this));
    $form = $('form');
    $form.one('submit', (function(_this) {
      return function() {
        $form.on('submit', function() {
          return false;
        });
        return true;
      };
    })(this));
    $limited_radio = $form.find('input[name="schedule"][value="1"]');
    return $form.find('input[name="schedule_date"]').on('focus', (function(_this) {
      return function() {
        return setTimeout(function() {
          if (+$form.find('input[name="schedule"]:checked').val() === 0) {
            return $limited_radio.prop('checked', true);
          }
        }, 0);
      };
    })(this));
  });

  App.pages.community.elements.header = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.community.elements.header.templates.message = _.template('<div class="msgBanner notice"><p><span class="icon"><i class="icon-exclamation"></i></span><span class="txt">退会すると削除が実行されます。注意文をご確認ください。</span></p></div>\n<p>\n  このコミュニティから退会します。退会すると、このコミュニティに投稿したすべての情報が削除されます。削除された情報は、元に戻すことができません。<br /><br />\n  またコミュニティ内の投稿内容や、通知も表示されなくなります。<br /><br />\n  よろしいですか？\n</p>');

  App.pages.community.elements.header.templates.join_message = _.template('<div class="msgBanner notice">\n  <p>\n    <span class="icon"><i class="icon-exclamation"></i></span>\n    <span class="txt">\n      招待されていない非公開のコミュニティに登録しようとしています。\n    </span>\n  </p>\n</div>');

  App.pages.community.elements.header.views.withdrawal = (function(_super) {
    __extends(withdrawal, _super);

    function withdrawal() {
      this.submit = __bind(this.submit, this);
      this.open_modal = __bind(this.open_modal, this);
      this.initialize = __bind(this.initialize, this);
      return withdrawal.__super__.constructor.apply(this, arguments);
    }

    withdrawal.prototype.el = '.js-community-header-withdrawal-container';

    withdrawal.prototype.events = {
      'click .js-community-header-withdrawal-link': 'open_modal'
    };

    withdrawal.prototype.$form = null;

    withdrawal.prototype.modal = null;

    withdrawal.prototype.initialize = function() {
      var community_name;
      this.$form = this.$el.siblings('.js-community-header-withdrawal-form');
      community_name = _(App.$body.find('.js-community-elements-header-community-name').text()).trim();
      community_name = _(community_name).escape();
      this.modal = new App.elements.modal({
        title: "" + community_name + "(退会)",
        body: App.pages.community.elements.header.templates.message(),
        decide_button_label: '退会する',
        cancel_button_label: '取り消す',
        buttons_type: 'cancel_delete'
      });
      return this.modal.on('decide', this.submit);
    };

    withdrawal.prototype.open_modal = function() {
      this.modal.show();
      return false;
    };

    withdrawal.prototype.submit = function() {
      return this.$form.submit();
    };

    return withdrawal;

  })(Backbone.View);

  App.pages.community.elements.header.views["delete"] = (function(_super) {
    __extends(_delete, _super);

    function _delete() {
      this.delete_community = __bind(this.delete_community, this);
      this.pop_confirm_delete = __bind(this.pop_confirm_delete, this);
      this.initialize = __bind(this.initialize, this);
      return _delete.__super__.constructor.apply(this, arguments);
    }

    _delete.prototype.el = '.js-community-elements-header-delete-link';

    _delete.prototype.events = {
      'click': 'pop_confirm_delete'
    };

    _delete.prototype.modal = null;

    _delete.prototype.name = null;

    _delete.prototype.$form = null;

    _delete.prototype.initialize = function() {
      this.name = $.trim($('.js-community-elements-header-community-name').text());
      return this.$form = $('#js-community-elements-header-delete-form');
    };

    _delete.prototype.pop_confirm_delete = function() {
      if (!this.modal) {
        this.modal = new App.elements.modal();
        this.modal.prevent_multiple_submit(this.$form);
        this.modal.set_title("" + this.name + "(削除)");
        this.modal.set_body('<div class="msgBanner notice"><p><span class="icon"><i class="icon-exclamation"></i></span><span class="txt">削除を実行しようとしています。注意文をご確認ください。</span></p></div>このコミュニティを削除します。削除すると、タイトル、説明文、メンバー、投稿内容など、このコミュニティに関連したすべての情報が削除されます。削除された情報は、元に戻すことができません。<br /><br />よろしいですか？');
        this.modal.set_footer_cancel_delete('取り消す', '削除する(元に戻せません)');
        this.modal.on('decide', this.delete_community);
      }
      this.modal.show();
      return false;
    };

    _delete.prototype.delete_community = function() {
      this.$form.submit();
      return false;
    };

    return _delete;

  })(Backbone.View);

  App.pages.community.elements.header.views.join = (function(_super) {
    __extends(join, _super);

    function join() {
      this.submit = __bind(this.submit, this);
      this.open_modal = __bind(this.open_modal, this);
      this.initialize = __bind(this.initialize, this);
      return join.__super__.constructor.apply(this, arguments);
    }

    join.prototype.el = '.js-community-header-community-container';

    join.prototype.events = {
      'click #js-community-header-limited-join-button': 'open_modal'
    };

    join.prototype.$form = null;

    join.prototype.modal = null;

    join.prototype.initialize = function() {
      this.$form = this.$el.find('.js-community-header-join-form');
      if (!this.modal) {
        this.modal = new App.elements.modal({
          title: '非公開コミュニティに参加しますか？',
          body: App.pages.community.elements.header.templates.join_message(),
          decide_button_label: 'このコミュニティに参加する',
          buttons_type: 'cancel_delete'
        });
        this.modal.on('decide', this.submit);
      }
      return this;
    };

    join.prototype.open_modal = function() {
      this.modal.show();
      return false;
    };

    join.prototype.submit = function() {
      return this.$form.submit();
    };

    return join;

  })(Backbone.View);

  add_route('/community/?.*', function() {
    var $header;
    $header = $('.js-community-header-container');
    if ($header.length > 0) {
      new App.pages.community.elements.header.views.withdrawal();
      new App.pages.community.elements.header.views.join();
      return new App.pages.community.elements.header.views["delete"]();
    }
  });

  App.pages.community.elements.list = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.community.elements.list.views.list_container = (function(_super) {
    __extends(list_container, _super);

    function list_container() {
      this.initialize = __bind(this.initialize, this);
      return list_container.__super__.constructor.apply(this, arguments);
    }

    list_container.prototype.el = '.js-community-index-list-container';

    list_container.prototype.initialize = function() {
      return $((function(_this) {
        return function() {
          return App.functions.align_height(_this.$el.find('li .boxCardItem .content'), 3);
        };
      })(this));
    };

    return list_container;

  })(Backbone.View);

  App.pages.community.elements.list.views.index = (function(_super) {
    __extends(index, _super);

    function index() {
      this.initialize = __bind(this.initialize, this);
      return index.__super__.constructor.apply(this, arguments);
    }

    index.prototype.initialize = function() {
      var list_container_view;
      return list_container_view = new App.pages.community.elements.list.views.list_container();
    };

    return index;

  })(Backbone.View);

  add_route('.*', function() {
    if ($('.js-community-index-list-container').length > 0) {
      return new App.pages.community.elements.list.views.index();
    }
  });

  App.pages.community.file = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.community.file.templates.file_row = {
    row: _.template('<td>\n  <p>\n    <a href="<%= model.get(\'file_path\') %>" target="_blank">\n      <%= model.get(\'file_name\') %>\n    </a>\n    <% if(show_related_icons){ %>\n      <% if(model.get(\'is_area_community\') && model.get(\'community_name\')){ %>\n        &gt;&gt;\n        <a href="<%= model.get(\'community_url\') %>">\n          <i title="<%= model.get(\'community_name\') %>" class="icon-community"></i>\n        </a>\n      <% } %>\n      <% if(model.get(\'is_area_event\') && model.get(\'event_name\')){ %>\n        &gt;&gt;\n        <a href="<%= model.get(\'event_url\') %>">\n          <i title="<%= model.get(\'event_name\') %>" class="icon-event"></i>\n        </a>\n      <% } %>\n    <% } %>\n  </p>\n  <div class="description">\n    <p class="note textContent js-community-file-row-caption-container"><%= model.get(\'file_caption\') %></p>\n  </div>\n</td>\n<td>\n  <p><img src="/assets/img/extensions/<%= model.get(\'file_extension\') %>.png" class="thumb small"></p>\n</td>\n<td>\n  <p class="size"><%= model.get(\'file_size\') %></p>\n</td>\n<td>\n  <p>\n    <a href="<%= model.get(\'user_link\') %>">\n      <%= model.get(\'user_name\') %>\n    </a>\n  </p>\n</td>\n<td>\n  <p class="timeStamp">\n    <%= model.get(\'created_at\') %>\n    <i class="icon-edited" title="コメント編集済み"></i>\n  </p>\n</td>\n<td class="js-community-file-row-balloon-container">\n</td>'),
    form: _.template('<div class="editContent">\n  <input class="inputField" type="text" value="<%= model.get(\'file_caption\') %>">\n  <button class="post-button js-community-file-row-edit-caption-button" type="button">編集する</button>\n</div>')
  };

  App.pages.community.file.models.feed_file = (function(_super) {
    __extends(feed_file, _super);

    function feed_file() {
      return feed_file.__super__.constructor.apply(this, arguments);
    }

    feed_file.prototype.urlRoot = "" + App.base_url + "/api/community/file/feed_file";

    feed_file.prototype.defaults = {
      id: null,
      file_feed_id: null,
      file_path: '',
      file_name: '',
      file_caption: '',
      file_size: '',
      user_link: '',
      user_name: '',
      created_at: null,
      can_edit: false,
      can_delete: false,
      is_area_community: false,
      is_area_event: false,
      community_name: null,
      community_url: null,
      event_name: null,
      event_url: null
    };

    return feed_file;

  })(Backbone.Model);

  App.pages.community.file.collections.feed_files = (function(_super) {
    __extends(feed_files, _super);

    function feed_files() {
      this.initialize = __bind(this.initialize, this);
      this.url = __bind(this.url, this);
      return feed_files.__super__.constructor.apply(this, arguments);
    }

    feed_files.prototype.url = function() {
      return "" + App.base_url + "/api/community/file/feed_files/" + this.community_id + "/" + this.limit + "/" + this.length + "?v=" + ((new Date()).getTime());
    };

    feed_files.prototype.model = App.pages.community.file.models.feed_file;

    feed_files.prototype.community_id = null;

    feed_files.prototype.limit = 20;

    feed_files.prototype.initialize = function(data, options) {
      if ((options != null ? options.community_id : void 0) != null) {
        this.community_id = +options.community_id;
      }
      if ((options != null ? options.limit : void 0) != null) {
        return this.limit = +options.limit;
      }
    };

    return feed_files;

  })(Backbone.Collection);

  App.pages.community.file.views.file_row = (function(_super) {
    __extends(file_row, _super);

    function file_row() {
      this.delete_file = __bind(this.delete_file, this);
      this.edit_file_caption = __bind(this.edit_file_caption, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return file_row.__super__.constructor.apply(this, arguments);
    }

    file_row.prototype.el = '<tr class="fileItem"></tr>';

    file_row.prototype.model = null;

    file_row.prototype.$caption = null;

    file_row.prototype.balloon = null;

    file_row.prototype.modal = null;

    file_row.prototype.$form = null;

    file_row.prototype.show_related_icons = false;

    file_row.prototype.initialize = function(options) {
      if (this.model.get('is_edited', false)) {
        this.$el.addClass('edited');
      }
      if ((options != null ? options.show_related_icons : void 0) != null) {
        this.show_related_icons = !!options.show_related_icons;
      }
      return this.render();
    };

    file_row.prototype.render = function() {
      var links;
      this.$el.html(App.pages.community.file.templates.file_row.row({
        model: this.model,
        show_related_icons: this.show_related_icons
      }));
      links = [];
      if (this.model.get('can_edit')) {
        links.push({
          path: '#',
          label: '編集する',
          event_name: 'edit_file'
        });
      }
      if (this.model.get('can_delete')) {
        links.push({
          path: '#',
          label: '削除する',
          event_name: 'delete_file'
        });
      }
      if (links.length > 0) {
        this.balloon = new App.elements.setting_balloon.views.setting_balloon({
          links: links
        });
        this.balloon.on('edit_file', this.edit_file_caption);
        this.balloon.on('delete_file', this.delete_file);
        this.$el.find('.js-community-file-row-balloon-container').append(this.balloon.$el);
      }
      this.$caption = this.$el.find('.js-community-file-row-caption-container');
      return this.$el;
    };

    file_row.prototype.edit_file_caption = function() {
      var $input;
      this.balloon.hide();
      if (this.$form != null) {
        return;
      }
      this.$form = $(App.pages.community.file.templates.file_row.form({
        model: this.model
      }));
      $input = this.$form.find('input');
      this.$form.find('.js-community-file-row-edit-caption-button').on('click', (function(_this) {
        return function() {
          return _this.model.save({
            file_caption: $input.val()
          }, {
            success: function() {
              return _this.$caption.text(_this.model.get('file_caption'));
            },
            complete: function() {
              if (_this.model.get('is_edited', false)) {
                _this.$el.addClass('edited');
              }
              _this.$form.remove();
              _this.$caption.show();
              return _this.$form = null;
            }
          });
        };
      })(this));
      this.$caption.after(this.$form);
      $input.focus();
      return this.$caption.hide();
    };

    file_row.prototype.delete_file = function() {
      this.balloon.hide();
      if (this.modal == null) {
        this.modal = new App.elements.modal();
      }
      this.modal.set_title('ファイルの削除');
      this.modal.set_body('この動作はもとに戻せませんがよろしいですか？');
      this.modal.set_footer_cancel_delete(null, '削除');
      this.modal.on('decide', (function(_this) {
        return function() {
          return _this.model.destroy({
            success: function() {
              return _this.$el.fadeOut(300, function() {
                return _this.remove();
              });
            },
            complete: function() {
              return _this.modal.hide();
            }
          });
        };
      })(this));
      this.modal.on('cancel', (function(_this) {
        return function() {
          return _this.modal.off('decide');
        };
      })(this));
      return this.modal.show();
    };

    return file_row;

  })(Backbone.View);

  App.pages.community.file.views.files = (function(_super) {
    __extends(files, _super);

    function files() {
      this.fetch_collection = __bind(this.fetch_collection, this);
      this.model_appended = __bind(this.model_appended, this);
      this.get_empty_collection = __bind(this.get_empty_collection, this);
      this.initialize = __bind(this.initialize, this);
      return files.__super__.constructor.apply(this, arguments);
    }

    files.prototype.el = '.js-community-file-list-container';

    files.prototype.table_selector = '.js-community-file-list-table';

    files.prototype.option_data_selector = '.js-community-file-list-options';

    files.prototype.initiali_data_selector = '.js-community-file-list-initial-data';

    files.prototype.collection = null;

    files.prototype.show_related_icons = false;

    files.prototype.$table = null;

    files.prototype.$tbody = null;

    files.prototype.initialize = function() {
      var initial_data, options;
      this.$table = this.$el.find(this.table_selector);
      this.$tbody = this.$table.find('tbody');
      options = getEmbeddedJSON(this.$el.find(this.option_data_selector));
      if ((options != null ? options.show_related_icons : void 0) != null) {
        this.show_related_icons = !!options.show_related_icons;
      }
      initial_data = getEmbeddedJSON(this.$el.find(this.initiali_data_selector));
      this.collection = this.get_empty_collection(options);
      this.collection.on('add', this.model_appended);
      this.collection.add(initial_data);
      this.scroll = new App.classes.scroll();
      this.scroll.on('bottom', this.fetch_collection);
      return this.scroll.enable();
    };

    files.prototype.get_empty_collection = function(options) {
      return new App.pages.community.file.collections.feed_files([], {
        community_id: options.community_id
      });
    };

    files.prototype.model_appended = function(feed_file) {
      var file_row;
      file_row = new App.pages.community.file.views.file_row({
        model: feed_file,
        show_related_icons: this.show_related_icons
      });
      return this.$tbody.append(file_row.$el);
    };

    files.prototype.fetch_collection = function() {
      var before_length;
      this.scroll.disable();
      before_length = this.collection.length;
      return this.collection.fetch({
        update: true,
        remove: false,
        add: true,
        success: (function(_this) {
          return function() {
            if (before_length !== _this.collection.length) {
              return _this.scroll.enable();
            }
          };
        })(this)
      });
    };

    return files;

  })(Backbone.View);

  add_route('(/community/file/[0-9]+/?)', function() {
    return new App.pages.community.file.views.files();
  });

  App.pages.community.image = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.community.image.templates.thumbnail = {
    wrapper: _.template('<ul class="listContainer item4"></ul>'),
    item: _.template('<div class="boxPhotoItem">\n  <div class="photoWrapper">\n    <p class="photo">\n      <% if (is_smartphone) { %>\n        <img class="<% if(model.get(\'aspect_ratio\') == \'horizon\'){ %>wLong<% }else{ %>hLong<% } %>"\n          src="<%= model.get(\'file\').get(\'thumb_path\') %>/588/588" alt="<%= model.get(\'file\').get(\'name\') %>">\n      <% } else { %>\n      <a href="#">\n        <img class="<% if(model.get(\'aspect_ratio\') == \'horizon\'){ %>wLong<% }else{ %>hLong<% } %>"\n          src="<%= model.get(\'file\').get(\'thumb_path\') %>" alt="<%= model.get(\'file\').get(\'name\') %>">\n      </a>\n      <% } %>\n    </p>\n  </div>\n  <div class="footer">\n    <div class="star js-community-images-thumbnail-star-container"></div>\n    <div class="comment js-community-images-thumbnail-comment-button-container"></div>\n  </div>\n</div>')
  };

  App.pages.community.image.templates.modal = {
    container: _.template('<div class="content">\n  <div class="boxPhotoItem">\n    <p class="photo">\n      <img\n        class="<% if(model.get(\'aspect_ratio\') == \'horizon\'){ %>wLong<% }else{ %>hLong<% } %>"\n        src="<%= model.get(\'image_path\') %>" alt="<%= model.get(\'image_name\') %>">\n    </p>\n    <p class="prev">\n      <a href="#">\n        <i class="icon-arrowLeft"></i>\n      </a>\n    </p>\n    <p class="next">\n      <a href="#">\n        <i class="icon-arrowRight"></i>\n      </a>\n    </p>\n  </div>\n  <div class="boxPhotoFeed">\n    <%= this.feed_section({model: model}) %>\n    <div class="commentWrapper">\n      <% count = model.get(\'comments\').length - comments_count %>\n      <% if(count > 0){ %>\n      <p class="showAll">\n        <a class="uiAnchor" href="#">\n          <i class="icon-comment"></i><span class="txt">他<%= count %>件のコメントを表示</span>\n        </a>\n      </p>\n      <% } %>\n      <div class="commentContainer">\n        <ul>\n          <% $.each(model.get(\'comments\'), function(comment, index){ %>\n            <%= this.comment({comment: comment}) %>\n          <% }); %>\n        </ul>\n      </div>\n    </div>\n  </div>\n</div>'),
    feed_section: _.template('<div class="boxFeedArticle">\n  <div class="profile">\n    <a href="#">\n      <img class="thumb" src="<%= model.get(\'user_icon\') %>" alt="<%= model.get(\'user_name\') %>">\n    </a>\n  </div>\n  <div class="content">\n    <div class="feedSection">\n      <p class="header">\n        <a href="#"><%= model.get(\'user_name\') %></a>\n      </p>\n    </div>\n    <div class="boxFeedFooter">\n      <div class="action">\n        <div class="star">\n          <a class="uiAnchor" href="#">\n            <i class="icon-star"></i><span class="txt"><%= model.get(\'stars\').length %></span>\n          </a>\n        </div>\n        <div class="comment">\n          <a class="uiAnchor" href="#">\n            <i class="icon-comment"></i><span class="txt"><%= model.get(\'comments\').length %></span>\n          </a>\n        </div>\n      </div>\n      <div class="data js-community-image-modal-feed-balloon-container">\n        <div class="timeStamp">\n          <span class="txt"><%= model.get(\'created_at\') %></span>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>'),
    comment: _.template('<li>\n  <div class="boxFeedComment">\n    <div class="profile">\n      <a href="#">\n        <img class="thumb" src="../../public/assets/img/theme/common/profile_x50.png" alt="">\n      </a>\n    </div>\n    <div class="content">\n      <div class="boxBalloon">\n        <div class="feedSection">\n          <p class="header">\n            <a href="#">安藤 あゆみ</a>\n          </p>\n          <div class="wysiwygContent">\n            <p>ようこそ伊藤さん！ </p>\n          </div>\n        </div>\n        <div class="boxFeedFooter">\n          <div class="action">\n            <div class="star">\n              <a class="uiAnchor" href="#"><i class="icon-star"></i><span class="txt">20</span></a>\n            </div>\n          </div>\n          <div class="data js-community-image-modal-comment-balloon-container">\n            <div class="timeStamp"><span class="txt">18:06</span></div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</li>')
  };

  App.pages.community.image.models.feed_image = (function(_super) {
    __extends(feed_image, _super);

    function feed_image() {
      return feed_image.__super__.constructor.apply(this, arguments);
    }

    return feed_image;

  })(App.elements.timeline.models.feed_image);

  App.pages.community.image.collections.feed_images = (function(_super) {
    __extends(feed_images, _super);

    function feed_images() {
      this.initialize = __bind(this.initialize, this);
      this.url = __bind(this.url, this);
      return feed_images.__super__.constructor.apply(this, arguments);
    }

    feed_images.prototype.url = function() {
      return "" + App.base_url + "/api/community/image/feed_images/" + this.community_id + "/" + this.limit + "/" + this.length + "?v=" + ((new Date()).getTime());
    };

    feed_images.prototype.model = App.pages.community.image.models.feed_image;

    feed_images.prototype.community_id = null;

    feed_images.prototype.limit = 20;

    feed_images.prototype.initialize = function(params, options) {
      if ((options != null ? options.community_id : void 0) != null) {
        this.community_id = +options.community_id;
      }
      if ((options != null ? options.limit : void 0) != null) {
        return this.limit = +options.limit;
      }
    };

    return feed_images;

  })(Backbone.Collection);

  App.pages.community.image.views.thumbnail = (function(_super) {
    __extends(thumbnail, _super);

    function thumbnail() {
      this.clicked = __bind(this.clicked, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return thumbnail.__super__.constructor.apply(this, arguments);
    }

    thumbnail.prototype.el = '<li></li>';

    thumbnail.prototype.events = {
      'click a': 'clicked'
    };

    thumbnail.prototype.model = null;

    thumbnail.prototype.star_url_base = App.elements.timeline.views.feed_images.prototype.star_url_base;

    thumbnail.prototype.initialize = function() {
      return this.render();
    };

    thumbnail.prototype.render = function() {
      var comment_button_view, star_view;
      this.$el.html(App.pages.community.image.templates.thumbnail.item({
        model: this.model,
        is_smartphone: App.current_user.is_smartphone
      }));
      if (App.current_user.is_smartphone === false) {
        star_view = new App.elements.timeline.views.star({
          model: this.model,
          url: "" + (this.star_url_base()) + "/" + (this.model.get('id'))
        });
        this.$el.find('.js-community-images-thumbnail-star-container').append(star_view.$el);
        comment_button_view = new App.elements.timeline.views.comment_button({
          collection: this.model.get('comments'),
          can_comment: this.model.get('can_comment', true)
        });
        comment_button_view.on('clicked', this.clicked);
        this.$el.find('.js-community-images-thumbnail-comment-button-container').append(comment_button_view.$el);
      }
      return this.$el;
    };

    thumbnail.prototype.clicked = function() {
      if (App.current_user.is_smartphone) {

      } else {
        this.trigger('clicked', this.model);
      }
      return false;
    };

    return thumbnail;

  })(Backbone.View);

  App.pages.community.image.views.image = (function(_super) {
    __extends(image, _super);

    function image() {
      this.get_model_from_collection = __bind(this.get_model_from_collection, this);
      this.show_next_modal = __bind(this.show_next_modal, this);
      this.show_previous_modal = __bind(this.show_previous_modal, this);
      this.re_sort_images = __bind(this.re_sort_images, this);
      this.delete_complete = __bind(this.delete_complete, this);
      this.load_more_comments = __bind(this.load_more_comments, this);
      this.hide_balloon = __bind(this.hide_balloon, this);
      this.delete_feed_image = __bind(this.delete_feed_image, this);
      this.edit_feed_image = __bind(this.edit_feed_image, this);
      this.open_modal = __bind(this.open_modal, this);
      this.add_thumbnails_wrapper = __bind(this.add_thumbnails_wrapper, this);
      this.get_thumbnails_wrapper = __bind(this.get_thumbnails_wrapper, this);
      this.render_thumbnail = __bind(this.render_thumbnail, this);
      this.model_appended = __bind(this.model_appended, this);
      this.fetch_collection = __bind(this.fetch_collection, this);
      this.get_empty_collection = __bind(this.get_empty_collection, this);
      this.initialize = __bind(this.initialize, this);
      return image.__super__.constructor.apply(this, arguments);
    }

    image.MODAL_COMMENTS_INITIAL_COUNT = 3;

    image.prototype.el = '.js-community-image-list-container';

    image.prototype.initial_data_selector = '.js-community-image-list-initial-data';

    image.prototype.option_data_selector = '.js-community-image-list-options';

    image.prototype.modal = null;

    image.prototype.max = 0;

    image.prototype.collection = null;

    image.prototype.scroll = null;

    image.prototype.thumbnails = {};

    image.prototype.show_related_icons = false;

    image.prototype.current_modal_feed_image = null;

    image.prototype.star_url_base = App.elements.timeline.views.feed_images.prototype.star_url_base;

    image.prototype.comment_url_base = App.elements.timeline.views.feed_images.prototype.comment_url_base;

    image.prototype.initialize = function() {
      var initial_data, options;
      this.thumbnails = {};
      options = getEmbeddedJSON(this.$el.find(this.option_data_selector));
      this.modal = new App.elements.modal({
        modal_classes: ['dialogPhoto', 'multiplePhotoModal'],
        set_buttons: false
      });
      this.modal.disable_auto_set_top();
      this.modal.add_container_class('jsModal-Photo');
      this.modal.add_header_heding_class('postModule');
      this.modal.set_title('', 'title textContent');
      this.modal.$el.on('click', '.prev a', this.show_previous_modal);
      this.modal.$el.on('click', '.next a', this.show_next_modal);
      initial_data = getEmbeddedJSON(this.$el.find(this.initial_data_selector));
      this.collection = this.get_empty_collection(options);
      this.collection.on('add', this.model_appended);
      this.collection.add(initial_data);
      this.scroll = new App.classes.scroll();
      this.scroll.on('bottom', this.fetch_collection);
      return this.scroll.enable();
    };

    image.prototype.get_empty_collection = function(options) {
      return new App.pages.community.image.collections.feed_images([], {
        community_id: +options.community_id
      });
    };

    image.prototype.fetch_collection = function() {
      var before_length;
      this.scroll.disable();
      before_length = this.collection.length;
      return this.collection.fetch({
        update: true,
        remove: false,
        add: true,
        success: (function(_this) {
          return function() {
            if (before_length !== _this.collection.length) {
              return _this.scroll.enable();
            }
          };
        })(this)
      });
    };

    image.prototype.model_appended = function(feed_image) {
      return this.render_thumbnail(feed_image);
    };

    image.prototype.render_thumbnail = function(feed_image) {
      var $wrapper, thumbnail;
      thumbnail = new App.pages.community.image.views.thumbnail({
        model: feed_image
      });
      thumbnail.on('clicked', this.open_modal);
      this.thumbnails[feed_image.cid] = thumbnail;
      $wrapper = this.get_thumbnails_wrapper();
      return $wrapper.append(thumbnail.$el);
    };

    image.prototype.get_thumbnails_wrapper = function() {
      var $wrapper;
      $wrapper = this.$el.find('.listContainer:last');
      if ($wrapper.length <= 0 || $wrapper.find('li').length >= 4) {
        $wrapper = this.add_thumbnails_wrapper();
      }
      return $wrapper;
    };

    image.prototype.add_thumbnails_wrapper = function() {
      var $wrapper;
      $wrapper = $(App.pages.community.image.templates.thumbnail.wrapper());
      this.$el.append($wrapper);
      return $wrapper;
    };

    image.prototype.open_modal = function(feed_image) {
      return App.elements.timeline.views.feed_images.prototype.open_modal.apply(this, [feed_image]);
    };

    image.prototype.edit_feed_image = function() {
      return App.elements.timeline.views.feed_images.prototype.edit_feed_image.apply(this);
    };

    image.prototype.delete_feed_image = function() {
      return App.elements.timeline.views.feed_images.prototype.delete_feed_image.apply(this);
    };

    image.prototype.hide_balloon = function() {
      return App.elements.timeline.views.feed_images.prototype.hide_balloon.apply(this);
    };

    image.prototype.load_more_comments = function() {
      return App.elements.timeline.views.feed_images.prototype.load_more_comments.apply(this);
    };

    image.prototype.delete_complete = function() {
      if (this.thumbnails[this.current_modal_feed_image.cid]) {
        this.thumbnails[this.current_modal_feed_image.cid].remove();
        delete this.thumbnails[this.current_modal_feed_image.cid];
      }
      this.current_modal_feed_image = null;
      return this.re_sort_images();
    };

    image.prototype.re_sort_images = function() {
      var $uls;
      $uls = this.$el.find('ul');
      return $uls.each((function(_this) {
        return function(index, ul) {
          var $li, $previous_ul, $ul;
          $ul = $(ul);
          $previous_ul = $ul.prev('ul');
          if ($previous_ul.length > 0 && $previous_ul.find('li').length < 4) {
            $li = $ul.find('li').filter(':first');
            if ($li.length > 0) {
              $previous_ul.append($li);
            }
          }
          if ($ul.find('li').length === 0) {
            return $ul.remove();
          }
        };
      })(this));
    };

    image.prototype.show_previous_modal = function() {
      this.open_modal(this.get_model_from_collection('previous'));
      return false;
    };

    image.prototype.show_next_modal = function() {
      this.open_modal(this.get_model_from_collection('next'));
      return false;
    };

    image.prototype.get_model_from_collection = function(method) {
      var index, model;
      if (method == null) {
        method = 'next';
      }
      index = this.collection.indexOf(this.current_modal_feed_image);
      if (method === 'next') {
        index++;
      } else {
        index--;
      }
      model = this.collection.at(index);
      if (!model) {
        if (method === 'next') {
          model = this.collection.at(0);
        } else {
          model = this.collection.at(this.collection.length - 1);
        }
      }
      return model;
    };

    return image;

  })(Backbone.View);

  add_route('(/community/image/[0-9]+/?)', function() {
    return new App.pages.community.image.views.image();
  });

  add_route('/community/show/[0-9]+/?', function() {
    var matches, url2link_class;
    matches = location.href.match(/\/community\/show\/([0-9]+)\/?/);
    App.elements.timeline.config.api_base_url = "" + App.base_url + "/api/community/timeline/" + matches[1];
    App.elements.timeline.config.show_related = false;
    App.elements.timeline.config.base_page = "community_top/" + matches[1];
    new App.elements.timeline.views.timeline();
    url2link_class = new App.elements.url2link.views.url2link();
    return url2link_class.renew_observe_click_url2link(true);
  });

  App.pages.community.user = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.community.user.views.list_container = (function(_super) {
    __extends(list_container, _super);

    function list_container() {
      this.align_height = __bind(this.align_height, this);
      this.initialize = __bind(this.initialize, this);
      return list_container.__super__.constructor.apply(this, arguments);
    }

    list_container.prototype.el = '.js-community-user-list-container';

    list_container.prototype.$items = null;

    list_container.prototype.$profiles = null;

    list_container.prototype.initialize = function() {
      this.$items = this.$el.find('li');
      this.$profiles = this.$items.find('p.profile');
      $((function(_this) {
        return function() {
          App.functions.align_height(_this.$items.find('p.name'), 6);
          return _this.align_height();
        };
      })(this));
      return this.$profiles.find('img').on('load', (function(_this) {
        return function() {
          return _this.align_height();
        };
      })(this));
    };

    list_container.prototype.align_height = function() {
      this.$profiles.attr('style', '');
      return App.functions.align_height(this.$profiles, 6);
    };

    return list_container;

  })(Backbone.View);

  App.pages.community.user.views.user = (function(_super) {
    __extends(user, _super);

    function user() {
      this.initialize = __bind(this.initialize, this);
      return user.__super__.constructor.apply(this, arguments);
    }

    user.prototype.initialize = function() {
      var list_container_view;
      return list_container_view = new App.pages.community.user.views.list_container();
    };

    return user;

  })(Backbone.View);

  add_route('/community/user/[0-9]+/?', function() {
    return new App.pages.community.user.views.user();
  });

  App.pages.conversation = {
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.conversation.index = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.conversation.index.views.user_select = (function(_super) {
    __extends(user_select, _super);

    function user_select() {
      this.submit = __bind(this.submit, this);
      this.initialize = __bind(this.initialize, this);
      return user_select.__super__.constructor.apply(this, arguments);
    }

    user_select.INITIAL_MAX_SHOW = 20;

    user_select.prototype.el = '.js-create-new-message-user-select';

    user_select.prototype.events = {
      'click .js-create-new-message-user-select-launch-button': 'launch'
    };

    user_select.prototype.initialize = function() {
      return user_select.__super__.initialize.apply(this, arguments);
    };

    user_select.prototype.submit = function() {
      this.fetch_selected_collection();
      this.render_selected_users();
      this.modal.hide();
      this.selected_collection.each((function(_this) {
        return function(user) {
          return $('<input />').attr('type', 'hidden').attr('name', 'users[]').attr('value', user.get('id')).appendTo('#create-new-message');
        };
      })(this));
      $('#create-new-message').submit();
      return false;
    };

    return user_select;

  })(App.elements.user_select.views.user_select);

  add_route('/conversation/?(index)?.*', function() {
    return new App.pages.conversation.index.views.user_select();
  });

  App.pages.conversation.show = {
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  MessageList_View = (function(_super) {
    __extends(MessageList_View, _super);

    function MessageList_View() {
      this.delete_edit_message = __bind(this.delete_edit_message, this);
      this.show_edit_message = __bind(this.show_edit_message, this);
      this.initialize = __bind(this.initialize, this);
      return MessageList_View.__super__.constructor.apply(this, arguments);
    }

    MessageList_View.prototype.el = '.js-messages';

    MessageList_View.prototype.events = {
      'click .js-btn-show-edit-message': 'show_edit_message',
      'click .js-btn-delete-message': 'delete_edit_message'
    };

    MessageList_View.prototype.templates = {
      text_edit: _.template('<div class="row-fluid js-text-edit-area">\n  <input type="hidden" name="message_id" value="<%= message_id %>">\n  <textarea placeholder="メッセージを記入してください" class="span7" rows="3" name="message" data-message-id="<%= message_id %>" ><%= message %></textarea>\n  <p class="align-center">\n    <button type="submit" class="btn btn-primary js-btn-update-message">\n      <i class="icon-edit icon-white"></i> 投稿\n    </button>\n  </p>\n</div>'),
      delete_message_id_hidden: _.template('<input type="hidden" name="delete_message_id" value="<%= message_id %>">')
    };

    MessageList_View.prototype.initialize = function() {
      return this.$form = $('.js-form-message-edit');
    };

    MessageList_View.prototype.show_edit_message = function(evt) {
      var $container, $message_content, message, message_id;
      $container = $(evt.target).parents('td.js-message-container');
      message_id = $container.data('message-id');
      if (message_id === null) {
        return false;
      }
      this.$el.find('.js-text-edit-area').remove();
      this.$el.find('div.js-message-content').show();
      $message_content = $container.find('div.js-message-content');
      $message_content.hide();
      message = $message_content.text();
      $container.append(this.templates.text_edit({
        message_id: message_id,
        message: message
      }));
      return this.$form.attr('action', "" + App.base_url + "/conversation/message/edit");
    };

    MessageList_View.prototype.delete_edit_message = function(evt) {
      var $container, message_id;
      $container = $(evt.target).parents('td.js-message-container');
      message_id = $container.data('message-id');
      if (message_id === null) {
        return false;
      }
      $(this.el).find('input[name="delete_message_id"]').remove();
      $container.append(this.templates.delete_message_id_hidden({
        message_id: message_id
      }));
      this.$form.attr('action', "" + App.base_url + "/conversation/message/delete");
      return this.$form.submit();
    };

    return MessageList_View;

  })(Backbone.View);

  App.pages.conversation.show.views.show = (function(_super) {
    __extends(show, _super);

    function show() {
      this.initialize = __bind(this.initialize, this);
      return show.__super__.constructor.apply(this, arguments);
    }

    show.prototype.initialize = function() {
      var message_list_view;
      return message_list_view = new MessageList_View();
    };

    return show;

  })(Backbone.View);

  add_route('/conversation/show/[0-9]+/?', function() {
    return new App.pages.conversation.show.views.show();
  });

  App.pages.event = {};

  App.pages.event.config = {
    api_base_url: "" + App.base_url + "/api/event",
    show_related: true
  };

  App.pages.event.elements = {};

  App.pages.event.elements.form = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.event.elements.form.templates.user_select = {
    selected_user: _.template('<td>\n  <p><%- model.get(\'name\') %></p>\n  <input type="hidden" name="<%= name %>" value="<%= model.get(\'id\') %>">\n</td>\n<td>\n  <p><% if(model.get(\'is_owner\') || model.get(\'is_manager\')) { %>主催者<% } else { %>一般<% } %></p>\n</td>\n<td>\n  <p>\n  <!-- 管理権限ユーザー以外選択可能 -->\n  <% if(model.get(\'is_manager\')){ %>\n    <input type="hidden" name="user_types[<%= model.get(\'id\') %>]" value="1">\n  <% }else{ %>\n    共同主催者に\n    <span class="choice">\n      <label>\n        <input type="radio" name="user_types[<%= model.get(\'id\') %>]" value="1"<% if(model.get(\'is_owner\')){ %> checked="checked"<% } %>>する\n      </label>\n      <label>\n        <input type="radio" name="user_types[<%= model.get(\'id\') %>]" value="0"<% if(!model.get(\'is_owner\')){ %> checked="checked"<% } %>>しない\n      </label>\n      <span></span>\n    </span>\n  <% } %>\n  </p>\n</td>\n<td class="delete">\n  <p>\n    <a class="uiAnchor" href="#">\n      <i class="icon-garbage js-event-elements-form-user-select-selected-user-delete"></i>\n    </a>\n  </p>\n</td>')
  };

  App.pages.event.elements.form.views.toggle_end_date = (function(_super) {
    __extends(toggle_end_date, _super);

    function toggle_end_date() {
      this.hide_end_date = __bind(this.hide_end_date, this);
      this.show_end_date = __bind(this.show_end_date, this);
      this.toggle_end_date = __bind(this.toggle_end_date, this);
      this.init_toggle_end_date = __bind(this.init_toggle_end_date, this);
      this.initialize = __bind(this.initialize, this);
      return toggle_end_date.__super__.constructor.apply(this, arguments);
    }

    toggle_end_date.prototype.el = '.js-event-form';

    toggle_end_date.prototype.$form_is_use_end_date = null;

    toggle_end_date.prototype.$end_date_container = null;

    toggle_end_date.prototype.$js_hidden_is_use_end_date = null;

    toggle_end_date.prototype.initialize = function() {
      this.$form_is_use_end_date = $('.js-form-is-use-end-date');
      this.$end_date_container = $('.js-end-date-container');
      this.$end_at_date = $('.js-form-end-at-date');
      this.$end_at_hour = $('.js-form-end-at-time_hour');
      this.$end_at_min = $('.js-form-end-at-time_minute');
      this.$js_hidden_is_use_end_date = jQuery('#js-hidden-is-use-end-date');
      this.init_toggle_end_date();
      return this;
    };

    toggle_end_date.prototype.events = {
      'click .js-form-is-use-end-date': 'toggle_end_date'
    };

    toggle_end_date.prototype.init_toggle_end_date = function() {
      if (this.$js_hidden_is_use_end_date.val()) {
        this.show_end_date();
      } else {
        this.hide_end_date();
      }
      return this;
    };

    toggle_end_date.prototype.toggle_end_date = function() {
      if (this.$end_date_container.is(":visible")) {
        this.hide_end_date();
      } else {
        this.show_end_date();
      }
      return false;
    };

    toggle_end_date.prototype.show_end_date = function() {
      this.$end_date_container.show(300);
      this.$form_is_use_end_date.text('終了日時を設定しない');
      this.$js_hidden_is_use_end_date.val(1);
      return this;
    };

    toggle_end_date.prototype.hide_end_date = function() {
      this.$end_date_container.hide(300);
      this.$end_at_date.val('');
      this.$end_at_hour.val('');
      this.$end_at_min.val('');
      this.$form_is_use_end_date.text('終了日時も設定する');
      this.$js_hidden_is_use_end_date.val('');
      return this;
    };

    return toggle_end_date;

  })(Backbone.View);

  App.pages.event.elements.form.views.selected_user = (function(_super) {
    __extends(selected_user, _super);

    function selected_user() {
      return selected_user.__super__.constructor.apply(this, arguments);
    }

    selected_user.prototype.el = '<tr></tr>';

    selected_user.prototype.events = {
      'click .js-event-elements-form-user-select-selected-user-delete': 'remove_self'
    };

    selected_user.prototype.template = App.pages.event.elements.form.templates.user_select.selected_user;

    return selected_user;

  })(App.elements.user_select.views.selected_user);

  App.pages.event.elements.form.views.user_select = (function(_super) {
    __extends(user_select, _super);

    function user_select() {
      this.add_user_row = __bind(this.add_user_row, this);
      this.render_all_selected_users = __bind(this.render_all_selected_users, this);
      this.render_selected_users = __bind(this.render_selected_users, this);
      this.initialize = __bind(this.initialize, this);
      return user_select.__super__.constructor.apply(this, arguments);
    }

    user_select.INITIAL_MAX_SHOW = 20;

    user_select.prototype.el = '.js-event-elements-form-user-select';

    user_select.prototype.events = {
      'click .js-event-elements-form-user-select-launch-button': 'launch'
    };

    user_select.prototype.$selected_container = null;

    user_select.prototype.$tbody = null;

    user_select.prototype.owner_ids = [];

    user_select.prototype.manager_ids = [];

    user_select.prototype.show_more = null;

    user_select.prototype.show_ids = [];

    user_select.prototype.initialize = function() {
      var owners;
      user_select.__super__.initialize.apply(this, arguments);
      this.$selected_container = this.$el.find('.js-event-elements-form-user-select-list-selected-container');
      this.$tbody = this.$selected_container.find('tbody');
      owners = getEmbeddedJSON(this.$el.find('.js-event-elements-form-user-select-event-owners'));
      if ((owners != null ? owners.owners : void 0) != null) {
        this.owner_ids = owners.owners;
      }
      if ((owners != null ? owners.managers : void 0) != null) {
        this.manager_ids = owners.managers;
      }
      this.show_more = new App.elements.show_more.views.show_more({
        label: 'もっと見る'
      });
      this.show_more.hide();
      this.show_more.on('clicked', this.render_all_selected_users);
      return this.$selected_container.after(this.show_more.$el);
    };

    user_select.prototype.render_selected_users = function() {
      this.$tbody.find('tr').not('.js-event-elements-form-users-list-container-creator-row').remove();
      this.show_ids = [];
      if (this.selected_collection.length > App.pages.event.elements.form.views.user_select.INITIAL_MAX_SHOW) {
        this.show_more.show();
      } else {
        this.show_more.hide();
      }
      return this.selected_collection.each((function(_this) {
        return function(user, index) {
          return _this.add_user_row(user, index >= App.pages.event.elements.form.views.user_select.INITIAL_MAX_SHOW);
        };
      })(this));
    };

    user_select.prototype.render_all_selected_users = function() {
      this.$tbody.find('tr').show();
      return this.show_more.hide();
    };

    user_select.prototype.add_user_row = function(user, hidden) {
      var $user_row, user_id, user_view;
      if (hidden == null) {
        hidden = false;
      }
      user_id = parseInt(user.get('id'), 10);
      if (_.indexOf(this.show_ids, user_id) >= 0) {
        return true;
      }
      user.set('is_owner', _.indexOf(this.owner_ids, user_id) >= 0);
      user.set('is_manager', _.indexOf(this.manager_ids, user_id) >= 0);
      user_view = new App.pages.event.elements.form.views.selected_user({
        model: user,
        name: this.name_attribute
      });
      $user_row = user_view.render();
      if (hidden) {
        $user_row.hide();
      }
      this.$tbody.append($user_row);
      return this.show_ids.push(user_id);
    };

    return user_select;

  })(App.elements.user_select.views.user_select);

  App.pages.event.elements.form.views.image = (function(_super) {
    __extends(image, _super);

    function image() {
      this.show_input_area = __bind(this.show_input_area, this);
      this.initialize = __bind(this.initialize, this);
      return image.__super__.constructor.apply(this, arguments);
    }

    image.prototype.events = {
      'click .js-file-input-container-thumbnail-delete-button': 'show_input_area'
    };

    image.prototype.initialize = function() {
      this.$file_area = this.$el.find('.js-file-input-container-file-area');
      return this.$input_area = this.$el.find('.js-file-input-container-input-area');
    };

    image.prototype.show_input_area = function() {
      this.$file_area.hide();
      return this.$input_area.show();
    };

    return image;

  })(Backbone.View);

  add_route('/event/(create|edit/[0-9]+)/?', function() {
    var $form, $limited_radio;
    if ($('.js-form-is-use-end-date').length > 0) {
      new App.pages.event.elements.form.views.toggle_end_date();
    }
    if ($('.js-event-elements-form-user-select').length > 0) {
      new App.pages.event.elements.form.views.user_select();
    }
    $('.js-file-input-container').each((function(_this) {
      return function(index, input) {
        new App.elements.file_input.views.file_input({
          el: input,
          accept_files: false,
          accept_images: true,
          dummy_path: '/assets/img/theme/common/event_x100.png'
        });
        return new App.pages.event.elements.form.views.image({
          el: input
        });
      };
    })(this));
    $form = $('form');
    $form.one('submit', (function(_this) {
      return function() {
        $form.on('submit', function() {
          return false;
        });
        return true;
      };
    })(this));
    $limited_radio = $form.find('input[name="schedule"][value="1"]');
    return $form.find('input[name="schedule_date"]').on('focus', (function(_this) {
      return function() {
        return setTimeout(function() {
          if (+$form.find('input[name="schedule"]:checked').val() === 0) {
            return $limited_radio.prop('checked', true);
          }
        }, 0);
      };
    })(this));
  });

  App.pages.event.elements.header = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.event.elements.header.templates.message = _.template('<div class="msgBanner notice"><p><span class="icon"><i class="icon-exclamation"></i></span><span class="txt">退会すると削除が実行されます。注意文をご確認ください。</span></p></div>\n<p>\n  このイベントから退会します。退会すると、このイベントに投稿したすべての情報が削除されます。削除された情報は、元に戻すことができません。<br /><br />\n  またイベント内の投稿内容や、通知も表示されなくなります。<br /><br />\n  よろしいですか？\n</p>');

  App.pages.event.elements.header.templates.status_users_balloon = {
    users: _.template('<% //TODO: 専用のスタイルを当てる必要アリ %>\n<div class="blnMember leftTale" style="position:absolute; z-index:100;">\n  <div class="content">\n    <ul>\n      <% data.collection.each(function(user){ %>\n      <li>\n        <a href="<%= data.base_url %>/user/<%= user.get(\'user_id\') %>">\n          <span class="profile">\n            <img class="thumb" src="<%= user.get(\'user_icon\') %>" alt="<%- user.get(\'user_name\') %>">\n          </span>\n          <span class="text"><em><%- user.get(\'user_name\') %></em></span>\n        </a>\n      </li>\n      <% }) %>\n    </ul>\n  </div>\n</div>', void 0, {
      variable: 'data'
    })
  };

  App.pages.event.elements.header.templates.join_message = _.template('<div class="msgBanner notice">\n  <p>\n    <span class="icon"><i class="icon-exclamation"></i></span>\n    <span class="txt">\n      招待されていない非公開のイベントに登録しようとしています。\n    </span>\n  </p>\n</div>');

  App.pages.event.elements.header.collections.status_users = (function(_super) {
    __extends(status_users, _super);

    function status_users() {
      this.url = __bind(this.url, this);
      return status_users.__super__.constructor.apply(this, arguments);
    }

    status_users.prototype.url = function() {
      return "" + App.pages.event.config.api_base_url + "/header/user/status_users";
    };

    status_users.prototype.parse = function(response) {
      if (!response) {
        return [];
      } else {
        return response;
      }
    };

    return status_users;

  })(Backbone.Collection);

  App.pages.event.elements.header.views.withdrawal = (function(_super) {
    __extends(withdrawal, _super);

    function withdrawal() {
      this.submit = __bind(this.submit, this);
      this.open_modal = __bind(this.open_modal, this);
      this.initialize = __bind(this.initialize, this);
      return withdrawal.__super__.constructor.apply(this, arguments);
    }

    withdrawal.prototype.el = '.js-event-header-withdrawal-container';

    withdrawal.prototype.events = {
      'click .js-event-header-withdrawal-link': 'open_modal'
    };

    withdrawal.prototype.$form = null;

    withdrawal.prototype.modal = null;

    withdrawal.prototype.initialize = function() {
      var event_name;
      this.$form = this.$el.siblings('.js-event-header-withdrawal-form');
      event_name = _(App.$body.find('.js-event-elements-header-event-name').text()).trim();
      event_name = _(event_name).escape();
      this.modal = new App.elements.modal({
        title: "" + event_name + "(退会)",
        body: App.pages.event.elements.header.templates.message(),
        decide_button_label: '退会する',
        cancel_button_label: '取り消す',
        buttons_type: 'cancel_delete'
      });
      return this.modal.on('decide', this.submit);
    };

    withdrawal.prototype.open_modal = function() {
      this.modal.show();
      return false;
    };

    withdrawal.prototype.submit = function() {
      return this.$form.submit();
    };

    return withdrawal;

  })(Backbone.View);

  App.pages.event.elements.header.views["delete"] = (function(_super) {
    __extends(_delete, _super);

    function _delete() {
      this.delete_event = __bind(this.delete_event, this);
      this.pop_confirm_delete = __bind(this.pop_confirm_delete, this);
      this.initialize = __bind(this.initialize, this);
      return _delete.__super__.constructor.apply(this, arguments);
    }

    _delete.prototype.el = '.js-event-elements-header-delete-link';

    _delete.prototype.events = {
      'click': 'pop_confirm_delete'
    };

    _delete.prototype.modal = null;

    _delete.prototype.name = null;

    _delete.prototype.$form = null;

    _delete.prototype.initialize = function() {
      this.name = $.trim($('.js-event-elements-header-event-name').text());
      return this.$form = $('#js-event-elements-header-delete-form');
    };

    _delete.prototype.pop_confirm_delete = function() {
      if (!this.modal) {
        this.modal = new App.elements.modal();
        this.modal.prevent_multiple_submit(this.$form);
        this.modal.set_title("" + this.name + "(削除)");
        this.modal.set_body('<div class="msgBanner notice"><p><span class="icon"><i class="icon-exclamation"></i></span><span class="txt">削除を実行しようとしています。注意文をご確認ください。</span></p></div>このイベントを削除します。削除すると、タイトル、説明文、開催場所、メンバー、投稿内容など、このイベントに関連したすべての情報が削除されます。削除された情報は、元に戻すことができません。<br /><br />よろしいですか？');
        this.modal.set_footer_cancel_delete('取り消す', '削除する(元に戻せません)');
        this.modal.on('decide', this.delete_event);
      }
      this.modal.show();
      return false;
    };

    _delete.prototype.delete_event = function() {
      this.$form.submit();
      return false;
    };

    return _delete;

  })(Backbone.View);

  App.pages.event.elements.header.views.attendance = (function(_super) {
    __extends(attendance, _super);

    function attendance() {
      this.submit = __bind(this.submit, this);
      this.initialize = __bind(this.initialize, this);
      return attendance.__super__.constructor.apply(this, arguments);
    }

    attendance.prototype.el = '.js-event-status-container';

    attendance.prototype.$from = null;

    attendance.prototype.events = {
      'click .js-event-header-attend-link': 'submit',
      'click .js-event-header-absent-link': 'submit'
    };

    attendance.prototype.initialize = function() {
      return this.$form = this.$el.find('form');
    };

    attendance.prototype.submit = function(event) {
      return this.$form.submit();
    };

    return attendance;

  })(Backbone.View);

  App.pages.event.elements.header.views.join = (function(_super) {
    __extends(join, _super);

    function join() {
      this.submit = __bind(this.submit, this);
      this.open_modal = __bind(this.open_modal, this);
      this.initialize = __bind(this.initialize, this);
      return join.__super__.constructor.apply(this, arguments);
    }

    join.prototype.el = '.js-event-header-event-container';

    join.prototype.events = {
      'click #js-event-header-join-button': 'submit',
      'click #js-event-header-limited-join-button': 'open_modal'
    };

    join.prototype.$form = null;

    join.prototype.modal = null;

    join.prototype.initialize = function() {
      this.$form = this.$el.find('.js-event-header-join-form');
      if (!this.modal) {
        this.modal = new App.elements.modal({
          title: '非公開イベントに参加しますか？',
          body: App.pages.event.elements.header.templates.join_message(),
          decide_button_label: 'このイベントに参加する',
          buttons_type: 'cancel_delete'
        });
        this.modal.on('decide', this.submit);
      }
      return this;
    };

    join.prototype.open_modal = function() {
      this.modal.show();
      return false;
    };

    join.prototype.submit = function() {
      return this.$form.submit();
    };

    return join;

  })(Backbone.View);

  App.pages.event.elements.header.views.statuses = (function(_super) {
    __extends(statuses, _super);

    function statuses() {
      this.check_user_count = __bind(this.check_user_count, this);
      this.hide_balloon = __bind(this.hide_balloon, this);
      this.toggle_status_users_balloon = __bind(this.toggle_status_users_balloon, this);
      this.show_absent_user = __bind(this.show_absent_user, this);
      this.initialize = __bind(this.initialize, this);
      return statuses.__super__.constructor.apply(this, arguments);
    }

    statuses.prototype.el = '.js-event-statuses';

    statuses.prototype.collection = App.pages.event.elements.header.collections.status_users;

    statuses.prototype.model = null;

    statuses.prototype.$more = null;

    statuses.prototype.$balloon = null;

    statuses.prototype.$target_link = null;

    statuses.prototype.events = {
      'click .js-show-absent-user': 'show_absent_user',
      'click .js-event-user-status-count-link': 'toggle_status_users_balloon'
    };

    statuses.prototype.initialize = function() {
      this.$more = this.$el.find('.js-show-absent-user');
      this.collection = new this.collection();
      return App.$document.on('click', (function(_this) {
        return function(event) {
          if (_this.$balloon && _this.$balloon.find(event.target).length === 0) {
            _this.hide_balloon();
          }
          return true;
        };
      })(this));
    };

    statuses.prototype.show_absent_user = function(event) {
      this.$el.addClass('expand');
      this.$el.removeClass('fold');
      return false;
    };

    statuses.prototype.toggle_status_users_balloon = function(event) {
      var event_id, status;
      this.$target_link = $(event.target);
      event_id = this.$target_link.data('event_id');
      status = this.$target_link.data('status');
      if (this.$balloon) {
        this.hide_balloon();
        return false;
      }
      this.collection.fetch({
        url: "" + (_.result(this.collection, 'url')) + "/" + event_id + "/" + status + "?v=" + ((new Date()).getTime()),
        complete: (function(_this) {
          return function() {
            var top;
            _this.check_user_count();
            top = _this.$target_link.offset().top - 40;
            return _this.$balloon.css('top', "" + top + "px");
          };
        })(this)
      });
      return false;
    };

    statuses.prototype.hide_balloon = function() {
      if (this.$balloon) {
        this.$balloon.remove();
        return this.$balloon = null;
      }
    };

    statuses.prototype.check_user_count = function() {
      if (this.collection.length !== 0) {
        this.$balloon = $(App.pages.event.elements.header.templates.status_users_balloon.users({
          base_url: App.base_url,
          collection: this.collection
        }));
        return this.$target_link.parent('p.number').after(this.$balloon);
      }
    };

    return statuses;

  })(Backbone.View);

  add_route('/event/?.*', function() {
    var $header;
    $header = $('.js-event-header-container');
    if ($header.length > 0) {
      new App.pages.event.elements.header.views.withdrawal();
      new App.pages.event.elements.header.views.attendance();
      new App.pages.event.elements.header.views.join();
      new App.pages.event.elements.header.views["delete"]();
      return new App.pages.event.elements.header.views.statuses();
    }
  });

  App.pages.event.elements.list = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.event.elements.list.views.list_container = (function(_super) {
    __extends(list_container, _super);

    function list_container() {
      this.initialize = __bind(this.initialize, this);
      return list_container.__super__.constructor.apply(this, arguments);
    }

    list_container.prototype.el = '.js-event-index-list-container';

    list_container.prototype.initialize = function() {
      return $((function(_this) {
        return function() {
          return App.functions.align_height(_this.$el.find('li .boxCardItem'), 3);
        };
      })(this));
    };

    return list_container;

  })(Backbone.View);

  App.pages.event.elements.list.views.index = (function(_super) {
    __extends(index, _super);

    function index() {
      this.initialize = __bind(this.initialize, this);
      return index.__super__.constructor.apply(this, arguments);
    }

    index.prototype.initialize = function() {
      var list_container_view;
      return list_container_view = new App.pages.event.elements.list.views.list_container();
    };

    return index;

  })(Backbone.View);

  add_route('.*', function() {
    if ($('.js-event-index-list-container').length > 0) {
      return new App.pages.event.elements.list.views.index();
    }
  });

  App.pages.event.file = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.event.file.collections.feed_files = (function(_super) {
    __extends(feed_files, _super);

    function feed_files() {
      this.url = __bind(this.url, this);
      return feed_files.__super__.constructor.apply(this, arguments);
    }

    feed_files.prototype.url = function() {
      return "" + App.base_url + "/api/event/file/feed_files/" + this.community_id + "/" + this.limit + "/" + this.length + "?v=" + ((new Date()).getTime());
    };

    return feed_files;

  })(App.pages.community.file.collections.feed_files);

  App.pages.event.file.views.files = (function(_super) {
    __extends(files, _super);

    function files() {
      this.get_empty_collection = __bind(this.get_empty_collection, this);
      return files.__super__.constructor.apply(this, arguments);
    }

    files.prototype.el = '.js-event-file-list-container';

    files.prototype.table_selector = '.js-event-file-list-table';

    files.prototype.option_data_selector = '.js-event-file-list-options';

    files.prototype.initiali_data_selector = '.js-event-file-list-initial-data';

    files.prototype.collection = null;

    files.prototype.show_related_icons = false;

    files.prototype.$table = null;

    files.prototype.$tbody = null;

    files.prototype.get_empty_collection = function(options) {
      return new App.pages.event.file.collections.feed_files([], {
        community_id: options.event_id
      });
    };

    return files;

  })(App.pages.community.file.views.files);

  add_route('(/event/file/[0-9]+/?)', function() {
    return new App.pages.event.file.views.files();
  });

  App.pages.event.image = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.event.image.collections.feed_images = (function(_super) {
    __extends(feed_images, _super);

    function feed_images() {
      this.url = __bind(this.url, this);
      return feed_images.__super__.constructor.apply(this, arguments);
    }

    feed_images.prototype.url = function() {
      return "" + App.base_url + "/api/event/image/feed_images/" + this.community_id + "/" + this.limit + "/" + this.length + "?v=" + ((new Date()).getTime());
    };

    return feed_images;

  })(App.pages.community.image.collections.feed_images);

  App.pages.event.image.views.image = (function(_super) {
    __extends(image, _super);

    function image() {
      this.get_empty_collection = __bind(this.get_empty_collection, this);
      return image.__super__.constructor.apply(this, arguments);
    }

    image.prototype.el = '.js-event-image-list-container';

    image.prototype.initial_data_selector = '.js-event-image-list-initial-data';

    image.prototype.option_data_selector = '.js-event-image-list-options';

    image.prototype.get_empty_collection = function(options) {
      return new App.pages.event.image.collections.feed_images([], {
        community_id: +options.event_id
      });
    };

    return image;

  })(App.pages.community.image.views.image);

  add_route('/event/image(/[0-9]+)/?', function() {
    return new App.pages.event.image.views.image();
  });

  App.pages.event.show = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.event.show.views.toggle_read_more = (function(_super) {
    __extends(toggle_read_more, _super);

    function toggle_read_more() {
      this.toggle_head_read_more = __bind(this.toggle_head_read_more, this);
      this.initialize = __bind(this.initialize, this);
      return toggle_read_more.__super__.constructor.apply(this, arguments);
    }

    toggle_read_more.prototype.el = '.unit_event_show';

    toggle_read_more.prototype.initialize = function() {};

    toggle_read_more.prototype.events = {
      'click .js-event-toggle-head-content': 'toggle_head_read_more'
    };

    toggle_read_more.prototype.toggle_head_read_more = function(evt) {
      var $all_context, $clicked_context, $parent_context, $truncated_context;
      $clicked_context = $(evt.target);
      $parent_context = $clicked_context.parent();
      if (!$parent_context) {
        return false;
      }
      $truncated_context = $parent_context.find('.js-truncated');
      $all_context = $parent_context.find('.js-all');
      if ($truncated_context.is(':visible')) {
        $truncated_context.hide();
        $all_context.show(500);
        $clicked_context.text('... 省略する');
      } else {
        $all_context.hide();
        $truncated_context.show(500);
        $clicked_context.text('もっと見る');
      }
      return false;
    };

    return toggle_read_more;

  })(Backbone.View);

  add_route('/event/show/[0-9]+/?', function() {
    var matches, url2link_class;
    matches = location.href.match(/\/event\/show\/([0-9]+)\/?/);
    App.elements.timeline.config.api_base_url = "" + App.base_url + "/api/event/timeline/" + matches[1];
    App.elements.timeline.config.show_related = false;
    App.elements.timeline.config.base_page = "event_top/" + matches[1];
    new App.elements.timeline.views.timeline();
    new App.pages.event.show.views.toggle_read_more();
    url2link_class = new App.elements.url2link.views.url2link();
    return url2link_class.renew_observe_click_url2link(true);
  });

  App.pages.feed = {};

  App.pages.feed.show = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.feed.show.views.feed = (function(_super) {
    __extends(feed, _super);

    function feed() {
      this.feed_deleted = __bind(this.feed_deleted, this);
      this.set_back_page = __bind(this.set_back_page, this);
      this.initialize = __bind(this.initialize, this);
      return feed.__super__.constructor.apply(this, arguments);
    }

    feed.prototype.el = '.js-feed-show-container';

    feed.prototype.$container = null;

    feed.prototype.feed = null;

    feed.prototype.redirect_basic = 'top';

    feed.prototype.redirect_to = {
      community_top: 'community/show',
      community_image: 'community/image',
      community_file: 'community/file',
      event_top: 'event/show',
      event_image: 'event/image',
      event_file: 'event/file',
      user_top: 'user/show',
      user_image: 'user/image',
      user_file: 'user/file'
    };

    feed.prototype.redirect_path = '';

    feed.prototype.initialize = function() {
      var data, feed_view, route, url2link_class;
      data = getEmbeddedJSON(this.$el.find('.js-feed-show-data'));
      if (data == null) {
        return false;
      }
      this.$container = this.$el.find('.js-feed-show-feeds-container');
      this.feed = new App.elements.timeline.models.feed();
      this.feed.on('destroy', this.feed_deleted);
      this.feed.set(this.feed.parse(data));
      feed_view = new App.elements.timeline.views.timeline_feed({
        model: this.feed,
        show_communication_link_mores: false,
        show_detail_link: false
      });
      this.$container.append(feed_view.$el);
      this.set_back_page();
      route = new App.pages.feed.show.routes.route();
      route.on('route:from', this.set_back_page);
      Backbone.history.start();
      url2link_class = new App.elements.url2link.views.url2link();
      $('.js-timeline-feed-body-html-container').each((function(_this) {
        return function(index, element) {
          return $(element).html(url2link_class.done_url2link($(element).html()));
        };
      })(this));
      return url2link_class.renew_observe_click_url2link();
    };

    feed.prototype.set_back_page = function(page, item_id) {
      this.redirect_path = App.base_url;
      if (this.redirect_to[page] != null) {
        return this.redirect_path += "/" + this.redirect_to[page] + "/" + item_id;
      } else {
        return this.redirect_path += "/" + this.redirect_basic;
      }
    };

    feed.prototype.feed_deleted = function() {
      var $wrapper, message;
      message = new App.elements.message_banner.views.message_banner({
        message: 'フィードを削除しました。自動で元のページに戻ります。',
        class_name: 'done'
      });
      $wrapper = $('<li class="sectionFeed feedPhoto"></li>');
      $wrapper.append(message.$el);
      this.$container.append($wrapper);
      return setTimeout((function(_this) {
        return function() {
          return location.href = _this.redirect_path;
        };
      })(this), 1500);
    };

    return feed;

  })(Backbone.View);

  App.pages.feed.show.routes.route = (function(_super) {
    __extends(route, _super);

    function route() {
      return route.__super__.constructor.apply(this, arguments);
    }

    route.prototype.routes = {
      ':type/:id': 'from'
    };

    return route;

  })(Backbone.Router);

  add_route('/feed/show/[0-9]+/?', function() {
    return new App.pages.feed.show.views.feed();
  });

  App.pages.message_room = {
    templates: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.message_room.templates.message_room_create_modal = $.extend({}, App.elements._modal_templates, {
    container: _.template('<div id="modalDlgContainer" class="modalDlgState jsModal-Message" style="display: none">\n  <div class="js-blackBack"></div>\n  <div class="modalDlgWrapper"></div>\n</div>')
  });

  App.pages.message_room.templates.message_room_create_user_select = $.extend({}, App.elements.user_select.templates.modal, {
    body: _.template('<div class="boxLabeled">\n  <p class="label">タイトル</p>\n  <p class="content">\n    <input type="text" name="name">\n  </p>\n</div>\n\n<div class="boxLabeled">\n  <p class="label">メンバー</p>\n  <div class="boxUserAdd">\n    <div class="searchSection">\n      <div class="content">\n        <span class="icon"><i class="icon-search"></i></span>\n        <span class="input">\n          <input class="searchField js-user-select-modal-filter-input" type="text" placeholder="姓名で検索する">\n        </span>\n      </div><!-- //.content -->\n      <div class="mySelection">\n        <% if (use_mysearch) { %>\n        <div class="col">\n          <select class="js-user-select-modal-fileter-select">\n          </select>\n        </div>\n        <% } %>\n        <div class="col">\n          <a href="#" class="btnM js-user-select-modal-all-user-select">リスト内のユーザーを全員追加する</a>\n        </div>\n      </div>\n    </div><!-- //.searchSection -->\n\n    <div class="suggestSection">\n      <div class="content">\n        <ul class="js-user-select-modal-users-list"></ul><!-- 対象のユーザー一覧 -->\n      </div><!-- //.content -->\n    </div><!-- //.suggestSection -->\n    <div class="selectedSection">\n      <div class="content js-user-select-modal-selected-users-list"></div><!-- 選択したユーザー一覧 -->\n    </div><!-- //.selectedSection -->\n  </div><!-- //.boxUserAdd -->\n</div>\n\n<% if (is_role_manager) { %>\n<div class="boxLabeled">\n  <p class="label">新着メッセージをメールで連絡する</p>\n  <p class="note">管理ユーザーのみが設定することができます。</p>\n  <p class="content">\n    <label class="bullet"><input name="is_changeable" type="radio" value="1" checked="checked">メンバーが各自設定する(通常)</label>\n  </p>\n  <p class="content">\n    <label class="bullet"><input name="is_changeable" type="radio" value="0">必ずメールで連絡</label>\n  </p>\n</div>\n<% } %>')
  });

  App.pages.message_room.templates.message_room_edit_modal = {
    body: _.template('<div class="boxLabeled">\n  <p class="label">タイトル</p>\n  <p class="content">\n    <input type="text" name="name" value="<%- name %>">\n  </p>\n</div>')
  };

  App.pages.message_room.events.input_file = _.extend({
    max_upload_cnt: 5,
    views: [],
    items: [],
    init: function() {
      this.modal = new App.elements.modal();
      App.pages.message_room.events.input_file.on('add_file', (function(_this) {
        return function(options) {
          return _.each(_this.views, function(view) {
            if (view.add_file != null) {
              return view.add_file(options);
            }
          });
        };
      })(this));
      App.pages.message_room.events.input_file.on('remove_file', (function(_this) {
        return function(options) {
          return _.each(_this.views, function(view) {
            if (view.remove_file != null) {
              return view.remove_file(options);
            }
          });
        };
      })(this));
      App.pages.message_room.events.input_file.on('add_item', (function(_this) {
        return function(item) {
          _.each(_this.views, function(view) {
            if (view.add_item != null) {
              return view.add_item(item);
            }
          });
          return _this.items.push(item);
        };
      })(this));
      App.pages.message_room.events.input_file.on('remove_item', (function(_this) {
        return function(item) {
          var idx;
          idx = _.indexOf(_this.items, item);
          if (idx >= 0) {
            return delete _this.items[idx];
          }
        };
      })(this));
      App.pages.message_room.events.input_file.on('show_error', (function(_this) {
        return function(options) {
          return _.each(_this.views, function(view) {
            if (view.clear_error != null) {
              view.clear_error();
            }
            if (view.show_error != null) {
              return view.show_error(options);
            }
          });
        };
      })(this));
      App.pages.message_room.events.input_file.on('show_notice', (function(_this) {
        return function(options) {
          return _.each(_this.views, function(view) {
            if (view.clear_notice != null) {
              view.clear_notice();
            }
            if (view.show_notice != null) {
              return view.show_notice(options);
            }
          });
        };
      })(this));
      App.pages.message_room.events.input_file.on('clear_error', (function(_this) {
        return function() {
          return _.each(_this.views, function(view) {
            if (view.clear_error != null) {
              return view.clear_error();
            }
          });
        };
      })(this));
      App.pages.message_room.events.input_file.on('clear_notice', (function(_this) {
        return function() {
          return _.each(_this.views, function(view) {
            if (view.clear_notice != null) {
              return view.clear_notice();
            }
          });
        };
      })(this));
      return App.pages.message_room.events.input_file.on('error', (function(_this) {
        return function(options) {
          if ((options != null ? options.title : void 0) != null) {
            _this.modal.set_title(options.title);
          }
          if ((options != null ? options.body : void 0) != null) {
            _this.modal.set_body(options.body);
          }
          if ((options != null ? options.cancel_only : void 0) != null) {
            _this.modal.set_footer_cancel_only();
          }
          return _this.modal.show();
        };
      })(this));
    },
    add: function(view) {
      return this.views.push(view);
    },
    get_items: function() {
      return this.items;
    },
    get_available: function() {
      var availables;
      availables = [];
      _.each(this.items, (function(_this) {
        return function(item) {
          if (!item) {
            return;
          }
          if (_this._is_target(item)) {
            return availables.push(item);
          }
        };
      })(this));
      return availables;
    },
    is_full_files: function() {
      var is_exist_empty;
      if (App.can_use_file_api === false) {
        is_exist_empty = false;
        _.each(this.items, (function(_this) {
          return function(item) {
            var $input;
            if (!item) {
              return;
            }
            $input = item.$el.find('input[type="file"]');
            if ($input.val().length === 0) {
              return is_exist_empty = true;
            }
          };
        })(this));
        if (is_exist_empty === true) {
          return false;
        }
      }
      return true;
    },
    is_appendable: function(num_append) {
      var count;
      count = num_append != null ? num_append + 0 : 0;
      count += this.get_available().length;
      return count <= this.max_upload_cnt;
    },
    _is_target: function(item) {
      var $input;
      if (App.can_use_file_api) {
        return true;
      }
      $input = item.$el.find('input[type="file"]');
      if (!((($input != null ? $input.length : void 0) != null) && $input.length >= 1)) {
        return true;
      }
      return $input.val().length > 0;
    }
  }, Backbone.Events);

  App.pages.message_room.models.message = (function(_super) {
    __extends(message, _super);

    function message() {
      this.parse = __bind(this.parse, this);
      return message.__super__.constructor.apply(this, arguments);
    }

    message.prototype.parse = function(resp, options) {
      if (parseInt(resp.error, 10) === 0) {
        return {
          id: resp.data.message_id
        };
      } else {
        return resp;
      }
    };

    return message;

  })(Backbone.Model);

  App.pages.message_room.views.index = (function(_super) {
    __extends(index, _super);

    function index() {
      this.click_new = __bind(this.click_new, this);
      this.initialize = __bind(this.initialize, this);
      return index.__super__.constructor.apply(this, arguments);
    }

    index.prototype.el = '#main';

    index.prototype.events = {
      'click #js-createNewRoomBtn': 'click_new'
    };

    index.prototype._modal = null;

    index.prototype.initialize = function() {
      var $modal_body;
      this._modal = new App.pages.message_room.views.index.message_room_create_user_select({
        data: getEmbeddedJSON(this.$el.find('#js-user-select-json-data'))
      });
      $modal_body = this._modal.$modal_body;
      this.$modal_name = $modal_body.find('[name="name"]');
      this.$modal_list_filter_input = $modal_body.find('.js-user-select-modal-filter-input');
      return this.$modal_is_changeable = $modal_body.find('[name="is_changeable"]:checked');
    };

    index.prototype.click_new = function(ev) {
      ev.preventDefault();
      this.$modal_name.val('');
      this.$modal_list_filter_input.val('');
      if (App.classes.current_user.is_role_manager()) {
        this.$modal_is_changeable.val([1]);
      }
      return this._modal.launch();
    };

    return index;

  })(Backbone.View);

  App.pages.message_room.views.show = (function(_super) {
    __extends(show, _super);

    function show() {
      this.check_scroll = __bind(this.check_scroll, this);
      this.enable_scroll_check = __bind(this.enable_scroll_check, this);
      this.initialize = __bind(this.initialize, this);
      return show.__super__.constructor.apply(this, arguments);
    }

    show.prototype.el = '#main';

    show.prototype.$title_content = null;

    show.prototype.$unit_content = null;

    show.prototype.head_height = null;

    show.prototype.title_content_height = null;

    show.prototype.initialize = function() {
      var current_user, message_room;
      current_user = new App.classes.current_user.get_data;
      message_room = getEmbeddedJSON($('#js-messageRoomData'));
      if (!message_room) {
        return false;
      }
      this.option_switch = new App.pages.message_room.views.show.option_switch({
        el: '#js-optionSwitch'
      });
      this.user_balloon = new App.pages.message_room.views.show.user_balloon({
        el: '#js-userBalloonContainer'
      });
      this.message_room_edit = new App.pages.message_room.views.show.message_room_edit({
        el: '#js-messageRoomEditSetting',
        current_user: current_user,
        message_room: message_room
      });
      this.message_list = new App.pages.message_room.views.show.message_list({
        el: '#js-listItemMsgTalk',
        current_user: current_user,
        message_room: message_room
      });
      this.read_user_balloon = new App.pages.message_room.views.show.read_user_balloon({
        el: '#js-readUserBalloonContainer'
      });
      this.has_more_message = new App.pages.message_room.views.show.has_more_message({
        el: '#js-hasMoreMessageContainer',
        current_user: current_user,
        message_room: message_room
      });
      this.postArea = [
        new App.pages.message_room.views.show.input_text(), new App.pages.message_room.views.show.input_file({
          current_user: current_user,
          message_room: message_room
        })
      ];
      this.has_more_message.start();
      App.event.global_balloon_mediator.init();
      App.event.global_balloon_mediator.register_document_clicked();
      this.$window = $(window);
      this.$title_content = this.$el.find('.js-message-title-content');
      this.$unit_content = this.$el.find('.js-message-unit-content');
      this.head_height = jQuery('#siteHeader').outerHeight(true);
      this.title_content_height = this.$title_content.outerHeight(true);
      this.$unit_content.css('padding-top', "" + this.title_content_height + "px");
      if (!App.current_user.is_smartphone) {
        this.enable_scroll_check();
        return this.check_scroll();
      }
    };

    show.prototype.enable_scroll_check = function() {
      return this.$window.on('scroll', this.check_scroll);
    };

    show.prototype.check_scroll = function() {
      var height;
      if (this.$window.scrollTop() > this.head_height) {
        return this.$title_content.css('top', 0);
      } else if (this.$window.scrollTop() < this.head_height && this.$window.scrollTop() > 0) {
        height = this.head_height - this.$window.scrollTop();
        return this.$title_content.css('top', height);
      } else if (this.$window.scrollTop() === 0) {
        return this.$title_content.css('top', "" + this.head_height + "px");
      }
    };

    return show;

  })(Backbone.View);

  App.pages.message_room.views.index.message_room_create_modal = (function(_super) {
    __extends(message_room_create_modal, _super);

    function message_room_create_modal() {
      this.initialize = __bind(this.initialize, this);
      return message_room_create_modal.__super__.constructor.apply(this, arguments);
    }

    message_room_create_modal.prototype.initialize = function(options) {
      var modal_class, _i, _len, _ref;
      this.templates = App.pages.message_room.templates.message_room_create_modal;
      if (options != null) {
        if (options.title != null) {
          this.title = options.title;
        }
        if (options.body != null) {
          this.body = options.body;
        }
        if (options.set_buttons != null) {
          this.set_buttons = !!options.set_buttons;
        }
        if (options.cancel_button_label != null) {
          this.cancel_button_label = options.cancel_button_label;
        }
        if (options.decide_button_label != null) {
          this.decide_button_label = options.decide_button_label;
        }
        if ((options.buttons_type != null) && (options.buttons_type === 'cancel_only' || options.buttons_type === 'cancel_decide' || options.buttons_type === 'cancel_delete')) {
          this.buttons_type = options.buttons_type;
        }
        if (options.modal_classes != null) {
          this.$el.removeClass('dialog');
          _ref = options.modal_classes;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            modal_class = _ref[_i];
            this.$el.addClass(modal_class);
          }
        }
      }
      this.$container = $(this.templates.container());
      this.$container_modal_wrapper = this.$container.find('.modalDlgWrapper');
      this.$container_modal_wrapper.append(this.$el);
      App.$body.append(this.$container);
      return this.render();
    };

    return message_room_create_modal;

  })(App.elements.modal);

  App.pages.message_room.views.index.message_room_create_user_select = (function(_super) {
    __extends(message_room_create_user_select, _super);

    function message_room_create_user_select() {
      this.submit = __bind(this.submit, this);
      this.build_modal = __bind(this.build_modal, this);
      return message_room_create_user_select.__super__.constructor.apply(this, arguments);
    }

    message_room_create_user_select.prototype.el = '#main';

    message_room_create_user_select.prototype.build_modal = function() {
      this.modal = new App.pages.message_room.views.index.message_room_create_modal({
        title: 'メッセージルームの登録',
        body: App.pages.message_room.templates.message_room_create_user_select.body({
          show_filter_checkbox: this.use_filter,
          use_mysearch: App.classes.current_user.is_role_manager() || App.classes.current_user.is_role_work() || App.classes.current_user.is_role_gx(),
          is_role_manager: App.classes.current_user.is_role_manager()
        }),
        decide_button_label: '登録する',
        cancel_button_label: '取り消す'
      });
      this.$modal_body = this.modal.get_body();
      this.$modal_list = this.$modal_body.find('.js-user-select-modal-users-list');
      this.$modal_list_selected = this.$modal_body.find('.js-user-select-modal-selected-users-list');
      this.$modal_list_filter_input = this.$modal_body.find('.js-user-select-modal-filter-input');
      this.$modal_list_filter_select = this.$modal_body.find('.js-user-select-modal-fileter-select');
      this.$modal_list_filter_checkbox = this.$modal_body.find('.js-user-select-modal-filter-checkbox');
      this.$modal_list_all_user_select = this.$modal_body.find('.js-user-select-modal-all-user-select');
      this.modal.on('decide', this.submit);
      this.$modal_list_filter_input.on('keyup', this.filter_users);
      this.$modal_list_filter_select.on('change', this.filter_users);
      this.$modal_list_filter_checkbox.on('change', this.filter_users);
      return this.$modal_list_all_user_select.on('click', this.add_all_users);
    };

    message_room_create_user_select.prototype.submit = function() {
      var $form, is_changeable, name, _ref, _ref1;
      message_room_create_user_select.__super__.submit.apply(this, arguments);
      $form = this.$el.find('#js-createNewRoomForm');
      name = ((_ref = this.$modal_body.find('[name="name"]')) != null ? _ref.val() : void 0) ? this.$modal_body.find('[name="name"]').val() : '';
      $form.append('<input type="hidden" name="name" value="' + _.escape(name) + '">');
      this.collection.each((function(_this) {
        return function(user) {
          if (user.get('selected')) {
            return $form.append('<input type="hidden" name="user_id[]" value="' + parseInt(user.id, 10) + '">');
          }
        };
      })(this));
      is_changeable = ((_ref1 = this.$modal_body.find('[name="is_changeable"]:checked')) != null ? _ref1.val() : void 0) ? this.$modal_body.find('[name="is_changeable"]:checked').val() : 1;
      $form.append('<input type="hidden" name="is_changeable" value="' + parseInt(is_changeable, 10) + '">');
      return $form.submit();
    };

    return message_room_create_user_select;

  })(App.elements.user_select.views.user_select);

  App.pages.message_room.views.show.input_file = (function(_super) {
    __extends(input_file, _super);

    function input_file() {
      this.initialize = __bind(this.initialize, this);
      return input_file.__super__.constructor.apply(this, arguments);
    }

    input_file.prototype.el = '#js-inputFile';

    input_file.prototype.is_accept_image = true;

    input_file.prototype.is_accept_file = true;

    input_file.prototype.initialize = function(options) {
      this.current_user = options.current_user;
      this.message_room = options.message_room;
      this.file = new App.classes.file(this.is_accept_image, this.is_accept_file);
      App.pages.message_room.events.input_file.init();
      App.pages.message_room.events.input_file.add(new App.pages.message_room.views.show.input_file.form({
        el: '#js-inputFile',
        current_user: this.current_user,
        message_room: this.message_room,
        file: this.file
      }));
      App.pages.message_room.events.input_file.add(new App.pages.message_room.views.show.input_file.thumbnail({
        el: '#js-thumbnail',
        current_user: this.current_user,
        message_room: this.message_room,
        file: this.file
      }));
      if (App.can_use_file_api) {
        App.pages.message_room.events.input_file.add(new App.pages.message_room.views.show.input_file.container({
          el: '#js-moduleContainer',
          current_user: this.current_user,
          message_room: this.message_room,
          file: this.file
        }));
      } else {
        this.$el.addClass('static');
        this.container = new App.pages.message_room.views.show.input_file.container_legacy({
          el: '#js-moduleContainer',
          current_user: this.current_user,
          message_room: this.message_room,
          file: this.file
        });
        this.container.add_input();
        App.pages.message_room.events.input_file.add(this.container);
      }
      return this;
    };

    return input_file;

  })(Backbone.View);

  App.pages.message_room.views.show.input_text = (function(_super) {
    __extends(input_text, _super);

    function input_text() {
      this.validate = __bind(this.validate, this);
      this.submit = __bind(this.submit, this);
      this.initialize = __bind(this.initialize, this);
      return input_text.__super__.constructor.apply(this, arguments);
    }

    input_text.prototype.el = '#js-inputText';

    input_text.prototype.events = {
      'focus #js-inputTextDefault': 'show_editor',
      'click .js-postButton': 'submit'
    };

    input_text.prototype.$default = null;

    input_text.prototype.$container = null;

    input_text.prototype.$input_text_wysiwyg = null;

    input_text.prototype.$ckeditor = null;

    input_text.prototype.$form = null;

    input_text.prototype.initialize = function() {
      this.$default = this.$el.find('#js-inputTextDefault');
      this.$container = this.$el.find('.js-inputTextContainer');
      this.$input_text_wysiwyg = this.$el.find('.js-inputTextWysiwyg');
      if (!App.current_user.is_smartphone) {
        this.$ckeditor = CKEDITOR.instances[this.$input_text_wysiwyg.attr('name')];
        this.$ckeditor.on('focus', (function(_this) {
          return function() {
            return App.event.global_balloon_mediator.trigger('unregister');
          };
        })(this));
      }
      this.$form = this.$el.find('#js-inputTextForm');
      if ($(':focus').hasClass('js-inputTextDefault')) {
        return this.show_editor();
      }
    };

    input_text.prototype.submit = function() {
      this.$el.find('.error').remove();
      App.functions.disable_form_multiple_submits(this.$form);
      if (this.validate()) {
        return this.$form.submit();
      }
    };

    input_text.prototype.validate = function() {
      var banner, content, is_valid;
      is_valid = true;
      content = this.$ckeditor.getData().replace(/&nbsp;/g, ' ').replace(/<br \/>/g, "\n").replace(/^[\s\u3000]*(.*?)[\s\u3000]*$/g, "$1");
      if (content.length === 0) {
        banner = new App.elements.message_banner.views.message_banner({
          message: 'メッセージを入力してください',
          class_name: 'error',
          has_close: true
        });
        this.$el.append(banner.$el);
        is_valid = false;
      }
      if (content.length > 10000) {
        banner = new App.elements.message_banner.views.message_banner({
          message: 'メッセージ は10000文字以内で入力してください',
          class_name: 'error',
          has_close: true
        });
        this.$el.append(banner.$el);
        is_valid = false;
      }
      return is_valid;
    };

    return input_text;

  })(App.elements.timeline.views.timeline_input_text);

  App.pages.message_room.views.show.message_list = (function(_super) {
    __extends(message_list, _super);

    function message_list() {
      this.add_message = __bind(this.add_message, this);
      this.initialize = __bind(this.initialize, this);
      return message_list.__super__.constructor.apply(this, arguments);
    }

    message_list.prototype.mediator = null;

    message_list.prototype.messages = [];

    message_list.prototype.initialize = function(options) {
      var url2link_class;
      this.mediator = new App.pages.message_room.views.show.message_list.mediator({
        $form: this.$el.find('#js-messageEditForm')
      });
      url2link_class = new App.elements.url2link.views.url2link();
      this.$el.find('.wysiwygContent').each((function(_this) {
        return function(index, element) {
          return $(element).html(url2link_class.done_url2link($(element).html()));
        };
      })(this));
      url2link_class.renew_observe_click_url2link();
      return this.$el.find('#js-messageListContainer').find('li.listItem.messageSection').each((function(_this) {
        return function(idx, _el) {
          var $el;
          $el = $(_el);
          if ($el.find('.setting')) {
            return _this.add_message($el);
          }
        };
      })(this));
    };

    message_list.prototype.add_message = function($el) {
      return this.messages.push(new App.pages.message_room.views.show.message_list.message({
        el: $el.context,
        mediator: this.mediator
      }));
    };

    return message_list;

  })(Backbone.View);

  App.pages.message_room.views.show.has_more_message = (function(_super) {
    __extends(has_more_message, _super);

    function has_more_message() {
      this.stop = __bind(this.stop, this);
      this.start = __bind(this.start, this);
      this.initialize = __bind(this.initialize, this);
      return has_more_message.__super__.constructor.apply(this, arguments);
    }

    has_more_message.prototype.interval = 30000;

    has_more_message.prototype.initialize = function(options) {
      this.message_room = options.message_room;
      this.current_user = options.current_user;
      if ((options != null ? options.interval : void 0) != null) {
        this.interval = options.interval;
      }
      return this.$anchor = this.$el.find('#js-hasMoreMessageAnchor');
    };

    has_more_message.prototype.start = function() {
      return this.timer = setInterval((function(_this) {
        return function() {
          return $.ajax({
            url: "" + App.base_url + "/api/message/room/user/has_more_message",
            data: {
              message_room_id: _this.message_room.id,
              user_id: _this.current_user.id
            }
          }).done(function(res, status, jqXHR) {
            var _ref;
            if ((((_ref = res.data) != null ? _ref.has_more_message : void 0) != null) && res.data.has_more_message === true) {
              _this.$anchor.show();
              return _this.stop();
            }
          }).fail(function(jqXHR, status, error) {});
        };
      })(this), this.interval + 0);
    };

    has_more_message.prototype.stop = function() {
      return clearInterval(this.timer);
    };

    return has_more_message;

  })(Backbone.View);

  App.pages.message_room.views.show.input_file.thumbnail = (function(_super) {
    __extends(thumbnail, _super);

    function thumbnail() {
      this.add_item = __bind(this.add_item, this);
      this.remove_file = __bind(this.remove_file, this);
      this.add_file = __bind(this.add_file, this);
      this.initialize = __bind(this.initialize, this);
      return thumbnail.__super__.constructor.apply(this, arguments);
    }

    thumbnail.prototype.initialize = function(options) {
      this.file = options.file;
      return this.$li_link_add = this.$el.find('#js-inputFileContainerLegacy');
    };

    thumbnail.prototype.add_file = function(options) {
      var item;
      if ((options != null ? options.file : void 0) != null) {
        item = new App.pages.message_room.views.show.input_file.thumbnail.item(options);
      } else {
        item = new App.pages.message_room.views.show.input_file.thumbnail.item_legacy(options);
      }
      this.listenTo(item, 'removed', function() {
        return App.pages.message_room.events.input_file.trigger('remove_item', item);
      });
      return App.pages.message_room.events.input_file.trigger('add_item', item);
    };

    thumbnail.prototype.remove_file = function() {};

    thumbnail.prototype.add_item = function(item) {
      if (!item) {
        return;
      }
      return this.$li_link_add.before(item.$el);
    };

    return thumbnail;

  })(Backbone.View);

  App.pages.message_room.views.show.input_file.container = (function(_super) {
    __extends(container, _super);

    function container() {
      this._validate_file = __bind(this._validate_file, this);
      this.clear_notice = __bind(this.clear_notice, this);
      this.clear_error = __bind(this.clear_error, this);
      this.show_notice = __bind(this.show_notice, this);
      this.show_error = __bind(this.show_error, this);
      this.dragleave = __bind(this.dragleave, this);
      this.drop = __bind(this.drop, this);
      this.dragover = __bind(this.dragover, this);
      this.dragenter = __bind(this.dragenter, this);
      this._reset_input = __bind(this._reset_input, this);
      this.change_file = __bind(this.change_file, this);
      this.click_add = __bind(this.click_add, this);
      this.initialize = __bind(this.initialize, this);
      return container.__super__.constructor.apply(this, arguments);
    }

    container.prototype.events = {
      'click #js-addFileButton': 'click_add',
      'change .js-inputFile': 'change_file'
    };

    container.prototype._active_class_name = 'active';

    container.prototype.initialize = function(options) {
      this.file = options.file;
      this.$el.find('#js-inputFileContainerLegacy').hide();
      this.$container = this.$el.find('#js-inputFileContainer').show().on('dragenter', this.dragenter).on('dragover', this.dragover).on('drop', this.drop);
      this.$input = this.$el.find('.js-inputFile').addClass('attach');
      return this.$shade = this.$el.find('#js-dropAreaShade').on('dragleave', this.dragleave);
    };

    container.prototype.click_add = function(ev) {
      return this.$input.trigger('click');
    };

    container.prototype.change_file = function(ev) {
      var file, files, _i, _len, _ref;
      if ((ev != null ? (_ref = ev.target) != null ? _ref.files : void 0 : void 0) == null) {
        return false;
      }
      if (ev.target.files.length === 0) {
        return false;
      }
      files = ev.target.files;
      for (_i = 0, _len = files.length; _i < _len; _i++) {
        file = files[_i];
        if (this._validate_file(file) === false) {
          this.file.show_file_type_error();
          return false;
        }
        if (App.pages.message_room.events.input_file.is_appendable(1) === false) {
          App.pages.message_room.events.input_file.trigger('error', {
            title: 'これ以上追加することができません',
            body: ['<p>一度にアップロードできる数は', App.pages.message_room.events.input_file.max_upload_cnt + 0, '個までです</p>'].join(''),
            cancel_only: true
          });
          this._reset_input();
          return false;
        }
        App.pages.message_room.events.input_file.trigger('add_file', {
          file: file
        });
      }
      return this._reset_input();
    };

    container.prototype._reset_input = function() {
      var $input;
      this.$input.off('change', this.change_file);
      $input = $('<input class="js-inputFile" type="file" name="file">').addClass('attach');
      this.$input = $input;
      return this.$input.on('change', this.change_file);
    };

    container.prototype.dragenter = function(ev) {
      this.$container.addClass(this._active_class_name);
      ev.stopPropagation();
      ev.preventDefault();
      return false;
    };

    container.prototype.dragover = function(ev) {
      ev.preventDefault();
      ev.originalEvent.dataTransfer.dropEffect = 'copy';
      return false;
    };

    container.prototype.drop = function(ev) {
      var file, files, _i, _len, _ref, _ref1, _results;
      if ((ev != null ? (_ref = ev.originalEvent.dataTransfer) != null ? _ref.files : void 0 : void 0) == null) {
        return false;
      }
      files = ev != null ? (_ref1 = ev.originalEvent.dataTransfer) != null ? _ref1.files : void 0 : void 0;
      this.$container.removeClass(this._active_class_name);
      if (App.pages.message_room.events.input_file.is_appendable(files.length) === false) {
        App.pages.message_room.events.input_file.trigger('error', {
          title: 'これ以上追加することができません',
          body: ['<p>一度にアップロードできる数は', App.pages.message_room.events.input_file.max_upload_cnt + 0, '個までです</p>'].join(''),
          cancel_only: true
        });
        return false;
      }
      _results = [];
      for (_i = 0, _len = files.length; _i < _len; _i++) {
        file = files[_i];
        if (this._validate_file(file)) {
          _results.push(App.pages.message_room.events.input_file.trigger('add_file', {
            file: file
          }));
        } else {
          _results.push(this.file.show_file_type_error());
        }
      }
      return _results;
    };

    container.prototype.dragleave = function(ev) {
      this.$container.removeClass(this._active_class_name);
      ev.stopPropagation();
      ev.preventDefault();
      return false;
    };

    container.prototype.show_error = function(options) {
      var banner;
      banner = new App.elements.message_banner.views.message_banner({
        message: options.message,
        class_name: 'error',
        has_close: true
      });
      return this.$el.append(banner.$el);
    };

    container.prototype.show_notice = function(options) {
      var banner;
      banner = new App.elements.message_banner.views.message_banner({
        message: options.message,
        class_name: 'notice',
        has_close: true
      });
      return this.$el.append(banner.$el);
    };

    container.prototype.clear_error = function() {
      return this.$el.find('.error').remove();
    };

    container.prototype.clear_notice = function() {
      return this.$el.find('.notice').remove();
    };

    container.prototype._validate_file = function(file) {
      var file_type;
      file_type = this.file.get_file_type(file);
      if (file_type.length === 0) {
        return false;
      }
      if ((file.size != null) === false) {
        return false;
      }
      if (this.file.is_accept_file_type(file_type) === false) {
        return false;
      }
      if (this.file.is_accept_file_size(file.size) === false) {
        return false;
      }
      return true;
    };

    return container;

  })(Backbone.View);

  App.pages.message_room.views.show.input_file.container_legacy = (function(_super) {
    __extends(container_legacy, _super);

    function container_legacy() {
      this.clear_notice = __bind(this.clear_notice, this);
      this.clear_error = __bind(this.clear_error, this);
      this.show_notice = __bind(this.show_notice, this);
      this.show_error = __bind(this.show_error, this);
      this.add_input = __bind(this.add_input, this);
      this.click_add = __bind(this.click_add, this);
      this.initialize = __bind(this.initialize, this);
      return container_legacy.__super__.constructor.apply(this, arguments);
    }

    container_legacy.prototype.events = {
      'click #js-addFileAnchor': 'click_add'
    };

    container_legacy.prototype.initialize = function(options) {
      this.file = options.file;
      this.$el.find('#js-inputFileContainer').hide();
      this.$legacy = this.$el.find('#js-inputFileContainerLegacy').show();
      return this.$container = this.$el.find('#js-thumbnail');
    };

    container_legacy.prototype.click_add = function(ev) {
      if (App.pages.message_room.events.input_file.is_full_files() === false) {
        App.pages.message_room.events.input_file.trigger('error', {
          title: 'ファイルを選択してください',
          body: ['ファイルが選択されていない箇所があります。追加するにはファイルを選択してください。'].join(''),
          cancel_only: true
        });
        return false;
      }
      if (App.pages.message_room.events.input_file.is_appendable(1) === false) {
        App.pages.message_room.events.input_file.trigger('error', {
          title: 'これ以上追加することができません',
          body: ['<p>一度にアップロードできる数は', App.pages.message_room.events.input_file.max_upload_cnt + 0, '個までです</p>'].join(''),
          cancel_only: true
        });
        return false;
      }
      this.add_input();
      return false;
    };

    container_legacy.prototype.add_input = function() {
      return App.pages.message_room.events.input_file.trigger('add_file');
    };

    container_legacy.prototype.show_error = function(options) {
      var banner;
      banner = new App.elements.message_banner.views.message_banner({
        message: options.message,
        class_name: 'error',
        has_close: true
      });
      return this.$el.append(banner.$el);
    };

    container_legacy.prototype.show_notice = function(options) {
      var banner;
      banner = new App.elements.message_banner.views.message_banner({
        message: options.message,
        class_name: 'notice',
        has_close: true
      });
      return this.$el.append(banner.$el);
    };

    container_legacy.prototype.clear_error = function() {
      return this.$el.find('.error').remove();
    };

    container_legacy.prototype.clear_notice = function() {
      return this.$el.find('.notice').remove();
    };

    return container_legacy;

  })(Backbone.View);

  App.pages.message_room.views.show.input_file.form = (function(_super) {
    __extends(form, _super);

    function form() {
      this._refresh = __bind(this._refresh, this);
      this._upload = __bind(this._upload, this);
      this._post_content = __bind(this._post_content, this);
      this.submit = __bind(this.submit, this);
      this.initialize = __bind(this.initialize, this);
      return form.__super__.constructor.apply(this, arguments);
    }

    form.prototype.events = {
      'click .js-postButton': 'submit'
    };

    form.prototype.$form = null;

    form.prototype._refresh_root = "" + App.base_url + "/message/room/show";

    form.prototype._api_root_url = "" + App.base_url + "/api/message/room/message";

    form.prototype._csrf_token_key = App.config.security.csrf_token_key;

    form.prototype._is_appendable = true;

    form.prototype._is_processing = false;

    form.prototype._has_error = false;

    form.prototype.initialize = function(options) {
      this.current_user = options.current_user;
      this.message_room = options.message_room;
      this.file = options.file;
      return this.$form = this.$el.find('#js-inputFileForm').fileupload({
        url: [this._api_root_url, 'file'].join('/'),
        dataType: 'json',
        type: 'post',
        autoUpload: !App.can_use_file_api,
        fileInput: null,
        paramName: 'file'
      });
    };

    form.prototype.submit = function() {
      var items, message;
      if (this._has_error) {
        this._is_processing = false;
      }
      if (this._is_processing) {
        return false;
      }
      $('.js-postButton').attr('disabled', true);
      this._is_processing = true;
      App.pages.message_room.events.input_file.trigger('clear_error');
      this._has_error = false;
      items = App.pages.message_room.events.input_file.get_available();
      if (items.length === 0) {
        App.pages.message_room.events.input_file.trigger('show_error', {
          message: 'ファイルが一つも選択されていません'
        });
        this._is_processing = false;
        $('.js-postButton').attr('disabled', false);
        return false;
      }
      message = this._post_content({
        message_room_id: this.message_room.id,
        user_id: this.current_user.id,
        content: this.$el.find('#js-inputFileContent').val()
      });
      if (!this._has_error) {
        return this._upload(message, items, 0);
      }
    };

    form.prototype._post_content = function(params) {
      var message;
      message = new App.pages.message_room.models.message({
        message_room_id: params.message_room_id,
        user_id: params.user_id,
        content: params.content
      });
      message.urlRoot = [this._api_root_url, 'index'].join('/');
      message.save(null, {
        async: false,
        success: (function(_this) {
          return function(model, res) {
            var errors, field;
            if ((res.error != null) && parseInt(res.error, 10) === 1) {
              errors = [];
              for (field in res.message) {
                errors.push(_.values(res.message[field]));
              }
              App.pages.message_room.events.input_file.trigger('show_error', {
                message: errors.join("<br>")
              });
              return _this._has_error = 1;
            }
          };
        })(this),
        error: (function(_this) {
          return function(model, res) {
            App.pages.message_room.events.input_file.trigger('show_error', {
              message: res.responseText
            });
            return _this._has_error = 1;
          };
        })(this)
      });
      return message;
    };

    form.prototype._upload = function(message, items, idx) {
      var caption, file, item;
      if (!items) {
        return;
      }
      if (!idx) {
        idx = 0;
      }
      item = items[idx];
      if (!item) {
        return;
      }
      file = item.get_file();
      caption = item.get_caption();
      if (!file) {
        this._is_processing = false;
        return;
      }
      this.$form.fileupload('option', 'formData', [
        {
          name: this._csrf_token_key,
          value: fuel_fetch_token()
        }, {
          name: 'response_with_html',
          value: App.can_use_file_api ? 0 : 1
        }, {
          name: 'message_id',
          value: message.get('id')
        }, {
          name: 'user_id',
          value: message.get('user_id')
        }, {
          name: 'caption',
          value: caption
        }
      ]);
      this.$form.fileupload('option', 'done', (function(_this) {
        return function(ev, res) {
          var errors, field, responseJSON, _ref, _ref1;
          if (((_ref = res.jqXHR) != null ? _ref.responseJSON : void 0) != null) {
            responseJSON = (_ref1 = res.jqXHR) != null ? _ref1.responseJSON : void 0;
          }
          if ((responseJSON.error != null) && parseInt(responseJSON.error) === 1) {
            item.hide_loader();
            errors = [];
            for (field in responseJSON.message) {
              errors.push(responseJSON.message[field]);
            }
            item.set_errors(errors);
            _this._has_error = true;
            return;
          }
          item.hide_loader();
          item.remove_input();
          if (items.length === idx + 1) {
            return _this._refresh();
          }
          return _this._upload(message, items, idx + 1);
        };
      })(this));
      this.$form.fileupload('option', 'fail', (function(_this) {
        return function(ev, res) {
          var error, responseJSON, _ref, _ref1;
          if (((_ref = res.jqXHR) != null ? _ref.responseJSON : void 0) != null) {
            responseJSON = (_ref1 = res.jqXHR) != null ? _ref1.responseJSON : void 0;
          }
          if (responseJSON) {
            error = {
              message: responseJSON.message
            };
          } else {
            error = {
              message: "エラーが発生しました. お手数ですがはじめからやり直してください"
            };
          }
          item.set_errors(error);
          return item.hide_loader();
        };
      })(this));
      this.$form.fileupload('option', 'always', (function(_this) {
        return function(ev, res) {
          if (_this._has_error) {
            message.urlRoot = [_this._api_root_url, 'index'].join('/');
            message.destroy({
              async: true,
              success: function(model, res) {},
              error: function(model, res) {
                return App.pages.message_room.events.input_file.trigger('show_error', {
                  message: "エラーが発生しました. お手数ですがはじめからやり直してください"
                });
              }
            });
            $('.js-postButton').attr('disabled', false);
            return;
          }
          return _this._is_processing = false;
        };
      })(this));
      item.show_loader();
      if (App.can_use_file_api) {
        return this.$form.fileupload('send', {
          files: [file]
        });
      } else {
        return this.$form.fileupload('add', {
          fileInput: file
        });
      }
    };

    form.prototype._refresh = function() {
      return location.href = [this._refresh_root, this.message_room.id].join('/');
    };

    return form;

  })(Backbone.View);

  App.pages.message_room.views.show.input_file.thumbnail.item = (function(_super) {
    __extends(item, _super);

    function item() {
      this.set_errors = __bind(this.set_errors, this);
      return item.__super__.constructor.apply(this, arguments);
    }

    item.prototype.set_errors = function(errors) {
      var message_banner, messages;
      messages = [];
      $.each(errors, (function(_this) {
        return function(index, error) {
          var err, key, _results;
          if (!(_.isObject(error) || _.isArray(error))) {
            error = [error];
          }
          _results = [];
          for (key in error) {
            err = error[key];
            _results.push(messages.push(err));
          }
          return _results;
        };
      })(this));
      message_banner = new App.elements.message_banner.views.message_banner({
        message: messages.join('<br>'),
        class_name: 'error',
        has_close: true
      });
      this.$el.find('.content').append(message_banner.$el);
      return this;
    };

    return item;

  })(App.elements.timeline.views.elements.input_file_thumbnail);

  App.pages.message_room.views.show.input_file.thumbnail.item_legacy = (function(_super) {
    __extends(item_legacy, _super);

    function item_legacy() {
      this.set_errors = __bind(this.set_errors, this);
      this.remove_input = __bind(this.remove_input, this);
      this.hide_loader = __bind(this.hide_loader, this);
      this.show_loader = __bind(this.show_loader, this);
      this.get_caption = __bind(this.get_caption, this);
      this.get_file = __bind(this.get_file, this);
      this.delete_input = __bind(this.delete_input, this);
      this.click_delete = __bind(this.click_delete, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return item_legacy.__super__.constructor.apply(this, arguments);
    }

    item_legacy.prototype.el = '<li></li>';

    item_legacy.prototype.template = _.template('<div class="content">\n  <p class="name">\n    <input type="file" name="file">\n    <a href="#" class="js-fileDeleteBtn">\n      <i class="icon-xmark"></i>\n    </a>\n  </p>\n  <p class="desc">\n    <input type="text" name="caption" placeholder="ファイルの概要文を入力してください">\n  </p>\n</div>');

    item_legacy.prototype.initialize = function(options) {
      return this.render();
    };

    item_legacy.prototype.render = function() {
      var $compiled;
      $compiled = $(this.template());
      this.$el.append($compiled);
      return $compiled.find('.js-fileDeleteBtn').on('click', this.click_delete);
    };

    item_legacy.prototype.click_delete = function(ev) {
      var $clicked;
      $clicked = $(ev.target);
      this.delete_input($clicked);
      return false;
    };

    item_legacy.prototype.delete_input = function($clicked) {
      var item;
      item = $clicked.parents('li');
      App.pages.message_room.events.input_file.trigger('remove_file', $(item));
      this.trigger('removed', $(item));
      return $(item).remove();
    };

    item_legacy.prototype.get_file = function() {
      return this.$el.find('input[type="file"]');
    };

    item_legacy.prototype.get_caption = function() {
      return this.$el.find('input[name="caption"]').val();
    };

    item_legacy.prototype.show_loader = function() {};

    item_legacy.prototype.hide_loader = function() {};

    item_legacy.prototype.remove_input = function() {};

    item_legacy.prototype.set_errors = function(errors) {
      var message_banner, messages;
      messages = [];
      _.each(errors, (function(_this) {
        return function(error) {
          if (!_.isObject(error) && !_.isArray(error)) {
            error = [error];
          }
          return _.each(error, function(err) {
            return messages.push(err);
          });
        };
      })(this));
      message_banner = new App.elements.message_banner.views.message_banner({
        message: messages.join('<br>'),
        class_name: 'error',
        has_close: true
      });
      return this.$el.find('.content').append(message_banner.$el);
    };

    return item_legacy;

  })(Backbone.View);

  App.pages.message_room.views.show.message_list.mediator = (function() {
    mediator.prototype.last_message = null;

    mediator.prototype.last_balloon = null;

    mediator.prototype.target = null;

    mediator.prototype.last_file = null;

    function mediator(options) {
      this._make_tag = __bind(this._make_tag, this);
      this.form_submit = __bind(this.form_submit, this);
      this.reset_last_file = __bind(this.reset_last_file, this);
      this.switch_file = __bind(this.switch_file, this);
      this.switch_balloon = __bind(this.switch_balloon, this);
      this.switch_message = __bind(this.switch_message, this);
      this.$form = options.$form;
    }

    mediator.prototype.switch_message = function(message) {
      if (this.last_message !== null) {
        this.last_message.reset();
      }
      return this.last_message = message;
    };

    mediator.prototype.switch_balloon = function(balloon) {
      if (this.last_balloon !== null) {
        this.last_balloon.hide();
      }
      return this.last_balloon = balloon;
    };

    mediator.prototype.switch_file = function(file) {
      this.reset_last_file();
      return this.last_file = file;
    };

    mediator.prototype.reset_last_file = function() {
      if (this.last_file !== null) {
        return this.last_file.reset();
      }
    };

    mediator.prototype.form_submit = function(options) {
      this.$form.empty();
      _.each(options.datas, (function(_this) {
        return function(data) {
          if (_.isArray(data.value)) {
            return _.each(data.value, function(kv) {
              if (kv.name != null) {
                return _this.$form.append(_this._make_tag('hidden', kv.name, kv.value));
              }
            });
          } else {
            if (data.name != null) {
              return _this.$form.append(_this._make_tag('hidden', data.name, data.value));
            }
          }
        };
      })(this));
      return this.$form.attr('action', options.url).submit();
    };

    mediator.prototype._make_tag = function(type, name, value) {
      var attrs;
      if (typeof value === "undefined") {
        value = '';
      }
      attrs = ['type="' + type + '"', 'name="' + name + '"', 'value="' + _(value).escape() + '"'];
      return '<input ' + attrs.join(' ') + '>';
    };

    return mediator;

  })();

  App.pages.message_room.views.show.message_list.message = (function(_super) {
    __extends(message, _super);

    function message() {
      this["delete"] = __bind(this["delete"], this);
      this.post = __bind(this.post, this);
      this.reset = __bind(this.reset, this);
      this.hide_modal = __bind(this.hide_modal, this);
      this.show_modal = __bind(this.show_modal, this);
      this.hide_balloon = __bind(this.hide_balloon, this);
      this.show_balloon = __bind(this.show_balloon, this);
      this.click_delete = __bind(this.click_delete, this);
      this.click_edit = __bind(this.click_edit, this);
      this.click_icon = __bind(this.click_icon, this);
      this.initialize = __bind(this.initialize, this);
      return message.__super__.constructor.apply(this, arguments);
    }

    message.prototype.events = {
      'click .setting .js-uiAnchor': 'click_icon',
      'click .js-editBtn': 'click_edit',
      'click .js-deleteBtn': 'click_delete'
    };

    message.prototype.initialize = function(options) {
      var event_mediator;
      this.mediator = options.mediator;
      this.$balloon = this.$el.find('.js-blnOption');
      event_mediator = _.extend({}, Backbone.Events);
      event_mediator.on('post', this.post);
      event_mediator.on('show_modal', this.show_modal);
      event_mediator.on('form_submit', this.mediator.form_submit);
      if (this.$el.attr('data-type') === 'text') {
        this.post_module = new App.pages.message_room.views.show.message_list.message.post_module({
          el: this.$el.find('.postModule').context,
          event_mediator: event_mediator
        });
      } else if (this.$el.attr('data-type') === 'file') {
        this.post_module = new App.pages.message_room.views.show.message_list.message.post_module_file({
          el: this.$el.find('.postModule').context,
          event_mediator: event_mediator
        });
      }
      this.files = new App.pages.message_room.views.show.message_list.message.files({
        el: this.$el.find('.js-listFile').context,
        event_mediator: event_mediator,
        message_mediator: this.mediator
      });
      this.modal = new App.elements.modal({
        decide_button_label: '削除する(元に戻せません)',
        cancel_button_label: '取り消す',
        buttons_type: 'cancel_delete'
      });
      return this.listenTo(this.modal, 'decide', this["delete"]);
    };

    message.prototype.click_icon = function(ev) {
      this.hide_balloon();
      this.show_balloon();
      return false;
    };

    message.prototype.click_edit = function(ev) {
      var message_id, _param, _url;
      ev.preventDefault();
      this.mediator.switch_message(this);
      this.mediator.reset_last_file();
      this.hide_balloon(ev);
      if (App.current_user.is_smartphone) {
        message_id = this.$el.data('message-id');
        _url = [App.base_url, "message/room/message/edit", message_id].join("/");
        _param = "home_url=" + location.href.split(App.base_url + '/')[1];
        location.href = _url + '?' + _param;
      } else {
        this.post_module.mode('edit');
        this.$target = this.$el;
      }
      return false;
    };

    message.prototype.click_delete = function(ev) {
      ev.preventDefault();
      this.mediator.switch_message(this);
      this.hide_balloon(ev);
      this.post_module.mode('show');
      this.show_modal('message', this.$el);
      return false;
    };

    message.prototype.show_balloon = function() {
      App.event.global_balloon_mediator.trigger('register', this.$balloon);
      return false;
    };

    message.prototype.hide_balloon = function() {
      App.event.global_balloon_mediator.trigger('unregister');
      return false;
    };

    message.prototype.show_modal = function(mode, $target) {
      this.mediator.switch_message(this);
      this.mediator.reset_last_file();
      this.hide_balloon();
      if (mode === 'message') {
        this.modal.set_title("メッセージの削除");
        this.modal.set_body('<div class="msgBanner notice"><p><span class="icon"><i class="icon-exclamation"></i></span><span class="txt">削除を実行しようとしています。注意文をご確認ください。</span></p></div>このメッセージを削除します。削除された情報は、元に戻すことができません。<br /><br />よろしいですか？');
      } else if (mode === 'file') {
        this.modal.set_body("ファイルを削除します。よろしいですか？");
      }
      this.$target = $target;
      return this.modal.show();
    };

    message.prototype.hide_modal = function() {
      return this.modal.hide();
    };

    message.prototype.reset = function() {
      this.$balloon.hide();
      return this.post_module.mode('show');
    };

    message.prototype.post = function() {
      this.post_module.clear_error();
      return this.mediator.form_submit({
        url: "" + App.base_url + "/message/room/message/edit",
        datas: [
          {
            name: App.config.security.csrf_token_key,
            value: fuel_fetch_token()
          }, {
            name: 'message_id',
            value: this.$el.data('message-id')
          }, {
            name: 'content',
            value: this.post_module.get_content()
          }
        ]
      });
    };

    message.prototype["delete"] = function() {
      var datas, file_id, message_id, url;
      this.hide_modal();
      if ((message_id = this.$target.data('message-id'))) {
        url = "" + App.base_url + "/message/room/message/delete";
        datas = [
          {
            name: App.config.security.csrf_token_key,
            value: fuel_fetch_token()
          }, {
            name: 'message_id',
            value: this.$target.attr('data-message-id')
          }
        ];
      } else if ((file_id = this.$target.data('file-id'))) {
        url = "" + App.base_url + "/message/room/message/file/delete";
        datas = [
          {
            name: App.config.security.csrf_token_key,
            value: fuel_fetch_token()
          }, {
            name: 'message_file_id',
            value: this.$target.attr('data-file-id')
          }
        ];
      }
      return this.mediator.form_submit({
        url: url,
        datas: datas
      });
    };

    return message;

  })(Backbone.View);

  App.pages.message_room.views.show.message_list.message.post_module = (function(_super) {
    __extends(post_module, _super);

    function post_module() {
      this.clear_error = __bind(this.clear_error, this);
      this.show_error = __bind(this.show_error, this);
      this.validate = __bind(this.validate, this);
      this.get_content = __bind(this.get_content, this);
      this.post = __bind(this.post, this);
      this.mode = __bind(this.mode, this);
      this.initialize = __bind(this.initialize, this);
      return post_module.__super__.constructor.apply(this, arguments);
    }

    post_module.prototype.events = {
      'click .js-postBtn': 'post'
    };

    post_module.event_mediator = null;

    post_module.prototype.initialize = function(options) {
      this.event_mediator = options.event_mediator;
      this.$content = this.$el.find('.wysiwygContent');
      this.$wysiwyg = this.$el.find('.editorWysiwyg');
      if (!App.current_user.is_smartphone) {
        this.$ckeditor = CKEDITOR.instances[this.$el.find('.js-inputTextWysiwyg').attr('id')];
        return this.$ckeditor.on('focus', (function(_this) {
          return function() {
            return App.event.global_balloon_mediator.trigger('unregister');
          };
        })(this));
      }
    };

    post_module.prototype.mode = function(name) {
      if (name === 'show') {
        this.$wysiwyg.hide();
        this.$content.show();
        return this.clear_error();
      } else if (name === 'edit') {
        this.$content.hide();
        return this.$wysiwyg.show();
      }
    };

    post_module.prototype.post = function(ev) {
      return this.event_mediator.trigger('post');
    };

    post_module.prototype.get_content = function() {
      return this.$ckeditor.getData();
    };

    post_module.prototype.validate = function(has_file) {
      var content, is_valid;
      is_valid = true;
      content = this.$ckeditor.getData().replace(/&nbsp;/g, ' ').replace(/<br \/>/g, "\n").replace(/^[\s\u3000]*(.*?)[\s\u3000]*$/g, "$1");
      if (has_file === false && content.length === 0) {
        this.show_error('メッセージを入力してください');
        is_valid = false;
      }
      if (content.length > 10000) {
        this.show_error('メッセージ は10000文字以内で入力してください');
        is_valid = false;
      }
      return is_valid;
    };

    post_module.prototype.show_error = function(message) {
      var banner;
      banner = new App.elements.message_banner.views.message_banner({
        message: message,
        class_name: 'error',
        has_close: true
      });
      return this.$el.find('.msgSection').append(banner.$el);
    };

    post_module.prototype.clear_error = function() {
      return this.$el.find('.error').remove();
    };

    return post_module;

  })(Backbone.View);

  App.pages.message_room.views.show.message_list.message.post_module_file = (function(_super) {
    __extends(post_module_file, _super);

    function post_module_file() {
      this.clear_error = __bind(this.clear_error, this);
      this.show_error = __bind(this.show_error, this);
      this.validate = __bind(this.validate, this);
      this.get_content = __bind(this.get_content, this);
      this.post = __bind(this.post, this);
      this.mode = __bind(this.mode, this);
      this.initialize = __bind(this.initialize, this);
      return post_module_file.__super__.constructor.apply(this, arguments);
    }

    post_module_file.prototype.events = {
      'click .js-postBtn': 'post'
    };

    post_module_file.event_mediator = null;

    post_module_file.prototype.initialize = function(options) {
      this.event_mediator = options.event_mediator;
      this.$content = this.$el.find('.wysiwygContent');
      this.$wysiwyg = this.$el.find('.editorWysiwyg');
      this.$ckeditor = this.$el.find('.js-inputTextWysiwyg');
      return this.$ckeditor.focus((function(_this) {
        return function() {
          return App.event.global_balloon_mediator.trigger('unregister');
        };
      })(this));
    };

    post_module_file.prototype.mode = function(name) {
      if (name === 'show') {
        this.$wysiwyg.hide();
        this.$content.show();
        return this.clear_error();
      } else if (name === 'edit') {
        this.$content.hide();
        return this.$wysiwyg.show();
      }
    };

    post_module_file.prototype.post = function(ev) {
      return this.event_mediator.trigger('post');
    };

    post_module_file.prototype.get_content = function() {
      return this.$ckeditor.val();
    };

    post_module_file.prototype.validate = function(has_file) {
      var content, is_valid;
      is_valid = true;
      content = this.$ckeditor.val().replace(/^[\s\u3000]*(.*?)[\s\u3000]*$/g, "$1");
      if (has_file === false && content.length === 0) {
        this.show_error('メッセージを入力してください');
        is_valid = false;
      }
      if (content.length > 10000) {
        this.show_error('メッセージ は10000文字以内で入力してください');
        is_valid = false;
      }
      return is_valid;
    };

    post_module_file.prototype.show_error = function(message) {
      var banner;
      banner = new App.elements.message_banner.views.message_banner({
        message: message,
        class_name: 'error',
        has_close: true
      });
      return this.$el.find('.msgSection').append(banner.$el);
    };

    post_module_file.prototype.clear_error = function() {
      return this.$el.find('.error').remove();
    };

    return post_module_file;

  })(Backbone.View);

  App.pages.message_room.views.show.message_list.message.files = (function(_super) {
    __extends(files, _super);

    function files() {
      this.add_file = __bind(this.add_file, this);
      this.initialize = __bind(this.initialize, this);
      return files.__super__.constructor.apply(this, arguments);
    }

    files.event_mediator = null;

    files.prototype.files = [];

    files.prototype.initialize = function(options) {
      this.event_mediator = options.event_mediator;
      this.message_mediator = options.message_mediator;
      return this.$el.find('.js-listFile').find('li.js-liFile').each((function(_this) {
        return function(idx, _el) {
          var $el;
          $el = $(_el);
          if ($el.find('.setting')) {
            return _this.add_file($el);
          }
        };
      })(this));
    };

    files.prototype.add_file = function($el) {
      return this.files.push(new App.pages.message_room.views.show.message_list.message.files.file({
        el: $el.context,
        event_mediator: this.event_mediator,
        message_mediator: this.message_mediator
      }));
    };

    return files;

  })(Backbone.View);

  App.pages.message_room.views.show.message_list.message.files.file = (function(_super) {
    __extends(file, _super);

    function file() {
      this.show_error = __bind(this.show_error, this);
      this.validate = __bind(this.validate, this);
      this.post = __bind(this.post, this);
      this.get_captions = __bind(this.get_captions, this);
      this["delete"] = __bind(this["delete"], this);
      this.mode = __bind(this.mode, this);
      this.reset = __bind(this.reset, this);
      this.hide_balloon = __bind(this.hide_balloon, this);
      this.show_balloon = __bind(this.show_balloon, this);
      this.click_edit = __bind(this.click_edit, this);
      this.click_icon = __bind(this.click_icon, this);
      this.initialize = __bind(this.initialize, this);
      return file.__super__.constructor.apply(this, arguments);
    }

    file.prototype.events = {
      'click .setting .js-file-uiAnchor': 'click_icon',
      'click .js-file-editBtn': 'click_edit',
      'click .js-file-deleteBtn': 'delete',
      'click .fileDeleteBtn': 'delete',
      'click .js-file-caption-edit-button': 'post'
    };

    file.event_mediator = null;

    file.prototype.initialize = function(options) {
      this.event_mediator = options.event_mediator;
      this.message_mediator = options.message_mediator;
      this.$caption = this.$el.find('.js-fileCaption');
      this.$input_caption = this.$el.find('.js-fileCaptionInput');
      this.$edit_caption_area = this.$el.find('.js-file-edit-caption');
      this.$btn_delete = this.$el.find('.js-file-deleteBtn');
      this.$parent = this.$el.parent();
      this.message_id = this.$parent.data('message-id');
      this.message_file_id = this.$el.data('file-id');
      return this.$balloon = this.$el.find('.js-file-blnOption');
    };

    file.prototype.click_icon = function(ev) {
      this.hide_balloon();
      this.show_balloon();
      return false;
    };

    file.prototype.click_edit = function(ev) {
      ev.preventDefault();
      if (this.message_mediator.last_message !== null) {
        this.message_mediator.last_message.reset();
      }
      if (this.message_mediator.last_balloon !== null) {
        this.message_mediator.last_balloon.reset();
      }
      this.message_mediator.switch_file(this);
      this.hide_balloon();
      this.mode('edit');
      this.$target = this.$el;
      return false;
    };

    file.prototype.show_balloon = function() {
      return App.event.global_balloon_mediator.trigger('register', this.$balloon);
    };

    file.prototype.hide_balloon = function() {
      return App.event.global_balloon_mediator.trigger('unregister');
    };

    file.prototype.reset = function() {
      this.$balloon.hide();
      this.mode('show');
      if (this.message_mediator.last_message !== null) {
        this.message_mediator.last_message.reset();
      }
      if (this.message_mediator.last_balloon !== null) {
        return this.message_mediator.last_balloon.reset();
      }
    };

    file.prototype.mode = function(name) {
      if (name === 'show') {
        this.$caption.show();
        this.$edit_caption_area.hide();
        return this.$btn_delete.show();
      } else if (name === 'edit') {
        this.$caption.hide();
        this.$edit_caption_area.show();
        return this.$btn_delete.show();
      }
    };

    file.prototype["delete"] = function(ev) {
      ev.preventDefault();
      this.event_mediator.trigger('show_modal', 'file', $(ev.target).parents('.js-liFile'));
      return false;
    };

    file.prototype.get_captions = function() {
      var rets;
      rets = [];
      _.each(this.$input_caption, (function(_this) {
        return function(_input) {
          return rets.push({
            name: $(_input).attr('name'),
            value: $(_input).val()
          });
        };
      })(this));
      return rets;
    };

    file.prototype.post = function() {
      var has_file;
      has_file = this.$input_caption.val() ? true : false;
      if (this.validate()) {
        return this.event_mediator.trigger('form_submit', {
          url: "" + App.base_url + "/message/room/message/file/edit_caption",
          datas: [
            {
              name: App.config.security.csrf_token_key,
              value: fuel_fetch_token()
            }, {
              name: 'message_file_id',
              value: this.message_file_id
            }, {
              name: 'message_id',
              value: this.message_id
            }, {
              name: 'caption',
              value: this.$input_caption.val()
            }
          ]
        });
      }
    };

    file.prototype.validate = function() {
      var is_valid;
      is_valid = true;
      _.each(this.$input_caption, (function(_this) {
        return function(_input) {
          var caption;
          caption = $(_input).val().replace(/^[\s\u3000]*(.*?)[\s\u3000]*$/g, "$1");
          if (caption.length > 10000) {
            _this.show_error('概要文 は10000文字以内で入力してください', $(_input).parents('.js-liFile'));
            return is_valid = false;
          }
        };
      })(this));
      return is_valid;
    };

    file.prototype.show_error = function(message, $target) {
      var banner;
      banner = new App.elements.message_banner.views.message_banner({
        message: message,
        class_name: 'error',
        has_close: true
      });
      return $target.append(banner.$el);
    };

    return file;

  })(Backbone.View);

  App.pages.message_room.views.show.message_room_edit = (function(_super) {
    __extends(message_room_edit, _super);

    function message_room_edit() {
      this.post = __bind(this.post, this);
      this.hide_modal = __bind(this.hide_modal, this);
      this.show_modal = __bind(this.show_modal, this);
      this.hide_balloon = __bind(this.hide_balloon, this);
      this.show_balloon = __bind(this.show_balloon, this);
      this.click_edit = __bind(this.click_edit, this);
      this.click_icon = __bind(this.click_icon, this);
      this.initialize = __bind(this.initialize, this);
      return message_room_edit.__super__.constructor.apply(this, arguments);
    }

    message_room_edit.prototype.events = {
      'click #js-messageRoomEditIcon': 'click_icon',
      'click #js-messageRoomEditBtn': 'click_edit'
    };

    message_room_edit.prototype.initialize = function(options) {
      var message_room;
      message_room = options.message_room;
      this.$form = this.$el.find('#js-messageRoomEditForm');
      App.functions.disable_form_multiple_submits(this.$form);
      this.$balloon = this.$el.find('.blnOption');
      this.modal = new App.pages.message_room.views.show.message_room_edit.modal({
        title: 'メッセージルームの編集',
        body: App.pages.message_room.templates.message_room_edit_modal.body({
          name: message_room.name
        }),
        decide_button_label: '編集する',
        cancel_button_label: '取り消す'
      });
      return this.listenTo(this.modal, 'decide', this.post);
    };

    message_room_edit.prototype.click_icon = function(ev) {
      this.show_balloon();
      return false;
    };

    message_room_edit.prototype.click_edit = function(ev) {
      ev.preventDefault();
      this.hide_balloon();
      return this.show_modal();
    };

    message_room_edit.prototype.show_balloon = function() {
      App.event.global_balloon_mediator.trigger('register', this.$balloon);
      return false;
    };

    message_room_edit.prototype.hide_balloon = function() {
      App.event.global_balloon_mediator.trigger('unregister');
      return false;
    };

    message_room_edit.prototype.show_modal = function() {
      return this.modal.show();
    };

    message_room_edit.prototype.hide_modal = function() {
      return this.modal.hide();
    };

    message_room_edit.prototype.post = function() {
      var name;
      name = this.modal.get_name();
      return this.$form.append('<input type="hidden" name="name" value="' + _(name).escape() + '">').submit();
    };

    return message_room_edit;

  })(Backbone.View);

  App.pages.message_room.views.show.message_room_edit.modal = (function(_super) {
    __extends(modal, _super);

    function modal() {
      this.get_name = __bind(this.get_name, this);
      return modal.__super__.constructor.apply(this, arguments);
    }

    modal.prototype.get_name = function() {
      var $body, _ref;
      $body = this.get_body();
      if ((_ref = $body.find('[name="name"]')) != null ? _ref.val() : void 0) {
        return $body.find('[name="name"]').val();
      } else {
        return '';
      }
    };

    return modal;

  })(App.elements.modal);

  App.pages.message_room.views.show.option_switch = (function(_super) {
    __extends(option_switch, _super);

    function option_switch() {
      this.change = __bind(this.change, this);
      this.initialize = __bind(this.initialize, this);
      return option_switch.__super__.constructor.apply(this, arguments);
    }

    option_switch.prototype.events = {
      'click .js-labelTagPositive': 'change'
    };

    option_switch.prototype.initialize = function(options) {
      this.$form = this.$el.find('#js-optionSwitchForm');
      return App.functions.disable_form_multiple_submits(this.$form);
    };

    option_switch.prototype.change = function(ev) {
      var $_el;
      $_el = $(ev.target);
      $_el.find('input').attr("checked", "checked");
      return this.$form.submit();
    };

    return option_switch;

  })(Backbone.View);

  App.pages.message_room.views.show.read_user_balloon = (function(_super) {
    __extends(read_user_balloon, _super);

    function read_user_balloon() {
      this.show = __bind(this.show, this);
      this.initialize = __bind(this.initialize, this);
      return read_user_balloon.__super__.constructor.apply(this, arguments);
    }

    read_user_balloon.prototype.events = {
      'click #js-readUserBalloonAnchor': 'show'
    };

    read_user_balloon.prototype.initialize = function(options) {
      return this.$balloon = this.$el.find('#js-readUserBalloon');
    };

    read_user_balloon.prototype.show = function(ev) {
      App.event.global_balloon_mediator.trigger('register', this.$balloon);
      return false;
    };

    return read_user_balloon;

  })(Backbone.View);

  App.pages.message_room.views.show.user_balloon = (function(_super) {
    __extends(user_balloon, _super);

    function user_balloon() {
      this.show = __bind(this.show, this);
      this.initialize = __bind(this.initialize, this);
      return user_balloon.__super__.constructor.apply(this, arguments);
    }

    user_balloon.prototype.events = {
      'click #js-userBalloonAnchor': 'show'
    };

    user_balloon.prototype.initialize = function(options) {
      return this.$balloon = this.$el.find('#js-userBalloon');
    };

    user_balloon.prototype.show = function(ev) {
      App.event.global_balloon_mediator.trigger('register', this.$balloon);
      return false;
    };

    return user_balloon;

  })(Backbone.View);

  add_route('/message/room/?', function() {
    return new App.pages.message_room.views.index();
  });

  add_route('/message/room/show/[0-9]+?(/(?:new)*)*?', (function(_this) {
    return function() {
      return new App.pages.message_room.views.show();
    };
  })(this));

  add_route('/message/room/show/[0-9]+?/read(/(?:[0-9])*)*?/*', function() {
    return new App.pages.message_room.views.show();
  });

  App.pages.sandbox = {};

  App.pages.sandbox.capture = {
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.sandbox.capture.models.capture = (function(_super) {
    __extends(capture, _super);

    function capture() {
      return capture.__super__.constructor.apply(this, arguments);
    }

    capture.prototype.urlRoot = "" + App.base_url + "/sandbox/capture_data";

    capture.prototype.defaults = {
      id: null,
      url: '',
      is_loaded: false,
      is_failed: false,
      path: ''
    };

    return capture;

  })(Backbone.Model);

  App.pages.sandbox.capture.collections.captures = (function(_super) {
    __extends(captures, _super);

    function captures() {
      return captures.__super__.constructor.apply(this, arguments);
    }

    captures.prototype.url = "" + App.base_url + "/sandbox/captures";

    captures.prototype.model = App.pages.sandbox.capture.models.capture;

    return captures;

  })(Backbone.Collection);

  App.pages.sandbox.capture.views.index = (function(_super) {
    __extends(index, _super);

    function index() {
      this.render_capture = __bind(this.render_capture, this);
      this.check_capture = __bind(this.check_capture, this);
      this.start_check_capture = __bind(this.start_check_capture, this);
      this.submit_add = __bind(this.submit_add, this);
      this.on_captures_reset = __bind(this.on_captures_reset, this);
      this.initialize = __bind(this.initialize, this);
      return index.__super__.constructor.apply(this, arguments);
    }

    index.prototype.collection = null;

    index.prototype.fetch_interval = 1000;

    index.prototype.$container = null;

    index.prototype.$button = null;

    index.prototype.$input_url = null;

    index.prototype.initialize = function() {
      this.$container = $('#captures');
      this.$input_url = $('#url');
      this.$button = $('#btn');
      this.collection = new App.pages.sandbox.capture.collections.captures();
      this.collection.on('add', this.on_captures_reset);
      this.collection.fetch({
        update: true,
        remove: false,
        add: true
      });
      return this.$button.on('click', this.submit_add);
    };

    index.prototype.on_captures_reset = function(capture) {
      if (!capture.get('is_loaded')) {
        this.start_check_capture(capture);
      }
      return this.render_capture(capture);
    };

    index.prototype.submit_add = function() {
      var capture;
      capture = new App.pages.sandbox.capture.models.capture();
      capture.save({
        url: this.$input_url.val()
      }, {
        success: (function(_this) {
          return function(capture) {
            _this.render_capture(capture);
            return _this.start_check_capture(capture);
          };
        })(this),
        error: (function(_this) {
          return function(capture, xhr) {
            if ((xhr != null ? xhr.response : void 0) != null) {
              return log(xhr.response);
            }
          };
        })(this)
      });
      return false;
    };

    index.prototype.start_check_capture = function(capture) {
      return setTimeout((function(_this) {
        return function() {
          return _this.check_capture(capture);
        };
      })(this), this.fetch_interval);
    };

    index.prototype.check_capture = function(capture) {
      return capture.fetch({
        success: (function(_this) {
          return function(capture) {
            if (!capture.get('is_loaded')) {
              return setTimeout(function() {
                return _this.check_capture(capture);
              }, _this.fetch_interval);
            }
          };
        })(this)
      });
    };

    index.prototype.render_capture = function(capture) {
      var capture_view;
      capture_view = new App.pages.sandbox.capture.views.capture({
        model: capture
      });
      return this.$container.append(capture_view.render());
    };

    return index;

  })(Backbone.View);

  App.pages.sandbox.capture.views.capture = (function(_super) {
    __extends(capture, _super);

    function capture() {
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return capture.__super__.constructor.apply(this, arguments);
    }

    capture.prototype.el = '<li></li>';

    capture.prototype.model = null;

    capture.prototype.templates = {
      capture: _.template('<img src="<%= model.get(\'path\') %>" style="width: 160px;"> <%= model.get(\'url\') %>\n<span class="label"><% if( model.get(\'is_loaded\') ){ %>loaded<% }else{ %>-<% } %></span>\n<span class="label label-warning"><% if( model.get(\'is_failed\') ){ %>failed<% }else{ %>-<% } %></span>')
    };

    capture.prototype.initialize = function() {
      return this.model.on('change', this.render);
    };

    capture.prototype.render = function() {
      this.$el.empty().html(this.templates.capture({
        model: this.model
      }));
      return this.$el;
    };

    return capture;

  })(Backbone.View);

  add_route('/sandbox/capture/?', function() {
    return new App.pages.sandbox.capture.views.index();
  });

  add_route('/startup/account/.+$', function() {
    return $('.js-file-input-container').each((function(_this) {
      return function(index, input) {
        return new App.elements.file_input.views.file_input({
          el: input,
          accept_files: false,
          width: 100,
          height: 100,
          accept_images: true,
          dummy_path: '/assets/img/theme/common/profile_x100.png'
        });
      };
    })(this));
  });

  App.pages.top = {
    views: {}
  };

  App.pages.top.views.index = (function(_super) {
    __extends(index, _super);

    function index() {
      this.user_system_announcement_close = __bind(this.user_system_announcement_close, this);
      this.hide_welcome_message = __bind(this.hide_welcome_message, this);
      this.initialize = __bind(this.initialize, this);
      return index.__super__.constructor.apply(this, arguments);
    }

    index.prototype.el = '#siteContainer';

    index.prototype.events = {
      'click .js-system-announcement-close-a': 'user_system_announcement_close'
    };

    index.prototype._date_json = null;

    index.prototype._modal = null;

    index.prototype.initialize = function() {
      this._data_json = getEmbeddedJSON($('#js-top-index-data-json'));
      if (this._data_json.welcome_message != null) {
        this._modal = new App.elements.modal({
          title: 'ウェルカムメッセージ',
          body: nl2br(this._data_json.welcome_message),
          decide_button_label: 'OK',
          buttons_type: 'decide_only'
        });
        this._modal.set_height_high_class();
        this._modal.show();
        this.listenTo(this._modal, 'decide', this.hide_welcome_message);
        return $.removeCookie('welcome_message', {
          path: this._data_json.cookie_path
        });
      }
    };

    index.prototype.hide_welcome_message = function() {
      return this._modal.hide();
    };

    index.prototype.user_system_announcement_close = function(event) {
      this.user_system_announcement_id = $(event.currentTarget).attr('data-id');
      this.$user_system_announcement_unit_section = $('#js-user-system-announcement-unitSection-' + this.user_system_announcement_id);
      return this.$user_system_announcement_unit_section.fadeOut(200, (function(_this) {
        return function() {
          var date;
          _this.$user_system_announcement_unit_section.remove();
          date = new Date();
          date.setTime(date.getTime() + (2 * 60 * 60 * 1000));
          return $.cookie('usa_' + _this.user_system_announcement_id + '_close', 1, {
            expires: date,
            secure: true
          });
        };
      })(this));
    };

    return index;

  })(Backbone.View);

  add_route('(?:/?|/top/?|/top/index/?)', function() {
    new App.elements.timeline.views.timeline();
    return new App.pages.top.views.index();
  });

  App.pages.user = {};

  App.pages.user.common = {
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.user.common.views.user_list_fixer = (function(_super) {
    __extends(user_list_fixer, _super);

    function user_list_fixer() {
      this.initialize = __bind(this.initialize, this);
      return user_list_fixer.__super__.constructor.apply(this, arguments);
    }

    user_list_fixer.prototype.el = '.js-side-column-user-list-container';

    user_list_fixer.prototype.initialize = function() {
      return $((function(_this) {
        return function() {
          return App.functions.align_height(_this.$el.find('li'), 5);
        };
      })(this));
    };

    return user_list_fixer;

  })(Backbone.View);

  add_route('((?!\/(admin|auth)\/?).)*', function() {
    return new App.pages.user.common.views.user_list_fixer();
  });

  App.pages.user.edit = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.user.edit.views.edit = (function(_super) {
    __extends(edit, _super);

    function edit() {
      this.move_to_anchor = __bind(this.move_to_anchor, this);
      this.initialize = __bind(this.initialize, this);
      return edit.__super__.constructor.apply(this, arguments);
    }

    edit.prototype.el = '.js-user-profile-edit-container';

    edit.prototype.initialize = function() {
      this.$el.find('.js-file-input-container').each((function(_this) {
        return function(index, input) {
          return new App.elements.file_input.views.file_input({
            el: input,
            accept_files: false,
            accept_images: true,
            dummy_path: '/assets/img/theme/common/profile_x140.png'
          });
        };
      })(this));
      App.functions.disable_form_multiple_submits(this.$el.find('form'));
      this.router = new App.pages.user.edit.routes.anchor();
      this.router.on('route:move_to_anchor', this.move_to_anchor);
      return Backbone.history.start();
    };

    edit.prototype.move_to_anchor = function(profile_id) {
      var $profile;
      if (profile_id == null) {
        return true;
      }
      $profile = this.$el.find("#profile-" + profile_id);
      if (!($profile.length > 0)) {
        return true;
      }
      $profile.find(':input:first').focus();
      return false;
    };

    return edit;

  })(Backbone.View);

  App.pages.user.edit.routes.anchor = (function(_super) {
    __extends(anchor, _super);

    function anchor() {
      return anchor.__super__.constructor.apply(this, arguments);
    }

    anchor.prototype.routes = {
      'profile/:id': 'move_to_anchor'
    };

    return anchor;

  })(Backbone.Router);

  add_route('/user/edit/?', function() {
    return new App.pages.user.edit.views.edit();
  });

  App.pages.user.edit.account = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.user.edit.account.templates.name = {
    container: _.template('<% if(data.is_edit_mode) { %>\n  <%= this.edit_name(data) %>\n<% }else{ %>\n  <%= this.name(data) %>\n<% } %>', void 0, {
      variable: 'data'
    }),
    edit_name: _.template('<div class="inner">\n  <h3 class="listItemLabel">名前</h3>\n  <div class="contentArea">\n    <form class="js-user-edit-account-name-form">\n      <div class="boxLabeled">\n        <div class="row">\n          <div class="boxLabeled">\n            <p class="label"><i class="required">*</i>姓</p>\n            <p class="content"><input type="text" class="js-user-edit-account-input-last-name" value="<%= data.name_model.get(\'last_name\') %>"></p>\n          </div>\n          <div class="boxLabeled">\n            <p class="label"><i class="required">*</i>名</p>\n            <p class="content"><input type="text" class="js-user-edit-account-input-first-name" value="<%= data.name_model.get(\'first_name\') %>"></p>\n          </div>\n        </div>\n        <div class="row">\n          <div class="boxLabeled">\n            <p class="label"><i class="required">*</i>姓(かな)</p>\n            <p class="content"><input type="text" class="js-user-edit-account-input-last-name-kana" value="<%= data.name_model.get(\'last_name_kana\') %>"></p>\n          </div>\n          <div class="boxLabeled">\n            <p class="label"><i class="required">*</i>名(かな)</p>\n            <p class="content"><input type="text" class="js-user-edit-account-input-first-name-kana" value="<%= data.name_model.get(\'first_name_kana\') %>"></p>\n          </div>\n        </div>\n      </div>\n      <div class="boxButtonArea ht right">\n        <p><a class="btnM btnBasic js-user-edit-name-cancelBtn" href="">取り消す</a></p>\n        <p><a class="btnM js-user-edit-account-name-sbmitBtn" href="">編集する</a></p>\n      </div>\n    </form>\n  </div>\n</div>', void 0, {
      variable: 'data'
    }),
    name: _.template('<a class="linkEdit inner js-user-edit-name-link" href="">\n  <% if(data.success_message) { %>\n    <div class="msgBanner done">\n     <p>\n       <span class="icon"><i class="icon-check"></i></span>\n       <span class="txt">編集しました。</span>\n    </p>\n    </div>\n  <% } %>\n  <h3 class="listItemLabel">姓名</h3>\n  <span class="editButton"><i class="icon-"></i>編集する</span>\n  <div class="contentArea">\n    <p class="usrFamilyName nameContainer">\n      <span class="rt"><%= data.name_model.get(\'last_name_kana\') %></span>\n      <strong><%= data.name_model.get(\'last_name\') %></strong>\n    </p>\n    <p class="usrFirstName nameContainer">\n     <span class="rt"><%= data.name_model.get(\'first_name_kana\') %></span>\n     <strong><%= data.name_model.get(\'first_name\') %></strong>\n    </p>\n  </div>\n</a>', void 0, {
      variable: 'data'
    }),
    error_message: _.template('<div class="msgBanner error">\n  <% _.each(data.message, function(value, element){  %>\n    <% _.each(value, function(message, validate){  %>\n    <p>\n      <span class="icon"><i class="icon-exclamation"></i></span>\n      <span class="txt"><%= data.convert_message[element] %> は <%= message %></span>\n    </p>\n    <% }); %>\n  <% }); %>\n</div>', void 0, {
      variable: 'data'
    })
  };

  App.pages.user.edit.account.templates.mail = {
    container: _.template('<% if(data.is_edit_mode) { %>\n  <%= this.edit_mail(data) %>\n<% }else{ %>\n  <%= this.mail(data) %>\n<% } %>', void 0, {
      variable: 'data'
    }),
    edit_mail: _.template('<div class="inner">\n		<h3 class="listItemLabel">メールアドレス</h3>\n		<div class="contentArea">\n			<div class="msgBanner done show" style="display: none;"><p><span class="icon"><i class="icon-exclamation"></i></span><span class="txt">メールアドレス追加の認証メールが送信されました。</span></p></div>\n			<form>\n				<div class="boxLabeled">\n					<div class="js-addMailFormArea">\n						<p class="content js-add-new-mail-btn"><i class="icon-add"></i><a class="js-user-edit-mail-add-new-mail-link" href="#">追加する</a></p>\n						<div class="boxLabeled js-addMailForm" style="display:none">\n							<p class="label"></p>\n							<input type="text" name="addMail" id="js-addNewMail" class="" value="" placeholder="認証メールを送信します。" style="width: 70%">\n							<button class="btnM js-addBtn js-user-edit-account-mail-sbmitBtn">追加する</button>\n							<span class="js-loader" style="display: none;"><span class="bkcurtain"></span><img src="/assets/img/loader/21-1.gif"></span>\n						</div>\n					</div>\n					<ul class="content js-emailList"></ul>\n        <div class="boxButtonArea ht right">\n          <p><a class="btnM btnBasic js-user-edit-mail-cancelBtn" href="">取り消す</a></p>\n        </div>\n				</div>\n			</form>\n		</div>\n</div>', void 0, {
      variable: 'data'
    }),
    mail_address: _.template('<span class="address"><%= data.model.get(\'mail\') %></span>\n  <% if(data.model.get(\'is_activate\') == false){ %>\n    <span class="small warning"><i class="icon-caution"></i>認証待ち</span> \n  <% } %>\n<% //登録未完了のアドレスか、最後の一個でなければ削除可能 %>\n<% if(data.model.get(\'is_delete\')){ %>\n  <a class="js-deleteButton" id=<%= data.model.get(\'id\') %> href="#"><i class="icon-xmark"></i></a>\n<% } %>', void 0, {
      variable: 'data'
    }),
    mail: _.template('<a class="linkEdit inner js-user-edit-mail-link" href="">\n  <% if(data.success_message) { %>\n      <div class="msgBanner done">\n        <p>\n         <span class="icon"><i class="icon-check"></i></span>\n         <span class="txt">編集しました。</span>\n      </p>\n      </div>\n  <% } %>\n <h3 class="listItemLabel">メールアドレス</h3>\n  <span class="editButton"><i class="icon-"></i>編集する</span>\n  <div class="contentArea">\n    <ul>\n    <% data.interim_collection.each(function (model){ %>\n       <li class="deactivate">\n        <%= model.get(\'mail\') %>\n        <span class="small warning"><i class="icon-caution"></i>認証待ち</span> \n       </li>\n    <% }) %>\n    <% data.collection.each(function (model){ %>\n       <li class="js-activate"><strong><%= model.get(\'mail\') %></strong></li>\n    <% }) %>\n    </ul>\n  </div>\n</a>', void 0, {
      variable: 'data'
    }),
    error_message: _.template('<% _.each(data.message[\'email_address\'], function(val){ %>\n  <em class="error"><%= val %></em>\n<% }); %>', void 0, {
      variable: 'data'
    }),
    modal_body: _.template('<div class="js-memo-modal">\n  <p>メールアドレス「<%= data.model.get(\'mail\') %>」を削除します。<br />よろしいですか？</p>\n</div>', void 0, {
      variable: 'data'
    })
  };

  App.pages.user.edit.account.templates.password = {
    container: _.template('<% if(data.is_edit_mode) { %>\n  <%= this.edit_password(data) %>\n<% }else{ %>\n  <%= this.password(data) %>\n<% } %>', void 0, {
      variable: 'data'
    }),
    edit_password: _.template('<div class="inner">\n  <h3 class="listItemLabel">パスワード</h3>\n  <div class="contentArea">\n    <form>\n      <div class="msgBanner" id="js-user-edit-account-password-msgBanner" style="display:none"></div>\n      <div class="boxLabeled">\n        <p class="label"><i class="required">*</i>現在のパスワード</p>\n        <p class="content"><input type="password" class="js-user-edit-account-input-old-password" autocomplete="<%= data.model.get(\'password_autocomplete\') %>" placeholder="利用中のパスワードを入力してください。"/></p>\n        <p class="content">このパスワードは、<%= data.model.get(\'password_last_updated_at\') %>に登録されました。</p>\n      </div>\n      <div class="boxLabeled">\n        <p class="label"><i class="required">*</i>新しいパスワード</p>\n        <p class="content"><input type="password" class="js-user-edit-account-input-new-password" autocomplete="<%= data.model.get(\'password_autocomplete\') %>" placeholder="半角英文字・数字・記号のうち2種類以上を利用し、8文字以上で入力してください。" /></p>\n      </div>\n      <div class="boxLabeled">\n        <p class="label"><i class="required">*</i>新しいパスワード（確認）</p>\n        <p class="content"><input type="password" class="js-user-edit-account-input-confirm-new-password" autocomplete="<%= data.model.get(\'password_autocomplete\') %>" placeholder="確認のため、新しいパスワードを再度入力してください" /></p>\n      </div>\n      <div class="boxButtonArea ht right">\n        <p><a class="btnM btnBasic js-user-edit-account-password-cancelBtn" href="">取り消す</a></p>\n        <p><a class="btnM js-user-edit-account-password-sbmitBtn" href="">登録する</a></p>\n      </div>\n    </form>\n  </div>\n</div>', void 0, {
      variable: 'data'
    }),
    password: _.template('<a class="linkEdit inner js-user-edit-password-link" href="">\n  <% if(data.success_message){ %>\n      <div class="msgBanner done">\n       <p>\n         <span class="icon"><i class="icon-check"></i></span>\n         <span class="txt">編集しました。</span>\n      </p>\n      </div>\n  <% } %>\n  <h3 class="listItemLabel">パスワード</h3>\n  <span class="editButton"><i class="icon-"></i>編集する</span>\n  <div class="contentArea">\n    <p>(セキュリティのため表示していません。)</p>\n  </div>\n</a>', void 0, {
      variable: 'data'
    }),
    error_message: _.template('\n<% _.each(data.message, function(obj){  %>\n  <% _.each(obj, function(message){  %>\n    <p>\n      <span class="icon"><i class="icon-exclamation"></i></span>\n      <span class="txt"><%= message %></span>\n    </p>\n  <% }); %>\n<% }); %>', void 0, {
      variable: 'data'
    }),
    succeess_message: _.template('<p>\n  <span class="icon"><i class="icon-check"></i></span>\n  <span class="txt">パスワードを変更しました。</span>\n  <span class="close"><a class="uiAnchor" href=""><i class="icon-xmark"></i></a></span>\n</p>', void 0, {
      variable: 'data'
    })
  };

  App.pages.user.edit.account.templates.lang = {
    container: _.template('<% if(data.is_edit_mode) { %>\n  <%= this.edit_lang(data) %>\n<% }else{ %>\n  <%= this.lang(data) %>\n<% } %>', void 0, {
      variable: 'data'
    }),
    edit_lang: _.template('<div class="inner">\n  <h3 class="listItemLabel">利用言語</h3>\n  <div class="contentArea">\n    <form>\n      <div class="boxLabeled">\n        <p class="content">\n          <select class="js-user-edit-account-lang-select">\n            <option value="ja" <% if(data.model.get(\'language\') == \'ja\'){ %> selected="selected" <% } %> >日本語</option>\n            <option value="en" <% if(data.model.get(\'language\') == \'en\'){ %> selected="selected" <% } %> >English</option>\n          </select>\n        </p>\n      </div>\n      <div class="boxButtonArea ht right">\n        <p><a class="btnM btnBasic js-user-edit-lang-cancelBtn" href="">キャンセル</a></p>\n        <p><a class="btnM js-user-edit-account-lang-sbmitBtn" href="">保存</a></p>\n      </div>\n    </form>\n  </div>\n</div>', void 0, {
      variable: 'data'
    }),
    lang: _.template('<a class="linkEdit inner js-user-edit-lang-link" href="">\n  <h3 class="listItemLabel">利用言語</h3>\n  <span class="editButton"><i class="icon-"></i>編集する</span>\n  <% if(data.success_message) { %>\n    <span class="message done">変更が保存されました</span>\n  <% } %>\n  <div class="contentArea">\n    <p>\n    <% if(data.model.get(\'language\') == \'ja\'){ %>\n      日本語\n    <% }else if(data.model.get(\'language\') == \'en\'){ %>\n      English\n    <% } %>\n    </p>\n  </div>\n</a>', void 0, {
      variable: 'data'
    })
  };

  App.pages.user.edit.account.models.name = (function(_super) {
    __extends(name, _super);

    function name() {
      return name.__super__.constructor.apply(this, arguments);
    }

    name.prototype.urlRoot = App.base_url + '/api/user/edit/account/name';

    name.prototype.defaults = {
      user_id: null,
      first_name: '',
      last_name: '',
      first_name_kana: '',
      last_name_kana: ''
    };

    return name;

  })(Backbone.Model);

  App.pages.user.edit.account.models.mail = (function(_super) {
    __extends(mail, _super);

    function mail() {
      return mail.__super__.constructor.apply(this, arguments);
    }

    mail.prototype.urlRoot = App.base_url + '/api/user/edit/account/mail';

    mail.prototype.defaults = {
      id: null,
      mail: '',
      is_activate: true,
      is_delete: true
    };

    return mail;

  })(Backbone.Model);

  App.pages.user.edit.account.models.interim_mail = (function(_super) {
    __extends(interim_mail, _super);

    function interim_mail() {
      return interim_mail.__super__.constructor.apply(this, arguments);
    }

    interim_mail.prototype.urlRoot = App.base_url + '/api/user/edit/account/interim_mail';

    interim_mail.prototype.defaults = {
      id: null,
      mail: '',
      is_activate: false,
      is_delete: true
    };

    return interim_mail;

  })(Backbone.Model);

  App.pages.user.edit.account.models.password = (function(_super) {
    __extends(password, _super);

    function password() {
      return password.__super__.constructor.apply(this, arguments);
    }

    password.prototype.urlRoot = App.base_url + '/api/user/edit/account/password';

    password.prototype.defaults = {
      id: null,
      old_password: '',
      new_password: '',
      confirm_new_password: '',
      password_last_updated_at: '',
      password_autocomplete: ''
    };

    return password;

  })(Backbone.Model);

  App.pages.user.edit.account.models.lang = (function(_super) {
    __extends(lang, _super);

    function lang() {
      return lang.__super__.constructor.apply(this, arguments);
    }

    lang.prototype.urlRoot = App.base_url + '/api/user/edit/account/lang';

    lang.prototype.defaults = {
      language: null
    };

    return lang;

  })(Backbone.Model);

  App.pages.user.edit.account.collections.mails = (function(_super) {
    __extends(mails, _super);

    function mails() {
      return mails.__super__.constructor.apply(this, arguments);
    }

    mails.model = App.pages.user.edit.account.models.mail;

    return mails;

  })(Backbone.Collection);

  App.pages.user.edit.account.collections.interim_mails = (function(_super) {
    __extends(interim_mails, _super);

    function interim_mails() {
      return interim_mails.__super__.constructor.apply(this, arguments);
    }

    interim_mails.model = App.pages.user.edit.account.models.interim_mail;

    return interim_mails;

  })(Backbone.Collection);

  App.pages.user.edit.account.views.elements = {};

  App.pages.user.edit.account.views.elements.mail_address = (function(_super) {
    __extends(mail_address, _super);

    function mail_address() {
      this.show_delete_modal = __bind(this.show_delete_modal, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return mail_address.__super__.constructor.apply(this, arguments);
    }

    mail_address.prototype.el = '<li></li>';

    mail_address.prototype.events = {
      'click .js-deleteButton': 'show_delete_modal'
    };

    mail_address.prototype.model = null;

    mail_address.prototype.$container = null;

    mail_address.prototype.$error_message_div = $('#js-error-message');

    mail_address.prototype.initialize = function(model) {
      this.model = model;
      this.$container = $('.js-user-edit-mail-container');
      return this.render();
    };

    mail_address.prototype.render = function() {
      this.el.innerHTML = App.pages.user.edit.account.templates.mail.mail_address({
        model: this.model
      });
      if (!this.model.get('is_activate')) {
        return this.$el.addClass('deactivate');
      } else {
        return this.$el.addClass('js-activate');
      }
    };

    mail_address.prototype.show_delete_modal = function(event) {
      this.$container.find('.msgBanner').hide();
      if (this.modal == null) {
        this.modal = new App.elements.modal();
        this.modal.set_title('削除確認');
        this.modal.set_body(App.pages.user.edit.account.templates.mail.modal_body({
          model: this.model
        }));
        this.modal.set_decide_button_label('削除');
        this.modal.set_footer_cancel_delete();
      }
      this.modal.show();
      this.modal.on('decide', (function(_this) {
        return function() {
          var collection, model;
          model = _this.model;
          collection = model.collection;
          return _this.model.destroy({
            success: function() {
              _this.modal.hide();
              _this.$el.slideUp(150, function() {
                return _this.remove();
              });
              if (collection.models.length === 1 && collection.constructor.name === 'mails') {
                collection.models[0].set('is_delete', false);
                return $('.js-emailList li').find("#" + collection.models[0].id).remove();
              }
            },
            error: function(model, error) {
              _this.modal.hide();
              collection.reset(model);
              $('.js-emailList  li.js-activate').remove();
              $('.js-emailList').append(_this.$el);
              $('.js-emailList li').find("#" + collection.models[0].id).remove();
              _this.$error_message_div.find('span.txt').text(error.response.message);
              _this.$error_message_div.show();
              return false;
            }
          });
        };
      })(this));
      return false;
    };

    return mail_address;

  })(Backbone.View);

  App.pages.user.edit.account.views.name = (function(_super) {
    var $box_user_tool_user_name, $side_section_user_name;

    __extends(name, _super);

    function name() {
      this.cancel = __bind(this.cancel, this);
      this.change_name = __bind(this.change_name, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return name.__super__.constructor.apply(this, arguments);
    }

    name.prototype.el = '.js-user-edit-name-container';

    name.prototype.events = {
      'click .js-user-edit-account-name-sbmitBtn': 'change_name',
      'click .js-user-edit-name-cancelBtn': 'cancel'
    };

    name.prototype.name_model = null;

    name.prototype.previous_name_model_data = null;

    name.prototype.account_views = null;

    name.prototype.$container = null;

    name.prototype.$select = null;

    $side_section_user_name = null;

    $box_user_tool_user_name = null;

    name.prototype.initialize = function() {
      var init_data;
      this.name_model = new App.pages.user.edit.account.models.name();
      init_data = getEmbeddedJSON($('.js-user-account-data-json'), {
        first_name: null,
        first_name_kana: null,
        last_name: null,
        last_name_kana: null
      });
      this.name_model.set(init_data);
      this.render();
      this.$side_section_user_name = jQuery('.js-side-section-user-name');
      return this.$box_user_tool_user_name = jQuery('.js-box-user-tool-user-name');
    };

    name.prototype.render = function(is_edit_mode, success_message) {
      this.$el.html(App.pages.user.edit.account.templates.name.container({
        name_model: this.name_model,
        is_edit_mode: is_edit_mode,
        success_message: success_message
      }));
      this.previous_name_model_data = this.name_model.toJSON();
      if (is_edit_mode) {
        this.$el.addClass('js-editMode');
        this.$el.removeClass('js-no-editMode');
      } else {
        this.$el.addClass('js-no-editMode');
        this.$el.removeClass('js-editMode');
      }
      return this.$el;
    };

    name.prototype.change_name = function(event) {
      this.name_model.set('last_name', this.$el.find('.js-user-edit-account-input-last-name').val());
      this.name_model.set('last_name_kana', this.$el.find('.js-user-edit-account-input-last-name-kana').val());
      this.name_model.set('first_name', this.$el.find('.js-user-edit-account-input-first-name').val());
      this.name_model.set('first_name_kana', this.$el.find('.js-user-edit-account-input-first-name-kana').val());
      this.name_model.save({}, {
        success: (function(_this) {
          return function() {
            var first_name, last_name, user_name;
            last_name = _.escape(_this.name_model.get('last_name'));
            first_name = _.escape(_this.name_model.get('first_name'));
            _this.name_model.set({
              'last_name': last_name,
              'first_name': first_name
            });
            _this.render('', true);
            user_name = last_name + ' ' + first_name;
            _this.$side_section_user_name.html(user_name);
            return _this.$box_user_tool_user_name.html(user_name);
          };
        })(this),
        error: (function(_this) {
          return function(xhr, error) {
            var convert_message;
            convert_message = {
              'first_name': '名',
              'first_name_kana': 'かな(名)',
              'last_name': '姓',
              'last_name_kana': 'かな(姓)'
            };
            _this.$error_message = _this.$el.find('.error');
            if (_this.$error_message.length > 0) {
              _this.$error_message.remove();
            }
            _this.name_model.set(_this.previous_name_model_data);
            _this.$name_form = _this.$el.find('.js-user-edit-account-name-form');
            return _this.$name_form.prepend(App.pages.user.edit.account.templates.name.error_message({
              message: error.response,
              convert_message: convert_message
            }));
          };
        })(this)
      });
      return false;
    };

    name.prototype.cancel = function(event) {
      this.name_model.set(this.previous_name_model_data);
      this.render();
      return false;
    };

    return name;

  })(Backbone.View);

  App.pages.user.edit.account.views.mail = (function(_super) {
    __extends(mail, _super);

    function mail() {
      this._display_complete_message = __bind(this._display_complete_message, this);
      this.cancel = __bind(this.cancel, this);
      this.complete_add_new_mail = __bind(this.complete_add_new_mail, this);
      this.add_mail_address = __bind(this.add_mail_address, this);
      this.show_add_new_mail_field = __bind(this.show_add_new_mail_field, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return mail.__super__.constructor.apply(this, arguments);
    }

    mail.prototype.el = '.js-user-edit-mail-container';

    mail.prototype.events = {
      'click .js-user-edit-account-mail-sbmitBtn': 'add_mail_address',
      'click .js-user-edit-mail-add-new-mail-link': 'show_add_new_mail_field',
      'click .js-user-edit-mail-cancelBtn': 'cancel'
    };

    mail.prototype.collection = null;

    mail.prototype.interim_collection = null;

    mail.prototype.account_views = null;

    mail.prototype.$js_loader = null;

    mail.prototype.$addForm = null;

    mail.prototype.$addBtn = null;

    mail.prototype.$field = null;

    mail.prototype.$ul = null;

    mail.prototype.form_flag = true;

    mail.prototype.initialize = function() {
      var address, init_data, model, _i, _len, _ref;
      this.collection = new App.pages.user.edit.account.collections.mails();
      this.interim_collection = new App.pages.user.edit.account.collections.interim_mails();
      init_data = getEmbeddedJSON($('.js-user-account-data-json'), {
        mail: null
      });
      _ref = init_data.mail;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        address = _ref[_i];
        if (address.is_activate) {
          model = new App.pages.user.edit.account.models.mail();
          model.set('id', address.id);
          model.set('mail', address.mail);
          this.collection.add(model);
        } else {
          model = new App.pages.user.edit.account.models.interim_mail();
          model.set('id', address.id);
          model.set('mail', address.mail);
          this.interim_collection.add(model);
        }
      }
      this.$el.keypress((function(_this) {
        return function(event) {
          if (event.target.tagName.toUpperCase() === 'INPUT' && ((event.which && event.which === 13) || (event.keyCode && event.keyCode === 13))) {
            if (_this.form_flag) {
              _this.form_flag = false;
              _this.add_mail_address();
            }
            return false;
          }
        };
      })(this));
      this.listenTo(this.interim_collection, 'add', this._display_complete_message);
      return this.render();
    };

    mail.prototype.render = function(is_edit_mode) {
      this.$el.html(App.pages.user.edit.account.templates.mail.container({
        collection: this.collection,
        interim_collection: this.interim_collection,
        is_edit_mode: is_edit_mode
      }));
      if (is_edit_mode) {
        this.$ul = this.$el.find('.js-emailList');
        this.$el.addClass('js-editMode');
        this.$el.removeClass('js-no-editMode');
        this.interim_collection.each((function(_this) {
          return function(model) {
            var $li;
            $li = new App.pages.user.edit.account.views.elements.mail_address(model);
            return _this.$ul.append($li.$el);
          };
        })(this));
        this.collection.each((function(_this) {
          return function(model) {
            var $li;
            if (_this.collection.length <= 1) {
              model.set('is_delete', false);
            }
            $li = new App.pages.user.edit.account.views.elements.mail_address(model);
            return _this.$ul.append($li.$el);
          };
        })(this));
        return this.$js_loader = this.$el.find('.js-loader');
      } else {
        this.$el.addClass('js-no-editMode');
        return this.$el.removeClass('js-editMode');
      }
    };

    mail.prototype.show_add_new_mail_field = function(event) {
      this.$el.find('.msgBanner').hide();
      this.$addBtn = this.$el.find('.js-add-new-mail-btn');
      this.$addForm = this.$el.find('.js-addMailForm');
      this.$addBtn.slideUp(130, (function(_this) {
        return function(event) {
          return _this.$addForm.slideDown(130, function(event) {
            return _this.$addForm.find('#js-addNewMail')[0].focus();
          });
        };
      })(this));
      App.functions.disable_form_multiple_clicks(this.$addForm.find('button'));
      this.form_flag = true;
      return false;
    };

    mail.prototype.add_mail_address = function() {
      var model;
      this.$js_loader.show();
      model = new App.pages.user.edit.account.models.mail();
      model.set('mail', this.$el.find('#js-addNewMail').val());
      model.save({}, {
        success: (function(_this) {
          return function(xhr, id) {
            return _this.complete_add_new_mail(id);
          };
        })(this),
        error: (function(_this) {
          return function(xhr, error) {
            var $msg;
            _this.$js_loader.hide();
            App.functions.disable_form_multiple_clicks(_this.$addForm.find('button'));
            _this.form_flag = true;
            $msg = _this.$el.find('.label');
            $msg.html(App.pages.user.edit.account.templates.mail.error_message({
              message: error.response
            }));
            return $msg.fadeIn(300);
          };
        })(this)
      });
      return false;
    };

    mail.prototype.complete_add_new_mail = function(id) {
      this.$field = this.$addForm.find('#js-addNewMail');
      return this.$addForm.slideUp(130, (function(_this) {
        return function() {
          _this.$js_loader.hide();
          _this.model = new App.pages.user.edit.account.models.interim_mail();
          _this.model.set('id', id.toString());
          _this.model.set('mail', _this.$field.val());
          _this.interim_collection.add(_this.model);
          _this.$field.val('');
          return _this.$addBtn.slideDown(130, function() {
            return {};
          });
        };
      })(this));
    };

    mail.prototype.cancel = function(event) {
      this.render();
      return false;
    };

    mail.prototype._display_complete_message = function() {
      var $address, mail_address_view;
      mail_address_view = new App.pages.user.edit.account.views.elements.mail_address(this.model, true);
      $address = mail_address_view.$el;
      $address.css({
        position: 'relative',
        left: '100%',
        opacity: 0
      });
      $address.prependTo(this.$ul).animate({
        left: 0,
        opacity: 1
      }, {
        duration: 350,
        complete: (function(_this) {
          return function() {
            return {};
          };
        })(this)
      });
      this.$el.find('.msgBanner').show();
      return this.$addForm.find('.error').hide();
    };

    return mail;

  })(Backbone.View);

  App.pages.user.edit.account.views.password = (function(_super) {
    __extends(password, _super);

    function password() {
      this.add_password_address = __bind(this.add_password_address, this);
      this.cancel_address = __bind(this.cancel_address, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return password.__super__.constructor.apply(this, arguments);
    }

    password.prototype.el = '.js-user-edit-password-container';

    password.prototype.events = {
      'click .js-user-edit-account-password-sbmitBtn': 'add_password_address',
      'click .js-user-edit-account-password-cancelBtn': 'cancel_address',
      'click .js-user-edit-account-new-password-link': 'show_add_new_password_field'
    };

    password.prototype.model = null;

    password.prototype.$msgBanner = null;

    password.prototype.initialize = function() {
      var init_data;
      this.model = new App.pages.user.edit.account.models.password();
      init_data = getEmbeddedJSON($('.js-user-account-data-json'), {
        password_last_updated_at: null,
        password_autocomplete: 'on'
      });
      this.model.set(init_data);
      return this.render();
    };

    password.prototype.render = function(is_edit_mode, success_message) {
      this.$el.html(App.pages.user.edit.account.templates.password.container({
        model: this.model,
        is_edit_mode: is_edit_mode,
        success_message: success_message
      }));
      if (is_edit_mode) {
        return this.$el.addClass('js-editMode');
      } else {
        return this.$el.removeClass('js-editMode');
      }
    };

    password.prototype.cancel_address = function() {
      this.render();
      return false;
    };

    password.prototype.add_password_address = function() {
      this.model.set('old_password', this.$el.find('.js-user-edit-account-input-old-password').val());
      this.model.set('new_password', this.$el.find('.js-user-edit-account-input-new-password').val());
      this.model.set('confirm_new_password', this.$el.find('.js-user-edit-account-input-confirm-new-password').val());
      this.$msgBanner = this.$el.find('#js-user-edit-account-password-msgBanner');
      this.model.save({}, {
        success: (function(_this) {
          return function() {
            _this.render('', true);
            _this.$msgBanner.removeClass('error');
            return _this.$msgBanner.hide();
          };
        })(this),
        error: (function(_this) {
          return function(xhr, error) {
            _this.$msgBanner.html(App.pages.user.edit.account.templates.password.error_message({
              message: error.response
            }));
            _this.$msgBanner.addClass('error');
            _this.$msgBanner.removeClass('done');
            return _this.$msgBanner.show();
          };
        })(this)
      });
      return false;
    };

    return password;

  })(Backbone.View);

  App.pages.user.edit.account.views.lang = (function(_super) {
    __extends(lang, _super);

    function lang() {
      this.cancel = __bind(this.cancel, this);
      this.change_lang = __bind(this.change_lang, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return lang.__super__.constructor.apply(this, arguments);
    }

    lang.prototype.el = '.js-user-edit-lang-container';

    lang.prototype.events = {
      'click .js-user-edit-account-lang-sbmitBtn': 'change_lang',
      'click .js-user-edit-lang-cancelBtn': 'cancel'
    };

    lang.prototype.model = null;

    lang.prototype.initialize = function() {
      var init_data;
      this.model = new App.pages.user.edit.account.models.lang();
      init_data = getEmbeddedJSON($('.js-user-account-data-json'), {
        language: null
      });
      this.model.set(init_data);
      return this.render();
    };

    lang.prototype.render = function(is_edit_mode, success_message) {
      this.$el.html(App.pages.user.edit.account.templates.lang.container({
        model: this.model,
        is_edit_mode: is_edit_mode,
        success_message: success_message
      }));
      if (is_edit_mode) {
        this.$el.addClass('js-editMode');
        this.$el.removeClass('js-no-editMode');
      } else {
        this.$el.addClass('js-no-editMode');
        this.$el.removeClass('js-editMode');
      }
      return this.$el;
    };

    lang.prototype.change_lang = function(event) {
      this.model.set('language', this.$el.find('.js-user-edit-account-lang-select').val());
      this.model.save({}, {
        success: (function(_this) {
          return function() {
            return _this.render('', true);
          };
        })(this),
        error: (function(_this) {
          return function(xhr, error) {};
        })(this)
      });
      return false;
    };

    lang.prototype.cancel = function(event) {
      this.render();
      return false;
    };

    return lang;

  })(Backbone.View);

  App.pages.user.edit.account.views.account = (function(_super) {
    __extends(account, _super);

    function account() {
      this.edit_password = __bind(this.edit_password, this);
      this.edit_mail = __bind(this.edit_mail, this);
      this.edit_name = __bind(this.edit_name, this);
      this.initialize = __bind(this.initialize, this);
      return account.__super__.constructor.apply(this, arguments);
    }

    account.prototype.el = '.settingList';

    account.prototype.events = {
      'click .js-user-edit-name-link': 'edit_name',
      'click .js-user-edit-mail-link': 'edit_mail',
      'click .js-user-edit-password-link': 'edit_password'
    };

    account.prototype.name_view = null;

    account.prototype.mail_view = null;

    account.prototype.password_view = null;

    account.prototype.initialize = function() {
      this.name_vies = new App.pages.user.edit.account.views.name();
      this.mail_view = new App.pages.user.edit.account.views.mail();
      return this.password_view = new App.pages.user.edit.account.views.password();
    };

    account.prototype.edit_name = function() {
      this.name_vies.render(true);
      this.mail_view.render();
      this.password_view.render();
      return false;
    };

    account.prototype.edit_mail = function() {
      this.name_vies.render();
      this.mail_view.render(true);
      this.password_view.render();
      return false;
    };

    account.prototype.edit_password = function() {
      this.name_vies.render();
      this.mail_view.render();
      this.password_view.render(true);
      return false;
    };

    return account;

  })(Backbone.View);

  add_route('/user/edit_account/?', function() {
    return new App.pages.user.edit.account.views.account();
  });

  App.pages.user.edit_communication = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.user.edit_communication.views.edit_communication = (function(_super) {
    __extends(edit_communication, _super);

    function edit_communication() {
      this.move_to_anchor = __bind(this.move_to_anchor, this);
      this.initialize = __bind(this.initialize, this);
      return edit_communication.__super__.constructor.apply(this, arguments);
    }

    edit_communication.prototype.el = '.js-user-communication-profile-edit-container';

    edit_communication.prototype.initialize = function() {
      App.functions.disable_form_multiple_submits(this.$el.find('form'));
      this.router = new App.pages.user.edit_communication.routes.anchor();
      this.router.on('route:move_to_anchor', this.move_to_anchor);
      return Backbone.history.start();
    };

    edit_communication.prototype.move_to_anchor = function(profile_id) {
      var $profile;
      if (profile_id == null) {
        return true;
      }
      $profile = this.$el.find("#profile-" + profile_id);
      if (!($profile.length > 0)) {
        return true;
      }
      $profile.find(':input:first').focus();
      return false;
    };

    return edit_communication;

  })(Backbone.View);

  App.pages.user.edit_communication.routes.anchor = (function(_super) {
    __extends(anchor, _super);

    function anchor() {
      return anchor.__super__.constructor.apply(this, arguments);
    }

    anchor.prototype.routes = {
      'profile/:id': 'move_to_anchor'
    };

    return anchor;

  })(Backbone.Router);

  add_route('/user/edit_communication/?', function() {
    return new App.pages.user.edit_communication.views.edit_communication();
  });

  App.pages.user.file = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.user.file.collections.feed_files = (function(_super) {
    __extends(feed_files, _super);

    function feed_files() {
      this.url = __bind(this.url, this);
      return feed_files.__super__.constructor.apply(this, arguments);
    }

    feed_files.prototype.url = function() {
      return "" + App.base_url + "/api/user/file/feed_files/" + this.community_id + "/" + this.limit + "/" + this.length + "?v=" + ((new Date()).getTime());
    };

    return feed_files;

  })(App.pages.community.file.collections.feed_files);

  App.pages.user.file.views.files = (function(_super) {
    __extends(files, _super);

    function files() {
      this.get_empty_collection = __bind(this.get_empty_collection, this);
      return files.__super__.constructor.apply(this, arguments);
    }

    files.prototype.el = '.js-user-file-list-container';

    files.prototype.table_selector = '.js-user-file-list-table';

    files.prototype.option_data_selector = '.js-user-file-list-options';

    files.prototype.initiali_data_selector = '.js-user-file-list-initial-data';

    files.prototype.user_id = 0;

    files.prototype.get_empty_collection = function(options) {
      if (options != null ? options.user_id : void 0) {
        this.user_id = options.user_id;
      }
      return new App.pages.user.file.collections.feed_files([], {
        community_id: this.user_id
      });
    };

    return files;

  })(App.pages.community.file.views.files);

  add_route('/user/file(/[0-9]+)/?', function() {
    return new App.pages.user.file.views.files();
  });

  App.pages.user.image = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.user.image.collections.feed_images = (function(_super) {
    __extends(feed_images, _super);

    function feed_images() {
      this.url = __bind(this.url, this);
      return feed_images.__super__.constructor.apply(this, arguments);
    }

    feed_images.prototype.url = function() {
      return "" + App.base_url + "/api/user/image/feed_images/" + this.community_id + "/" + this.limit + "/" + this.length + "?v=" + ((new Date()).getTime());
    };

    return feed_images;

  })(App.pages.community.image.collections.feed_images);

  App.pages.user.image.views.image = (function(_super) {
    __extends(image, _super);

    function image() {
      this.get_empty_collection = __bind(this.get_empty_collection, this);
      return image.__super__.constructor.apply(this, arguments);
    }

    image.prototype.el = '.js-user-image-list-container';

    image.prototype.initial_data_selector = '.js-user-image-list-initial-data';

    image.prototype.option_data_selector = '.js-user-image-list-options';

    image.prototype.show_related_icons = true;

    image.prototype.user_id = 0;

    image.prototype.get_empty_collection = function(options) {
      if ((options != null ? options.user_id : void 0) != null) {
        this.user_id = options.user_id;
      }
      return new App.pages.user.image.collections.feed_images([], {
        community_id: this.user_id
      });
    };

    return image;

  })(App.pages.community.image.views.image);

  add_route('/user/image(/[0-9]+)/?', function() {
    return new App.pages.user.image.views.image();
  });

  App.pages.user.index = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.user.index.templates.user = _.template('<p class="profile">\n  <a href="<%= path %>">\n    <img class="thumb" src="<%= model.get(\'icon_x140\') %>" alt="<%- model.get(\'name\') %>">\n  </a>\n</p>\n<p class="name">\n  <a href="<%= path %>"><%= type %><%- model.get(\'name\') %></a>\n</p>');

  App.pages.user.index.models.user = (function(_super) {
    __extends(user, _super);

    function user() {
      return user.__super__.constructor.apply(this, arguments);
    }

    user.prototype.defaults = {
      id: null,
      name: '',
      kana: '',
      icon_x40: '',
      icon_x100: '',
      icon_x140: '',
      type: 'prospective'
    };

    return user;

  })(Backbone.Model);

  App.pages.user.index.collections.users = (function(_super) {
    __extends(users, _super);

    function users() {
      return users.__super__.constructor.apply(this, arguments);
    }

    users.prototype.model = App.pages.user.index.models.user;

    return users;

  })(Backbone.Collection);

  App.pages.user.index.views.user = (function(_super) {
    __extends(user, _super);

    function user() {
      this.model_destroied = __bind(this.model_destroied, this);
      this.render = __bind(this.render, this);
      this.initialize = __bind(this.initialize, this);
      return user.__super__.constructor.apply(this, arguments);
    }

    user.prototype.el = '<li></li>';

    user.prototype.model = null;

    user.prototype.initialize = function() {
      this.model.on('remove', this.model_destroied);
      return this.render();
    };

    user.prototype.render = function() {
      var type;
      switch (this.model.get('type')) {
        case 'prospective':
          type = '<i class="icon-member0"></i>';
          break;
        case 'common_employee':
        case 'new_employee':
          type = '<i class="icon-member2"></i>';
          break;
        case 'personal':
          type = '<i class="icon-member1"></i>';
          break;
        default:
          type = '<i class="icon-member0"></i>';
      }
      this.$el.html(App.pages.user.index.templates.user({
        model: this.model,
        path: "" + App.base_url + "/user/" + (this.model.get('id')),
        type: type
      }));
      return this.$el;
    };

    user.prototype.model_destroied = function() {
      return this.remove();
    };

    return user;

  })(Backbone.View);

  App.pages.user.index.views.index = (function(_super) {
    __extends(index, _super);

    function index() {
      this.fetch_complete = __bind(this.fetch_complete, this);
      this.fetch_collection = __bind(this.fetch_collection, this);
      this.select_changed = __bind(this.select_changed, this);
      this.remove_user_view = __bind(this.remove_user_view, this);
      this.add_user_view = __bind(this.add_user_view, this);
      this.align_height = __bind(this.align_height, this);
      this.initialize = __bind(this.initialize, this);
      return index.__super__.constructor.apply(this, arguments);
    }

    index.prototype.el = '.js-user-index-container';

    index.prototype.events = {
      'change .js-user-index-group-select': 'select_changed'
    };

    index.prototype.limit = 20;

    index.prototype.offset = 0;

    index.prototype.user_type_name = null;

    index.prototype.collection = null;

    index.prototype.user_views = null;

    index.prototype.scroll = null;

    index.prototype.$container = null;

    index.prototype.$select = null;

    index.prototype.data_end = false;

    index.prototype.initialize = function() {
      var init_data;
      this.offset = 0;
      this.user_views = {};
      this.$box_blank_msg = $('.js-user-index-users-box-blank-msg');
      this.$container = $('.js-user-index-users-container');
      this.$select = $('.js-user-index-group-select');
      this.collection = new App.pages.user.index.collections.users();
      this.collection.on('add', this.add_user_view);
      init_data = getEmbeddedJSON($('.js-user-index-users-data-json'));
      this.collection.add(init_data);
      this.scroll = new App.classes.scroll();
      this.scroll.on('bottom', (function(_this) {
        return function() {
          return _this.fetch_collection(_this.user_type_name);
        };
      })(this));
      this.scroll.enable();
      return this.fetch_collection(this.user_type_name);
    };

    index.prototype.align_height = function() {
      return App.functions.align_height(this.$container.find('li'), 6);
    };

    index.prototype.add_user_view = function(user) {
      var user_view;
      user_view = new App.pages.user.index.views.user({
        model: user
      });
      if (this.collection.length === this.$container.find('li').length + 1) {
        user_view.$el.find('.profile img').on('load', (function(_this) {
          return function() {
            return _this.align_height();
          };
        })(this));
      }
      this.$container.append(user_view.$el);
      return this.user_views[user.cid] = user_view;
    };

    index.prototype.remove_user_view = function(user) {
      if (this.user_views[user.cid] != null) {
        this.user_views[user.cid].remove();
        delete this.user_views[user.cid];
        return this.align_height();
      }
    };

    index.prototype.select_changed = function() {
      var user_type_name;
      this.data_end = false;
      if (this.$container.css('display') === 'none') {
        this.$box_blank_msg.hide();
        this.$container.show();
      }
      $('#search_user_name').val('');
      user_type_name = this.$select.val();
      if (user_type_name !== this.user_type_name) {
        this.fetch_collection(user_type_name);
      }
      return false;
    };

    index.prototype.fetch_collection = function(user_type_name) {
      var fetch_options, search_user_name;
      if (user_type_name == null) {
        user_type_name = null;
      }
      if (!this.data_end) {
        this.scroll.disable();
        fetch_options = {
          data: {},
          update: true,
          add: true,
          type: 'post',
          success: this.fetch_complete
        };
        fetch_options.data[App.config.security.csrf_token_key] = fuel_fetch_token();
        if (user_type_name === this.user_type_name) {
          fetch_options.remove = false;
        } else {
          this.collection.each((function(_this) {
            return function(user) {
              return _this.remove_user_view(user);
            };
          })(this));
          this.collection.reset();
          this.offset = 0;
          this.user_type_name = user_type_name;
          fetch_options.remove = true;
        }
        search_user_name = $('#search_user_name').val();
        fetch_options.url = "" + App.base_url + "/api/user/users/" + this.limit + "/" + this.offset;
        fetch_options.url += "/" + this.user_type_name;
        fetch_options.url += "/" + encodeURI(search_user_name);
        return this.collection.fetch(fetch_options);
      }
    };

    index.prototype.fetch_complete = function(users, data) {
      if (((data != null ? data.length : void 0) != null) && data.length > 0) {
        if (this.offset === this.collection.length) {
          return this.data_end = true;
        } else {
          this.offset = this.collection.length;
          this.scroll.check_scroll_bar();
          return this.scroll.enable();
        }
      } else {
        return this.data_end = true;
      }
    };

    return index;

  })(Backbone.View);

  add_route('/user(/index)?/?', function() {
    return new App.pages.user.index.views.index();
  });

  App.pages.user.profile = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.user.profile.models.user_profile = (function(_super) {
    __extends(user_profile, _super);

    function user_profile() {
      this.urlRoot = __bind(this.urlRoot, this);
      return user_profile.__super__.constructor.apply(this, arguments);
    }

    user_profile.prototype.urlRoot = function() {
      return "" + App.base_url + "/api/user/profile/communication_profile";
    };

    user_profile.prototype.defaults = {
      id: null
    };

    return user_profile;

  })(Backbone.Model);

  App.pages.user.profile.views.communication_profile = (function(_super) {
    __extends(communication_profile, _super);

    function communication_profile() {
      this.hide_balloon = __bind(this.hide_balloon, this);
      this.delete_feed_complete = __bind(this.delete_feed_complete, this);
      this.delete_feed = __bind(this.delete_feed, this);
      this.delete_feed_confirm = __bind(this.delete_feed_confirm, this);
      this.navigate_to_feed_page = __bind(this.navigate_to_feed_page, this);
      this.initialize = __bind(this.initialize, this);
      return communication_profile.__super__.constructor.apply(this, arguments);
    }

    communication_profile.prototype.el = null;

    communication_profile.prototype.model = null;

    communication_profile.prototype.feed_model = null;

    communication_profile.prototype.modal = null;

    communication_profile.prototype.error_modal = null;

    communication_profile.prototype.star_url_base = App.elements.timeline.views.timeline_feed.prototype.star_url_base;

    communication_profile.prototype.initialize = function() {
      var feed_data, links, user_profile_id;
      feed_data = getEmbeddedJSON(this.$el.find('.js-user-profile-communication-profiles-feed-data'));
      user_profile_id = this.$el.find('.js-user-profile-communication-profiles-item-footer').attr('data-user-profile-id');
      if (!(user_profile_id && feed_data)) {
        return false;
      }
      this.model = new App.pages.user.profile.models.user_profile({
        id: user_profile_id
      });
      this.feed_model = new App.elements.timeline.models.feed();
      this.feed_model.set(this.feed_model.parse(feed_data));
      this.stars_view = new App.elements.timeline.views.star({
        model: this.feed_model,
        url: "" + (this.star_url_base()) + "/" + (this.feed_model.get('id'))
      });
      this.$el.find('.js-user-profile-communication-stars-container').append(this.stars_view.$el);
      this.comment_button_view = new App.elements.timeline.views.comment_button({
        collection: this.feed_model.get('comments'),
        can_comment: this.feed_model.get('can_comment', true)
      });
      this.comment_button_view.on('clicked', this.navigate_to_feed_page);
      this.$el.find('.js-user-profile-communication-comments-container').append(this.comment_button_view.$el);
      if (+this.$el.attr('data-can-edit') === 1) {
        links = [
          {
            path: "" + App.base_url + "/user/edit_communication/" + (this.$el.attr('data-profile-id')),
            label: '編集する'
          }
        ];
      }
      if (+this.$el.attr('data-can-delete') === 1) {
        if (links == null) {
          links = [];
        }
        links.push({
          path: "#",
          label: '削除する',
          event_name: 'delete'
        });
      }
      if (links != null) {
        this.balloon = new App.elements.setting_balloon.views.setting_balloon({
          links: links
        });
        this.balloon.on('delete', this.delete_feed_confirm);
        return this.$el.find('.js-user-profile-communication-setting-container').append(this.balloon.$el);
      }
    };

    communication_profile.prototype.navigate_to_feed_page = function() {
      location.href = "" + App.base_url + "/feed/show/" + (this.feed_model.get('id'));
      return false;
    };

    communication_profile.prototype.delete_feed_confirm = function() {
      if (this.modal == null) {
        this.modal = new App.elements.modal();
        this.modal.add_container_class('jsModal-Dialog');
        this.modal.set_title('投稿の削除');
        this.modal.set_body(App.elements.timeline.templates.feed_modals.delete_body());
        this.modal.set_footer_cancel_delete(null, '削除する(元に戻せません)');
      }
      this.modal.show();
      this.modal.on('cancel', this.hide_balloon);
      return this.modal.on('decide', (function(_this) {
        return function() {
          return _this.delete_feed(_this.modal);
        };
      })(this));
    };

    communication_profile.prototype.delete_feed = function(modal) {
      if (modal == null) {
        modal = null;
      }
      return this.model.destroy({
        success: (function(_this) {
          return function() {
            return _this.delete_feed_complete(modal);
          };
        })(this),
        error: (function(_this) {
          return function(xhr, error) {
            var error_message;
            if (!_this.delete_error_modal) {
              _this.delete_error_modal = new App.elements.modal();
              _this.delete_error_modal.set_title('削除に失敗しました');
              error_message = new App.elements.message_banner.views.message_banner({
                message: 'フィードの削除に失敗しました。既に削除されている可能性があります'
              });
              error_message.change_to_error();
              _this.delete_error_modal.$modal_body.append(error_message.$el);
              _this.delete_error_modal.set_footer_cancel_only('閉じる');
            }
            return _this.delete_error_modal.show();
          };
        })(this)
      });
    };

    communication_profile.prototype.delete_feed_complete = function(modal) {
      var linkMores;
      if (modal == null) {
        modal = null;
      }
      if (modal != null) {
        modal.hide();
      }
      this.$el.find('.wysiwygContent').remove();
      this.$el.find('.js-user-profile-communication-profiles-item-footer').remove();
      linkMores = [
        {
          url: "" + App.base_url + "/user/edit_communication/" + (this.$el.attr('data-profile-id')),
          anchor: "" + App.elements.timeline.config.linkMore.edit_communication_anchor_for_my_profile
        }
      ];
      return this.$el.find('.statement').html(App.elements.timeline.templates.feed.link_more({
        linkMores: linkMores
      }));
    };

    communication_profile.prototype.hide_balloon = function() {
      if (this.balloon) {
        return this.balloon.hide();
      }
    };

    return communication_profile;

  })(Backbone.View);

  App.pages.user.profile.views.communication_profiles = (function(_super) {
    __extends(communication_profiles, _super);

    function communication_profiles() {
      this.initialize = __bind(this.initialize, this);
      return communication_profiles.__super__.constructor.apply(this, arguments);
    }

    communication_profiles.prototype.el = '.js-user-profile-communication-profile-items';

    communication_profiles.prototype.initialize = function() {
      var $items, url2link_class;
      $items = this.$el.find('.js-user-profile-communication-profile-item');
      $items.each((function(_this) {
        return function(index, item) {
          var $footer, $item, item_view;
          $item = $(item);
          $footer = $item.find('.js-user-profile-communication-profiles-item-footer');
          if ($footer.length === 0) {
            return true;
          }
          return item_view = new App.pages.user.profile.views.communication_profile({
            el: $item
          });
        };
      })(this));
      url2link_class = new App.elements.url2link.views.url2link();
      $('.js-communication-profile-textarea').each((function(_this) {
        return function(index, element) {
          return $(element).html(url2link_class.done_url2link($(element).html()));
        };
      })(this));
      return url2link_class.renew_observe_click_url2link();
    };

    return communication_profiles;

  })(Backbone.View);

  App.pages.user.profile.views.profile = (function(_super) {
    __extends(profile, _super);

    function profile() {
      this.initialize = __bind(this.initialize, this);
      return profile.__super__.constructor.apply(this, arguments);
    }

    profile.prototype.initialize = function() {
      var url2link_class;
      url2link_class = new App.elements.url2link.views.url2link();
      $('.js-basic-profile-textarea').each((function(_this) {
        return function(index, element) {
          return $(element).html(url2link_class.done_url2link($(element).html()));
        };
      })(this));
      return url2link_class.renew_observe_click_url2link();
    };

    return profile;

  })(Backbone.View);

  add_route('/user/profile(/[0-9]+)?/?$', function() {
    new App.pages.user.profile.views.profile();
    return new App.pages.user.profile.views.communication_profiles();
  });

  App.pages.user.show = {};

  add_route('/user/show/?$|^/user/(show/)?[0-9]+/?', function() {
    var matches, regexp, user_id;
    regexp = new RegExp("^" + App.base_url + "/user(/show)?/([0-9]+)/?$");
    matches = location.pathname.match(regexp);
    if ((matches != null ? matches[2] : void 0) != null) {
      user_id = matches[2];
    } else {
      user_id = App.classes.current_user.get_id();
    }
    App.elements.timeline.config.api_base_url = "" + App.base_url + "/api/user/timeline/" + user_id;
    App.elements.timeline.config.show_related = true;
    App.elements.timeline.config.base_page = "user_top/" + user_id;
    return new App.elements.timeline.views.timeline();
  });

  App.pages.work = {};

  App.pages.work.announcement = {};

  App.pages.work.announcement.assign = {};

  App.pages.work.announcement.assign.show = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.work.announcement.assign.show.views.show = (function(_super) {
    __extends(show, _super);

    function show() {
      this.initialize = __bind(this.initialize, this);
      return show.__super__.constructor.apply(this, arguments);
    }

    show.prototype.el = null;

    show.prototype.initialize = function() {
      var url2link_class;
      url2link_class = new App.elements.url2link.views.url2link();
      $('.listItemMsgTalk div.wysiwygContent p').each((function(_this) {
        return function(index, element) {
          return $(element).html(url2link_class.done_url2link($(element).html()));
        };
      })(this));
      $('div.boxLabeled ul.content li').each((function(_this) {
        return function(index, element) {
          return $(element).html(url2link_class.done_url2link($(element).html()));
        };
      })(this));
      return url2link_class.renew_observe_click_url2link();
    };

    return show;

  })(Backbone.View);

  add_route('/work/announcement/assign/show/[0-9]+/?', function() {
    return new App.pages.work.announcement.assign.show.views.show();
  });

  App.pages.work.questionnaire = {};

  App.pages.work.questionnaire.assign = {};

  App.pages.work.questionnaire.assign.show = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.work.questionnaire.assign.show.views.show = (function(_super) {
    __extends(show, _super);

    function show() {
      this.submit = __bind(this.submit, this);
      this.questionnaire_form_submit = __bind(this.questionnaire_form_submit, this);
      this.initialize = __bind(this.initialize, this);
      return show.__super__.constructor.apply(this, arguments);
    }

    show.prototype.el = '#admin-work-questionnaire-form';

    show.prototype.events = {
      'click #js-questionnaire-form': 'questionnaire_form_submit'
    };

    show.prototype.initialize = function() {
      var url2link_class;
      this.$form = $('#admin-work-questionnaire-form');
      url2link_class = new App.elements.url2link.views.url2link();
      $('.listItemMsgTalk div.wysiwygContent p').each((function(_this) {
        return function(index, element) {
          return $(element).html(url2link_class.done_url2link($(element).html()));
        };
      })(this));
      $('div.boxLabeled p.note').each((function(_this) {
        return function(index, element) {
          return $(element).html(url2link_class.done_url2link($(element).html()));
        };
      })(this));
      $('div.boxLabeled p.content').each((function(_this) {
        return function(index, element) {
          var $input;
          $input = $(element).find('input');
          if ($input.length > 0) {
            return false;
          }
          return $(element).html(url2link_class.done_url2link($(element).html()));
        };
      })(this));
      return url2link_class.renew_observe_click_url2link();
    };

    show.prototype.questionnaire_form_submit = function() {
      this.modal = new App.elements.modal({
        title: 'アンケート回答の確認',
        body: '回答します。<br /><br />よろしいですか？',
        decide_button_label: '回答する',
        cancel_button_label: '取り消す'
      });
      this.modal.on('decide', this.submit);
      this.modal.show();
      return false;
    };

    show.prototype.submit = function() {
      App.functions.disable_form_multiple_submits(this.$form);
      return this.$form.submit();
    };

    return show;

  })(Backbone.View);

  add_route('/work/questionnaire/assign/show/[0-9]+/?', function() {
    return new App.pages.work.questionnaire.assign.show.views.show();
  });

  App.pages.work.task = {};

  App.pages.work.task.assign = {};

  App.pages.work.task.assign.show = {
    templates: {},
    config: {},
    events: {},
    models: {},
    collections: {},
    views: {},
    routes: {}
  };

  App.pages.work.task.assign.show.views.show = (function(_super) {
    __extends(show, _super);

    function show() {
      this.submit = __bind(this.submit, this);
      this.task_form_submit = __bind(this.task_form_submit, this);
      this.clear_file_input = __bind(this.clear_file_input, this);
      this.initialize = __bind(this.initialize, this);
      return show.__super__.constructor.apply(this, arguments);
    }

    show.prototype.el = '#admin-work-elements-form';

    show.prototype.events = {
      'click .js-work-task-assign-show-input-file-container a': 'clear_file_input',
      'click #js-task-form': 'task_form_submit'
    };

    show.prototype.$file_input = null;

    show.prototype.$file_input_clone = null;

    show.prototype.initialize = function() {
      var url2link_class;
      this.$file_input = this.$el.find('.js-work-task-assign-show-input-file-container input[type="file"]');
      this.$file_input_clone = this.$file_input.clone();
      this.$form = $('#admin-work-elements-form');
      url2link_class = new App.elements.url2link.views.url2link();
      $('.listItemMsgTalk div.wysiwygContent p').each((function(_this) {
        return function(index, element) {
          return $(element).html(url2link_class.done_url2link($(element).html()));
        };
      })(this));
      $('div.boxLabeled ul.content li').each((function(_this) {
        return function(index, element) {
          return $(element).html(url2link_class.done_url2link($(element).html()));
        };
      })(this));
      return url2link_class.renew_observe_click_url2link();
    };

    show.prototype.clear_file_input = function() {
      var $new_input, input_value;
      this.$file_input.val('');
      input_value = this.$file_input.val();
      if (((input_value != null ? input_value.length : void 0) != null) && input_value.length > 0) {
        $new_input = this.$file_input_clone.clone();
        this.$file_input.replaceWith($new_input);
        this.$file_input = $new_input;
      }
      return false;
    };

    show.prototype.task_form_submit = function() {
      this.modal = new App.elements.modal({
        title: '提出物の確認',
        body: '提出します。<br /><br />よろしいですか？',
        decide_button_label: '提出する',
        cancel_button_label: '取り消す'
      });
      this.modal.on('decide', this.submit);
      this.modal.show();
      return false;
    };

    show.prototype.submit = function() {
      App.functions.disable_form_multiple_submits(this.$form);
      return this.$form.submit();
    };

    return show;

  })(Backbone.View);

  add_route('/work/task/assign/show/[0-9]+/?', function() {
    return new App.pages.work.task.assign.show.views.show();
  });

  (function() {
    var check, current_count, init, init_count, run, run_flag, _i, _len, _ref, _results;
    run = function() {
      var cb, pathname, regexp, route, routing, _i, _len, _ref, _results;
      regexp = new RegExp('^' + App.base_url);
      pathname = location.pathname.replace(regexp, '');
      _ref = App.routes;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        routing = _ref[_i];
        route = new RegExp('^' + routing.path + '$');
        cb = routing.cb;
        if ((route instanceof RegExp) && typeof cb === 'function' && pathname.match(route) !== null) {
          _results.push(cb());
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };
    init_count = App.initializes.length;
    current_count = 0;
    run_flag = false;
    check = function() {
      current_count++;
      if (current_count >= init_count && run_flag === false) {
        run_flag = true;
        return run();
      }
    };
    if (init_count > 0) {
      _ref = App.initializes;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        init = _ref[_i];
        _results.push(init(check));
      }
      return _results;
    } else {
      return run();
    }
  })();

}).call(this);
