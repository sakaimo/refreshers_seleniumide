var _Plugin = {};
var App = {
	Utility: {},
	Admin: {},

	config: {},
	functions: {},
	classes: {},
	helpers: {},
	elements: {},
	pages: {},
	instances: {},
	routes: [],
	initializes: []
};

  App.config = {
    debug_mode: true,
    lang: {
      path: '/api/i18n.json'
    },
    security: {
      csrf_token_key: 'hZVLiFAGt6YSJEzt'
    }
  };


//  Backbone.Model.prototype._destroy = Backbone.Model.prototype.destroy;
//
//  Backbone.Model.prototype.destroy = function(options) {
//    var err;
//    if (!(options != null)) {
//      options = {};
//    }
//    if (!(options.data != null)) {
//      options.data = {};
//    }
//    if (typeof options.data === 'string') {
//      try {
//        options.data = JSON.parse(options.data);
//      } catch (_error) {
//        err = _error;
//        options.data = {};
//      }
//    }
//    options.data[App.config.security.csrf_token_key] = fuel_fetch_token();
//    options.data = JSON.stringify(options.data);
//    return Backbone.Model.prototype._destroy.apply(this, [options]);
//  };

/* -----------------------------------------------------------------
 * prototype.function
 *	trim();
 * -----------------------------------------------------------------*/
if( !String.prototype.trim ) {
	String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g, "");
	};
}

(function($, w, d){
	// when browser scroll
	// webkit: $(body), other: $(html)
	App.Utility.getBody = function() {
		return (!document.uniqueID && !window.opera && !window.sidebar && window.localStorage && typeof window.orientation == "undefined")? 'body' : 'html';
	};

	App.Utility.sizeFormat = function(bite) {
		function ceil3rdDecimalPlace(n) {
			return Math.ceil(n * 100) / 100;
		}
		// 1024B  = 1KB
		// 1024KB = 1MB
		// 1024MB = 1GB
		var kb = 1024,
				mb = kb * kb;
		if(bite < mb) {
			return ceil3rdDecimalPlace( (bite / kb) ) + 'KB';
		} else {
			return ceil3rdDecimalPlace( (bite / mb) ) + 'MB';
		}
	};

	App.Utility.getDocumentSize = function() {
		return {
			width: d.documentElement.clientWidth,
			heigt: d.documentElement.clientHeight
		};
	};

	App.Utility.hasProp = {}.hasOwnProperty;
	App.Utility.extend  = function(parent, child) {
		var p;
		child = child || {};
		for(p in parent) {
			if( App.Utility.hasProp.call( parent, p ) ) {
				child[p] = parent[p];
			}
		}
		return child;
	};

	$(function(){
	        App.Utility.$d = jQuery(d);
	        App.Utility.$w = jQuery(w);
	        App.Utility.$b = jQuery('body');
	});
})(jQuery, window, document);


  App.functions.fuel_fetch_token = function() {
    var c_end, c_name, c_start;
    if (document.cookie.length > 0) {
      c_name = App.config.security.csrf_token_key;
      c_start = document.cookie.indexOf(c_name + '=');
      if (c_start !== -1) {
        c_start = c_start + c_name.length + 1;
        c_end = document.cookie.indexOf(';', c_start);
        if (c_end === -1) {
          c_end = document.cookie.length;
        }
        return unescape(document.cookie.substring(c_start, c_end));
      }
    }
    return '';
  };

  fuel_fetch_token = App.functions.fuel_fetch_token;

  App.functions.fuel_set_token = function(form) {
    var $csrf, c_name, element, has_hidden, value, _i, _len, _ref;
    value = fuel_fetch_token();
    c_name = App.config.security.csrf_token_key;
    if (form instanceof jQuery) {
      $csrf = form.find("[name=\"" + c_name + "\"]");
      if ($csrf.length <= 0) {
        $csrf = $('<input type="hidden" name="' + App.config.security.csrf_token_key + '">');
        form.append($csrf);
      }
      return $csrf.val(value);
    } else if ((form != null ? form.elements : void 0) != null) {
      if (value !== '') {
        has_hidden = false;
        _ref = form.elements;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          element = _ref[_i];
          if (element.name === c_name) {
            element.value = value;
            has_hidden = true;
            break;
          }
        }
        if (!has_hidden) {
          return $(form).append("<input type=\"hidden\" name=\"" + c_name + "\" value=\"" + value + "\"");
        }
      }
    }
  };

  fuel_set_token = App.functions.fuel_set_token;

